module Main where

import qualified Mayhem.Engine.CompileAnimations

main :: IO ()
main = Mayhem.Engine.CompileAnimations.main
