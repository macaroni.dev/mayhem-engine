{-# OPTIONS_GHC -Wno-all #-}

{-# LANGUAGE OverloadedStrings #-}

module SDL.Utils
( module SDL.Utils
, module SDL.Optics
, module SDL
) where

import Control.Monad
import Control.Monad.Managed
import qualified Data.Set
import Data.Foldable
import Data.Traversable
import UnliftIO (bracket)

import Foreign.C.String

import SDL
import SDL.Raw
import SDL.Optics

functionKeycodes :: Data.Set.Set SDL.Keycode
functionKeycodes = Data.Set.fromList
  [ SDL.KeycodeF1
  , SDL.KeycodeF2
  , SDL.KeycodeF3
  , SDL.KeycodeF4
  , SDL.KeycodeF5
  , SDL.KeycodeF6
  , SDL.KeycodeF7
  , SDL.KeycodeF8
  , SDL.KeycodeF9
  , SDL.KeycodeF10
  , SDL.KeycodeF11
  , SDL.KeycodeF12
  ]

eventLogger :: IO ()
eventLogger = do
  SDL.initializeAll
  w <- SDL.createWindow "SDL.eventLogger" SDL.defaultWindow
  SDL.showWindow w
  screen <- SDL.getWindowSurface w
  SDL.surfaceFillRect screen Nothing (SDL.V4 maxBound maxBound maxBound maxBound)
  SDL.updateWindowSurface w

  withCString "assets/gamecontrollerdb.txt" $ \db ->
    SDL.Raw.gameControllerAddMappingsFromFile db >>= \r ->
      putStrLn $ unwords ["Added", show r, "GameController mappings"]

  SDL.availableJoysticks >>= \joys -> do
    traverse_ SDL.openJoystick joys
    traverse_ (SDL.Raw.gameControllerOpen . SDL.joystickDeviceId) joys
  let loop = do
        es <- SDL.pollEvents
        traverse_ print es
        unless (SDL.QuitEvent `elem` fmap SDL.eventPayload es) loop
  loop
  SDL.destroyWindow w
  SDL.quit

loadControllerDB :: MonadIO m => FilePath -> m ()
loadControllerDB fp =
  liftIO $ withCString fp $ \db -> do
    ret <- SDL.Raw.gameControllerAddMappingsFromFile db
    when (ret < 0) $ error "Unable to initialize controller db"

withAllControllers :: MonadManaged m => m [(SDL.Joystick, SDL.Raw.GameController)]
withAllControllers =
  liftIO SDL.availableJoysticks >>= \joys -> fmap toList $ for joys $ \j -> do
    sdl'j <- managed (bracket (SDL.openJoystick j) SDL.closeJoystick)
    sdl'gc <- managed (bracket (SDL.Raw.gameControllerOpen j.joystickDeviceId) SDL.Raw.gameControllerClose)
    pure (sdl'j, sdl'gc)
