module SDL.Optics.Rules where

import Optics
import Optics.TH
import Language.Haskell.TH

import Data.List (stripPrefix)
import Data.Char (toLower, toUpper)

rules :: LensRules
rules =
  noPrefixFieldLabels
  & lensField .~ underscorePrefix

underscorePrefix :: FieldNamer
underscorePrefix = \_ _ -> pure . TopName . mkName . ('_' :) . upperFirst . nameBase

stripType :: FieldNamer
stripType = \ty _ f -> maybe [] id . fmap (pure . TopName . mkName . lowerFirst) $ stripPrefix (fmap toLower $ nameBase ty) (nameBase f)

lowerFirst :: String -> String
lowerFirst [] = []
lowerFirst (s : rest) = toLower s : rest

upperFirst :: String -> String
upperFirst [] = []
upperFirst (s : rest) = toUpper s : rest
