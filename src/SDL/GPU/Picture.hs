module SDL.GPU.Picture where

import Data.Foldable
import SDL.GPU.Utils qualified as GPU
import Foreign.Ptr
import Data.Sum
import Acc
import GHC.Exts (IsList (..))
import Data.Map (Map)
import Data.Map.Strict qualified as Map

-- Ideas:
-- - No camera - you manage model-view-projection via Picture
-- - BlitX options
-- - Somehow support shaders
-- - Somehow support 3D
-- - Maybe naturally support layers
-- - Make it extensible - a la dalek?
data Picture ps where
    Sequence :: Acc (Picture ps) -> Picture ps
    Render :: (Ptr GPU.Target -> IO ()) -> Picture ps
    Plugin :: Sum ps (Picture ps) -> Picture ps

instance Semigroup (Picture ps) where
  Sequence xs <> Sequence ys = Sequence (xs <> ys)
  Sequence xs <> p = Sequence $ snoc p xs
  p <> Sequence ys = Sequence $ cons p ys
  x <> y = Sequence $ pure x <> pure y

instance Monoid (Picture ps) where
  mempty = Sequence mempty

renderIO :: Ptr GPU.Target -> Picture '[] -> IO ()
renderIO screen = \case
  Sequence ps -> traverse_ (renderIO screen) ps
  Render f -> f screen
  Plugin _ -> pure () -- Not possible

-- TODO: This isn't general enough yet - needs to be cleff-level
-- e.g. Scale need to be able to:
-- pushMatrix
-- scale
-- render everything else to IO <- this is the trick!
-- popMatrix
--
-- We could do it with the Render ctor..maybe that's fine?
renderPass
  :: Functor (Sum ps)
  => Functor p
  => (p (Picture  ps) -> Picture ps)
  -> Picture (p ': ps)
  -> Picture ps
renderPass f = \case
  Sequence ps -> foldMap (renderPass f) ps
  Render r -> Render r
  Plugin s -> case decompose s of
    Left rest -> Plugin (renderPass f <$> rest)
    Right p -> f (renderPass f <$> p)

data Scale p = Scale Float Float p

-- TODO: Test this
renderScale :: Scale (Picture ps) -> Picture ps
renderScale (Scale x y p) =
  Sequence $ fromList
  [ Render $ \screen -> do
      GPU.pushMatrix
      GPU.matrixMode screen GPU.model
      GPU.scale x y 1.0
  , p
  , Render $ \screen -> do
      GPU.matrixMode screen GPU.model
      GPU.popMatrix
  ]

-- Right now this is "local" layering instead of "global"
-- Can we change Picture to allow for "global"?
data Layered k p = Layered (Map k p)

renderLayered :: Layered k (Picture ps) -> Picture ps
renderLayered (Layered mkp) =
  foldMap snd (Map.toAscList mkp)
