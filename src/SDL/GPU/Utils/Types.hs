-- TODO: Optics?
module SDL.GPU.Utils.Types where

import Control.Monad
import Foreign.Ptr
import qualified SDL.GPU.Simple as GPU
import Memorable

-- HACK: NULL image can be used for padding
data Blit = Blit
  { image :: Ptr GPU.Image
  , src_rect :: Maybe GPU.Rect
  , x :: Float
  , y :: Float
  } deriving (Show)

-- TODO: This assumes top-left (0,0) image origin
blitExtentX :: Blit -> IO Float
blitExtentX Blit{..} = if image == nullPtr then pure x else do
  imgW <- image *-> (.w)
  pure (x + fromIntegral imgW)

runBlit :: Ptr GPU.Target -> Blit -> IO ()
runBlit screen Blit{..} =
  unless (image == nullPtr) $
    GPU.blit image src_rect screen x y
