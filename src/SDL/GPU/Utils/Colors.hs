-- | Copied from Graphics.Gloss.Color
module SDL.GPU.Utils.Colors
  (
  -- ** Color functions
    mixColors
  , addColors
  , dim,   bright
  , light, dark
  , transparent

  -- ** Pre-defined colors
  , greyN,  black,  white, brown
  -- *** Primary
  , red,    green,  blue
  -- *** Secondary
  , yellow,     cyan,       magenta

  -- *** Tertiary
  , rose,   violet, azure, aquamarine, chartreuse, orange
  -- *** Internal Shims
  , ColorT (..)
  , toGPUColor
  , fromGPUColor
  , normaliseColor
  , clampColor
  ) where

import Data.Word
import SDL.GPU.Simple qualified as GPU (Color (..))

-- | An abstract color value.
--  We keep the type abstract so we can be sure that the components
--  are in the required range. To make a custom color use 'makeColor'.
data ColorT
  -- | Holds the color components. All components lie in the range [0..1.
  = RGBA  Float Float Float Float
  deriving (Show, Eq)

toGPUColor :: ColorT -> GPU.Color
toGPUColor (RGBA r g b a) = GPU.Color (c8 r) (c8 g) (c8 b) (c8 a)
  where
    c8 x = ceiling (x * 255) :: Word8

fromGPUColor :: GPU.Color -> ColorT
fromGPUColor (GPU.Color r g b a) = RGBA (cf r) (cf g) (cf b) (cf a)
  where
    cf x = (fromIntegral x / 255) :: Float

-- Internal

-- | Clamp components of a color into the required range.
clampColor :: ColorT -> ColorT
clampColor cc
 = let  (RGBA r g b a)  = cc
   in RGBA (min 1 r) (min 1 g) (min 1 b) (min 1 a)

-- | Normalise a color to the value of its largest RGB component.
normaliseColor :: ColorT -> ColorT
normaliseColor (RGBA r g b a)
 = let m   = maximum [r, g, b]
   in RGBA (r / m) (g / m) (b / m) a


-- Color functions ------------------------------------------------------------

-- | Mix two colors with the given ratios.
mixColors
  :: Float  -- ^ Ratio of first color.
  -> Float  -- ^ Ratio of second color.
  -> GPU.Color  -- ^ First color.
  -> GPU.Color  -- ^ Second color.
  -> GPU.Color  -- ^ Resulting color.

mixColors ratio1 ratio2 (fromGPUColor->c1) (fromGPUColor->c2)
 = let  RGBA r1 g1 b1 a1  = c1
        RGBA r2 g2 b2 a2  = c2

        total = ratio1 + ratio2
        m1  = ratio1 / total
        m2  = ratio2 / total

   in toGPUColor $
      RGBA  (m1 * r1 + m2 * r2)
      (m1 * g1 + m2 * g2)
      (m1 * b1 + m2 * b2)
      (m1 * a1 + m2 * a2)


-- | Add RGB components of a color component-wise, then normalise
--  them to the highest resulting one. The alpha components are averaged.
addColors :: GPU.Color -> GPU.Color -> GPU.Color
addColors (fromGPUColor->c1) (fromGPUColor->c2)
 = let  RGBA r1 g1 b1 a1  = c1
        RGBA r2 g2 b2 a2  = c2

   in toGPUColor $ normaliseColor
   $ RGBA (r1 + r2)
    (g1 + g2)
    (b1 + b2)
    ((a1 + a2) / 2)


-- | Make a dimmer version of a color, scaling towards black.
dim :: GPU.Color -> GPU.Color
dim (fromGPUColor->RGBA r g b a)
  = toGPUColor $ RGBA (r / 1.2) (g / 1.2) (b / 1.2) a


-- | Make a brighter version of a color, scaling towards white.
bright :: GPU.Color -> GPU.Color
bright (fromGPUColor->RGBA r g b a)
  = toGPUColor
  $ clampColor $ RGBA (r * 1.2) (g * 1.2) (b * 1.2) a


-- | Lighten a color, adding white.
light :: GPU.Color -> GPU.Color
light (fromGPUColor->RGBA r g b a)
  = toGPUColor
  $ clampColor $ RGBA (r + 0.2) (g + 0.2) (b + 0.2) a


-- | Darken a color, adding black.
dark :: GPU.Color -> GPU.Color
dark (fromGPUColor->RGBA r g b a)
  = toGPUColor
  $ clampColor $ RGBA (r - 0.2) (g - 0.2) (b - 0.2) a

transparent :: Float -> GPU.Color -> GPU.Color
transparent a (fromGPUColor -> RGBA r g b _) =
  toGPUColor $ clampColor $ RGBA r g b a

-- Pre-defined Colors ---------------------------------------------------------
-- | A greyness of a given magnitude.
greyN   :: Float  -- ^ Range is 0 = black, to 1 = white.
        -> GPU.Color
greyN n   = toGPUColor $ RGBA n   n   n   1.0

black, white :: GPU.Color
black   = toGPUColor $ RGBA 0.0 0.0 0.0 1.0
white   = toGPUColor $ RGBA 1.0 1.0 1.0 1.0

brown :: GPU.Color
brown = GPU.Color 160 82 45 255

-- Colors from the additive color wheel.
red, green, blue :: GPU.Color
red   = toGPUColor $ RGBA 1.0 0.0 0.0 1.0
green   = toGPUColor $ RGBA 0.0 1.0 0.0 1.0
blue    = toGPUColor $ RGBA 0.0 0.0 1.0 1.0

-- secondary
yellow, cyan, magenta :: GPU.Color
yellow    = addColors red   green
cyan    = addColors green blue
magenta   = addColors red   blue

-- tertiary
rose, violet, azure, aquamarine, chartreuse, orange :: GPU.Color
rose    = addColors red     magenta
violet    = addColors magenta blue
azure   = addColors blue    cyan
aquamarine  = addColors cyan    green
chartreuse  = addColors green   yellow
orange    = addColors yellow  red
