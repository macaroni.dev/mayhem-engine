module SDL.GPU.Utils
  ( module SDL.GPU.Utils
  , module GPU
  ) where


import Prelude hiding (Float)
import Prelude qualified
import Control.Monad
import Control.Monad.IO.Unlift
import UnliftIO.Exception
import Data.Word
import Data.Function
import Data.Foldable
import Data.List.Extra (maximumOn)

import Foreign.Ptr
import Foreign.Storable (peek)
import Foreign.Marshal.Alloc

import SDL.GPU.Simple as GPU
import qualified SDL.GPU.Simple as GPU.Image (ImageF (..))
import qualified SDL.GPU.Simple as GPU.Target (TargetF (..))
import qualified SDL.GPU.Simple as GPU.Rect (RectF (..))
import qualified SDL.GPU.Simple as GPU.Camera (CameraF (..))
import SDL.GPU.Utils.Types as GPU
import SDL.GPU.Utils.Colors as GPU
import qualified SDL.GPU.Utils.Types as GPU.Blit (Blit (..))
import SDL.Raw.Types (Color (..))
import SDL.Vect
import Memorable

-- TODO: Remove this dep + upstream this module
import Mayhem.Engine.Utils

-- TODO: It's white for now
horizontalLine
  :: Ptr GPU.Target
  -> Float -- ^ x
  -> Float -- ^ y
  -> Float -- ^ len
  -> IO ()
horizontalLine screen x y len = GPU.line screen x y (x + len) y (Color 255 255 255 128)

horizontalScreenLine
  :: Ptr GPU.Target
  -> Float -- ^ y
  -> IO ()
horizontalScreenLine screen y = do
  w <- screen *-> (.w)
  horizontalLine screen 0 y (fromIntegral w)

-- TODO: It's white for now
verticalLine
  :: Ptr GPU.Target
  -> Float -- ^ x
  -> Float -- ^ y
  -> Float -- ^ len
  -> IO ()
verticalLine screen x y len =
  GPU.line screen x y x (y + len) (Color 255 255 255 255)

verticalScreenLine
  :: Ptr GPU.Target
  -> Float -- ^ x
  -> IO ()
verticalScreenLine screen x = do
  h <- screen *-> (.h)
  verticalLine screen x 0 (fromIntegral h)

withBlendFunc
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> GPU.BlendFuncEnum
  -> GPU.BlendFuncEnum
  -> GPU.BlendFuncEnum
  -> GPU.BlendFuncEnum
  -> m r
  -> m r
withBlendFunc img bf1 bf2 bf3 bf4 ior =
  bracket_ (GPU.setBlendFunction img bf1 bf2 bf3 bf4) (GPU.setBlendMode img GPU.blendNormal) ior

withBlendMode
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> GPU.BlendPresetEnum
  -> m r
  -> m r
withBlendMode img mode ior =
  bracket_ (GPU.setBlendMode img mode) (GPU.setBlendMode img GPU.blendNormal) ior

withColor
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> Color
  -> m r
  -> m r
withColor img color ior =
  bracket_ (GPU.setColor img color) (GPU.unsetColor img) ior

withScreenColor
  :: MonadUnliftIO m
  => Ptr GPU.Target
  -> Color
  -> m r
  -> m r
withScreenColor screen color ior =
  bracket_ (GPU.setTargetColor screen color) (GPU.unsetTargetColor screen) ior

withLineThickness
  :: MonadUnliftIO m
  => Float
  -> m r
  -> m r
withLineThickness thickness k =
  bracket (liftIO GPU.getLineThickness) (liftIO . GPU.setLineThickness) $ \_ -> liftIO (GPU.setLineThickness thickness) >> k


centerAnchor :: MonadIO m => Ptr GPU.Image -> m ()
centerAnchor img = GPU.setAnchor img 0.5 0.5

bottomCenterAnchor :: MonadIO m => Ptr GPU.Image -> m ()
bottomCenterAnchor img = GPU.setAnchor img 0.5 1.0

-- Anchor is between 0.0 and 1.0
withAnchor
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> Float
  -> Float
  -> m r
  -> m r
withAnchor img x y ior = do
  og_x <- liftIO $ img *-> GPU.Image.anchor_x
  og_y <- liftIO $ img *-> GPU.Image.anchor_y
  bracket_ (liftIO $ GPU.setAnchor img x y) (liftIO $ GPU.setAnchor img og_x og_y) ior

withCenterAnchor
  :: MonadUnliftIO m
  => Ptr GPU.Image
  -> m r
  -> m r
withCenterAnchor img ior = withAnchor img 0.5 0.5 ior

toggleFullscreen :: IO Prelude.Bool
toggleFullscreen  = do
  current <- GPU.getFullscreen
  -- TODO: Force is useless now that letterbox is separate..just call GPU.setFullscreen!
  GPU.setFullscreen (not current) True

-- https://github.com/grimfang4/sdl-gpu/issues/188
letterbox :: Word16 -> Word16 -> Ptr GPU.Target -> IO ()
letterbox virtW virtH screen = do
  let !aspect = virtW // virtH
  newW <- screen *-> GPU.Target.base_w
  newH <- screen *-> GPU.Target.base_h

  let !scaleW = newW // virtW
  let !scaleH = newH // virtH

  let rect =
        if | scaleW < scaleH ->
             let !rectH = fromIntegral newW / aspect
             in GPU.Rect
                { w = fromIntegral newW
                , h = rectH
                , x = 0
                , y = (fromIntegral newH - rectH)/2
                }
           | otherwise ->
             let !rectW = fromIntegral newH * aspect
             in GPU.Rect
                { w = rectW
                , h = fromIntegral newH
                , x = (fromIntegral newW - rectW)/2
                , y = 0
                }

  GPU.setViewport screen rect

-- TODO: This is broken slightly..maybe?
screen2virtual
  :: Ptr GPU.Target
  -> Point V2 Float
  -> IO (Point V2 Float)
screen2virtual screen (P (V2 screen_x screen_y)) = do
  screen_w :: Float <- fmap fromIntegral (screen *-> GPU.Target.base_w)
  screen_h :: Float <- fmap fromIntegral (screen *-> GPU.Target.base_h)
  V2 virt_w virt_h :: V2 Float <- fmap fromIntegral <$> GPU.getVirtualResolution screen
  let scale_x :: Float = screen_w / virt_w
  let scale_y :: Float = screen_h / virt_h

  -- TODO: Do we even need to consider the viewport?

  let out = P $ V2 ((screen_x) / scale_x) ((screen_y) / scale_y)
  {-
  putStrLn $ unwords
    [ "view_x =", show view_x, "view_y =", show view_y
    , "view_w =", show view_w, "view_h =", show view_h
    , "screen_w =", show screen_w, "screen_h =", show screen_h
    , "virt_w =", show virt_w, "virt_h =", show virt_h
    , "screen_x =", show screen_x, "screen_y =", show screen_y
    , "scale_x =", show scale_x, "scale_y =", show scale_y
    , "out =", show out
    ]
  -}
  pure out

-- | 'screen2world' and 'world2screen' ported from
-- https://github.com/grimfang4/sdl-gpu/blob/47a3e2b2a9326c33ad6f177794705987399de8cf/tests/camera/main.c
--
-- TODO: Untested
screen2world
  :: Ptr GPU.Target
  -> (Float, Float)
  -> IO (Float, Float)
screen2world screen (screenX, screenY) = allocable @GPU.Camera $ \camera -> do
  camera <- GPU.getCamera screen
  screen_w :: Float <- fmap fromIntegral (screen *-> GPU.Target.w)
  screen_h :: Float <- fmap fromIntegral (screen *-> GPU.Target.h)
  let camera_zoom_x = camera.zoom_x
  let camera_zoom_y = camera.zoom_y
  let camera_x = camera.x
  let camera_y = camera.y

  pure
    ( ((screenX - screen_w/2) / camera_zoom_x) + camera_x + screen_w/2
    , ((screenY - screen_h/2) / camera_zoom_y) + camera_y + screen_h/2
    )

-- TODO: Untested
world2screen
  :: Ptr GPU.Target
  -> (Float, Float)
  -> IO (Float, Float)
world2screen screen (worldX, worldY) = do
  camera <- GPU.getCamera screen
  screen_w :: Float <- fmap fromIntegral (screen *-> GPU.Target.w)
  screen_h :: Float <- fmap fromIntegral (screen *-> GPU.Target.h)
  let camera_zoom_x = camera.zoom_x
  let camera_zoom_y = camera.zoom_y
  let camera_x = camera.x
  let camera_y = camera.y

  pure
    ( ((worldX - camera_x - screen_w/2) * camera_zoom_x) + screen_w/2
    , ((worldY - camera_y - screen_h/2) * camera_zoom_y) + screen_h/2
    )

modifyCamera
  :: MonadIO m
  => Ptr GPU.Target
  -> (GPU.Camera -> GPU.Camera)
  -> m ()
modifyCamera screen f =
  GPU.getCamera screen >>=
    GPU.setCamera screen . f

resetCamera
  :: Ptr GPU.Target
  -> IO ()
resetCamera screen =
  GPU.getDefaultCamera >>=
    GPU.setCamera screen

-- TODO: This doesn't work when it's diagonal - it isn't long enough. And rotating a sprite
-- diagonally causes noticeable tearing.
--
-- TODO: Parallax factor (allow for n Images + relative locs?)
scrollHorizontalOLD
  :: Integral x
  => Ptr GPU.Target
  -> Ptr GPU.Image
  -> Float -- ^ y
  -> x -- ^ Where you are in the infinite plane
  -> IO ()
scrollHorizontalOLD screen img y offset = do
  screenW <- screen *-> GPU.Target.w
  imgW <- img *-> GPU.Image.w
  let offsetPercent :: Float = fromIntegral offset / fromIntegral imgW
  let x0 = -1 * (offsetPercent * fromIntegral imgW)
  let xs = takeWhile (< fromIntegral screenW) $ iterate (+ fromIntegral imgW) x0
  for_ xs $ \x -> GPU.blit img Nothing screen x y

-- TODO: Will crash on empty blit list -_-
scrollHorizontal
  :: forall x
   . Integral x
  => Show x -- debug
  => Ptr GPU.Target
  -> x
  -> Float
  -> [GPU.Blit]
  -> IO ()
scrollHorizontal screen loc p blits = do
  screenW <- screen *-> GPU.Target.w
  blitW :: x <- (maximum . fmap ceiling) <$> traverse GPU.blitExtentX blits
  let blitW16 :: Word16 = fromIntegral blitW
  let blitWF :: Float = fromIntegral blitW
  let ploc = ceiling (fromIntegral loc * p) -- Scale the world loc by p
  let offset = -1 * fromIntegral (ploc `mod` blitW) :: Float
  let addBlitOffset o GPU.Blit{..} = GPU.Blit{x = x + o, ..}
  let offsetBlits o bs = fmap (addBlitOffset o) bs
  let loopedBlits = mconcat $ zipWith offsetBlits [offset, offset+blitWF..] (repeat blits)
  -- TODO: This + (2*blitW16) is a hack
  let toBlit = takeWhile (\GPU.Blit{..} -> x < fromIntegral (screenW + (2*blitW16))) loopedBlits
  for_ toBlit $ GPU.runBlit screen
  pure ()

cross
  :: Ptr GPU.Target -- ^ screen
  -> Float -- ^ center_x
  -> Float -- ^ center_y
  -> Float -- ^ radius
  -> Color
  -> IO ()
cross screen center_x center_y r c = do
  let center = V2 center_x center_y
  do
    let V2 bottomRight_x bottomRight_y = center + (SDL.Vect.angle (pi/4) ^* r)
    let V2 topLeft_x topLeft_y = center + (SDL.Vect.angle (5*pi/4) ^* r)
    GPU.line screen bottomRight_x bottomRight_y topLeft_x topLeft_y c

  do
    let V2 bottomLeft_x bottomLeft_y = center + (SDL.Vect.angle (3*pi/4) ^* r)
    let V2 topRight_x topRight_y = center + (SDL.Vect.angle (7*pi/4) ^* r)
    GPU.line screen bottomLeft_x bottomLeft_y topRight_x topRight_y c

blitTile
  :: forall m
   . MonadUnliftIO m
  => Ptr GPU.Image
  -> Ptr GPU.Target
  -> Rect
  -> m ()
blitTile img screen r = withAnchor img 0 0 $ do
  imgW <- liftIO $ img *-> GPU.Image.w
  imgH <- liftIO $ img *-> GPU.Image.h
  let xys = do
        x <- takeWhile (< r.w) $ iterate (+ fromIntegral imgW) r.x
        y <-  takeWhile (< r.h) $ iterate (+ fromIntegral imgH) r.y
        pure (x, y)
  for_ xys $ \(x, y) -> GPU.blit img Nothing screen x y
