module SDL.GPU.CuteC2
  ( SDL.GPU.CuteC2.c2Poly
  , c2PolyFilled
  , SDL.GPU.CuteC2.c2AABB
  , c2AABBFilled
  , SDL.GPU.CuteC2.c2Circle
  , c2CircleFilled
  , SDL.GPU.CuteC2.c2Capsule
  , c2CapsuleFilled
  , c2Shape
  , c2ShapeFilled
  , c2Manifold
  , c2Demo
  ) where

import Control.Monad
import Foreign.Ptr
import Control.Monad.IO.Class
import Data.Functor ((<&>))
import Data.Foldable (for_, toList)
import Control.Monad.Managed
import UnliftIO (bracket)
import Data.Map qualified as Map

import Linear.V2
import SDL.GPU.Simple qualified as GPU
import SDL qualified
import CuteC2
import Linear.Vector
import Linear.Metric

import Control.Concurrent

c2Poly'
  :: forall m
   . MonadIO m
  => Bool -- ^ filled
  -> Ptr GPU.Target
  -> C2Poly
  -> GPU.Color
  -> m ()
c2Poly' filled screen C2Poly{..} color = do
  let draw = if filled then GPU.polygonFilled else GPU.polygon
  draw screen (toList verts <&> \c2v -> V2 c2v.x c2v.y) color

c2Poly
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Poly
  -> GPU.Color
  -> m ()
c2Poly = c2Poly' False

c2PolyFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Poly
  -> GPU.Color
  -> m ()
c2PolyFilled = c2Poly' True

c2AABB'
  :: forall m
   . MonadIO m
  => Bool -- ^ Filled
  -> Ptr GPU.Target
  -> C2AABB
  -> GPU.Color
  -> m ()
c2AABB' filled screen aabb color = do
  let draw = if filled then GPU.rectangleFilled else GPU.rectangle
  draw screen aabb.min.x aabb.min.y aabb.max.x aabb.max.y color

c2AABB
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2AABB
  -> GPU.Color
  -> m ()
c2AABB = c2AABB' False

c2AABBFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2AABB
  -> GPU.Color
  -> m ()
c2AABBFilled = c2AABB' True

c2Circle'
  :: forall m
   . MonadIO m
  => Bool -- ^ Filled
  -> Ptr GPU.Target
  -> C2Circle
  -> GPU.Color
  -> m ()
c2Circle' filled screen circle color = do
  let draw = if filled then GPU.circleFilled else GPU.circle
  draw screen circle.p.x circle.p.y circle.r color

c2Circle
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Circle
  -> GPU.Color
  -> m ()
c2Circle = c2Circle' False

c2CircleFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Circle
  -> GPU.Color
  -> m ()
c2CircleFilled = c2Circle' True

c2Capsule'
  :: forall m
   . MonadIO m
  => Bool -- ^ Filled
  -> Ptr GPU.Target
  -> C2Capsule
  -> GPU.Color
  -> m ()
c2Capsule' filled screen cap color = do
  let drawArc = if filled then GPU.arcFilled else GPU.arc
  let drawPoly = if filled then GPU.polygonFilled else GPU.polygon

  let radToDeg r = r * 180 / pi
  let a = V2 cap.a.x cap.a.y
  let b = V2 cap.b.x cap.b.y
  let a2b = b - a
  let a2b'perp = perp a2b
  let a2b'perp'unit = signorm a2b'perp
  let a2b'perpCCW'rad = unangle $ a2b'perp
  let a2b'perpCCW'deg = radToDeg a2b'perpCCW'rad
  let r = cap.r

  let a1 = a + r *^ a2b'perp'unit
  let a2 = a - r *^ a2b'perp'unit
  let b1 = b - r *^ a2b'perp'unit -- a's CW point goes to b's CW point first
  let b2 = b + r *^ a2b'perp'unit

  -- TODO: This draws the full rectangle when unfilled. Can get around it with
  -- a smarter drawPoly that uses two lines for unfilled. It's fine for now though.
  drawArc screen cap.a.x cap.a.y r a2b'perpCCW'deg (a2b'perpCCW'deg + 180) color
  drawPoly screen [a1, a2, b1, b2] color
  drawArc screen cap.b.x cap.b.y r a2b'perpCCW'deg (a2b'perpCCW'deg - 180) color

c2Capsule
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Capsule
  -> GPU.Color
  -> m ()
c2Capsule = c2Capsule' False

c2CapsuleFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Capsule
  -> GPU.Color
  -> m ()
c2CapsuleFilled = c2Capsule' True

c2Shape'
  :: forall m
   . MonadIO m
  => Bool -- ^ Filled
  -> Ptr GPU.Target
  -> C2Shape
  -> GPU.Color
  -> m ()
c2Shape' filled screen shape color = case shape of
  C2Circle' x -> c2Circle' filled screen x color
  C2AABB' x -> c2AABB' filled screen x color
  -- TODO: Handle the poly transforms
  C2Poly' _ x -> c2Poly' filled screen x color
  C2Capsule' x -> c2Capsule' filled screen x color

c2Shape
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Shape
  -> GPU.Color
  -> m ()
c2Shape = c2Shape' False

c2ShapeFilled
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Shape
  -> GPU.Color
  -> m ()
c2ShapeFilled = c2Shape' True

-- TODO: Don't know if this is correct .. it doesn't exactly look correct
-- but maybe it's just I don't understand manifolds exactly
c2Manifold
  :: forall m
   . MonadIO m
  => Ptr GPU.Target
  -> C2Manifold
  -> Float -- ^ contact_point_radius
  -> GPU.Color
  -> m ()
c2Manifold screen manifold r color = do
  let n = manifold.n
  for_ (manifold.depths `zip` manifold.contactPoints) $ \(d, p) -> do
    GPU.circle screen p.x p.y r color
    GPU.line screen p.x p.y (p.x + n.x * d) (p.y + n.y * d) color
    pure ()

c2Demo
  :: [C2Shape]
  -> IO ()
c2Demo shapes = runInBoundThread $ runManaged $ do
  liftIO $ GPU.setDebugLevel GPU.debugLevelMax
  screen <- managed (bracket (GPU.init 640 480 GPU.defaultInitFlags) (const GPU.quit))
  let shapeMap = Map.fromList $ [(0 :: Int)..] `zip` shapes
  let shapeKeys = Map.keys shapeMap

  let draw = do
        for_ shapes $ \s -> c2Shape screen s (GPU.Color 255 255 255 255)
        for_ (Map.toList shapeMap) $ \(i, s'i) -> do
          for_ (filter (/= i) shapeKeys) $ \j -> do
            for_ (Map.lookup j shapeMap) $ \s'j -> do
              let manifoldMay = c2Collide s'i s'j
              --for_ manifoldMay $ \m -> liftIO $ putStrLn $ unwords [show i, show j, "collided:", show m]
              for_ manifoldMay $ \m -> c2Manifold screen m 5 (GPU.Color 255 0 0 255)


  -- We use a game-style loop here. If we just do a single flip before waiting, it doesn't work.
  -- See https://github.com/grimfang4/sdl-gpu/issues/245 for details.
  let waitForQuit = do
        GPU.clear screen
        --GPU.circleFilled screen 50 50 50 (GPU.Color 255 255 255 255)
        draw
        es <- SDL.pollEvents
        isScanPressed <- SDL.getKeyboardState
        unless (SDL.QuitEvent `elem` fmap SDL.eventPayload es || isScanPressed SDL.ScancodeEscape) $ do
          GPU.flip screen
          SDL.delay 100
          waitForQuit


  liftIO waitForQuit
  pure ()
