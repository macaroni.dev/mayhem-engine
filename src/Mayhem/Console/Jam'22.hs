{-# LANGUAGE PolyKinds #-}
module Mayhem.Console.Jam'22
 ( module Mayhem.Console.Jam'22
 , module X
 ) where

import Mayhem.Engine.Effects as X
import Mayhem.Engine.Effects.Assets as X
import Mayhem.Engine.Effects.GHCi as X
import Mayhem.Engine.Effects.Stepper as X
import Mayhem.Engine.Effects.Debug as X
import Mayhem.Engine.Effects.Expansion as X
import Mayhem.Engine.Assets as X
import Mayhem.Engine.Resolution as X
import Data.Function ((&))
import Control.Monad.Managed
import SDL.GPU.FC.Simple qualified as FC
import GHC.TypeLits
import Data.Typeable

type Jam'22 assets params world =
  [ -- HACK: We keep GHCi at the top so we can use
    -- Tail to calculate the effects it has access to
    GHCi
  , Log
  , Debug
  , Stepper
  , Clock
  , Assets assets
  , Params params
  , ApecsE world
  , Cute_Sound8000
  , Cute_Sound
  , SDL_GPU
  , IOE
  ]

type family Tail (xs :: [k]) :: [k] where
  Tail '[] = TypeError ('Text "ERROR: Type-level Tail of empty list")
  Tail (x ': xs) = xs

type Jam'22'GHCi a p w = Tail (Jam'22 a p w)

runJam'22
  :: forall a p w m
   . Asset a
  => MonadManaged m
  => Typeable a
  => Typeable p
  => Typeable w
  => Bool -- ^ devmode
  -> (forall es. The Assets es a => Eff es (Ptr FC.Font))
  -> Maybe (GHCiPipe (Jam'22'GHCi a p w))
  -> NativeResolution
  -> IO w
  -> IO p
  -> m (Eff (Jam'22 a p w) :~> m)
runJam'22 devmode debugFont ghciPipe gameRez mkWorld mkParams =
  runLiftIOE
    &   exp'SDL_GPU SDL_GPU'Config {enableVsync = False, nativeRez = gameRez}
    >>= exp'Cute_Sound
    >>= exp'Cute_Sound8000
    >>= exp'Apecs mkWorld
    >>= exp'Params mkParams
    >>= exp'Assets @a "assets"
    >>= exp'Clock
    >>= (if devmode then exp'Stepper else exp'NoStepper)
    >>= (if devmode then exp'Debug (debugFont) else exp'NoDebug)
    >>= (if devmode then exp'Log'Clocked else exp'NoLog)
    >>= maybe exp'GHCiDummy exp'GHCiPipe ghciPipe
