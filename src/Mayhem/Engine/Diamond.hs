module Mayhem.Engine.Diamond where

import Data.Functor
import Data.Foldable (asum, foldl')
import Data.Function (on)
import Data.Maybe (catMaybes, mapMaybe)
import Data.List.NonEmpty (nonEmpty, NonEmpty (..))
import Control.Monad
import Safe (minimumMay, lookupJustNote)
import Data.IntMap qualified as IntMap
import GHC.Records

import Linear hiding (trace)
import Mayhem.Engine.Geometry

import Mayhem.Engine.Utils
import Debug.Trace
import Data.Functor.Identity

data Point = TOP | BOTTOM | LEFT | RIGHT
  deriving stock (Eq, Ord, Show, Enum, Bounded)

allPoints :: [Point]
allPoints = [minBound..maxBound]

newtype Diamond = Diamond { at :: (Point -> V2 Float) }

-- | Doc comment test to see what this looks like in haddock
instance HasField "offset" Diamond (V2 Float -> Diamond) where
  getField d = \offset -> Diamond $ \p -> d.at p + offset

instance Show Diamond where
  show (Diamond d) = unwords ["Diamond", show $ fmap (\p -> (p, d p)) allPoints]

groundedDiamond
  :: V2 Float -- ^ loc
  -> Float -- ^ width
  -> Float -- ^ height
  -> Diamond
groundedDiamond loc w h = Diamond \case
  -- NOTE: We use graphics coord convention. Should we though?
  -- https://gamemath.com/book/multiplespaces.html
  -- Nothing else in this library cares about that though.
  TOP -> loc - (V2 0 h)
  BOTTOM -> loc
  LEFT -> loc + (V2 (-w/2) (-h/2))
  RIGHT -> loc + (V2 (w/2) (-h/2))

-- There's _almost_ nothing 2D-platformer specific about these.
-- The only thing that kind of is the inconsistency in what
-- we test against what.
--
-- Maybe someday we can type-class over that and this can be generally useful
-- for other 2D games (say, top-down ones like Zelda or Hades).
data SurfaceType = Ground | Ceiling | LeftWall | RightWall
  deriving stock (Eq, Ord, Show, Enum, Bounded)

data Surface tag = Surface
  { tag :: tag
  , ty :: SurfaceType
  , line :: Seg Float
  } deriving stock (Show, Foldable, Functor, Traversable)

data PointCollision tag = PointCollision
  { surface :: Surface tag
  , point :: Point
  , sweep :: V2 Float
  } deriving stock (Show)

instance Eq (PointCollision tag) where
  (==) = (==) `on` norm . (.sweep)

instance Ord (PointCollision tag) where
  compare = compare `on` norm . (.sweep)

diamondFromList :: [(Point, V2 Float)] -> Diamond
diamondFromList xs = Diamond $ \p -> lookupJustNote "Incomplete Diamond list" p xs

pointSeg :: Point -> Diamond -> Diamond -> Seg Float
pointSeg p d'from d'to = Seg (d'from.at p) (d'to.at p)

-- TODO: Need to consider slopes when consider what is going down/up/etc
--
-- Could the answer be: Normalize the surface & compare x/y? Need to consider signs
-- and if the slope is going left-to-right or vice-versa I think
isSurfaceRelevant :: Point -> Diamond -> Diamond -> Surface tag -> Bool
isSurfaceRelevant p d'from d'to s = not parallel && case s.ty of
  Ground -> let V2 _ dy = d'to.at p - d'from.at p in dy > 0 -- going down
  Ceiling -> let V2 _ dy = d'to.at p - d'from.at p in dy < 0 -- going up
  LeftWall -> let V2 dx _ = d'to.at p - d'from.at p in dx < 0 -- going left
  RightWall -> let V2 dx _ = d'to.at p - d'from.at p in dx > 0 -- going right
  where
    -- TODO: We still need to handle, say, moving just left/right but hitting ground
    parallel = nearZero $ s'norm - d'norm
    d'norm@(V2 d'norm'x d'norm'y) = abs $ normalize (d'to.at p - d'from.at p)
    s'norm@(V2 s'norm'x s'norm'y) = abs $ segnorm s.line
    lt'ish x y = case y - x of
      z | nearZero z -> False
      z -> z > 0
    gt'ish x y = case y - x of
      z | nearZero z -> False
      z -> z < 0

pointCollision :: Diamond -> Diamond -> Surface tag -> Point -> Maybe (PointCollision tag)
pointCollision d'from d'to surface point = do
  guard $ isSurfaceRelevant point d'from d'to surface
  let dseg = pointSeg point d'from d'to

  -- Due to (I think) floating-point precision, when a diamond-cast is close to the target
  -- surface on either end, the seg-seg intersection test can fail. What's worse, this
  -- is a pretty common case since moving the diamond exactly to the sweep point is
  -- how the sweep is meant to be used. So we special-case those and see if either end
  -- is on the line (modulo epsilon).
  let dseg'startsOnSurface =
        pointOnSeg surface.line dseg.seg'from
  let dseg'endsOnSurface =
        pointOnSeg surface.line dseg.seg'to
  -- The same can happen in reverse - the diamond-cast hitting a surface endpoint
  let surface'startsOnDseg =
        pointOnSeg dseg surface.line.seg'from
  let surface'endsOnDseg =
        pointOnSeg dseg surface.line.seg'to

  -- We could also check for colinearity here, but isSurfaceRelevant already
  -- checks if the diamond-cast and surface are parallel
  if | dseg'startsOnSurface -> do
         let sweep = 0 -- Don't move anywhere
         pure PointCollision{..}
     | dseg'endsOnSurface -> do
         -- Move where we wanted to go all along, but flag it
         -- as a collision
         let sweep = dseg.seg'to - dseg.seg'from
         pure PointCollision{..}
     | surface'startsOnDseg -> do
         -- Move to the surface's starting point
         let sweep = surface.line.seg'from - dseg.seg'from
         pure PointCollision{..}
     | surface'endsOnDseg -> do
         -- Move to the surface's ending point
         let sweep = surface.line.seg'to - dseg.seg'from
         pure PointCollision{..}
     | otherwise -> intersectSegSegParam dseg surface.line <&> \t ->
         let sweep't = t --
             -- I considered rounding this when it was close to 0 to avoid
             -- floating point weirdness but it doesn't seem necessary
             -- from my tests so far.
             --
             -- sweep't = max 0 (t - 0.00001)
             sweep'to = applyIntersectSegSegParam sweep't dseg
             sweep = sweep'to - dseg.seg'from
         in PointCollision{..}

minPointCollision :: Diamond -> Diamond -> Surface tag -> [Point] -> Maybe (PointCollision tag)
minPointCollision d'from d'to s =
    minimumMay
  . catMaybes
  . fmap (pointCollision d'from d'to s)

pointSweepTest :: forall tag. Diamond -> Diamond -> Surface tag -> Maybe (PointCollision tag)
pointSweepTest d'from d'to s = case s.ty of
  Ground -> pointCollision d'from d'to s BOTTOM
  -- TODO: We can prune the points we check based on wall angle
  Ceiling -> minPointCollision d'from d'to s [TOP, LEFT, RIGHT]
  LeftWall -> minPointCollision d'from d'to s [LEFT, TOP, BOTTOM]
  RightWall -> minPointCollision d'from d'to s [RIGHT, TOP, BOTTOM]

pointSweep :: forall tag. Diamond -> Diamond -> [Surface tag] -> Maybe (PointCollision tag)
pointSweep d'from d'to surfaces = minimumMay $ mapMaybe (pointSweepTest d'from d'to) surfaces

---------------------------------------
-- | Little helpers that make it easier to write games around pointSweep:

-- | It's often useful to test if your 'Diamond' is still on a 'Ground' 'Surface'
-- for purposes of, say, falling off platforms.
diamondGrounded :: Diamond -> Surface tag -> Bool
diamondGrounded d Surface{..} = ty == Ground && pointOnSeg line (d.at BOTTOM)

-- | When you want to have your diamond follow a grounded surface (a slope especially),
-- it's important to account for floating-point error as you calculate the angle. This
-- function tells you where the diamond intersects the Surface so you can ensure it
-- remains on the surface frame-over-frame and doesn't accumulate error.
diamondGroundedAt :: Diamond -> Surface tag -> Maybe (V2 Float)
diamondGroundedAt d Surface{..} = do
  let p = d.at BOTTOM
  guard (ty == Ground)
  v <- closestPointOnSeg line p
  guard (nearZero $ p - v)
  pure v

diamondAlongGround :: V2 Float -> Diamond -> Surface tag -> Maybe (V2 Float)
diamondAlongGround v d s@Surface{..} = do
  guard (norm v > 0)
  guard (diamondGrounded d s)
  pure $ redirect (segnorm line) v

-- TODO: It doesn't pick up endpoints..maybe do some extra math to detect when
-- the velocity vector hits another surface? Feels a little redundant with pointSweep
--
-- Maybe we need to recursively project our velocity - first on the grounded seg.
-- Then move the diamond forward & do it again on any seg we are now on with remaining
-- velocity. Tricky math but makes sense in principle.
diamondAlongGrounds :: V2 Float -> Diamond -> [Surface tag] -> Maybe (V2 Float)
diamondAlongGrounds v@(V2 vx _) d sfs = do
  let groundedSfs = filter (diamondGrounded d) sfs
  s :| ss <- fmap (.line) <$> nonEmpty groundedSfs
  let picker = if vx < 0 then leftmostSeg else rightmostSeg
  let picked = foldl' picker s ss
  --when (length groundedSfs > 1) $ traceM $ unwords ["picked =", show picked]
  --pure $ redirect (segnorm $ foldl' picker s ss) v
  segV2 <$> segredirect (d.at BOTTOM) (foldl' picker s ss) v
  --when (length groundedSfs > 1) $ traceM $ unwords ["res =", show res]

-- TODO: Broad phase the ground with a distance check
{-diamondAlongGrounds2 :: V2 Float -> Diamond -> [Surface tag] -> [V2 Float]
diamondAlongGrounds2 v d ss =
  followSegs2 v (d.at BOTTOM) (fmap (.line) $ IntMap.fromList $ zip [0..] $ filter ((== Ground) . (.ty)) ss)
-}

data GroundedSweep tag =
    GroundedSweep'Unimpeded { sweep :: V2 Float }
  | GroundedSweep'Impeded { collision :: PointCollision tag }
-- TODO: Need to expose new velocity too for transfer when leaving surfaces
-- TODO: Have some notion of "leaving the ground" in case the caller wants special
-- fall-off logic
-- TODO: Better type (GroundedSweep?)
groundedPointSweep
  :: Show tag
  => V2 Float -- v
  -> Diamond
  -> [Surface tag]
  -> GroundedSweep tag
groundedPointSweep v d surfaces =
      -- TODO: Prune surfaces s.t. only ones within v of (d.at BOTTOM) are considered
  let ground = fmap (.line) $ IntMap.fromList $ zip [0..] $ filter ((== Ground) . (.ty)) surfaces
      path = followSegs v (d.at BOTTOM) ground
      diamondPath = flip fmap path $ \pos ->
        let ogBottom = d.at BOTTOM
            newBottom = pos
            offset = newBottom - ogBottom
        in Diamond $ \case
          BOTTOM -> newBottom
          x -> d.at x + offset
      diamondCasts = (d : diamondPath) `zip` diamondPath
      collisions = flip fmap diamondCasts $ \(d'from, d'to) ->
        let relevantSurfaces = filter (not . diamondGrounded d'from) surfaces
        in do
          pc <- pointSweep d'from d'to relevantSurfaces
          let totalSweep = (d'from.at BOTTOM + pc.sweep) - (d.at BOTTOM)
          pure $ PointCollision{sweep = totalSweep, point = pc.point, surface = pc.surface}
  in
    case asum collisions of -- Find the first collision (laziness ftw)
      Just pc -> GroundedSweep'Impeded  pc
      Nothing -> GroundedSweep'Unimpeded $ case path of
        [] -> v
        _ -> last path - d.at BOTTOM
