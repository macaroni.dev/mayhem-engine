{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.GameController where

import Cleff

import SDL qualified
import SDL.Raw qualified
import SDL.Utils

import Data.IORef

import Mayhem.Engine.Effects.Expansion

data GameController :: Effect where
  ListControllers :: GameController m [SDL.Raw.GameController]

data GameControllerState = GCS
  { controllers :: IORef [(SDL.Joystick, SDL.Raw.GameController)]
  }

-- TODO: Expect an SDL effect
-- Need to have SDL_GPU initialize after SDL first though instead
-- of letting SDL_GPU do everything. It's about time! Will need this
-- for imgui too
exp'GameController :: IOE :> es => MonadManaged m => Eff es FilePath -> Expansion m GameController es
exp'GameController getDBPath = \Run{..} -> do
  dbPath <- run getDBPath
  loadControllerDB dbPath
  allControllers <- withAllControllers
  controllers <- liftIO $ newIORef allControllers
  pure $ Run $ run . runGameController GCS{..}

runGameController :: IOE :> es => GameControllerState -> Eff (GameController : es) a -> Eff es a
runGameController GCS{..} = interpret $ \case
  ListControllers -> liftIO $ fmap (fmap snd) $ readIORef controllers

-- TH_CODE
makeEffect ''GameController
