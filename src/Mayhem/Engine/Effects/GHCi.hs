{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.GHCi where

import Cleff

import Data.Dynamic
import Data.Typeable
import Control.Concurrent.MVar
import UnliftIO.Exception
import Mayhem.Engine.Effects.Expansion
import Control.Monad.Managed
import Data.Foldable (for_)
import Data.Traversable (for)

data GHCi :: Effect where
  TickGHCi :: GHCi m ()

data GHCiPipe es = GHCiPipe
  { ghciToEngine :: MVar (Eff es Dynamic)
  , ghciFromEngine :: MVar (Either SomeException Dynamic)
  }

newGHCiPipe :: forall es. IO (GHCiPipe es)
newGHCiPipe = GHCiPipe <$> newEmptyMVar <*> newEmptyMVar

exp'GHCiDummy
  :: Applicative m => Expansion m GHCi es
exp'GHCiDummy = \Run{..} ->
  pure $ Run $ run . runGHCiDummy
  where
    runGHCiDummy :: Eff (GHCi : es) ~> Eff es
    runGHCiDummy = interpret $ \case
      TickGHCi -> pure ()

exp'GHCiPipe
  :: Typeable es
  => Applicative m
  => IOE :> es
  => Subset ies es
  => GHCiPipe ies
  -> Expansion m GHCi es
exp'GHCiPipe pipe = \Run{..} -> do
  pure $ Run $ run . runGHCi pipe

runGHCi
  :: forall es ies
   . Typeable es
  => Subset ies es
  => IOE :> es
  => GHCiPipe ies
  -> (Eff (GHCi : es) ~> Eff es)
runGHCi GHCiPipe{..} = interpret $ \case
  TickGHCi -> do
    mEffDyn <- liftIO (tryTakeMVar ghciToEngine)
    res <- traverse (tryAny . inject) mEffDyn
    liftIO $ for_ res (putMVar ghciFromEngine)

sendGHCi
  :: forall a ies
   . Typeable a
  => Typeable ies
  => GHCiPipe ies
  -> Eff ies a
  -> IO a
sendGHCi GHCiPipe{..} eff = do
  putMVar ghciToEngine (toDyn <$> eff)
  takeMVar ghciFromEngine >>= \case
    Left e -> throwIO e
    Right dynA -> case fromDynamic @a dynA of
      Nothing -> throwString $ unwords
        [ "Type Error! (this shouldn't happen)"
        , "got:", show dynA
        , "expected:", show (typeRep (Proxy @a))
        ]
      Just a -> pure a

-- TH_CODE
makeEffect ''GHCi
