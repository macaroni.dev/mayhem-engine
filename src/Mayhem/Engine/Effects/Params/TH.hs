{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Params.TH where

import Cleff

data Params p :: Effect where
  AskParams :: Params p m p
  ReloadParams :: p -> Params p m ()

-- TH_CODE
makeEffect ''Params
