{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Cute_Sound where

import Cleff
import Control.Monad.Managed

import qualified Cute.Sound.C as CS
import Foreign.Ptr
import UnliftIO (bracket)

import Mayhem.Engine.Effects.Expansion

data Cute_Sound :: Effect where
  AskCuteSound :: Cute_Sound m (Ptr CS.Context)

exp'Cute_Sound :: MonadManaged m => Expansion m Cute_Sound es
exp'Cute_Sound = \Run{..} -> do
  cs_ctx <- managed (bracket (CS.makeContext nullPtr 44100 15 0 nullPtr) CS.shutdownContext)
  pure $ Run $ run . runCute_Sound cs_ctx

runCute_Sound :: Ptr CS.Context -> Eff (Cute_Sound : es) a -> Eff es a
runCute_Sound cs_ctx = interpret $ \case
  AskCuteSound -> pure cs_ctx

data Cute_Sound8000 :: Effect where
  AskCuteSound8000 :: Cute_Sound8000 m (Ptr CS.Context)

exp'Cute_Sound8000 :: MonadManaged m => Expansion m Cute_Sound8000 es
exp'Cute_Sound8000 = \Run{..} -> do
  cs_ctx <- managed (bracket (CS.makeContext nullPtr 8000 15 0 nullPtr) CS.shutdownContext)
  pure $ Run $ run . runCute_Sound8000 cs_ctx

runCute_Sound8000 :: Ptr CS.Context -> Eff (Cute_Sound8000 : es) a -> Eff es a
runCute_Sound8000 cs_ctx = interpret $ \case
  AskCuteSound8000 -> pure cs_ctx

-- TH_CODE
makeEffect ''Cute_Sound
makeEffect ''Cute_Sound8000
