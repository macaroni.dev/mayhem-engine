{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Debug where

import Cleff

import Control.Monad.Reader
import Control.Monad.Extra (whenM)
import Data.Foldable
import Data.Functor
import Data.Typeable
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.DList (DList)
import qualified Data.DList as DList
import Data.IORef
import Data.String

import Foreign.Ptr
import Foreign.C.String
import Foreign.Marshal.Array
import Foreign.Marshal.Utils
import Memorable

import Linear
import Apecs (SystemT)
import CuteC2

import qualified SDL.GPU.Utils as GPU
import qualified SDL.GPU.Simple as GPU.C
import qualified SDL.GPU.FC.Simple as FC
import Mayhem.Engine.Effects.SDL_GPU
import Mayhem.Engine.Effects.Expansion
import Mayhem.Engine.Utils

data Sink = Sink
  { unSink :: IORef (Map Level (DList Entry))
  , font :: Ptr FC.Font
  -- TODO: Filters: (forall level. Typeable level => level -> Bool)
  }

resetIO :: Sink -> IO ()
resetIO (Sink r _) = atomicWriteIORef r mempty

debugIO :: Sink -> Level -> Entry -> IO ()
debugIO (Sink r _) level entry = do
  atomicUpdateIORef r $ \entries ->
    Map.alter (<> Just (pure entry)) level entries

flushIO :: Ptr GPU.Target -> Sink -> IO ()
flushIO screen sink = renderIO screen sink >> resetIO sink

renderIO :: Ptr GPU.Target -> Sink -> IO ()
renderIO screen (Sink ref font) = do
  entries <- readIORef ref
  for_ (Map.elems entries >>= DList.toList) $ \case
    FreeForm k -> k screen
    ShapeEntry loc color shape -> case shape of
      PolygonFilled pts ->
          GPU.C.polygonFilled screen pts color
      RectangleFilled v1 v2 -> do
        let V2 x1 y1 = v1 + loc
            V2 x2 y2 = v2 + loc
        GPU.C.rectangleFilled screen x1 y1 x2 y2 color
      Rectangle v1 v2 -> do
        let V2 x1 y1 = v1 + loc
            V2 x2 y2 = v2 + loc
        GPU.C.rectangle screen x1 y1 x2 y2 color
      HorizontalLine y -> do
        let V2 locX locY = loc
        width <- screen *-> (.w)
        GPU.C.line screen (0 + locX) (y + locY) (fI width + locX) (y + locY) color
      Circle r -> do
        let V2 x y = loc
        GPU.C.circle screen x y r color
      CircleFilled r -> do
        let V2 x y = loc
        GPU.C.circleFilled screen x y r color
      Line (V2 x1 y1) (V2 x2 y2) -> do -- TODO (BUG): Line is absolute right now!
        GPU.C.line screen x1 y1 x2 y2 color
    TextEntry (V2 x y) txt ->
      void $ FC.draw font screen x y txt

data Level where
  Level :: forall a. (Show a, Ord a, Typeable a) => a -> Level

instance Eq Level where
  (Level (x :: a)) == (Level (y :: b)) = case eqT @a @b of
    Nothing -> False
    Just Refl -> x == y

instance Ord Level where
  compare (Level (x :: a)) (Level (y :: b)) = case eqT @a @b of
    Nothing -> compare (typeRep (Proxy :: Proxy a)) (typeRep (Proxy :: Proxy b))
    Just Refl -> compare x y

instance Show Level where
  show (Level (x :: a)) =
       "Level "
    ++ "(" ++ (show (typeRep (Proxy :: Proxy a))) ++ ") "
    ++ "(" ++ (show x) ++ ")"

instance IsString Level where
  fromString = Level

-- TODO: I hate Entry and Level as names..
data Entry =
    ShapeEntry (V2 Float) GPU.Color Shape
  | TextEntry  (V2 Float) String -- Use Comic Mono (or one of the variants) for some fun :)
  | FreeForm (Ptr GPU.Target -> IO ())
-- TODO: Move this to sdl-gpu-hs?
data Shape =
    PolygonFilled [V2 Float] -- NOTE: These are relative points .. the shape entry has their absolute location
  | RectangleFilled (V2 Float) (V2 Float)
  | Rectangle (V2 Float) (V2 Float)
  | HorizontalLine Float
  | Circle Float
  | CircleFilled Float
  | Line (V2 Float) (V2 Float)

squareFilled :: Float -> Shape
squareFilled s = RectangleFilled (V2 0 0) (V2 s s)

debugAABB :: GPU.Color -> C2AABB -> Entry
debugAABB color (C2AABB (C2V x1 y1) (C2V x2 y2)) = ShapeEntry (V2 0 0) color $ Rectangle (V2 x1 y1) (V2 x2 y2)

debugAABBFilled :: GPU.Color -> C2AABB -> Entry
debugAABBFilled color (C2AABB (C2V x1 y1) (C2V x2 y2)) = ShapeEntry (V2 0 0) color $ RectangleFilled (V2 x1 y1) (V2 x2 y2)

-- The effect itself
data Debug :: Effect where
  Debug :: Level -> Entry -> Debug m ()
  Toggle :: Debug m ()
  Render :: Debug m ()
  Reset :: Debug m ()
  Flush :: Debug m ()
  Enabled :: Debug m Bool

exp'NoDebug :: Applicative m => Expansion m Debug es
exp'NoDebug = \Run{..} -> pure $ Run $ run . noRun
  where
    noRun :: Eff (Debug : es) a -> Eff es a
    noRun = interpret $ \case
      Debug _ _ -> pure ()
      Toggle -> pure ()
      Render -> pure ()
      Reset -> pure ()
      Flush -> pure ()
      Enabled -> pure False

exp'Debug :: MonadIO m => [SDL_GPU, IOE] :>> es => (Eff es (Ptr FC.Font)) -> Expansion m Debug es
exp'Debug getFont = \Run{..} -> do
  enabled <- liftIO $ isEnvSet "DEBUG" >>= newIORef
  font <- run getFont
  sink <- liftIO $ Sink <$> newIORef mempty <*> pure font
  pure $ Run $ run . runDebug enabled sink

-- TODO: A way to turn off debug - no DEBUG env var should mean you cannot turn on debug
-- And then - expose that to the user
runDebug :: forall es a. [SDL_GPU, IOE] :>> es => IORef Bool -> Sink -> Eff (Debug : es) a -> Eff es a
runDebug enabled sink = interpret $ \case
  Debug lvl e -> liftIO $ debugIO sink lvl e
  Toggle -> liftIO $ atomicUpdateIORef enabled not
  Render -> do
    whenM (liftIO $ readIORef enabled) $
      askGPU >>= \screen -> liftIO $ renderIO screen sink
  Reset -> liftIO $ resetIO sink
  Flush -> do
    screen <- askGPU
    liftIO $ whenM (readIORef enabled) $
      flushIO screen sink
    liftIO $ resetIO sink
  Enabled -> liftIO $ readIORef enabled

-- TH_CODE
makeEffect ''Debug
