{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Assets where

import Cleff

import Mayhem.Engine.Assets
import Mayhem.Engine.Effects.Expansion

data Assets a :: Effect where
  AskAssets :: Assets a m a

exp'Assets :: forall a m es. Asset a => MonadManaged m => FilePath -> Expansion m (Assets a) es
exp'Assets dir = \Run{..} -> do
  assets <- managed (bracket (loadAssets @a dir) freeAsset)
  pure $ Run $ run . runAssets assets

runAssets :: a -> Eff (Assets a : es) r -> Eff es r
runAssets a = interpret $ \case
  AskAssets -> pure a

-- TH_CODE
makeEffect ''Assets
