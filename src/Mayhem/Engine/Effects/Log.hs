{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Log where

import Cleff

import qualified Data.ByteString.Char8
import Data.Text (Text)
import qualified Data.Text
import Jsonifier as Json
import Debug.Trace

import Mayhem.Engine.Effects.Clock
import Mayhem.Engine.Effects.Expansion

data Ctx = Ctx Text Json

data Log :: Effect where
  LogJSON :: Json -> Log m ()
  LogText :: Text -> Log m ()
  WithContext :: [Ctx] -> m a -> Log m a

exp'NoLog :: Applicative m => Expansion m Log es
exp'NoLog = \Run{..} -> pure $ Run $ run . noRun
  where
    noRun :: Eff (Log : es) a -> Eff es a
    noRun = interpret $ \case
      LogJSON _ -> pure ()
      LogText _ -> pure ()
      WithContext _ m -> toEff m

exp'Log'Clocked :: Applicative m => [IOE, Clock] :>> es => Expansion m Log es
exp'Log'Clocked = \Run{..} ->
  pure $ Run $ \eff -> run (clockJson >>= \clk -> runLog [Ctx (Data.Text.pack "clock") clk] eff)

exp'Log :: Applicative m => IOE :> es => Expansion m Log es
exp'Log = \Run{..} -> pure $ Run $ run . runLog []

runLog :: forall es a. IOE :> es => [Ctx] -> Eff (Log : es) a -> Eff es a
runLog x = interpret (h x)
  where
    h :: [Ctx] -> Handler Log es
    h ctx = \case
      WithContext c m -> toEffWith (h (c ++ ctx)) m
      LogJSON j ->
          liftIO
        $ traceIO
        $ Data.ByteString.Char8.unpack
        $ Json.toByteString
        $ Json.object
        $ fmap (\(Ctx k v) -> (k, v))
        $ Ctx (Data.Text.pack "body") j : ctx
      LogText s ->
          liftIO
        $ traceIO
        $ Data.ByteString.Char8.unpack
        $ Json.toByteString
        $ Json.object
        $ fmap (\(Ctx k v) -> (k, v))
        $ Ctx (Data.Text.pack "message") (Json.textString s) : ctx

-- TH_CODE
makeEffect ''Log
