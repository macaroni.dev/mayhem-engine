{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Apecs where

import Cleff
import Apecs qualified as Upstream

import Mayhem.Engine.Effects.Expansion

-- IDEA: Have a "stack" of worlds. Might need singletons, but that's okay.
-- https://gamedev.stackexchange.com/a/25232
-- https://web.archive.org/web/20120320093956/http://create.msdn.com/en-US/education/catalog/sample/game_state_management
data ApecsE world :: Effect where
  LiftApecs :: Upstream.SystemT world m a -> ApecsE world m a

exp'Apecs :: MonadIO m => IOE :> es => IO world -> Expansion m (ApecsE world) es
exp'Apecs initWorld = \Run{..} -> do
  w <- liftIO $ initWorld
  pure $ Run $ run . runApecs w

runApecs :: IOE :> es => world -> Eff (ApecsE world : es) a -> Eff es a
runApecs w = interpret $ \case
  LiftApecs st -> withToIO $ \toIO -> do
    toIO $ Upstream.runSystem st w

-- TODO: Use makeEffect_ .. it requires us to be able to write code after TH_CODE
-- though. Gotta improve the splice script first.

-- TH_CODE

makeEffect ''ApecsE
