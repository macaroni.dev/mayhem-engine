{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Stepper where

import Cleff
import Data.IORef

import Mayhem.Engine.Effects.Expansion
import Mayhem.Engine.Frame

-- TODO: Clean up this API. It's been driven by use-cases and not principles.
data Stepper :: Effect where
  StepGo   :: Stepper m ()
  StepStop :: Stepper m ()
  StepToggle :: Stepper m ()
  StepN    :: Frame -> Stepper m ()
  Stepped  :: m () -> Stepper m ()

data StepState = StepGoing | StepStopped | StepForN Frame deriving (Eq, Show)

exp'NoStepper :: Applicative m => Expansion m Stepper es
exp'NoStepper = \Run{..} -> pure $ Run $ run . noRun
  where
    noRun :: Eff (Stepper : es) a -> Eff es a
    noRun = interpret $ \case
      StepGo -> pure ()
      StepStop -> pure ()
      StepToggle -> pure ()
      StepN _ -> pure ()
      Stepped m -> toEff m

exp'Stepper :: MonadIO m => IOE :> es => Expansion m Stepper es
exp'Stepper = \Run{..} -> do
  ssref <- liftIO (newIORef StepGoing)
  pure $ Run $ run . runStepper ssref

runStepper :: IOE :> es => IORef StepState -> Eff (Stepper : es) a -> Eff es a
runStepper ssref = interpret $ \case
  StepGo -> liftIO $ writeIORef ssref StepGoing
  StepStop -> liftIO $ writeIORef ssref StepStopped
  StepToggle -> liftIO $ readIORef ssref >>= \case
    StepGoing -> writeIORef ssref StepStopped
    StepStopped -> writeIORef ssref StepGoing
    _ -> pure ()
  StepN n -> liftIO $ writeIORef ssref (StepForN n)
  Stepped m -> liftIO (readIORef ssref) >>= \case
    StepGoing -> toEff m
    StepStopped -> pure ()
    StepForN n -> do
      toEff m
      liftIO $
        if n <= 1
        then writeIORef ssref StepStopped
        else writeIORef ssref (StepForN (n - 1))

-- TH_CODE
makeEffect ''Stepper
