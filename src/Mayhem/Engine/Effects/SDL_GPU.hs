{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.SDL_GPU where

import Control.Monad
import Control.Monad.Managed
import Data.Bits

import Cleff

import SDL.GPU.Utils qualified as GPU
import SDL qualified
import SDL.Raw qualified
import Foreign.Ptr
import UnliftIO (bracket)

import Mayhem.Engine.Effects.Expansion
import Mayhem.Engine.Resolution

data SDL_GPU :: Effect where
  AskGPU :: SDL_GPU m (Ptr GPU.Target)
  AskNativeRez :: SDL_GPU m NativeResolution

data SDL_GPU'Config = SDL_GPU'Config
  { enableVsync :: Bool
  , nativeRez :: NativeResolution
  }

exp'SDL_GPU :: MonadManaged m => SDL_GPU'Config -> Expansion m SDL_GPU es
exp'SDL_GPU SDL_GPU'Config{..} = \Run{..} -> do
  let NativeResolution{..} = nativeRez
  liftIO $ GPU.setDebugLevel GPU.debugLevelMax
  when enableVsync $ liftIO $ GPU.setPreInitFlags GPU.initEnableVsync -- TODO: upstream typo!

  -- TODO: The game crashes on quit if we `initializeAll` here..should try things one-by-one to
  -- figure out why.
  --
  -- TODO: Should this be in here at all?
  SDL.initialize [SDL.InitGameController]

  screen <- managed (bracket (GPU.init nativeWidth nativeHeight (GPU.defaultInitFlags .|. SDL.Raw.SDL_WINDOW_RESIZABLE)) (\_ -> GPU.quit))

  liftIO $ GPU.setVirtualResolution screen nativeWidth nativeHeight
  liftIO $ GPU.setDefaultAnchor 0 0
  SDL.cursorVisible SDL.$= False

  pure $ Run $ run . runSDLGPU nativeRez screen

runSDLGPU :: NativeResolution -> Ptr GPU.Target -> (Eff (SDL_GPU : es) ~> Eff es)
runSDLGPU nativeRez screen = interpret $ \case
  AskGPU -> pure screen
  AskNativeRez -> pure nativeRez

-- TH_CODE
makeEffect ''SDL_GPU
