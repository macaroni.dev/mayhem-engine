{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Repl.TH where

import Data.Aeson
import Cleff
import Data.Kind

data Repl (req :: Type) :: Effect where
  Eval :: FromJSON req
       => (req -> m ())
       -> Repl req m ()

-- TH_CODE
makeEffect ''Repl
