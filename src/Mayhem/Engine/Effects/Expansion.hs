module Mayhem.Engine.Effects.Expansion
  ( module Mayhem.Engine.Effects.Expansion
  , module X
  ) where

import Cleff
import Control.Monad.Managed as X (MonadManaged, managed, Managed, runManaged)
import UnliftIO as X (bracket)

-- | We need this type to be able to use do notation:
--
-- * https://gitlab.haskell.org/ghc/ghc/-/issues/18324
-- * https://gitlab.haskell.org/ghc/ghc/-/issues/20020
-- * https://gitlab.haskell.org/ghc/ghc/-/issues/19110

data m :~> n = Run { run :: forall a. m a -> n a }

-- TODO: Just make the Console a concrete type?
type Expansion m e (es :: [Effect]) = (Eff es :~> m) -> m (Eff (e : es) :~> m)

runLiftIOE :: MonadIO m => Eff '[IOE] :~> m
runLiftIOE = Run (liftIO . runIOE)
