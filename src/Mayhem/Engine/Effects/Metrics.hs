{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Metrics where

import Cleff

import System.TimeIt

data MetricsE :: Effect where
  Timed :: String -> m a -> MetricsE m a
  Emit  :: String -> Double -> MetricsE m ()

runMetricsE :: IOE :> es => (String -> Double -> IO ()) -> Eff (MetricsE : es) a -> Eff es a
runMetricsE f = interpret $ \case
  Timed s ma -> do
    (x, a) <- timeItT $ toEff ma
    liftIO $ f s x
    pure a
  Emit s x -> liftIO $ f s x

-- TH_CODE
makeEffect ''MetricsE
