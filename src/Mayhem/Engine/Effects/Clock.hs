{-# LANGUAGE TemplateHaskell #-}
module Mayhem.Engine.Effects.Clock where

import Mayhem.Engine.Frame
import Mayhem.Engine.Effects.Expansion
import Mayhem.Engine.Utils

import Cleff

import Data.IORef
import qualified Jsonifier as Json

data Clock :: Effect where
  GetClock :: Clock m Frame
  -- TODO: Only LD51 uses this
  ResetClock :: Clock m ()
  Clocked :: m () -> Clock m ()

exp'Clock :: MonadIO m => IOE :> es => Expansion m Clock es
exp'Clock = \Run{..} -> do
  clk <- liftIO $ newIORef 0
  pure $ Run $ run . runClock clk

runClock :: IOE :> es => IORef Frame -> Eff (Clock : es) a -> Eff es a
runClock clk = interpret $ \case
  GetClock -> liftIO $ readIORef clk
  ResetClock -> liftIO $ writeIORef clk 0
  Clocked m -> do
    toEff m
    liftIO $ atomicUpdateIORef clk succ

clockJson :: Clock :> es => Eff es Json.Json
clockJson = Json.wordNumber . fromIntegral <$> send GetClock

-- TH_CODE
makeEffect ''Clock
