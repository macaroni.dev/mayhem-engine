module Mayhem.Engine.Effects.Params
  ( module X
  , module Mayhem.Engine.Effects.Params
  ) where

import Mayhem.Engine.Effects.Params.TH as X
import Mayhem.Engine.Effects.Expansion

import Cleff

import Optics
import Data.IORef

exp'Params :: MonadIO m => IOE :> es => Applicative m => IO p -> Expansion m (Params p) es
exp'Params mkP = \Run{..} -> do
  ps <- liftIO $ initParamStore mkP
  pure $ Run $ run . runParams ps

runParams :: IOE :> es => ParamStore p -> Eff (Params p : es) a -> Eff es a
runParams ps = interpret $ \case
  AskParams -> liftIO $ readParams ps
  ReloadParams new'p -> liftIO $ writeIORef (currParams ps) new'p

viewParams
  :: Is k A_Getter
  => Params params :> es
  => Optic' k is params b
  -> Eff es b
viewParams x = view x <$> askParams

data ParamStore params = ParamStore
  { currParams :: IORef params
  }

readParams :: ParamStore params -> IO params
readParams ParamStore{..} = readIORef currParams

initParamStore :: IO params -> IO (ParamStore params)
initParamStore load = do
  !p <- load
  ref <- newIORef p
  pure ParamStore
    { currParams = ref
    }
