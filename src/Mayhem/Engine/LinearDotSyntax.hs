{-# OPTIONS_GHC -fno-warn-orphans #-}
module Mayhem.Engine.LinearDotSyntax where

import GHC.Records
import Linear

instance HasField "x" (V2 a) a where
  getField (V2 x _) = x

instance HasField "y" (V2 a) a where
  getField (V2 _ y) = y
