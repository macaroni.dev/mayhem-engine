-- TODO: Move framerate limiting stuff in here someday?
-- TODO: User-defined sub-clocks that dynamically spawn & die
module Mayhem.Engine.Clock where

import Data.IORef
import qualified Jsonifier as Json

import Mayhem.Engine.Frame
import Mayhem.Engine.Utils

data Clock = Clock
  { global :: IORef Frame
  }

new :: IO Clock
new = do
  global <- newIORef 0
  pure Clock{..}

tick :: Clock -> IO ()
tick Clock{..} = atomicUpdateIORef global succ

toJson :: Clock -> IO Json.Json
toJson Clock{..} = Json.wordNumber . fromIntegral <$> readIORef global
-- ^ NOTE: This uses the system word, which happens to be 64-bits on most computers

toString :: Clock -> IO String
toString Clock{..} = frame2numstr <$> readIORef global
