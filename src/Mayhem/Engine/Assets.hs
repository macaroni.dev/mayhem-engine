{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
module Mayhem.Engine.Assets
  ( module Mayhem.Engine.Assets
  , module Foreign.Ptr
  ) where

import Foreign.Ptr
import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Marshal.Utils (toBool)
import Foreign.C.String
import System.FilePath
import System.Environment
import Data.List.Split
import Data.Traversable
import Data.Foldable
import Data.Foldable.Extra (findM)
import qualified Data.Map.Strict as Map

import Control.Monad (when, unless, replicateM)
import GHC.TypeLits
import GHC.Generics
import Data.Kind
import Data.Proxy
import Data.Aeson (FromJSON)
import Debug.Trace (traceIO)

import Mayhem.Engine.TH

import qualified Cute.Sound.C as CS
import qualified SDL.Raw.Types as SDLRaw
import qualified SDL.GPU.Simple as GPU
import qualified SDL.GPU.FC.Simple as FC
import Memorable

import Mayhem.Engine.Utils
import Mayhem.Engine.Frame
import qualified Mayhem.Engine.Animation as Animation

data Color = Color Nat Nat Nat Nat

type family KN (ns :: [Nat]) :: Constraint where
  KN '[] = ()
  KN (n ': ns) = (KnownNat n, KN ns)

-- TODO?: Make this take a list of configs that we fold over
newtype Font (size :: Nat) (color :: Color) = Font { rawFont :: (Ptr FC.Font) }

floatVal :: forall n. KnownNat n => Float
floatVal = fromIntegral (natVal (Proxy :: Proxy n))

intgVal :: forall n a. KnownNat n => Integral a => a
intgVal = fromIntegral (natVal (Proxy :: Proxy n))

cleanAssetPath :: FilePath -> FilePath
cleanAssetPath path = takeDirectory path </> dropWhile (== '_') (takeBaseName path)

class Asset a where
  loadAsset :: FilePath -> IO a
  default loadAsset :: (Generic a, GAsset (Rep a)) => FilePath -> IO a
  loadAsset path = to <$> gloadAsset path

  freeAsset :: a -> IO ()
  default freeAsset :: (Generic a, GAsset (Rep a)) => a -> IO ()
  freeAsset = gfreeAsset . from

instance Asset () where
  loadAsset _ = pure ()
  freeAsset _ = pure ()

-- Assumes png atm
-- TODO: Filetype discovery
instance Asset (Ptr GPU.Image) where
  loadAsset (cleanAssetPath->path) = do
    img <- GPU.loadImage (path <.> "png")
    when (img == nullPtr) $ error $ "Could not load " ++ path <.> "png"
    -- TODO: These settings should be configurable with sane defaults..
    -- HKDing assets would make that nice

    -- Gfx-convention: Anchor top-left
    GPU.setAnchor img 0 0

    -- Keeps our pixel art crisp
    -- https://csantosbh.wordpress.com/2014/01/25/manual-texture-filtering-for-pixelated-games-in-webgl/#more-55
    -- This is a pretty interesting improvement, but naive nearest neighbor is good enough for now :)
    GPU.setImageFilter img GPU.filterNearest

    pure img

  freeAsset = GPU.freeImage

instance (KN '[size, r, g, b, a]) => Asset (Font size ('Color r g b a)) where
  loadAsset (cleanAssetPath->path) = do
    -- "__" suffixes allow you to have multiple of the same font in different
    -- styles, sizes, colors, etc
    let realPath = head $ splitOn "__" path
    let color = GPU.Color (intgVal @r) (intgVal @g) (intgVal @b) (intgVal @a)
    font <- FC.loadFont (realPath <.> "ttf") (intgVal @size) color 0
    FC.setFilterMode font FC.filterNearest
    pure $ Font font
  freeAsset = FC.freeFont . rawFont

instance Asset (Ptr CS.LoadedSound) where
  loadAsset path = do
    sound <- mallocEmpty @CS.LoadedSound
    withCString (path <.> "wav") $ \p -> CS.loadWav sound p
    CS.errorReason >>= \s -> unless (s == nullPtr) $ do
      err <- peekCString s
      error $ "Failed to load " ++ path <.> "wav" ++ " due to " ++ err
    pure sound

  freeAsset s = CS.freeSound s >> free s

instance Asset CS.PlaySoundDef where
  loadAsset path = do
    loaded <- loadAsset @(Ptr CS.LoadedSound) path
    alloca $ \ptr -> CS.makeDef ptr loaded >> peek ptr

  freeAsset CS.PlaySoundDef{loaded} = freeAsset loaded

-- TODO: Phantoms for config (e.g. loop)
-- TODO: Utils to restart / have concurrent sounds - should it be here or should
-- the engine manage those resources?
data CuteSound = CuteSound { loaded :: Ptr CS.LoadedSound, sound :: Ptr CS.PlayingSound }

instance Asset CuteSound where
  loadAsset path = do
    loaded <- loadAsset @(Ptr CS.LoadedSound) path
    sound <- mallocEmpty @CS.PlayingSound
    CS.makePlayingSound sound loaded
    pure CuteSound{..}

  freeAsset CuteSound{..} = CS.stopSound sound >> freeAsset loaded >> free sound

data CuteSoundN (n :: Nat) = CuteSoundN { loaded :: Ptr CS.LoadedSound, pool :: [Ptr CS.PlayingSound] }

instance KnownNat n => Asset (CuteSoundN n) where
  loadAsset path = do
    loaded <- loadAsset @(Ptr CS.LoadedSound) path
    pool <- replicateM (intgVal @n) $ do
      sound <- mallocEmpty @CS.PlayingSound
      CS.makePlayingSound sound loaded
      pure sound
    pure CuteSoundN{..}

  freeAsset CuteSoundN{..} = traverse_ CS.stopSound pool >> freeAsset loaded >> traverse_ free pool

withPooledSound :: CuteSoundN n -> (Ptr CS.PlayingSound -> IO r) -> IO ()
withPooledSound CuteSoundN{..} f =
  findM (fmap (not . toBool) . CS.isActive) pool >>= traverse_ f

instance Asset Animation.Script where
  loadAsset (cleanAssetPath->path) = Animation.Script <$> Animation.readSpriteSheetYAML load (path <.> "yaml")
    where
      load _ (Just _) = error "Transparency color not supported"
      load fp Nothing =
        let fullPath = takeDirectory path </> fp
         in do
          img <- GPU.loadImage fullPath
          when (img == nullPtr) $ error $ "Could not load " ++ fullPath
          -- For pixel art
          GPU.setImageFilter img GPU.filterNearest
          pure img

  freeAsset (Animation.Script Animation.SpriteSheet{..}) = GPU.freeImage ssImage

instance (Show k, Enum k, Bounded k, Ord k, Asset a) => Asset (k -> a) where
  loadAsset path = do
    kas <- for [minBound..maxBound :: k] $ \k -> (k,) <$> loadAsset (path ++ "__" ++ show k)
    let kam = Map.fromList kas
    pure $ \k -> case Map.lookup k kam of
      Just a -> a
      Nothing -> error "IMPOSSIBLE"
  freeAsset f = for_ [minBound..maxBound :: k] (freeAsset . f)

-- TODO: TextFilePath asset (just a FilePath under the hood)

newtype TextFilePath = TextFilePath { path :: FilePath }

instance Asset TextFilePath where
  loadAsset = pure . TextFilePath . (<.> "txt")
  freeAsset _ = pure ()

-- Generic deriving: Use records to reflect your assets' directory structure
class GAsset a where
  gloadAsset :: FilePath -> IO (a x)
  gfreeAsset :: a x -> IO ()

instance GAsset U1 where
  gloadAsset _ = pure U1
  gfreeAsset _ = pure ()

instance (GAsset a, GAsset b) => GAsset (a :*: b) where
  gloadAsset path = (:*:) <$> gloadAsset path <*> gloadAsset path
  gfreeAsset (a :*: b) = gfreeAsset a >> gfreeAsset b

instance GAsset a => GAsset (D1 _1 a) where
  gloadAsset path = M1 <$> gloadAsset path
  gfreeAsset (M1 a) = gfreeAsset a

instance GAsset a => GAsset (C1 _1 a) where
  gloadAsset path = M1 <$> gloadAsset path
  gfreeAsset (M1 a) = gfreeAsset a

instance (Asset asset, KnownSymbol name) =>
  GAsset (S1 ('MetaSel ('Just name) _1 _2 _3)
          (K1 _4 asset)) where
  gloadAsset path = (M1 . K1) <$> loadAsset (path </> (symbolVal (Proxy @name)))
  gfreeAsset (M1 (K1 asset)) = freeAsset asset

----

getAssetPrefix :: IO FilePath
getAssetPrefix = lookupEnv "USE_PWD_ASSETS" >>= \case
  Just "1" -> pure ""
  _ -> lookupEnv "ASSETS_PREFIX" >>= \case
    Just prefix -> pure prefix
    _ -> takeDirectory <$> getExecutablePath

loadAssets :: forall a. Asset a => FilePath -> IO a
loadAssets dir = do
  prefix <- getAssetPrefix
  loadAsset (prefix </> dir)

-- TH_CODE
mkOpticsLabels ''Font
mkOpticsLabels ''CuteSound
mkOpticsLabels ''CuteSoundN
