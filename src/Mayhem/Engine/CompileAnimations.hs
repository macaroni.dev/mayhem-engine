{-# LANGUAGE StrictData #-}
{-# LANGUAGE OverloadedStrings #-}

-- Missing features:
-- - Multiple animations per sheet
module Mayhem.Engine.CompileAnimations where

import Control.Monad
import Control.Monad.IO.Class
import Data.Word
import Data.Function
import Data.List
import Data.Foldable
import System.Directory
import System.Environment
import qualified Animate
import qualified Animate.Frames as Animate
import Data.Digest.Pure.MD5
import Data.List.Split
import Control.Monad.Trans.RWS.CPS
import Data.DList (DList)
import System.FilePath
import System.Exit
import System.Process
import qualified Data.ByteString.Lazy as B
import Data.List.NonEmpty (nonEmpty, NonEmpty (..))
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import qualified Data.Text.IO as T

main :: IO ()
main = getArgs >>= \case
  ["--prefix", prefix, "--src", src, "--dst", dst] -> genSpriteSheets prefix src dst
  args -> error $ "Must pass exactly --prefix PREFIX --src SRC --dst DST (that order).. NOT " ++ show args

data Sprite = Sprite
  { path :: FilePath
  , delay :: Word64 -- read @Word64 handles leading 0s
  , clip  :: Animate.SpriteClip ()
  } deriving Show

data S = S
  { lastHash :: MD5Digest
  , frames :: Word64 -- Running delay counter
  , runningHeight :: Word64 -- Accumulate heights of images..used to set Y in SpriteClips
  , currentFile :: FilePath
  } deriving Show

genSpriteSheets :: String -> FilePath -> FilePath -> IO ()
genSpriteSheets prefix src dst = do
  createDirectoryIfMissing True dst
  -- TODO: It is too loose about prefix..
  -- e.g. duck1.png and duckduck1.png will be picked up when prefix = "duck"
  let validFile = (&&) <$> isPrefixOf prefix <*> isExtensionOf "png"
  files <- fmap (src </>) . sort . filter validFile <$> listDirectory src
  nonEmpty files & \case
    Nothing -> putStrLn $ "No files for prefix: " ++ prefix
    Just (firstFile :| rest) -> do
      h <- md5 <$> B.readFile firstFile
      (_, dsprites) <- execRWST (traverse_ spriteStep rest >> tellSprite) () $ S
        { lastHash = h
        , frames = 1
        , runningHeight = 0
        , currentFile = firstFile
        }
      let sprites = toList dsprites
      putStrLn $ unwords ["Found", show (length sprites), "sprites =", show sprites]
      let sheetName = prefix ++ "_sheet" <.> "png"
      putStrLn $ unwords ["Writing", sheetName, "..."]
      let cmd = unwords
            [ "convert"
            , unwords (fmap path sprites)
            , "-append"
            , dst </> sheetName
            ]
      putStrLn $ unwords ["Running cmd:`", cmd, "`"]
      code <- system cmd
      unless (code == ExitSuccess) $
        error "convert failed"

      let sheetInfo = mkSheetInfo sheetName sprites
      putStrLn "Writing spritesheet yaml:"
      T.writeFile (dst </> prefix <.> "yaml") $ Animate.customWriteSpriteSheetInfo sheetInfo

spriteStep :: FilePath -> RWST () (DList Sprite) S IO ()
spriteStep = \file -> do
    S{..} <- get
    h <- md5 <$> liftIO (B.readFile file)
    if h == lastHash
      then
      put S
      { lastHash = h
      , frames = frames + 1
      , runningHeight = runningHeight
      , currentFile = currentFile
      }


      else do
      imgH <- tellSprite
      put S
        { lastHash = h
        , frames = 1
        , runningHeight = runningHeight + imgH
        , currentFile = file
        }

tellSprite :: RWST () (DList Sprite) S IO Word64
tellSprite = do
  S{..} <- get
  (code, identifyOut, identifyErr) <- liftIO $ readProcessWithExitCode "identify" ["-format", "'(%w, %h)'", currentFile] ""

  unless (code == ExitSuccess) $ do
    error $ unwords ["identify failed for", currentFile, ":", identifyErr]

  let cleanOut = filter (/= '\'') identifyOut
  liftIO $ putStrLn $ "clean out = " ++ cleanOut
  let (imgW, imgH) = read @(Word64, Word64) cleanOut

  tell $ pure Sprite
    { path = currentFile
    , delay = frames
    , clip = Animate.SpriteClip
      { scX = 0
      , scY = fromIntegral runningHeight
      , scW = fromIntegral imgW
      , scH = fromIntegral imgH
      , scOffset = Nothing -- not supported
      }
    }
  pure imgH

mkSheetInfo :: FilePath -> [Sprite] -> Animate.SpriteSheetInfo () Float
mkSheetInfo sheetPath sprites = Animate.SpriteSheetInfo
  { ssiImage = sheetPath
  , ssiAlpha = Nothing
  , ssiClips = clip <$> sprites
  , ssiAnimations = Map.singleton "Animation" $ zip [0..] (fmap (fromIntegral . delay) sprites)
  }

-- Animate.customWriteSpriteSheetInfo will do the trick


