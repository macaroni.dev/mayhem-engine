module Mayhem.Engine.Metrics where

import Data.Word
import Data.IORef
import Data.IntMap (IntMap)
import Data.IntMap qualified
import Data.Map.Strict (Map)
import Data.Map.Strict qualified as Data.Map
import Debug.Trace

import Cleff
import Mayhem.Engine.Effects.Log
import Mayhem.Engine.Utils
-- TODO: Make this more sophisticated down the line?

data Metrics = Metrics
  { numFrames :: Word64
  , totalFrameTime :: Word64
  , frameTimeDist :: IntMap Word64
  , droppedFrames :: Word64
  , frameMetrics :: Map String Double
  , droppedFrameMetrics :: [(Word32, Map String Double)]
  } deriving (Eq, Show)

instance Semigroup Metrics where
  m1 <> m2 = Metrics
    { numFrames = numFrames m1 + numFrames m2
    , totalFrameTime = totalFrameTime m1 + totalFrameTime m2
    , frameTimeDist = Data.IntMap.unionWith (+) (frameTimeDist m1) (frameTimeDist m2)
    , droppedFrames = droppedFrames m1 + droppedFrames m2
    , frameMetrics = frameMetrics m2
    , droppedFrameMetrics = droppedFrameMetrics m1 <> droppedFrameMetrics m2
    }

instance Monoid Metrics where
  mempty = Metrics 0 0 mempty 0 mempty []

newtype MetricStore = MetricStore (IORef Metrics)

newMetricStore :: IO MetricStore
newMetricStore = MetricStore <$> newIORef mempty

-- TODO: Allow multiple metrics to emit per frame & agg
emitFrameMetric :: MetricStore -> String -> Double -> IO ()
emitFrameMetric (MetricStore ref) s x = modifyIORef ref $ \Metrics{..} ->
  Metrics{ frameMetrics = Data.Map.insert s x frameMetrics, ..}

recordFrameTime :: MetricStore -> Word32 -> IO ()
recordFrameTime (MetricStore ref) t = do
  Metrics{frameMetrics} <- readIORef ref
  let m = Metrics
        { numFrames = 1
        , totalFrameTime = fromIntegral t
        , frameTimeDist = Data.IntMap.singleton (fromIntegral t) 1
        , droppedFrames = if t > 16 then 1 else 0
        , frameMetrics = mempty
        , droppedFrameMetrics = if t > 16 then [(t, frameMetrics)] else []
        }
  modifyIORef' ref (<> m)

logMetricStore :: '[IOE, Log] :>> es => MetricStore -> Eff es ()
logMetricStore (MetricStore ref) = do
  metrics <- liftIO $ readIORef ref
  logText (tshow metrics)
