{-# LANGUAGE OverloadedStrings #-}
module Mayhem.Engine.Menu where

import Control.Monad
import Data.Foldable
import Data.Monoid.Generic
import Data.Text (Text)
import Data.Text qualified
import Data.Text.Lazy.Builder qualified as TB
import Foreign.Ptr
import Mayhem.Engine.Effects
import Mayhem.Engine.Resolution
import Mayhem.Engine.Utils
import Mayhem.Engine.Input
import Mayhem.Engine.Loop
import SDL.GPU.FC.Simple qualified as FC
import SDL.GPU.Utils qualified as GPU
import SDL qualified
import SDL.Raw qualified
import GHC.Generics

-- A Source-engine-style console
data Console = Console
  { history :: [Text]
  , currentInput :: Text
  }
  deriving stock (Generic, Show)
  deriving Semigroup via GenericSemigroup Console
  deriving Monoid via GenericMonoid Console

instance Component Console where type Storage Console = Global Console

data Console'Config = Console'Config
  { font :: Ptr FC.Font
  }

initConsole
  :: '[SDL_GPU, IOE] :>> es
  => The ApecsE es w
  => Modify w es Console
  => Eff es ()
initConsole = do
  NativeResolution{..} <- askNativeRez
  liftIO $ SDL.startTextInput SDL.Raw.Rect
    { rectX = 0
    , rectY = 0
    , rectH = fromIntegral nativeHeight
    , rectW = fromIntegral nativeWidth
    }

tickConsole
  :: The ApecsE es w
  => Modify w es Console
  => '[IOE, Input [SDL.Event], Log] :>> es
  => (Text -> Eff es Text) -- ^ Input handler
  -> (Eff es ()) -- ^ on quit
  -> Eff es ()
tickConsole eval onQuit = do
  events <- inputs (fmap SDL.eventPayload)
  for_ @[] events $ \case
    SDL.TextInputEvent SDL.TextInputEventData{..} ->
      gmodify $ \Console{..} -> Console
      { currentInput = currentInput <> textInputEventText
      , ..
      }
    KeypressBackspace ->
      gmodify $ \Console{..} -> Console
      { currentInput = maybe Data.Text.empty fst $ Data.Text.unsnoc currentInput
      , ..
      }
    KeypressReturn -> do
      Console{..} <- gget
      when (not $ Data.Text.null currentInput) $ do
        if | currentInput == ":q" -> do
               gset Console { currentInput = mempty, .. }
               liftIO SDL.stopTextInput
               onQuit
           | otherwise -> do
               res <- eval currentInput
               gset Console
                 { currentInput = mempty
                 , history = Data.Text.cons '=' res : Data.Text.cons '>' currentInput : history
                 }
    _ -> pure ()

-- TODO: Optional flipped orientation
drawConsole
  :: [IOE, SDL_GPU] :>> es
  => The ApecsE es w
  => Get w es Console
  => Console'Config
  -> Eff es ()
drawConsole Console'Config{..} = do
  Console{..} <- gget
  screen <- askGPU
  let txt =
        unlines $
        ('>' : Data.Text.unpack currentInput) : fmap Data.Text.unpack history

  liftIO $ void $ FC.drawAlign font screen 0 0 FC.alignLeft txt
