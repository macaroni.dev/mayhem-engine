{-# OPTIONS_GHC -fno-warn-orphans #-}
module Mayhem.Engine.LambdaCaseShowEnum where

lambdaCaseShowEnum :: forall a r. Show r => Show a => Enum a => Bounded a => (a -> r) -> String
lambdaCaseShowEnum f =
  ("\\case\n" ++) $ flip foldMap [minBound @a..maxBound] $ \a ->
    mconcat ["  ", show a, " -> ", show (f a), "\n"]

instance (Show r, Show a, Enum a, Bounded a) => Show (a -> r) where
  show = lambdaCaseShowEnum
