module Mayhem.Engine.Frame where

import Data.Word
import Data.Aeson (FromJSON)

newtype Frame = Frame Word64
  deriving stock (Show)
  deriving newtype (Eq, Ord, Enum, Bounded, Num, Integral, Real, FromJSON)

cntdown :: Frame -> Frame
cntdown f = if f == 0 then 0 else pred f

frame2numstr :: Frame -> String
frame2numstr (Frame w) = show w

frameProgress :: Frame -> Frame -> Double
frameProgress x y = fromIntegral x / fromIntegral y

newtype FrameDown = FrameDown Frame
  deriving stock (Show)
  deriving newtype (Eq, Ord, Enum, Bounded, Num, Integral, Real, FromJSON)

cntDown :: FrameDown -> Maybe FrameDown
cntDown f = if f == 0 then Nothing else Just (pred f)

newtype FrameUp = FrameUp Frame
  deriving stock (Show)
  deriving newtype (Eq, Ord, Enum, Bounded, Num, Integral, Real, FromJSON)

cntUpTo :: Frame -> FrameUp -> Maybe FrameUp
cntUpTo to fu@(FrameUp f) = if f == to then Nothing else Just (succ fu)
