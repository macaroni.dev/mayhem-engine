module Mayhem.Engine.Loop
  ( module Mayhem.Engine.Loop
  , module X
  , module Cleff
  , module Foreign.Ptr
  , module Foreign.C.String
  , module Foreign.Storable
  , module Foreign.Marshal.Alloc
  , module Control.Monad.IO.Unlift
  ) where

import Control.Monad
import Control.Applicative
import Control.Monad.IO.Unlift
import qualified Data.ByteString.Char8
import qualified Data.Text
import Data.Word
import Data.Bits
import Data.Foldable
import Data.Function
import Data.Functor
import Data.Monoid
import Data.Maybe

import Foreign.Ptr
import Foreign.C.String
import Foreign.Storable
import Foreign.Marshal.Alloc
import Control.Monad.Managed
import System.Mem as X
import qualified Jsonifier as Json
import Data.IORef
import Debug.Trace
import System.TimeIt
import Control.Concurrent.MVar

import qualified SDL
import qualified SDL.Raw
import qualified Cute.Sound.C as CS
import qualified SDL.GPU.Utils as GPU
import qualified SDL.GPU.Simple as Target (TargetF(..))
import Memorable
import Linear
import Linear.Affine
import Optics

import Mayhem.Engine.Assets
import Mayhem.Engine.Input
import Mayhem.Engine.Utils
import qualified Mayhem.Engine.Clock as Clock
import Mayhem.Engine.Metrics
import Mayhem.Engine.Resolution

import Mayhem.Engine.Effects as X
import Mayhem.Engine.Effects.Expansion as X
import Mayhem.Engine.Effects.Apecs as X
import Mayhem.Engine.Effects.Clock as X
import Mayhem.Engine.Effects.Assets as X
import Mayhem.Engine.Effects.Params as X
import Mayhem.Engine.Effects.Metrics as X
import Mayhem.Engine.Effects.Debug as X (Debug)
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Engine.Effects.Log as X
import Mayhem.Engine.Effects.SDL_GPU as X
import Mayhem.Engine.Effects.Cute_Sound as X
import Mayhem.Engine.Effects.Stepper

import Cleff
import Cleff.Reader
import Cleff.Input as X

data Game es =
  Loop
  { loopTick   :: Eff es ()
  , loopDraw   :: Eff es ()
  }

adaptToWindowResize :: [IOE, SDL_GPU, Input [SDL.Event]] :>> es => Eff es ()
adaptToWindowResize = do
  events <- input
  screen <- askGPU
  NativeResolution {..} <- askNativeRez
  liftIO $ onWindowResize events $ \(V2 newW newH) -> do
    _ <- GPU.setWindowResolution newW newH
    GPU.setVirtualResolution screen nativeWidth nativeHeight
    GPU.letterbox nativeWidth nativeHeight screen

f11Fullscreen :: [IOE, SDL_GPU, Input [SDL.Event]] :>> es => Eff es ()
f11Fullscreen = do
  events <- input
  screen <- askGPU
  NativeResolution {..} <- askNativeRez
  liftIO $ for_ (keypresses events) $ \case
    SDL.KeycodeF11 -> do
      _ <- GPU.toggleFullscreen
      GPU.letterbox nativeWidth nativeHeight screen
      pure ()
    _ -> pure ()

f7ToggleDebug :: [Debug.Debug, Log, Input [SDL.Event]] :>> es => Eff es ()
f7ToggleDebug = do
  events <- input
  for_ (keypresses events) $ \case
    SDL.KeycodeF7 -> do
      logString "Toggling debug mode due to F7 press..."
      Debug.toggle
    _ -> pure ()

-- F2 to step 1 frame
-- Shift+F2 to stop/start at current frame
f2Stepper :: [Log, Stepper, Input [SDL.Event]] :>> es => Eff es ()
f2Stepper = do
  events <- input
  for_ (keysymPresses events) $ \case
    SDL.Keysym{keysymKeycode=SDL.KeycodeF2, ..} -> do
      if SDL.keyModifierLeftShift keysymModifier then stepToggle else stepN 1
    _ -> pure ()


data HotPipe es = HotPipe
  { hotToEngine :: MVar (Eff es ())
  }

newHotPipe :: forall es. IO (HotPipe es)
newHotPipe = HotPipe <$> newEmptyMVar

hotReload :: HotPipe es -> Eff es () -> IO ()
hotReload HotPipe{..} = putMVar hotToEngine

-- TODO: [SDL.Event] isn't quite good enough
-- (for instance, for handling pauses correctly)
--
-- TODO: MayhemOpts effect that is like RTS opts
-- Use it here to configure stuff like spinlock..we may need
-- structural types..or maybe it's not worth it lol. I don't know
-- how you'd even do that in a composable nice way.
type Framed es = [Input [SDL.Event], MetricsE] ++ es
framed :: [IOE, Log, SDL_GPU] :>> es => Eff (Framed es) () -> Eff es ()
framed = framedHot Nothing

framedHot :: forall es. [IOE, Log, SDL_GPU] :>> es => Maybe (HotPipe (Framed es)) -> Eff (Framed es) () -> Eff es ()
framedHot mHotPipe eff'init = do
  metrics <- liftIO newMetricStore

  loop metrics eff'init
  logMetricStore metrics
  where
    ticksPerFrame :: Word32 = ceiling (1000 / 60 :: Double)
    loop :: MetricStore -> Eff (Framed es) () -> Eff es ()
    loop metrics eff = do
      -- TODO: Configurable fps
      startTicks <- liftIO SDL.ticks
      (events_t, events) <- timeItT $ liftIO SDL.pollEvents
      debug_midTicks <- liftIO SDL.ticks
      eff' <- liftIO $ case mHotPipe of
        Nothing -> pure eff
        Just HotPipe{..} -> fmap (fromMaybe eff) $ tryTakeMVar hotToEngine
      (loop_t, _) <- timeItT $ runMetricsE (emitFrameMetric metrics) $ runInputConst events eff'
      endTicks <- liftIO SDL.ticks
      let elapsedTicks = (endTicks - startTicks)
      liftIO $ emitFrameMetric metrics "loop" loop_t
      liftIO $ emitFrameMetric metrics "pollEvents" events_t
      liftIO $ emitFrameMetric metrics "DEBUGGING_midTicks" (fromIntegral (debug_midTicks - startTicks) / 1000)
      liftIO $ emitFrameMetric metrics "elapsed_ticks" (fromIntegral elapsedTicks / 1000)
      liftIO $ recordFrameTime metrics elapsedTicks
      let remainingTicks = if (ticksPerFrame > elapsedTicks) then ticksPerFrame - elapsedTicks else 0

      -- TODO: Spin Lock / use getPerformanceCounter for more
      -- accurate & consistent fps limiting
      -- TODO: Validate that this FPS limiting is working
      when (remainingTicks > 0) $ liftIO $ SDL.delay remainingTicks

      unless (SDL.QuitEvent `elem` fmap SDL.eventPayload events || SDL.KeycodeEscape `elem` keypresses events) (loop metrics eff')

infixr 2 .||.
(.||.) :: Applicative f => f Bool -> f Bool -> f Bool
(.||.) = liftA2 (||)

infixr 3 .&&.
(.&&.) :: Applicative f => f Bool -> f Bool -> f Bool
(.&&.) = liftA2 (&&)

debug_ :: Debug.Debug :> es => Debug.Entry -> Eff es ()
debug_ = Debug.debug (Debug.Level ())
