-- To workaround TH x-compilation issues, we sometimes
-- need some modules in scope that we don't need otherwise
module Mayhem.Engine.TH (module X) where

import Optics.Iso as X
import Optics.Label as X
import Optics.Internal.Optic.Types as X
import Optics.Internal.Magic as X
import Optics.Lens as X
import Optics.Prism as X
import Optics.AffineTraversal as X
import Optics.TH as X
