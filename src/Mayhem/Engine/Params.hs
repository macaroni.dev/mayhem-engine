module Mayhem.Engine.Params where

import Data.IORef
import Control.Monad.IO.Class

import Optics
import Apecs

class MonadIO m => MonadParams params m | m -> params where
  askParams :: m (Params params)

instance MonadParams params m => MonadParams params (SystemT w m) where
  askParams = lift askParams

viewParams
  :: Is k A_Getter
  => MonadParams params m
  => Optic' k is params b
  -> m b
viewParams x = askParams >>= \Params{currParams} -> liftIO $ do
  curr <- readIORef currParams
  pure (curr ^. x)

getParams :: MonadParams params m => m params
getParams = askParams >>= liftIO . readParams

data Params params = Params
  { currParams :: IORef params
  , loadParams :: IO params
  }

readParams :: Params params -> IO params
readParams Params{..} = readIORef currParams

initParams :: IO params -> IO (Params params)
initParams load = do
  !p <- load
  ref <- newIORef p
  pure Params
    { currParams = ref
    , loadParams = load
    }
