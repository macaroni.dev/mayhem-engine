module Mayhem.Engine.Movement where

import Data.Foldable
import Data.Functor.Identity

import Linear hiding (trace)

import Mayhem.Engine.Utils (isElem, traceTagM)
import Debug.Trace

-- Shared types
newtype Position = Position (V2 Float)
  deriving newtype (Show, Eq)
newtype Velocity = Velocity (V2 Float)
  deriving newtype (Show, Eq)

-- TODO: Dashing/running/walking/crouching
-- These affect Velocity & ECB
-- All "states" affect input handling, but maybe that's a separate module

-- TODO: Jumping/airborne/landing
-- I think these are also a separate module. This module comes into play
-- _after_ the effects of inputs/state are processed. This module is blind
-- to the entity's other state - it just moves it according to the Surface
-- rules
--
-- Even for something like WDing, the caller just receives a general Movement
-- result, and the caller uses the fact that they were in an airdodge state
-- to transition to WD (which in turn results next frame in changes to
-- velocity/position

-- Platforming
moveTo
  :: [Surface]
  -> ECB
  -> Position -- curr
  -> Velocity
  -> Movement
moveTo ss ecb@ECB{} (Position p) (Velocity v) = runIdentity $ do
  let p' =
        if | Just _ <- isOnSurface ecb ss ->
             -- Move (norm v) along the surface..
             -- idk how to determine direction..
               p + v -- TODO: Actually use the surface
           | otherwise -> p + v

  -- With p', we can now calculate our linecasts ..
  let ecbc@ECBCasts{} = mkCasts ecb p p'
  traceTagM "ecbc" ecbc

  -- .. & compare them to relevant surfaces

  --
  -- If they connect, we MoveOnto the interesection point
  -- If they don't connect, we just MoveTo p'
  pure MoveNowhere

-- TODO: This might not be the right way to go about it..especially when
-- sliding along surfaces (due to imprecision)
--
-- If we handle sliding along surfaces by making it explicitly always move
-- to a known point calculated on the surface, it'll work
isOnSurface
  :: ECB
  -> [Surface]
  -> Maybe Surface
isOnSurface ECB{bottom} =
    find (intersects bottom . line)
  . filter (isElem [PLATFORM, FLOOR] . type_)


mkCasts
  :: ECB
  -> V2 Float -- start
  -> V2 Float -- finish
  -> ECBCasts
mkCasts ECB{..} p@(V2 _ _) p'@(V2 _ _) =
  trace ("p p' = " ++ show (p, p')) $ ECBCasts
  { bottomCast = mkSegment bottom
  , topCast = mkSegment top
  , leftCast = mkSegment left
  , rightCast = mkSegment right
  }
  where
    mkSegment _ = error "TODO"

data Movement =
    MoveNowhere
  | MoveBy Velocity
  | MoveTo Position
  | MoveOnto Position Surface
  deriving stock (Show, Eq)

-- TODO: Own module
data LS2 a = LS2
  { p :: V2 a
  , d :: V2 a
  , mag :: a
  }
  deriving (Show, Eq)
-- more utils..
intersects :: V2 a -> LS2 a -> Bool
intersects = undefined

-- These are relative to position
data ECB = ECB
  { bottom :: V2 Float
  , top    :: V2 Float
  , left   :: V2 Float
  , right  :: V2 Float
  } deriving stock (Show, Eq)

data ECBCasts = ECBCasts
  { bottomCast :: LS2 Float
  , topCast    :: LS2 Float
  , leftCast   :: LS2 Float
  , rightCast  :: LS2 Float
  } deriving stock (Show, Eq)

data Surface = Surface
  { line  :: LS2 Float
  , type_ :: SurfaceType
  } deriving stock (Show, Eq)

data SurfaceType =
    PLATFORM
  | FLOOR
  | CEILING
  | WALL_LEFT
  | WALL_RIGHT
  deriving stock (Show, Eq)
