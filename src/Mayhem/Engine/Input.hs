{-# LANGUAGE PatternSynonyms #-}

module Mayhem.Engine.Input where

import Control.Monad (guard)
import Data.Maybe
import Data.Monoid (Last (..))
import Optics

import Mayhem.Engine.Utils
import qualified SDL.Utils as SDL

-- TODO: Fold would provide composable and performant event processing
-- These combinators require multiple traversals of events per frame
-- Probably doesn't matter..but it's low-hanging perf fruit and is probably
-- as nice or better of a programming API anyways
keypresses :: [SDL.Event] -> [SDL.Keycode]
keypresses = mapMaybe $ \e -> do
  SDL.KeyboardEventData{..} <- e ^? SDL._EventPayload % SDL._KeyboardEvent
  guard (keyboardEventKeyMotion == SDL.Pressed)
  guard (not keyboardEventRepeat)
  pure $ keyboardEventKeysym ^. SDL._KeysymKeycode

keysymPresses :: [SDL.Event] -> [SDL.Keysym]
keysymPresses = mapMaybe $ \e -> do
  SDL.KeyboardEventData{..} <- e ^? SDL._EventPayload % SDL._KeyboardEvent
  guard (keyboardEventKeyMotion == SDL.Pressed)
  guard (not keyboardEventRepeat)
  pure $ keyboardEventKeysym

isKeycodePressed :: SDL.Keycode -> SDL.EventPayload -> Bool
isKeycodePressed kc e = maybe False (const True) $ do
  SDL.KeyboardEventData{..} <- e ^? SDL._KeyboardEvent
  guard (keyboardEventKeyMotion == SDL.Pressed)
  guard (keyboardEventKeysym ^. SDL._KeysymKeycode == kc)

pattern KeypressBackspace :: SDL.EventPayload
pattern KeypressBackspace <- (isKeycodePressed SDL.KeycodeBackspace -> True)

pattern KeypressReturn :: SDL.EventPayload
pattern KeypressReturn <- (isKeycodePressed SDL.KeycodeReturn -> True)

keyreleases :: [SDL.Event] -> [SDL.Keycode]
keyreleases = mapMaybe $ \e -> do
  SDL.KeyboardEventData{..} <- e ^? SDL._EventPayload % SDL._KeyboardEvent
  guard (keyboardEventKeyMotion == SDL.Released)
  guard (not keyboardEventRepeat)
  pure $ keyboardEventKeysym ^. SDL._KeysymKeycode

scanmotions :: [SDL.Event] -> [(SDL.Scancode, SDL.InputMotion)]
scanmotions = mapMaybe $ \e -> do
  SDL.KeyboardEventData{..} <- e ^? SDL._EventPayload % SDL._KeyboardEvent
  guard (not keyboardEventRepeat)
  pure $ (keyboardEventKeysym ^. SDL._KeysymScancode, keyboardEventKeyMotion)

scanpresses :: [SDL.Event] -> [SDL.Scancode]
scanpresses = fmap fst . filter ((== SDL.Pressed) . snd) . scanmotions

scanreleases :: [SDL.Event] -> [SDL.Scancode]
scanreleases = fmap fst . filter ((== SDL.Released) . snd) . scanmotions

scancodeEdge :: SDL.Scancode -> [SDL.Event] -> Maybe SDL.InputMotion
scancodeEdge sc = getLast . fmap snd . foldMap (Last . guarded ((== sc) . fst)) . scanmotions

mouseButtonEdge :: SDL.MouseButton -> [SDL.Event] -> Maybe SDL.InputMotion
mouseButtonEdge button =
    getLast
  . fmap snd
  . foldMap (Last . guarded ((== button) . fst))
  . mouseButtonMotions

mouseButtonMotions :: [SDL.Event] -> [(SDL.MouseButton, SDL.InputMotion)]
mouseButtonMotions = mapMaybe $ \e -> do
    SDL.MouseButtonEventData{..} <- e ^? SDL._EventPayload % SDL._MouseButtonEvent
    pure (mouseButtonEventButton, mouseButtonEventMotion)

wasMousePressed :: SDL.MouseButton -> [SDL.Event] -> Bool
wasMousePressed button = not . null . filter (== (button, SDL.Pressed)) . mouseButtonMotions

firstEdge :: [Maybe SDL.InputMotion] -> Maybe SDL.InputMotion
firstEdge = getLast . foldMap Last
