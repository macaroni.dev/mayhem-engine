{-# LANGUAGE PatternGuards #-}

-- | Cribbed from Graphics.Gloss.Geometry.Line:
--
-- Geometric functions concerning lines and segments.
--
--   A @Line@ is taken to be infinite in length, while a @Seg@ is finite length line segment represented by its two endpoints.
module Mayhem.Engine.Geometry
  ( Line (..)
  , Seg  (..)
  , segV2
  , segnorm
  , segredirect
  , redirect
  , followSegs
  , normsub
  , leftmostSeg
  , rightmostSeg
  , LeftmostSeg (..)
  , RightmostSeg (..)
  , Box  (..)
  , segToLine
  -- * Box checks
  , pointInBox
  , segClearsBox
  , boxW
  , boxH

  -- * Closest points
  , closestPointOnLine
  , closestPointOnSeg
  , pointOnSeg
  , closestPointOnLineParam

  -- * Line-Line intersection
  , intersectLineLine

  -- * Seg-Line intersection
  , intersectSegLine
  , intersectSegHorzLine
  , intersectSegVertLine

  -- * Seg-Seg intersection
  , intersectSegSeg
  , intersectSegSegParam
  , applyIntersectSegSegParam
  -- * Angle utilies
  , degToRad
  , radToDeg
  , normalizeAngle
  ) where

-- NOTE: Parallel lines that overlap are not considered to be intersecting!

import Control.Applicative
import Control.Monad (guard)
import Data.Maybe (fromMaybe)

import Data.IntMap (IntMap)
import Data.IntMap qualified as IntMap

import Linear.V2
import Linear.Vector
import Linear.Metric
import Linear.Epsilon

import Mayhem.Engine.Utils


data Line a = Line (V2 a) (V2 a)
  deriving (Eq, Ord, Show)

data Seg a = Seg { seg'from :: (V2 a), seg'to :: (V2 a) }
  deriving (Eq, Ord, Show)

segV2 :: Num a => Seg a -> V2 a
segV2 (Seg x y) = y - x

segnorm :: Epsilon a => Floating a => Seg a -> V2 a
segnorm = normalize . segV2

segToLine :: Seg a -> Line a
segToLine (Seg x y) = Line x y

leftmostSeg :: Ord a => Seg a -> Seg a -> Seg a
leftmostSeg a@(Seg (V2 ax1 _) (V2 ax2 _)) b@(Seg (V2 bx1 _) (V2 bx2 _)) =
  if min ax1 ax2 < min bx1 bx2 then a else b

rightmostSeg :: Ord a => Seg a -> Seg a -> Seg a
rightmostSeg a@(Seg (V2 ax1 _) (V2 ax2 _)) b@(Seg (V2 bx1 _) (V2 bx2 _)) =
  if max ax1 ax2 > max bx1 bx2 then a else b

-- | @redirect u v@ creates a new vector with @v@'s magnitude in @u@'s direction
--
-- TODO: Unit/property test this
redirect :: Ord a => Epsilon a => Floating a => V2 a -> V2 a -> V2 a
redirect u v =
  if | nearZero u -> 0
     | nearZero v -> 0
     | nearZero (u `dot` v) -> normalize u ^* norm v
     | u `dot` v < 0 -> normalize u ^* negate (norm v)
     | otherwise -> normalize u ^* norm v

normsub
  :: Floating a
  => Epsilon a
  => V2 a -- x
  -> V2 a -- y
  -> V2 a -- z s.t. norm z = norm (x - y)
normsub x y =
  let norm'rem = norm x - norm y
  in  normalize x ^* norm'rem

-- TODO: Unit test this!
--
-- TODO: Need the "remainder"
segredirectOLD
  :: Ord a
  => Epsilon a
  => Floating a
  => V2 a -- loc - must be on the seg to return Just
  -> Seg a -- seg to project onto
  -> V2 a -- v to project
  -> Seg a
segredirectOLD loc s v =
  let rv = redirect (segnorm s) v
      unimpeded = Seg loc (loc + rv)
  in if | pointOnSeg s (loc + rv) -> unimpeded -- TODO..use closestPointOnSeg here?
        | pointOnSeg (Seg loc (loc + rv)) s.seg'from -> Seg loc s.seg'from
        | otherwise -> Seg loc s.seg'to

segredirect
  :: Show a => Ord a
  => Epsilon a
  => Floating a
  => V2 a -- loc - must be on the seg to return Just
  -> Seg a -- seg to project onto
  -> V2 a -- v to project
  -> Maybe (Seg a)
segredirect loc s v = do
  loc'from <- closestPointOnSeg s loc
  guard (nearZero $ loc - loc'from)

  let rv = redirect (segnorm s) v
  let loc'rv = loc'from + rv
  let loc'unimpeded = do
        -- In this case, loc'rv is still on the line
        loc'rv'real <- closestPointOnSeg s loc'rv
        guard (nearZero $ loc'rv'real - loc'rv)
        pure loc'rv'real
  let seg'rv = Seg loc'from loc'rv

  let on'seg'from = pointOnSeg seg'rv s.seg'from
  let on'seg'to = pointOnSeg seg'rv s.seg'to

  let loc'clip'from = do
        guard (not $ nearZero $ loc'from - s.seg'from) -- If our output from is an endpoint, ignore the endpoint
        guard (pointOnSeg seg'rv s.seg'from)
        Just s.seg'from

  let loc'clip'to = do
        guard (not $ nearZero $ loc'from - s.seg'to) -- If our output from is an endpoint, ignore the endpoint
        guard (pointOnSeg seg'rv s.seg'to)
        Just s.seg'to

  loc'to <- loc'unimpeded <|> loc'clip'from <|> loc'clip'to
  Just $ Seg loc'from loc'to


newtype RightmostSeg a = RightmostSeg { unRightmostSeg :: Seg a }
instance Ord a => Semigroup (RightmostSeg a) where
  RightmostSeg x <> RightmostSeg y = RightmostSeg $ rightmostSeg x y

newtype LeftmostSeg a = LeftmostSeg { unLeftmostSeg :: Seg a }
instance Ord a => Semigroup (LeftmostSeg a) where
  LeftmostSeg x <> LeftmostSeg y = LeftmostSeg $ leftmostSeg x y

-- TODO: Unit test
followSegs
  :: forall a
   . Show a
  => Num a
  => Ord a
  => Epsilon a
  => Floating a
  => V2 a -- v
  -> V2 a -- loc
  -> IntMap (Seg a) -- segs - follows until no longer connected and finishes w/remainder
  -> [V2 a] -- path of locs
followSegs v'init loc'init all'segs = go v'init loc'init all'segs (IntMap.toList all'segs)
  where
    go
      :: V2 a -- v
      -> V2 a -- loc
      -> IntMap (Seg a)
      -> [(IntMap.Key, Seg a)]
      -> [V2 a]
    go v loc segs = \case
      _ | nearZero v -> []
      [] -> [loc + v]
      (i, s) : rest | Just sv <- segredirect loc s v  ->
        let v0 = segV2 sv
            v' = (redirect (segnorm s) v) - v0
            loc' = sv.seg'to
            segs' = IntMap.delete i segs
            loc'n = go v' loc' segs' (IntMap.toList segs')
        in loc' : loc'n
      (i, s) : rest -> go v loc segs rest

data Box a =
  Box
  (V2 a) -- Lower left point of box
  (V2 a) -- Upper right point of box
  deriving (Eq, Ord, Show)

boxH :: Num a => Box a -> a
boxH (Box (V2 _ y1) (V2 _ y2)) = abs (y2 - y1)

boxW :: Num a => Box a -> a
boxW (Box (V2 x1 _) (V2 x2 _)) = abs (x2 - x1)

pointInBox
  :: Ord a
  => Box a
  -> V2 a
  -> Bool
pointInBox (Box (V2 xL yL) (V2 xU yU)) (V2 x y) =
  x >= xL && x <= xU && y >= yL && y <= yU

-- | Check if line segment (P1-P2) clears a box (P3-P4) by being well outside it.
segClearsBox
  :: Ord a
  => Seg a
  -> Box a
  -> Bool
segClearsBox (Seg (V2 x1 y1) (V2 x2 y2)) (Box (V2 xa ya) (V2 xb yb))
 | x1 < xa, x2 < xa = True
 | x1 > xb, x2 > xb = True
 | y1 < ya, y2 < ya = True
 | y1 > yb, y2 > yb = True
 | otherwise  = False

-- | Given an infinite line which intersects `P1` and `P1`,
-- return the point on that line that is closest to `P3`
closestPointOnLine
  :: Fractional a
  => Line a
  -> V2 a
  -> V2 a

{-# INLINE closestPointOnLine #-}

closestPointOnLine l@(Line p1 p2) p3
  = p1 + (u *^ (p2 - p1))
 where u = closestPointOnLineParam l p3

-- | Given an line segment which intersects `P1` and `P2`,
-- return the point on that infinite line through `P1` and `P2`
-- that is closest to `P3` IFF that point is on the segment
closestPointOnSeg
  :: Fractional a
  => Ord a
  => Seg a
  -> V2 a
  -> Maybe (V2 a)

{-# INLINE closestPointOnSeg #-}

closestPointOnSeg s@(Seg p1 p2) p3 = do
  guard (u >= 0 && u <= 1)
  pure $ p1 + (u *^ (p2 - p1))
  where u = closestPointOnLineParam (segToLine s) p3

-- | Test if a given point is on a given line segment
pointOnSeg
  :: Fractional a
  => Ord a
  => Epsilon a
  => Seg a
  -> V2 a
  -> Bool
pointOnSeg s p = case closestPointOnSeg s p of
  Nothing -> False
  Just x -> nearZero $ p - x

-- | Given an infinite line which intersects P1 and P2,
-- let P4 be the point on the line that is closest to P3.
--
-- Return an indication of where on the line P4 is relative to P1 and P2.
--
-- @
--      if P4 == P1 then 0
--      if P4 == P2 then 1
--      if P4 is halfway between P1 and P2 then 0.5
-- @
--
-- @
--        |
--       P1
--        |
--     P4 +---- P3
--        |
--       P2
--        |
-- @
--
{-# INLINE closestPointOnLineParam #-}
closestPointOnLineParam
  :: Fractional a
  => Line a
  -> V2 a
  -> a

closestPointOnLineParam (Line p1 p2) p3
  = (p3 - p1) `dot` (p2 - p1)
  / (p2 - p1) `dot` (p2 - p1)



-- Line-Line intersection -------------------------------------------------------------------------

-- | Given four points specifying two lines, get the point where the two lines cross, if any.
--   Note that the lines extend off to infinity, so the intersection point might not lie
--   between either of the two pairs of points.
--
-- @
--     \\      /
--      P1  P4
--       \\ /
--        +
--       / \\
--      P3  P2
--     /     \\
-- @
--
intersectLineLine
  :: Fractional a
  => Eq a
  => Line a
  -> Line a
  -> Maybe (V2 a)
intersectLineLine (Line (V2 x1 y1) (V2 x2 y2)) (Line (V2 x3 y3) (V2 x4 y4))
 = let dx12 = x1 - x2
       dx34 = x3 - x4

       dy12 = y1 - y2
       dy34 = y3 - y4

       den = dx12 * dy34  - dy12 * dx34

   in if den == 0
      then Nothing
      else let
               det12 = x1*y2 - y1*x2
               det34 = x3*y4 - y3*x4

               numx = det12 * dx34 - dx12 * det34
               numy = det12 * dy34 - dy12 * det34
           in Just $ V2 (numx / den) (numy / den)


-- Segment-Line intersection ----------------------------------------------------------------------

-- | Get the point where a segment @P1-P2@ crosses an infinite line @P3-P4@, if any.
--
intersectSegLine
  :: Fractional a
  => Ord a
  => Seg a
  -> Line a
  -> Maybe (V2 a)
intersectSegLine (Seg p1 p2) l
 -- TODO: merge closest point check with intersection, reuse subterms.
 | Just p0 <- intersectLineLine (Line p1 p2) l
 , t12  <- closestPointOnLineParam (Line p1 p2) p0
 , t12 >= 0 && t12 <= 1
 = Just p0

 | otherwise
 = Nothing

-- | Get the point where a segment crosses a horizontal line, if any.
--
-- @
--                + P1
--               /
--       -------+---------
--             /        y0
--         P2 +
-- @
--
intersectSegHorzLine
  :: Fractional a
  => Ord a
  => Seg a
  -> a
  -> Maybe (V2 a)
intersectSegHorzLine (Seg (V2 x1 y1) (V2 x2 y2)) y0
 -- seg is on line
 | y1 == y0, y2 == y0 = Nothing

 -- seg is above line
 | y1 > y0,  y2 > y0 = Nothing

 -- seg is below line
 | y1 < y0,  y2 < y0 = Nothing

 -- seg is a single point on the line.
 -- this should be caught by the first case,
 -- but we'll test for it anyway.
 | y2 - y1 == 0
 = Just (V2 x1 y1)

 | otherwise
 = Just ( V2 ((y0 - y1) * (x2 - x1) / (y2 - y1) + x1) y0 )



-- | Get the point where a segment crosses a vertical line, if any.
--
-- @
--              |
--              |   + P1
--              | /
--              +
--            / |
--       P2 +   |
--              | x0
-- @
--
intersectSegVertLine
  :: Fractional a
  => Ord a
  => Seg a
  -> a
  -> Maybe (V2 a)

intersectSegVertLine (Seg (V2 x1 y1) (V2 x2 y2)) x0

 -- seg is on line
 | x1 == x0, x2 == x0 = Nothing

 -- seg is to right of line
 | x1 > x0,  x2 > x0 = Nothing

 -- seg is to left of line
 | x1 < x0,  x2 < x0 = Nothing

 -- seg is a single point on the line.
 -- this should be caught by the first case,
 -- but we'll test for it anyway.
 | x2 - x1 == 0
 = Just (V2 x1 y1)

 | otherwise
 = Just ( V2 x0 ((x0 - x1) * (y2 - y1) / (x2 - x1) + y1))

-- Segment-Segment intersection -------------------------------------------------------------------

-- | Get the point where a segment @P1-P2@ crosses another segement @P3-P4@, if any.
intersectSegSeg
  :: Fractional a
  => Ord a
  => Epsilon a
  => Seg a
  -> Seg a
  -> Maybe (V2 a)
intersectSegSeg s1@(Seg p1 p2) s2@(Seg p3 p4)
 | [x] <- filter (pointOnSeg s1) [p3, p4] = Just x
 | [_, _] <- filter (pointOnSeg s1) [p3, p4] = Nothing -- parallel
 | [x] <- filter (pointOnSeg s2) [p1, p2] = Just x
 | [_, _] <- filter (pointOnSeg s2) [p1, p2] = Nothing -- parallel
 -- TODO: merge closest point checks with intersection, reuse subterms.
 | Just p0 <- intersectLineLine (Line p1 p2) (Line p3 p4)
 , t12  <- closestPointOnLineParam (Line p1 p2) p0
 , t23  <- closestPointOnLineParam (Line p3 p4) p0
 , t12 >= 0 && t12 <= 1
 , t23 >= 0 && t23 <= 1
 = Just p0

 | otherwise
 = Nothing

-- | From "Real-Time Collision Detection" - Section 5.1.9.1: 2D Segment Intersection
intersectSegSegParam
  :: Fractional a
  => Ord a
  => Seg a
  -> Seg a
  -> Maybe a
intersectSegSegParam (Seg a b) (Seg c d) = do
  let signedTriArea (V2 ax ay) (V2 bx by) (V2 cx cy) =
        (ax - cx) * (by - cy) - (ay - cy) * (bx - cx)

  let a1 = signedTriArea a b d
  let a2 = signedTriArea a b c
  guard (a1 * a2 < 0.0)

  let a3 = signedTriArea c d a
  let a4 = a3 + a2 - a1
  guard (a3 * a4 < 0.0)

  pure $ a3 / (a3 - a4)

-- | From "Real-Time Collision Detection" - Section 5.1.9.1: 2D Segment Intersection
applyIntersectSegSegParam
  :: Num a
  => a
  -> Seg a
  -> V2 a
applyIntersectSegSegParam t (Seg a b) = a + (t *^ (b - a))

{-
TODO: Don't know if I need to port these..


-- | Check if an arbitrary segment intersects a horizontal segment.
--
-- @
--                 + P2
--                /
-- (xa, y3)  +---+----+ (xb, y3)
--              /
--          P1 +
-- @

intersectSegHorzSeg
 :: Point  -- ^ P1 First point of segment.
 -> Point  -- ^ P2 Second point of segment.
 -> Float  -- ^ (y3) y value of horizontal segment.
 -> Float        -- ^ (xa) Leftmost x value of horizontal segment.
 -> Float  -- ^ (xb) Rightmost x value of horizontal segment.
 -> Maybe Point -- ^ (x3, y3) Intersection point, if any.

intersectSegHorzSeg p1@(x1, y1) p2@(x2, y2) y0 xa xb
 | segClearsBox p1 p2 (xa, y0) (xb, y0)
 = Nothing

 | x0 < xa = Nothing
 | x0 > xb = Nothing
 | otherwise = Just (x0, y0)

 where x0 | (y2 - y1) == 0  = x1
   | otherwise  = (y0 - y1) * (x2 - x1) / (y2 - y1) + x1


-- | Check if an arbitrary segment intersects a vertical segment.
--
-- @
--      (x3, yb) +
--               |   + P1
--               | /
--               +
--             / |
--        P2 +   |
--               + (x3, ya)
-- @

intersectSegVertSeg
 :: Point -- ^ P1 First point of segment.
 -> Point  -- ^ P2 Second point of segment.
 -> Float  -- ^ (x3) x value of vertical segment
 -> Float -- ^ (ya) Lowest y value of vertical segment.
 -> Float -- ^ (yb) Highest y value of vertical segment.
 -> Maybe Point -- ^ (x3, y3) Intersection point, if any.

intersectSegVertSeg p1@(x1, y1) p2@(x2, y2) x0 ya yb
 | segClearsBox p1 p2 (x0, ya) (x0, yb)
 = Nothing

 | y0 < ya = Nothing
 | y0 > yb = Nothing
 | otherwise = Just (x0, y0)

 where y0 | (x2 - x1) == 0 = y1
   | otherwise  = (x0 - x1) * (y2 - y1) / (x2 - x1) + y1

-}

-- Copied from Graphics.Gloss.Geometry.Angle:
-- | Convert degrees to radians
{-# INLINE degToRad #-}
degToRad :: Float -> Float
degToRad d = d * pi / 180


-- | Convert radians to degrees
{-# INLINE radToDeg #-}
radToDeg :: Float -> Float
radToDeg r = r * 180 / pi


-- | Normalize an angle to be between 0 and 2*pi radians
{-# INLINE normalizeAngle #-}
normalizeAngle :: Float -> Float
normalizeAngle f
 | f < 0
 = normalizeAngle (f + 2 * pi)

 | f > 2 * pi
 = normalizeAngle (f - 2 * pi)

 | otherwise
 = f
