-- TODO: Move this to cute-c2-hs (CuteC2.Player2D) since it really is
-- just cute-c2 logic
--
-- One question is should Linear be a requirement? Or should this just use C2Vs
--
-- Also include generic renderer helpers
module Mayhem.Engine.Player2D where

import Data.Foldable
import Data.Traversable
import Data.Maybe
import Control.Monad.Identity
import Safe (minimumByMay)
import Data.Function (on)
import Data.Monoid (Any (..), Ap (..))
import Linear
import CuteC2
import Data.Vector.Sized qualified as VS
import Control.Monad.State.Strict
import Control.Monad.Reader
import Control.Monad.Trans.Maybe

import Mayhem.Engine.Utils
import Debug.Trace (traceM)

-- TODO: An extensible-record would probably make more sense
data Player2D = Player2D
  { pos :: V2 Float
  , vel :: V2 Float
  , onGround :: Bool
--  , canJump :: Bool
  } deriving stock (Eq, Show)

data Player2D'Log =
    Player2D'NGS'FailedToConverge
  | Player2D'NGS'Manifold C2Manifold
  | Player2D'NGS'InflatedBoxStep C2Shape
  | Player2D'NGS'InflatedBoxResult C2Shape
  | Player2D'Sweep'BoxStep C2Shape
  | Player2D'Sweep'BoxResult C2Shape
  | Player2D'FailedToExhaustTimestep
  deriving stock (Eq, Show)

traceLogger :: Monad m => Player2D'Log -> m ()
traceLogger = traceM . show

data Player2D'Env m = Player2D'Env
  { -- | The level is just a list of shapes. The caller can filter out irrelevant tiles.
    -- FUTURE: Abstract this to allow for a broad phase.
    level :: [C2Shape]
    -- | The caller can change this depending on the situation with local. For instance,
    -- a different shape can be used for general collisions vs the ground check.
  , box :: C2Shape
  , feetOffset :: Float
  , skinFactor :: Float
  , correctiveFactor :: Float
  , startingPlayer :: Player2D
  , logger :: Player2D'Log -> Player2D'T m ()
  , hooks :: Player2D'Hook -> Player2D'T m ()
  }

data Player2D'Hook =
  Hook'End (Maybe C2TOIResult)
  deriving (Eq, Show)

c2 :: V2 Float -> C2V
c2 (V2 x y) = C2V x y

v2 :: C2V -> V2 Float
v2 (C2V x y) = V2 x y

type Player2D'T m = StateT Player2D (ReaderT (Player2D'Env m) m)

runPlayer2D :: Player2D'Env Identity -> Player2D
runPlayer2D env = runIdentity $ flip runReaderT env $ execStateT step env.startingPlayer

runPlayer2D'T :: Monad m => Player2D'Env m -> m Player2D
runPlayer2D'T env = flip runReaderT env $ execStateT step env.startingPlayer

playerSweep :: forall m. Monad m => Player2D'T m (Maybe C2TOIResult)
playerSweep = do
  Player2D'Env{..} <- ask
  player <- get
  let playerBox = c2Move (c2 player.pos) box
  let minTOI = c2minTOI player.vel playerBox level
  pure minTOI

playerNGS' :: forall m. Monad m => Player2D'T m ()
playerNGS' = do
  Player2D'Env{..} <- ask
  let inflatedBox = c2Inflate skinFactor box
  let loop (n :: Int) =
        case n of
          100 -> do
            logger Player2D'NGS'FailedToConverge
          _ -> for_ level $ \levelBox -> do
            player <- get
            case c2Collide levelBox (c2Move (c2 player.pos) inflatedBox) of
              Just manifold -> do
                modify $ \Player2D{..} -> Player2D{pos = pos + (v2 manifold.n ^* correctiveFactor), ..}
                loop (succ n)
              Nothing -> pure ()

  loop 0

playerNGS :: forall m. Monad m => Player2D'T m ()
playerNGS = do
  Player2D'Env{..} <- ask
  let inflatedBox = c2Inflate skinFactor box
  ngsResult <- boundedIters 100 $ do
    Any hitSomething <- getAp $ flip foldMap level $ \levelBox -> Ap $ do
      player <- get
      case c2Collide levelBox (c2Move (c2 player.pos) inflatedBox) of
        Nothing -> pure (Any False)
        Just manifold -> do
          let newPos = player.pos + (v2 manifold.n ^* correctiveFactor)
          traceM $ unwords ["ngs manifold =", show manifold, "new pos =", show newPos]
          modify $ \Player2D{..} -> Player2D{pos = newPos, ..}
          pure (Any True)

    pure $ if hitSomething then Just () else Nothing

  when (isNothing ngsResult) $
    logger Player2D'NGS'FailedToConverge

slideAlongWall :: forall m. Monad m => C2TOIResult -> Player2D'T m ()
slideAlongWall C2TOIResult{..} = modify $ \Player2D{..} ->
  let n2 = v2 n
  in Player2D{vel = vel - (n2 ^* (dot vel n2)), ..}

moveToTOI :: Monad m => Maybe C2TOIResult -> Player2D'T m ()
moveToTOI = \case
  Just toi ->
    modify $ \Player2D{..} ->
    Player2D{ pos = pos + (vel ^* toi.toi), .. }
  Nothing -> modify $ \Player2D{..} ->Player2D { pos = pos + vel, .. }

step :: forall m. Monad m => Player2D'T m ()
step = void $ timestepped 100 $ do
  Player2D'Env{..} <- ask
  -- sweep player and find toi
  toi <- playerSweep
  traceM $ unwords ["step toi =", show toi]
  -- Move player to toi
  moveToTOI toi

  -- ngs out of potentially colliding configurations
  for_ toi $ \_ -> playerNGS

  -- Slide along the wall if we are against one
  traverse_ slideAlongWall toi

  -- Ground check
  traverse_ checkForGround toi
  hooks $ Hook'End toi
  pure toi

-- FIXME: RandyGaul uses the TOI to do a "near feet" check. That is
-- currently not implemented. This results in collisions with walls
-- while going down causing the player to float / get stuck (or its
-- at least one reason that is happening..)
checkForGround :: Monad m => C2TOIResult -> Player2D'T m ()
checkForGround toi = do
  Player2D'Env{..} <- ask
  let Player2D{..} = startingPlayer
  let goingDown = dot vel (V2 0 1) > 0.85
  let (V2 _ posY) = pos
  -- BUG?: toi.p doesn't look right when I debug. It seems to depend on
  -- the shape we collide with and not the actual location of the collision
  --
  -- Maybe try to repro in ghci and ask on Discord
  traceM $ unwords ["toi.p.y =", show toi.p.y, "posY =", show posY, "feetOffset =", show feetOffset]
  let hitNearFeet = toi.p.y > posY + feetOffset
  when (goingDown && hitNearFeet) $
    put Player2D{onGround = True, ..}

-- TODO: player_can_fall check after the main loop

-- Nothing means there are no TOIs in the level.
c2minTOI :: V2 Float -> C2Shape -> [C2Shape] -> Maybe C2TOIResult
c2minTOI (V2 vx vy) collider collidees = minimumByMay (compare `on` (.toi)) $ mapMaybe doTOI collidees
  where
    doTOI box =
      let res = c2TOI box (C2V 0 0) collider (C2V vx vy) True
      in if res.hit then Just res else Nothing

boundedIters :: Monad m => Int -> m (Maybe a) -> m (Maybe a)
boundedIters b ma = boundedItersN b $ \_ -> ma

boundedItersN :: Monad m => Int -> (Int -> m (Maybe a)) -> m (Maybe a)
boundedItersN bound k =
  let loop n res =
        if n == bound then pure res else k n >>= \case
          Nothing -> pure Nothing
          res'@Just{} -> loop (succ n) res'
  in loop 0 Nothing

timestepped :: Monad m => Int -> (Player2D'T m (Maybe C2TOIResult)) -> Player2D'T m ()
timestepped bound k = do
  Player2D'Env{..} <- ask
  let loop n t =
        if n == bound then logger Player2D'FailedToExhaustTimestep else k >>= \case
          Nothing -> pure () -- No collision - we are done
          Just toi -> unless (toi.toi == 0.0) $ loop (succ n) (t * toi.toi)
  loop 0 1.0
-----------------------------------------------------------------------------
