{-# LANGUAGE NoFieldSelectors #-}
-- |
-- - Lots of primitive controller-building types (with Monoid instances)
-- - HKD-driven control parsing? Doesn't need to be generic - can use an explicit
--   record to have the user specify where each controller comes from. Can also
--   just be heterogenous data.
module Mayhem.Engine.Controller where

import SDL qualified
import SDL.Input.GameController qualified as SDL
import SDL.Internal.Numbered qualified as SDL
import SDL.Raw qualified
import SDL.Vect

import Control.Monad
import Control.Arrow (Kleisli (..))
import Data.Sequence qualified as Seq
import Data.Maybe
import Data.Functor
import Data.Foldable
import Data.Traversable
import Control.Applicative
import Control.Monad.IO.Class
import GHC.Generics
import Data.Proxy
import GHC.Records
import GHC.TypeLits
import Data.Monoid.Generic

import Mayhem.Engine.Input
import Mayhem.Engine.Utils

import Mayhem.Engine.LambdaCaseShowEnum

-------------------------------------------------------------------------------
-- What do we do with input?
-- - We update it based on SDL.Events
-- - We maintain some state. We could
--   close over it, but making it explicit in
--   the type system will allow for better
--   introspection.
-- - We diff our state with previous states - in a buffer

data ProcessInput i
data State s

type family Controller f a where
  Controller (ProcessInput i) a = i -> a

-------------------------------------------------------------------------------

data Button = Button
  { state :: ButtonState
  , event :: Maybe ButtonEvent
  } deriving stock (Show, Eq)

instance Semigroup Button where
  x <> y = Button
    { state = case (x.state, y.state) of
        (Button'Held, _) -> Button'Held
        (_, Button'Held) -> Button'Held
        _ -> Button'Unheld
    , event = x.event <|> y.event
    }

instance Monoid Button where
  mempty = Button Button'Unheld Nothing

data ButtonState = Button'Held | Button'Unheld deriving (Eq, Ord, Show, Read, Enum, Bounded)
data ButtonEvent = Button'Press | Button'Release deriving (Eq, Ord, Show, Read, Enum, Bounded)

instance HasField "isHeld" Button Bool where
  getField Button{..} = case state of
    Button'Held -> True
    Button'Unheld -> False

instance HasField "wasPressed" Button Bool where
  getField Button{..} = case event of
    Just Button'Press -> True
    _ -> False

instance HasField "wasReleased" Button Bool where
  getField Button{..} = case event of
    Just Button'Release -> True
    _ -> False

bothButtons :: Button -> Button -> Button
bothButtons x y = Button
  { state = case (x.state, y.state) of
    (Button'Held, Button'Held) -> Button'Held
    _ -> Button'Unheld
  , event = x.event <|> y.event
  }

scancodeButton :: MonadIO m => SDL.Scancode -> [SDL.Event] -> m Button
scancodeButton code events = do
  isScanPressed <- SDL.getKeyboardState
  let event = scancodeEdge code events <&> \case
        SDL.Pressed -> Button'Press
        SDL.Released -> Button'Release
  let state = if isScanPressed code then Button'Held else Button'Unheld
  pure Button{..}

mouseButton :: MonadIO m => SDL.MouseButton -> [SDL.Event] -> m Button
mouseButton mb events = do
  isButtonPressed <- SDL.getMouseButtons
  let event = mouseButtonEdge mb events <&> \case
        SDL.Pressed -> Button'Press
        SDL.Released -> Button'Release
  let state = if isButtonPressed mb then Button'Held else Button'Unheld
  pure Button{..}

unifiedGameControllerButton
  :: MonadIO m
  => [SDL.Raw.GameController]
  -> SDL.ControllerButton
  -> [SDL.Event]
  -> m Button
unifiedGameControllerButton gcs cb events = do
  sdlButtons <- traverse (flip SDL.Raw.gameControllerGetButton (SDL.toNumber cb)) gcs
  let isHeld = any (/= 0) sdlButtons
  let state = if isHeld then Button'Held else Button'Unheld

  let event = asum $ flip fmap events $ \e -> case e.eventPayload of
        SDL.ControllerButtonEvent SDL.ControllerButtonEventData{..} -> do
          guard (controllerButtonEventButton == cb)
          case controllerButtonEventState of
            SDL.ControllerButtonPressed -> Just Button'Press
            SDL.ControllerButtonReleased -> Just Button'Release
            SDL.ControllerButtonInvalidState -> Nothing
        _ -> Nothing
  pure Button{..}

-------------------------------------------------------------------------------
lambdaCaseShowEnum :: forall a r. Show r => Show a => Enum a => Bounded a => (a -> r) -> String
lambdaCaseShowEnum f =
  ("\\case\n" ++) $ flip foldMap [minBound @a..maxBound] $ \a ->
    mconcat ["  ", show a, " -> ", show (f a), "\n"]

data DPad = DPad
  { left :: Button
  , right :: Button
  , up :: Button
  , down :: Button
  } deriving stock (Show, Generic)
  deriving Semigroup via GenericSemigroup DPad
  deriving Monoid via GenericMonoid DPad

wasd :: MonadIO m => [SDL.Event] -> m DPad
wasd = runKleisli $ do
  up <- Kleisli $ scancodeButton SDL.ScancodeW
  left <- Kleisli $ scancodeButton SDL.ScancodeA
  down <- Kleisli $ scancodeButton SDL.ScancodeS
  right <- Kleisli $ scancodeButton SDL.ScancodeD
  pure $ DPad{..}

unifiedGameControllerDPad
  :: MonadIO m
  => [SDL.Raw.GameController]
  -> [SDL.Event]
  -> m DPad
unifiedGameControllerDPad gcs es = do
  left <- unifiedGameControllerButton gcs SDL.ControllerButtonDpadLeft es
  right <- unifiedGameControllerButton gcs SDL.ControllerButtonDpadRight es
  up <- unifiedGameControllerButton gcs SDL.ControllerButtonDpadUp es
  down <- unifiedGameControllerButton gcs SDL.ControllerButtonDpadDown es
  pure DPad{..}

-------------------------------------------------------------------------------

data ArcadeStickF a = ArcadeStick
  { left :: a
  , up'left :: a
  , down'left :: a
  , right :: a
  , up'right :: a
  , down'right :: a
  , up :: a
  , down :: a
  } deriving stock (Eq, Show, Generic, Functor, Foldable, Traversable)

deriving via GenericSemigroup (ArcadeStickF a) instance Semigroup a => Semigroup (ArcadeStickF a)
deriving via GenericMonoid (ArcadeStickF a) instance Monoid a => Monoid (ArcadeStickF a)

type ArcadeStick = ArcadeStickF Button

instance HasField "leftish" ArcadeStick Button where
  getField ArcadeStick{..} = mconcat [left, up'left, down'left]

instance HasField "rightish" ArcadeStick Button where
  getField ArcadeStick{..} = mconcat [right, up'right, down'right]

instance HasField "downish" ArcadeStick Button where
  getField ArcadeStick{..} = mconcat [down, down'right, down'left]

instance HasField "upish" ArcadeStick Button where
  getField ArcadeStick{..} = mconcat [up, up'left, up'right]

data ArcadeDirCfg = ArcadeDirCfg
  { buttons :: [DPad -> Button]
  , holdMasks :: [DPad -> Button]
  , pressMasks :: [DPad -> Button]
  }

arcadeStickV2 :: ArcadeStick -> ArcadeStickF (V2 Float)
arcadeStickV2 ArcadeStick{..} = ArcadeStick
  { left = if left.isHeld then V2 -1 0 else 0
  , up'left = if up'left.isHeld then V2 -0.5 -0.5 else 0
  , down'left = if down'left.isHeld then V2 -0.5 0.5 else 0
  , right = if right.isHeld then V2 1 0 else 0
  , up'right = if up'right.isHeld then V2 0.5 -0.5 else 0
  , down'right = if down'right.isHeld then V2 0.5 0.5 else 0
  , up = if up.isHeld then V2 0 -1 else 0
  , down = if down.isHeld then V2 0 1 else 0
  }

mkArcadeDir2 :: DPad -> ArcadeDirCfg -> Button
mkArcadeDir2 dpad ArcadeDirCfg{..} =
  Button
  {  state =
     if (all (.isHeld) $ fmap ($ dpad) buttons) && not (any (.isHeld) $ fmap ($ dpad) holdMasks)
     then Button'Held
     else Button'Unheld
  , event = asum
     [ do
         guard $ all (.wasPressed) $ fmap ($ dpad) buttons
         guard $ not $ any ((||) <$> (.wasPressed) <*> (.isHeld)) $ fmap ($ dpad) pressMasks
         pure Button'Press
     , do
         guard $ all (.wasReleased) $ fmap ($ dpad) buttons
         guard $ not $ any (.wasReleased) $ fmap ($ dpad) pressMasks
         pure Button'Release
     , Nothing
     ]
  }

dpadArcadeStick2 :: DPad -> ArcadeStick
dpadArcadeStick2 dpad =
  ArcadeStick
  { left = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.left)]
    , holdMasks = [(.right), (.up), (.down)]
    , pressMasks = [(.down), (.up)]
    }
  , up'left = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.left), (.up)]
    , holdMasks = [(.right), (.down)]
    , pressMasks = [(.right), (.down)]
    }
  , down'left = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.left), (.down)]
    , holdMasks = [(.right), (.up)]
    , pressMasks = [(.right), (.up)]
    }
  , right = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.right)]
    , holdMasks = [(.left), (.up), (.down)]
    , pressMasks = [(.down), (.up)]
    }
  , up'right = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.right), (.up)]
    , holdMasks = [(.left), (.down)]
    , pressMasks = [(.left), (.down)]
    }
  , down'right = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.right), (.down)]
    , holdMasks = [(.left), (.up)]
    , pressMasks = [(.left), (.up)]
    }
  , up = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.up)]
    , holdMasks = [(.left), (.right), (.down)]
    , pressMasks = [(.left), (.right), (.down)]
    }
  , down = mkArcadeDir2 dpad ArcadeDirCfg
    { buttons = [(.down)]
    , holdMasks = [(.left), (.right), (.up)]
    , pressMasks = [(.left), (.right), (.up)]
    }
  }
dpadArcadeStick :: DPad -> ArcadeStick
dpadArcadeStick dpad =
  ArcadeStick
  { left = mkArcadeDir [i'left]
  , up'left = mkArcadeDir [i'left, i'up]
  , down'left = mkArcadeDir [i'left, i'down]
  , right = mkArcadeDir [i'right]
  , up'right = mkArcadeDir [i'right, i'up]
  , down'right = mkArcadeDir [i'right, i'down]
  , down = mkArcadeDir [i'down]
  , up = mkArcadeDir [i'up]
  }
  where
    i'left = 0
    i'right = 1
    i'up = 2
    i'down = 3
    dpadSeq = Seq.fromList [dpad.left, dpad.right, dpad.up, dpad.down]
    mkArcadeDir ns =
      let btns = Seq.index dpadSeq <$> ns
          others = fmap snd $ filter (\(i, _) -> not $ i `elem` ns) $ zip [0..] $ toList dpadSeq
      in
        Button
        { state =
          if all (.isHeld) btns && not (any (.isHeld) others)
          then Button'Held
          else Button'Unheld
        , event = asum
          [ do
              guard $ all (.wasPressed) btns
              guard $ not $ any (\x -> x.wasPressed || x.isHeld) others
              pure Button'Press
          , do
              guard $ all (.wasReleased) btns
              guard $ not $ any (.wasReleased) others
              pure Button'Release
          , Nothing
          ]
        }
-------------------------------------------------------------------------------

data ControlStick = ControlStick
  { -- | Vector with magnitude between 0 and 1 representing
    -- location relative to stick origin
    location :: V2 Float
  }
  deriving (Show)

instance Semigroup ControlStick where
  x <> y =
    let raw = x.location + y.location
        normed = norm raw
        updated = if | nearZero raw -> V2 0 0
                     | normed > 1 -> (/normed) <$> raw
                     | otherwise -> raw
    in ControlStick updated

instance Monoid ControlStick where
  mempty = ControlStick 0

arcadeControlStick :: ArcadeStick -> ControlStick
arcadeControlStick = ControlStick . sum . arcadeStickV2

-------------------------------------------------------------------------------

-- | The head of the list is the "most recent" sample in the buffer
newtype Buffer (n :: Nat) c = Buffer [c] deriving stock (Show)

-- Semigroup/Monoid for Controllers seem to be more about combining on a single
-- step instead of step-over-step. Which means Buffer's is probably more align-based?
-- But I'm not sure that'll ever be useful.

-- Maybe this is a more universal type? Maybe all Controllers are `Buffer n [SDL.Event]`?
-- Buffer is a core concept to input handling, after all.
