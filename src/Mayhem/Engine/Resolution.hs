module Mayhem.Engine.Resolution where

import Data.Word
import Data.Functor (void)
import Linear.V2

import qualified SDL

import Mayhem.Engine.Utils

data NativeResolution = NativeResolution
  { nativeWidth :: Word16
  , nativeHeight :: Word16
  } deriving (Eq, Show)

nativeAspect :: NativeResolution -> Float
nativeAspect NativeResolution{..} = (fromIntegral nativeWidth) / (fromIntegral nativeHeight)

hdtv :: NativeResolution
hdtv = NativeResolution 1920 1080

sdtv :: NativeResolution
sdtv = NativeResolution 640 480

edtv :: NativeResolution
edtv = NativeResolution 848 480

gameboy :: NativeResolution
gameboy = NativeResolution 160 144

gamegear :: NativeResolution
gamegear = NativeResolution 160 144

gba :: NativeResolution
gba = NativeResolution 240 160

ds :: NativeResolution
ds = NativeResolution 256 192

nes :: NativeResolution
nes = NativeResolution 256 240

snes :: NativeResolution
snes = NativeResolution 256 224

genesis :: NativeResolution
genesis = NativeResolution 320 224

onWindowResize :: [SDL.Event] -> (V2 Word16 -> IO r) -> IO ()
onWindowResize events f = void $ traverse f $ findMaybe getResize $ fmap SDL.eventPayload events
  where
    getResize = \case
      SDL.WindowResizedEvent payload -> Just (fmap fromIntegral $ SDL.windowResizedEventSize payload)
      _ -> Nothing

-- Scale a resolution to a height (as close as possible)
scaleH :: Word16 -> NativeResolution -> NativeResolution
scaleH goalH NativeResolution{..} =
  let ratio :: Float = fromIntegral nativeWidth / fromIntegral nativeHeight
      goalW = round (ratio * fromIntegral goalH)
  in NativeResolution { nativeWidth = goalW, nativeHeight = goalH }

aspectRatio :: NativeResolution -> (Word16, Word16)
aspectRatio NativeResolution{..} =
  -- Divide w and h by their gcd to simplify
  let d = gcd nativeWidth nativeHeight
  in (nativeWidth `div` d, nativeHeight `div` d)
