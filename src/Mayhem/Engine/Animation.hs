module Mayhem.Engine.Animation
  ( module Mayhem.Engine.Animation
  -- * Re-exports
  , A.KeyName (..)
  , A.SpriteSheet (..)
  , A.SpriteClip (..)
  , A.Loop (..)
  , A.Animations(..)
  , A.readSpriteSheetYAML
  , A.currentLocation
  ) where

import qualified Animate as A
import Prelude hiding (init)

import Data.Function((&))
import Data.Maybe (fromMaybe)
import qualified Data.Vector as Vector

import Foreign.Ptr
import Foreign.Marshal.Alloc
import qualified SDL.GPU.Simple as GPU
import Memorable
import Linear
import Apecs

import Mayhem.Engine.Frame
import Mayhem.Engine.Utils

data The'Animation = The'Animation
  deriving stock (Show, Eq, Ord, Bounded, Enum)
  deriving anyclass (A.KeyName)

newtype Script = Script { unScript ::  (A.SpriteSheet The'Animation (Ptr GPU.Image) Frame) }

data State = State
  { script :: Script
  , pos    :: A.Position The'Animation Frame
  , paused :: Bool
  , clock  :: Frame
  }

emptyState :: State
emptyState = State
  { script = Script $ A.SpriteSheet
    { ssAnimations = A.Animations mempty
    , ssImage = nullPtr
    }
  , pos = A.Position
    { pKey = The'Animation
    , pFrameIndex = 0
    , pCounter = 0
    , pLoop = A.Loop'Always
    }
  , paused = False
  , clock = 0
  }

-- TODO: Pause, Ptr GPU.Image -> IO () for tweaks, etc

-- TODO: Unsure if making this a Component really helps
-- - Should pos be a list to allow for an entity to be made of multiple animations? Or should the storage allow for it? Does it matter?
-- - How can an entity "cancel" one animation but not the other?
-- - We could use Typeable?

instance Component State where type Storage State = Map State

-- TODO: Time-scaling (multiply / mod clock?)
-- TODO: Rotation
-- TODO: Positional jittering

init :: A.Loop -> Script -> State
init loop s =
  State
  { script = s
  , pos = A.initPositionWithLoop The'Animation loop
  , paused = False
  , clock = 0
  }

initAt :: Frame -> A.Loop -> Script -> State
initAt n loop s = stepN n $ init loop s

pad :: Frame -> Frame -> Script -> Script
pad front back (Script as) =
  let fs  = (A.unAnimations $ A.ssAnimations as) Vector.! 0
      f0  = fs Vector.! 0
      n   = length fs - 1
      fN  = fs Vector.! n
      us  =
        [ ( 0 , f0 { A.fDelay = A.fDelay f0 + front })
        , ( n , fN { A.fDelay = A.fDelay fN + back })
        ]
      fs' = fs Vector.// us
  in Script $ as { A.ssAnimations = A.Animations $ pure fs' }

switch :: A.Loop -> Script -> State -> State
switch loop newScript State{..} =
  State
  { script = newScript
  , pos = A.stepPosition
          (A.ssAnimations (unScript newScript))
          (A.initPositionWithLoop The'Animation loop)
          clock
  , clock = clock
  , paused = paused
  }

pause :: State -> State
pause s = s { paused = True }

unpause :: State -> State
unpause s = s { paused = False }

step :: State -> State
step s@State{..} =
  let anims = A.ssAnimations (unScript script)
   in if paused then s else State { script = script, pos = A.stepPosition anims pos 1, clock = clock + 1, .. }

stepN :: Frame -> State -> State
stepN n s@State{..} =
  let anims = A.ssAnimations (unScript script)
   in if paused then s else State { script = script, pos = A.stepPosition anims pos n, clock = clock + n, .. }

isDone :: State -> Bool
isDone State{..} =
    let anims = A.ssAnimations (unScript script)
    in A.isAnimationComplete anims pos

renderAt :: Num a => Frame -> A.Loop -> Script -> (Ptr GPU.Image -> GPU.Rect -> V2 a -> IO r) -> IO r
renderAt n loop s k = render (initAt n loop s) k

-- TODO: This offset nonsense...use animate-sdl as reference
-- Retu
render :: Num a => State -> (Ptr GPU.Image -> GPU.Rect -> V2 a -> IO r) -> IO r
render State{..} k = do
  let anims = A.ssAnimations (unScript script)
  let A.SpriteClip{..} = A.currentLocation anims pos
  -- Lifted from animate-sdl2
  -- TODO: Still not sure how these offsets will work w/sdl-gpu..gotta test!
  -- TODO: I think this offset stuff is waaay off
  let vOffset = fromMaybe
        (V2 0 0)
        ((\(x,y) -> fromIntegral <$> V2 (-x) (-y)) <$> scOffset)
  let rect = GPU.Rect { x = fromIntegral scX, y = fromIntegral scY, w = fromIntegral scW, h = fromIntegral scH }
  k (A.ssImage (unScript script)) rect vOffset
