module Mayhem.Engine.Automaton
  ( module Mayhem.Engine.Automaton
  , Type
  ) where

import Data.Kind
import Data.Functor.Identity

import Apecs qualified as A

import Mayhem.Engine.Frame
import Mayhem.Engine.Effects

data IsTimed  = Timed | Untimed

type Automaton :: (IsTimed -> Type) -> Type
data Automaton f where
  TickDown :: f Timed -> FrameDown -> Automaton f
  TickForever :: f Untimed -> Frame -> Automaton f

instance A.Component (Automaton f) where type Storage (Automaton f) = A.Map (Automaton f)

tickForever :: f Untimed -> Automaton f
tickForever x = TickForever x 0

tick :: forall f. Automaton f -> (f Timed -> Automaton f) -> Automaton f
tick a k = runIdentity $ tickM a (pure . k)

tickM :: forall f m. Applicative m => Automaton f -> (f Timed -> m (Automaton f)) -> m (Automaton f)
tickM a k = case a of
  TickDown ft fdown -> case cntDown fdown of
    Nothing -> k ft
    Just fdown' -> pure $ TickDown ft fdown'
  TickForever fut frm -> pure $ TickForever fut (succ frm)

autoCase :: forall r f. Automaton f -> (forall t. f t -> r) -> r
autoCase a k = case a of
  TickDown x _ -> k x
  TickForever x _ -> k x

autoEsac :: forall r f. (forall t. f t -> r) -> Automaton f -> r
autoEsac = flip autoCase

autoMap :: forall f. (forall t. f t -> f t) -> Automaton f -> Automaton f
autoMap f a = case a of
  TickDown x fd -> TickDown (f x) fd
  TickForever x fr -> TickForever (f x) fr

autoFor :: forall r f. Automaton f -> (forall t. f t -> f t) -> Automaton f
autoFor = flip autoMap

autoFrameMap :: forall f. (forall t. Frame -> f t -> f t) -> Automaton f -> Automaton f
autoFrameMap f a = case a of
  TickDown x (FrameDown fd) -> TickDown (f fd x) (FrameDown fd)
  TickForever x fr -> TickForever (f fr x) fr

autoCmap
  :: forall cy f cx w es
   . IOE :> es
  => The ApecsE es w
  => Get w es (Automaton f)
  => Get w es cx
  => Set w es cy
  => (forall (t :: IsTimed). cx -> f t -> cy)
  -> Eff es ()
autoCmap k = cmap $ \(a :: Automaton f, x :: cx) -> autoCase a (k x)

autoCmapM
  :: forall cx cy f w es
   . IOE :> es
  => The ApecsE es w
  => Get w es (Automaton f)
  => Get w es cx
  => Set w es cy
  => (forall (t :: IsTimed). cx -> f t -> Eff es cy)
  -> Eff es ()
autoCmapM k = cmapM $ \(a :: Automaton f, x :: cx) -> autoCase a (k x)

autoCmapM_
  :: forall cx f w es
   . IOE :> es
  => The ApecsE es w
  => Get w es (Automaton f)
  => Get w es cx
  => (forall (t :: IsTimed). cx -> f t -> Eff es ())
  -> Eff es ()
autoCmapM_ k = cmapM_ $ \(a :: Automaton f, x :: cx) -> autoCase a (k x)

tickMap
  :: forall c f w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es (Automaton f)
  => Get w es c
  => (c -> f Timed -> Automaton f)
  -> Eff es ()
tickMap k = cmap $ \(a :: Automaton f, x :: c) -> tick a (k x)

tickMapM
  :: forall c f w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es (Automaton f)
  => Get w es c
  => (c -> f Timed -> Eff es (Automaton f))
  -> Eff es ()
tickMapM k = cmapM $ \(a :: Automaton f, x :: c) -> tickM a (k x)

tickMapMaybe
  :: forall c f w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es (Automaton f)
  => Get w es c
  => (c -> f Timed -> Maybe (Automaton f))
  -> Eff es ()
tickMapMaybe k = cmap $ \(a :: Automaton f, x :: c) ->
  case a of
    TickDown ft fdown -> case cntDown fdown of
      Nothing -> do
        k x ft
      Just fdown' -> Just $ TickDown ft fdown'
    TickForever fut frm -> Just $ TickForever fut (succ frm)

tickMapM_
  :: forall c f w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es (Automaton f)
  => Get w es c
  => (c -> f Timed -> Eff es ())
  -> Eff es ()
tickMapM_ k = cmapM $ \(a :: Automaton f, x :: c, ety :: Entity) ->
  case a of
    TickDown ft fdown -> case cntDown fdown of
      Nothing -> do
        destroyC @(Automaton f) ety
        k x ft
      Just fdown' -> cset ety $ TickDown ft fdown'
    TickForever fut frm -> cset ety $ TickForever fut (succ frm)
