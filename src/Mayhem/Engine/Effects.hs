{-# LANGUAGE ImplicitParams #-}

module Mayhem.Engine.Effects
  ( module Mayhem.Engine.Effects
  , module X
  ) where

import Cleff as X

import Foreign.Ptr
import Data.Text qualified
import Jsonifier qualified
import Data.Typeable
import Data.Monoid
import Data.Semigroup (Min (..))

import SDL.GPU.Simple qualified as GPU

import Mayhem.Engine.Effects.Assets as X
import Mayhem.Engine.Effects.Params as X
import Mayhem.Engine.Effects.Apecs as X
import Mayhem.Engine.Effects.Log as X
import Mayhem.Engine.Effects.SDL_GPU as X
import Mayhem.Engine.Effects.Cute_Sound as X
import Mayhem.Engine.Effects.Clock as X
import Mayhem.Engine.Effects.GameController as X
import Mayhem.Engine.Frame
import Cleff.Input as X
import Data.Proxy as X
import Data.Kind as X
import Optics

import Apecs as X
  ( Entity
  , Proxy (..)
  , Not (..)
  , lift
  , Component (..)
  , Global
  , Unique
  , Map -- should we?
  )
import Apecs (SystemT)
import Apecs qualified
import Apecs.Core qualified
import Apecs.Components qualified

import GHC.Classes as X (IP (..))
import GHC.TypeLits

import Mayhem.Engine.Resolution

logString :: Log :> es => String -> Eff es ()
logString = logText . Data.Text.pack

------------------------------------------------------------
-- Imp

type Imp x a = IP x (Proxy a)

type ImpAssets a = Imp "impAssets" a
type ImpParams p = Imp "impParams" p
type ImpApecs w = Imp "impApecs" w

withImpAssets
  :: forall a r
   . ((?impAssets :: Proxy a) => r)
  -> r
withImpAssets k = let ?impAssets = Proxy @a in k

withImpParams
  :: forall p r
   . ((?impParams :: Proxy p) => r)
  -> r
withImpParams k = let ?impParams = Proxy @p in k

withImpApecs
  :: forall w r
   . ((?impApecs :: Proxy w) => r)
  -> r
withImpApecs k = let ?impApecs = Proxy @w in k

impAssets
  :: forall a es
   . (?impAssets :: Proxy a)
  => Assets a :> es
  => Eff es a
impAssets = askAssets @a

impParams
  :: forall p es
   . ImpParams p
  => Params p :> es
  => Eff es p
impParams = askParams @p

impLiftApecs
  :: forall w es a
   . ImpApecs w
  => ApecsE w :> es
  => SystemT w (Eff es) a
  -> Eff es a
impLiftApecs = liftApecs @w

viewAssets
  :: Is k A_Getter
  => Assets assets :> es
  => Optic' k is assets b
  -> Eff es b
viewAssets x = fmap (^. x) askAssets

impViewAssets
  :: Is k A_Getter
  => ImpAssets assets
  => Assets assets :> es
  => Optic' k is assets b
  -> Eff es b
impViewAssets x = fmap (^. x) impAssets

impViewParams
  :: Is k A_Getter
  => ImpParams params
  => Params params :> es
  => Optic' k is params b
  -> Eff es b
impViewParams x = view x <$> impParams


------------------------------------------------------------
-- The

class HasNone (b :: Type) (fe :: Type -> Effect) (es :: [Effect])

instance TypeError
  ('Text "Found The " ':<>: 'ShowType fe ':<>: 'Text " twice:" ':$$:
   'Text "" ':$$:
   'Text "* " ':<>: 'ShowType (fe a) ':$$:
   'Text "* " ':<>: 'ShowType (fe b)
  ) => HasNone b fe (fe a : es)
instance {-# OVERLAPPABLE #-} HasNone b fe es => HasNone b fe (_1 : es)
instance HasNone _1 _2 '[]

class fe a :> es => The (fe :: Type -> Effect) (es :: [Effect]) (a :: Type) | es fe -> a

instance HasNone a fe es => The fe (fe a : es) a
instance {-# OVERLAPPABLE #-} The fe es a => The fe (_1 : es) a

testThe
  :: forall es w
   . The ApecsE es w
  => Proxy w
  -> Eff es ()
testThe _ = liftApecs @w (pure ())

theLiftApecs
  :: forall w es a
   . The ApecsE es w
  => SystemT w (Eff es) a
  -> Eff es a
theLiftApecs = liftApecs @w

theAssets
  :: forall a es
   . The Assets es a
  => Eff es a
theAssets = askAssets @a

theParams
  :: forall p es
   . The Params es p
  => Eff es p
theParams = askParams @p

theAssetss
  :: forall a es r
   . The Assets es a
  => (a -> r)
  -> Eff es r
theAssetss f = f <$> askAssets @a

theParamss
  :: forall p es r
   . The Params es p
  => (p -> r)
  -> Eff es r
theParamss f = f <$> askParams @p

theViewAssets
  :: Is k A_Getter
  => The Assets es assets
  => Optic' k is assets b
  -> Eff es b
theViewAssets x = fmap (^. x) theAssets

theViewParams
  :: Is k A_Getter
  => The Params es params
  => Optic' k is params b
  -> Eff es b
theViewParams x = view x <$> theParams

assetly
  :: forall a es r
   . The Assets es a
  => ((?assets :: a) => Eff es r)
  -> Eff es r
assetly k = do
  assets <- theAssets
  let ?assets = assets in k

paramly
  :: forall p es r
   . The Params es p
  => ((?params :: p) => Eff es r)
  -> Eff es r
paramly k = do
  params <- theParams
  let ?params = params in k

clockly
  :: forall es r
   . Clock :> es
  => ((?clock :: Frame) => Eff es r)
  -> Eff es r
clockly k = do
  clock <- getClock
  let ?clock = clock in k

gpuly
  :: forall es r
   . SDL_GPU :> es
  => ((?screen :: Ptr GPU.Target) => Eff es r)
  -> Eff es r
gpuly k = do
  screen <- askGPU
  let ?screen = screen in k

------------------------------------------------------------
-- Apecs wrapper

type Get w es c = Apecs.Get w (Eff es) c
type Set w es c = Apecs.Set w (Eff es) c
type Modify w es c = (Get w es c, Set w es c)
type Members w es c = Apecs.Members w (Eff es) c
type IsGlobal c = Storage c ~ Global c
type IsUnique c = (Storage c ~ Unique c, Typeable c)

gset
  :: forall c w es
   . (IsGlobal c, The ApecsE es w, ApecsE w :> es, Set w es c)
  => c -> Eff es ()
gset = theLiftApecs . Apecs.set Apecs.global

gget
  :: forall c w es
   . (IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.Has w (Eff es) c, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c))
  => Eff es c
gget = theLiftApecs $ Apecs.get Apecs.global

gfor
  :: forall c w es a
   . (IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.Has w (Eff es) c, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c))
  => (c -> Eff es a) -> Eff es a
gfor f = gget >>= f

ggets
  :: forall c w r es
   . ( IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.Has w (Eff es) c, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c))
  => (c -> r)
  -> Eff es r
ggets f = f <$> gget

gmodify
  :: forall c w es
   . ( IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c), Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c))
  => (c -> c)
  -> Eff es ()
gmodify = theLiftApecs . Apecs.modify Apecs.global

gmappend
  :: forall c w es
   . ( Semigroup c, IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c), Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c))
  => c
  -> Eff es ()
gmappend = gmodify . (<>)

gmodifying
  :: forall c w es
   . ( IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c), Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c))
  => (c -> c)
  -> Eff es c
gmodifying f = do
  c <- gget
  let c' = f c
  gset c'
  pure c'


gmodifyM
  :: forall c w es
   . ( IsGlobal c, The ApecsE es w, ApecsE w :> es, Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c), Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c))
  => (c -> Eff es c)
  -> Eff es ()
gmodifyM f = do
  c <- gget
  c' <- f c
  gset c'

existsC
  :: forall c w es
   . ( The ApecsE es w, ApecsE w :> es, Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c)
     )
  => Entity
  -> Eff es Bool
existsC ety = theLiftApecs $ Apecs.exists ety (Proxy @c)

destroyC
  :: forall c w es
   . ( The ApecsE es w, ApecsE w :> es, Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplDestroy (Eff es) (Apecs.Core.Storage c)
     )
  => Entity
  -> Eff es ()
destroyC e = theLiftApecs $ Apecs.destroy e (Proxy @c)

destroyUC
  :: forall c w es
   . ( The ApecsE es w, ApecsE w :> es, IOE :> es, Apecs.Core.Has w (Eff es) c
     , IsUnique c
     )
  => Eff es ()
destroyUC = cmapM_ $ \(_ :: c, e) -> destroyC @c e

destroyAllC
  :: forall c w es
   . ( The ApecsE es w, ApecsE w :> es, Apecs.Core.Has w (Eff es) c
     , Apecs.Core.ExplDestroy (Eff es) (Apecs.Core.Storage c)
     , Apecs.Core.ExplMembers (Eff es) (Apecs.Core.Storage c)
     , Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c)
     )
  => Eff es ()
destroyAllC = cmapM_ $ \(_ :: c, e) -> destroyC @c e

cmap
  :: forall cx cy w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage cx)
  => Apecs.Core.ExplMembers (Eff es) (Apecs.Core.Storage cx)
  => Apecs.Core.Has w (Eff es) cx
  => Apecs.Core.Has w (Eff es) cy
  => Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage cy)
  => (cx -> cy)
  -> Eff es ()
cmap = theLiftApecs . Apecs.cmap

cmapM_
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c)
  => Apecs.Core.ExplMembers (Eff es) (Apecs.Core.Storage c)
  => (c -> Eff es ())
  -> Eff es ()
cmapM_ f = theLiftApecs $ Apecs.cmapM_ (lift . f)

cmapM
  :: forall cx cy w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Has w (Eff es) cx
  => Apecs.Core.Has w (Eff es) cy
  => Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage cx)
  => Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage cy)
  => Apecs.Core.ExplMembers (Eff es) (Apecs.Core.Storage cx)
  => (cx -> Eff es cy)
  -> Eff es ()
cmapM f = theLiftApecs $ Apecs.cmapM (lift . f)

cset
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c)
  => Entity -> c -> Eff es ()
cset ety c = theLiftApecs $ Apecs.set ety c

cget
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c)
  => Entity -> Eff es c
cget ety = theLiftApecs $ Apecs.get ety

cgetMay
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c)
  => Entity -> Eff es (Maybe c)
cgetMay ety = existsC @c ety >>= \case
  True -> fmap Just $ theLiftApecs $ Apecs.get ety
  False -> pure Nothing

cmodify
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c)
  => Apecs.Core.ExplGet (Eff es) (Apecs.Core.Storage c)
  => Entity -> (c -> c) -> Eff es ()
cmodify ety f = theLiftApecs $ Apecs.modify ety f

newEntity
  :: forall c w es
   . The ApecsE es w
  => [ApecsE w, IOE] :>> es
  => Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c)
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.Has w (Eff es) Apecs.EntityCounter
  => c
  -> Eff es Entity
newEntity = theLiftApecs . Apecs.newEntity

newEntity_
  :: forall c w es
   . The ApecsE es w
  => [ApecsE w, IOE] :>> es
  => Apecs.Core.ExplSet (Eff es) (Apecs.Core.Storage c)
  => Apecs.Core.Has w (Eff es) c
  => Apecs.Core.Has w (Eff es) Apecs.EntityCounter
  => c
  -> Eff es ()
newEntity_ = theLiftApecs . Apecs.newEntity_

cfoldMap
  :: forall c w es a
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => Monoid a
  => (c -> a)
  -> Eff es a
cfoldMap f = theLiftApecs $ Apecs.cfold (\acc c -> mappend acc (f c)) mempty

cfoldMapM
  :: forall c w es a
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es)c
  => Monoid a
  => (c -> Eff es a)
  -> Eff es a
cfoldMapM f = theLiftApecs $ Apecs.cfoldM (\acc c -> mappend <$> pure acc <*> lift (f c)) mempty

cany
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => (c -> Bool)
  -> Eff es Bool
cany f = getAny <$> cfoldMap (Any . f)

cminBy
 :: forall c b w es
   . The ApecsE es w
  => Ord b
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => (c -> b)
  -> Eff es (Maybe b)
cminBy f = getMinMay <$> cfoldMap (MinMay . Just . f)

newtype MinMay a = MinMay { getMinMay :: (Maybe a) }

instance Ord a => Semigroup (MinMay a) where
  MinMay Nothing <> x = x
  x <> MinMay Nothing = x
  MinMay (Just x) <> MinMay (Just y) = MinMay $ Just (min x y)

instance Ord a => Monoid (MinMay a) where
  mempty = MinMay Nothing

ucget
  :: forall c es w
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => IsUnique c
  => Eff es (Maybe c)
ucget = getFirst <$> cfoldMap (First . Just)

ucgetE
  :: forall c es w
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => IsUnique c
  => Eff es (Maybe (c, Entity))
ucgetE = getFirst <$> cfoldMap (First . Just)


ucgetEC
  :: forall u c es w
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => Apecs.Core.Members w (Eff es) u
  => Apecs.Core.Get w (Eff es) u
  => IsUnique u
  => Eff es (Maybe (u, c, Entity))
ucgetEC = getFirst <$> cfoldMap (First . Just)

ucset
  :: forall c es w
   . The ApecsE es w
  => IOE :> es
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => IsUnique c
  => Apecs.Core.Has w (Eff es) Apecs.EntityCounter
  => c
  -> Eff es ()
ucset new = ucget @c >>= \case
  Just _ -> cmap $ \(_ :: c) -> new
  Nothing -> newEntity_ new

ccount
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => Eff es Int
ccount = getSum <$> cfoldMap (\(_ :: c) -> Sum 1)

cexists
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => Eff es Bool
cexists = getAny <$> cfoldMap (\(_ :: c) -> Any True)

cfind
  :: forall c w es
   . The ApecsE es w
  => ApecsE w :> es
  => Apecs.Core.Members w (Eff es) c
  => Apecs.Core.Get w (Eff es) c
  => (c -> Bool)
  -> Eff es (Maybe c)
cfind p = fmap getFirst $ cfoldMap $ \case
  x | p x -> First $ Just x
  _ -> First Nothing

cList
  :: forall c w es
   . Get w es c
  => Members w es c
  => The ApecsE es w
  => Eff es [c]
cList = cfoldMap (\x -> [x])

clearAllC
  :: forall c w es
   . ( The ApecsE es w, ApecsE w :> es, Apecs.Core.Get w (Eff es) c
     , Apecs.Core.Members w (Eff es) c, Apecs.Core.Set w (Eff es) c
     , Apecs.Core.ExplDestroy (Eff es) (Apecs.Core.Storage c)
     )
  => Eff es ()
clearAllC = theLiftApecs $ Apecs.cmap $ \(_ :: c) -> Nothing :: Maybe c

askNativeRezF :: SDL_GPU :> es => Eff es (Float, Float)
askNativeRezF = askNativeRez <&> \NativeResolution{..} ->
  ( fromIntegral nativeWidth
  , fromIntegral nativeHeight
  )
