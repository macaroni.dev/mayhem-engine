-- TODO: Split this out into a Mayhem.Lib.* and Mayhem.Prelude
--
-- Can also have a Mayhem.Engine.Prelude for gamedev-specific imports
module Mayhem.Engine.Utils where

import Prelude hiding (min, max)
import Control.Applicative (Alternative (..))
import Control.Monad ((>=>), when, unless)
import Control.Monad.IO.Class
import Control.Monad.IO.Unlift
import Data.Align (Semialign (..), Align (..))
import Data.Align qualified
import Data.Bool (bool)
import Data.Maybe
import Data.List.NonEmpty (NonEmpty (..))
import Data.Monoid (Ap (..))
import Data.IntMap.Strict qualified as IntMap
import Data.Map.Strict qualified as Map
import Text.Read (readMaybe)
import Safe
import Debug.Trace
import System.Random
import Data.IORef
import Data.Foldable
import System.Environment
import Debug.Trace
import Data.Text qualified as T
import Ease

import Foreign.Storable
import Foreign.Marshal.Alloc
import Foreign.Ptr
import Foreign.C.String

import CuteC2
import Linear hiding (trace)

import Optics
import qualified Language.Haskell.TH as TH

import Memorable

{-
FIXME:

src/Mayhem/Engine/Utils.hs:24:14: warning: [-Wsimplifiable-class-constraints]
    • The constraint ‘MemData a’ matches
        instance Storable a => MemData a -- Defined in ‘Memorable’
      This makes type inference for inner bindings fragile;
        either use MonoLocalBinds, or simplify it using the instance
    • In the type signature:
        allocable :: forall a b m.
                     MemData a => MonadUnliftIO m => (Ptr a -> m b) -> m b
   |
24 | allocable :: forall a b m. MemData a => MonadUnliftIO m => (Ptr a -> m b) -> m b
   |              ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-}

don't :: Applicative m => m () -> m ()
don't _ = pure ()

enumerated :: (Bounded a, Enum a) => [a]
enumerated = [minBound..maxBound]

allocable :: forall a b m. MemData a => MonadUnliftIO m => (Ptr a -> m b) -> m b
allocable k = do
  UnliftIO{..} <- askUnliftIO
  liftIO $ allocaBytesAligned (memDataSize @a) (memDataAlignment @a) (\p -> unliftIO $ k p)

allocaWith :: forall a b m. Storable a => MonadUnliftIO m => a -> (Ptr a -> m b) -> m b
allocaWith a f = do
  UnliftIO{..} <- askUnliftIO
  liftIO $ alloca $ \ptr -> poke ptr a >> unliftIO (f ptr)

mallocEmpty :: forall a. Storable a => IO (Ptr a)
mallocEmpty = mallocBytes (sizeOf (undefined :: a))

putCStrLn :: CString -> IO ()
putCStrLn = peekCString >=> putStrLn

findMaybe :: (a -> Maybe b) -> [a] -> Maybe b
findMaybe f = headMay . mapMaybe f

isElem :: Eq a => Foldable f => f a -> a -> Bool
isElem = flip elem

toZero :: Float -> Float
toZero x = if nearZero x then 0 else x

sameSign :: Float -> Float -> Bool
sameSign x y =
  if | x < 0 && y < 0 -> True
     | x > 0 && y > 0 -> True
     | otherwise -> False

(//) :: Integral a => Integral b => a -> b -> Float
a // b = fromIntegral a / fromIntegral b

(.-) :: Ord a => Num a => a -> a -> a
a .- b = if b > a then 0 else a - b

(-!) :: Ord a => Num a => a -> a -> a
a -! b = if b > a then error "(-!) Subtracting below 0!" else a - b

(-.) :: Ord a => Num a => a -> a -> a
a -. b = if b > a then 0 else a - b

(+-.) :: Ord a => Ord b => Num b => Integral b => Num a => a -> b -> a
a +-. b = if b < 0 then a -. fromIntegral (-1 * b) else a + fromIntegral b

(<~) :: Ord a => Epsilon a => a -> a -> Bool
x <~ y = if nearZero (y - x) then True else x < y

(>~) :: Ord a => Epsilon a => a -> a -> Bool
x >~ y = if nearZero (y - x) then True else x > y

fI :: (Integral a, Num b) => a -> b
fI = fromIntegral

traceNamed :: Show a => String -> a -> a
traceNamed n a = trace (n ++ ": " ++ show a) $ a

mkOpticsLabels :: TH.Name -> TH.DecsQ
mkOpticsLabels = Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels

c2v2v2 :: C2V -> V2 Float
c2v2v2 (C2V x y) = V2 x y

v22c2v :: V2 Float -> C2V
v22c2v (V2 x y) = C2V x y

offsetC2Shape :: V2 Float -> C2Shape -> C2Shape
offsetC2Shape off = \case
  C2Circle' C2Circle{..} ->
    let V2 cX cY = off + c2v2v2 p
    in C2Circle' (C2Circle (C2V cX cY) r)
  C2AABB' C2AABB{..} ->
    let V2 minX minY = off + c2v2v2 min
        V2 maxX maxY = off + c2v2v2 max
    in C2AABB' (C2AABB (C2V minX minY) (C2V maxX maxY))
  C2Capsule' C2Capsule{..} -> do
    let V2 aX aY = off + c2v2v2 a
        V2 bX bY = off + c2v2v2 b
     in C2Capsule' (C2Capsule (C2V aX aY) (C2V bX bY) r)
  C2Poly' _ _ -> error "offsetC2Shape: C2Poly unsupported"

randomChoice :: MonadIO m => [a] -> m a
randomChoice as = do
  i <- randomRIO (0, length as - 1)
  pure (as !! i)

atomicUpdateIORef :: IORef a -> (a -> a) -> IO ()
atomicUpdateIORef ref f = atomicModifyIORef' ref $ \a -> (f a, ())

isEnvSet :: String -> IO Bool
isEnvSet = fmap isJust . lookupEnv

foldMapA
  :: forall t f a b
   . Foldable t
  => Applicative f
  => Monoid b
  => (a -> f b)
  -> t a
  -> f b
foldMapA f = getAp . foldMap (Ap . f)

foldA
  :: forall t f a
   . Foldable t
  => Applicative f
  => Monoid a
  => t (f a)
  -> f a
foldA = getAp . foldMap Ap

foldFor
  :: forall t a b
   . Foldable t
  => Monoid b
  => t a
  -> (a -> b)
  -> b
foldFor = flip foldMap

foldForA
  :: forall t f a b
   . Foldable t
  => Applicative f
  => Monoid b
  => t a
  -> (a -> f b)
  -> f b
foldForA = flip foldMapA

towards
  :: Num a
  => V2 a -- ^ from
  -> V2 a -- ^ to
  -> V2 a
towards (V2 fx fy) (V2 tx ty) = V2 (tx - fx) (ty - fy)

getDecimal :: forall a. RealFrac a => a -> a
getDecimal = snd . properFraction @a @Int
--------------
-- DEBUG TRACE

traceTag :: Show a => String -> a -> a
traceTag tag a = trace (tag ++ ": " ++ show a) a

traceTagM :: Applicative m => Show a => String -> a -> m ()
traceTagM tag a = traceM (tag ++ ": " ++ show a)

-- TODO: Scheme style traces for recursion. Requires the user to
-- use fix and stuff to make it viable.

tshow :: Show a => a -> T.Text
tshow = T.pack . show

treadMaybe :: Read a => T.Text -> Maybe a
treadMaybe = readMaybe . T.unpack

treadEither :: Read a => T.Text -> Either String a
treadEither = readEitherSafe. T.unpack

easeBetween :: Integral a => Fractional f => Ease f -> a -> a -> f
easeBetween ef a1 a2 = ef (fromIntegral a1 / fromIntegral a2)

easePeriodically :: Integral a => Fractional f => Ease f -> a -> a -> f
easePeriodically ef period a = ef (fromIntegral (a `mod` period) / fromIntegral period)

pieceEase :: Ord a => Fractional a => a -> Ease a -> Ease a -> Ease a
pieceEase cutoff ef1 ef2 = \x -> if x < cutoff then ef1 (x / cutoff) else ef2 ((x - cutoff) / (1 - cutoff))

invEase :: Num a => Ease a -> Ease a
invEase ef = \x -> ef (1 - x)

newtype UnionIntMap a = UnionIntMap { unUnionIntMap :: IntMap.IntMap a }
  deriving stock (Functor)
instance Semigroup a => Semigroup (UnionIntMap a) where
  UnionIntMap x <> UnionIntMap y = UnionIntMap $ IntMap.unionWith (<>) x y

instance Semigroup a => Monoid (UnionIntMap a) where
  mempty = UnionIntMap mempty

newtype UnionMap k a = UnionMap { unUnionMap :: Map.Map k a }
  deriving stock (Functor)

instance (Ord k, Semigroup a) => Semigroup (UnionMap k a) where
  UnionMap x <> UnionMap y = UnionMap $ Map.unionWith (<>) x y

instance (Ord k, Semigroup a) => Monoid (UnionMap k a) where
  mempty = UnionMap mempty

unionMapSingleton :: k -> a -> UnionMap k a
unionMapSingleton k a = UnionMap $ Map.singleton k a

newtype Aligned f a = Aligned { unAligned :: f a }

instance (Semigroup a, Semialign f) => Semigroup (Aligned f a) where
  Aligned x1 <> Aligned x2 = Aligned $ Data.Align.salign x1 x2

instance (Semigroup a, Align f) => Monoid (Aligned f a) where
  mempty = Aligned Data.Align.nil

unlessm :: Monoid a => Bool -> a -> a
unlessm b a = if b then mempty else a

whenm :: Monoid a => Bool -> a -> a
whenm b a = if b then a else mempty

(|:) :: [a] -> a -> NonEmpty a
[] |: a = pure a
(x : xs) |: a = x :| (xs ++ [a])


------------------------------------------------------
-- Combinators copied from protolude

guarded :: forall f a. (Alternative f) => (a -> Bool) -> a -> f a
guarded p x = bool empty (pure x) (p x)

