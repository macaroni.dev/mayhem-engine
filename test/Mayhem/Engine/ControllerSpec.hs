module Mayhem.Engine.ControllerSpec where

import Data.Foldable (toList)

import Test.Hspec
import Test.QuickCheck

import Mayhem.Engine.Controller
import Mayhem.Engine.Controller as DPad (DPad (..))
import Mayhem.Engine.Controller as ArcadeStick (ArcadeStickF (..))

spec :: Spec
spec = do
  describe "dpadArcadeStick - arbitrary properties" $ do
    it "only has one button held at a time" $
      forAll (dpadGen buttonGen) $ \(toList . dpadArcadeStick -> stick) ->
        length (filter (.isHeld) stick) `elem` [0, 1]
    it "only has one button press event at a time" $
      forAll (dpadGen buttonGen) $ \(toList . dpadArcadeStick -> stick) ->
        length (filter (.wasPressed) stick) `elem` [0, 1]
    it "only has one button release event at a time" $
      forAll (dpadGen buttonGen) $ \(toList . dpadArcadeStick -> stick) ->
        length (filter (.wasReleased) stick) `elem` [0, 1]
  describe "realisticButtonGen" $ do
    it "isRealisticButton" $ do
      forAll realisticButtonGen isRealisticButton
  describe "dpadArcadeStick - realistic properties" $ do
    it "maintains realistic buttons" $
      forAll (dpadGen realisticButtonGen) $ \(toList . dpadArcadeStick -> stick) ->
        all (isRealisticButton) stick
  describe "dpadArcadeStick - unit tests" $ do
    it "should handle basic held directions" $ do
      dpadArcadeStick (mempty { DPad.left = heldButton }) `shouldBe` mempty { ArcadeStick.left = heldButton }
      dpadArcadeStick (mempty { DPad.right = heldButton }) `shouldBe` mempty { ArcadeStick.right = heldButton }
      dpadArcadeStick (mempty { DPad.up = heldButton }) `shouldBe` mempty { ArcadeStick.up = heldButton }
      dpadArcadeStick (mempty { DPad.down = heldButton }) `shouldBe` mempty { ArcadeStick.down = heldButton }
      dpadArcadeStick (mempty { DPad.left = heldButton, DPad.up = heldButton }) `shouldBe` mempty { ArcadeStick.up'left = heldButton }
      dpadArcadeStick (mempty { DPad.left = heldButton, DPad.down = heldButton }) `shouldBe` mempty { ArcadeStick.down'left = heldButton }
      dpadArcadeStick (mempty { DPad.right = heldButton, DPad.up = heldButton }) `shouldBe` mempty { ArcadeStick.up'right = heldButton }
      dpadArcadeStick (mempty { DPad.right = heldButton, DPad.down = heldButton }) `shouldBe` mempty { ArcadeStick.down'right = heldButton }
      dpadArcadeStick (mempty { DPad.right = heldButton, DPad.up = heldButton }) `shouldBe` mempty { ArcadeStick.up'right = heldButton }
      dpadArcadeStick (mempty { DPad.right = heldButton, DPad.down = heldButton }) `shouldBe` mempty { ArcadeStick.down'right = heldButton }


heldButton :: Button
heldButton = Button { event = Nothing, state = Button'Held }

unheldButton :: Button
unheldButton = Button { event = Nothing, state = Button'Unheld }

pressedButton :: Button
pressedButton = Button { event = Just Button'Press, state = Button'Held }

releasedButton :: Button
releasedButton = Button { event = Just Button'Release, state = Button'Unheld }

isRealisticButton :: Button -> Bool
isRealisticButton = \case
  Button{event=Just Button'Press, state=Button'Held} -> True
  Button{event=Just Button'Press, state=Button'Unheld} -> False
  Button{event=Just Button'Release, state=Button'Unheld} -> True
  Button{event=Just Button'Release, state=Button'Held} -> False
  Button{} -> True

realisticButtonGen :: Gen Button
realisticButtonGen = do
  event <- maybeGen enumGen
  state <- case event of
    Just Button'Press -> pure $ Button'Held
    Just Button'Release -> pure $ Button'Unheld
    Nothing -> enumGen
  pure Button{..}

dpadGen :: Gen Button -> Gen DPad
dpadGen btn = do
  up <- btn
  down <- btn
  left <- btn
  right <- btn
  pure DPad{..}

buttonGen :: Gen Button
buttonGen = do
  state <- enumGen
  event <- maybeGen enumGen
  pure Button{..}

enumGen :: Enum a => Bounded a => Gen a
enumGen = oneof $ pure <$> [minBound..maxBound]

maybeGen :: Gen a -> Gen (Maybe a)
maybeGen g = oneof [pure Nothing, Just <$> g]
