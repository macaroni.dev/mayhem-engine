module Main where

import Test.Hspec

import Mayhem.Engine.ControllerSpec qualified
main :: IO ()
main = hspec $ sequence_
  [ Mayhem.Engine.ControllerSpec.spec
  ]
