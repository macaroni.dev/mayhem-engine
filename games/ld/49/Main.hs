{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Monad.Extra
import Data.Functor

import qualified SDL.GPU.Utils as GPU

import Mayhem.Engine.Loop
import Mayhem.Engine.Utils

import qualified LD49.Game

main :: IO ()
main = do
  LD49.Game.main


