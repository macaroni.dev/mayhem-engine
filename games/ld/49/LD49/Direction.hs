module LD49.Direction where

import Control.Arrow ((***))

newtype RowNo = RowNo Int
  deriving stock (Show, Read)
  deriving newtype (Eq, Ord, Enum, Bounded, Num)

newtype ColNo = ColNo Int
  deriving stock (Show, Read)
  deriving newtype (Eq, Ord, Enum, Bounded, Num)

data Direction = LEFT | RIGHT | UP | DOWN
  deriving stock (Eq, Ord, Show, Read, Enum, Bounded)

revDir :: Direction -> Direction
revDir = \case
  LEFT -> RIGHT
  RIGHT -> LEFT
  UP -> DOWN
  DOWN -> UP

dir2delta :: Direction -> (RowNo, ColNo) -> (RowNo, ColNo)
dir2delta = \case
  LEFT -> id *** pred
  RIGHT -> id *** succ
  UP -> succ *** id
  DOWN -> pred *** id

dirCode :: Direction -> String
dirCode = \case
  LEFT -> "<"
  RIGHT -> ">"
  UP -> "^"
  DOWN -> "v"
