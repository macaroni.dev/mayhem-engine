-- NOTE: We are going to be unsafe indexing a lot..we could use DTs
-- but I'm not sure if it's worth it to start.
--
-- Can always layer them on later
module LD49.Isotope where

import Control.Monad
import Control.Monad.IO.Class
import Data.Foldable
import Data.Function ((&))
import Data.Functor
import Data.Functor.Identity
import Data.Sequence (Seq)
import qualified Data.Sequence as Seq
import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IntMap
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import Data.Maybe (mapMaybe, fromMaybe)
import System.Random.Stateful (Uniform(..), uniform, uniformWord32R, getStdRandom)
import GHC.Generics

import Mayhem.Engine.Frame
import Mayhem.Engine.Utils

import LD49.Direction

data Element =
    Flammable
  | Coolant
  | Metallic
  | Radioactive
  | Noble
  deriving stock (Eq, Ord, Show, Enum, Bounded)

allElements :: Vector Element
allElements = Vector.fromList [minBound..maxBound]

instance Uniform Element where
  uniformM g = do
    i <- uniformWord32R (fI (length allElements) - 1) g
    pure $ Vector.unsafeIndex allElements (fI i)

elementCode :: Element -> String
elementCode = \case
  Flammable -> "F"
  Coolant   -> "C"
  Metallic  -> "M"
  Radioactive -> "R"
  Noble -> "N"

data Tile =
    TileEmpty
  | TileIdle Element
  | TileSwap
      { dir :: Direction
      , currElement :: Maybe Element
      , nextElement :: Maybe Element
      , clock :: Frame
      }
  -- NOTE: The tile is in control of its "charge up"
  | TileMatch { element :: Element, clock :: Frame }
  | TileUnstable { element :: Element, clock :: Frame }
  deriving stock (Eq, Ord, Show)

tileCode :: Tile -> String
tileCode = \case
  TileEmpty -> "X"
  TileIdle e -> elementCode e
  TileSwap{..} -> maybe "X" elementCode currElement ++ dirCode dir
  TileMatch{..} -> elementCode element ++ "!"
  TileUnstable{clock} -> elementCode (fakeRandElement clock)

tickTile :: Applicative m => (Element -> m ()) -> Tile -> m Tile
tickTile matchCallback = \case
  t@TileEmpty -> pure t
  t@TileIdle{} -> pure t
  TileSwap{..} ->
    pure $ if clock == 0
    then maybe TileEmpty TileIdle nextElement
    else TileSwap{clock = pred clock, ..}
  TileMatch{..} ->
    if clock == 0
    then matchCallback element $> TileEmpty
    else pure TileMatch{clock = pred clock, ..}
  TileUnstable{..} ->
    pure $ if clock == 0
    then TileIdle element
    else TileUnstable{clock = pred clock, ..}

fakeRandElement :: Frame -> Element
fakeRandElement n = Vector.unsafeIndex allElements (fI (n `div` 1) `mod` length allElements)

swappableElem :: Tile -> Maybe (Maybe Element)
swappableElem = \case
  TileEmpty -> Just Nothing
  TileIdle e -> Just (Just e)
  _ -> Nothing

idleElement :: Tile -> Maybe Element
idleElement = \case
  TileIdle e -> Just e
  _ -> Nothing

tileIdle :: Tile -> Bool
tileIdle = \case
  TileIdle _ -> True
  _ -> False

tileEmpty :: Tile -> Bool
tileEmpty = \case
  TileEmpty -> True
  _ -> False

mkTileSwap :: Frame -> Direction -> Maybe Element -> Maybe Element -> Tile
mkTileSwap swapLen dir curr next =
  TileSwap
  { dir = dir
  , currElement = curr
  , nextElement = next
  , clock = swapLen
  }

randElement :: MonadIO m => m Element
randElement = getStdRandom $ \g -> uniform g

newtype Row a = Row { unRow :: (Seq a) }
  deriving newtype (Eq, Ord, Show)
  deriving stock (Functor, Foldable, Traversable)

-- Keys in IntMap are ColNo
-- Assumptions:
-- - Higher ColNo -> closer to the top
-- - Keys are always increasing & never skip numbers.
-- - They start at 0
newtype Isotope a = Isotope
  { elements :: IntMap (Row a)
  }
  deriving stock (Eq, Ord, Show, Functor, Foldable, Traversable)
  deriving newtype (Semigroup, Monoid)

rowGet :: ColNo -> Row a -> Maybe a
rowGet (ColNo i) = Seq.lookup i . unRow

rowSet :: ColNo -> a -> Row a -> Row a
rowSet (ColNo i) a = Row . Seq.update i a . unRow

isotopeGetRow :: RowNo -> Isotope a -> Maybe (Row a)
isotopeGetRow (RowNo i) = IntMap.lookup i . elements

isotopeGet :: RowNo -> ColNo -> Isotope a -> Maybe a
isotopeGet row col = isotopeGetRow row >=> rowGet col

isotopeGet' :: Isotope a -> RowNo -> ColNo -> Maybe a
isotopeGet' i row col = isotopeGet row col i

isotopeSet :: RowNo -> ColNo -> a -> Isotope a -> Isotope a
isotopeSet (RowNo r) col a = Isotope . IntMap.adjust (rowSet col a) r . elements

isotopeTraverse
  :: Applicative m
  => (RowNo -> ColNo -> a -> m b)
  -> Isotope a
  -> m (Isotope b)
isotopeTraverse f Isotope{..} =
  fmap Isotope $ flip IntMap.traverseWithKey elements $ \ri (Row row) ->
  fmap Row $ flip Seq.traverseWithIndex row $ \ci a ->
  f (RowNo ri) (ColNo ci) a

isotopeFromList :: [[a]] -> Isotope a
isotopeFromList =
    Isotope
  . IntMap.fromList
  . zipWith (,) [0..]
  . fmap (Row . Seq.fromList)
  . reverse

isotopeLen :: Isotope a -> Int
isotopeLen = IntMap.size . elements

rowToIndexedList :: Row a -> [(Int, ColNo, a)]
rowToIndexedList (Row row) = zipWith f [0..] (toList row)
  where
    f i a = (i, ColNo i, a)

-- Index = 0 means it's at the bottom
isotopeToIndexedList :: Isotope a -> [(Int, RowNo, [(Int, ColNo, a)])]
isotopeToIndexedList Isotope{..} =
  zipWith f [0..] (IntMap.assocs elements)
  where
    f i (ri, row) = (i, RowNo ri, rowToIndexedList row)

randRow :: forall a m. MonadIO m => Uniform a => Int -> m (Row a)
randRow n = fmap (Row . Seq.fromList) $ replicateM n $ getStdRandom $ \g -> uniform g

-- NOTE: If you try to flip to or from a coordinate not in the isotope,
-- this function is a no-op
--
-- TODO: Allow it to custom twiddle the a's instead of straight swapping..
-- Then we can put them in mutally "swapping" state"
flipElems
  :: (a -> Maybe b) -- ^ Whether we are allowed to swap
  -> (   Direction -- ^ swap ctor
      -> b -- ^ curr
      -> b -- ^ next
      -> a
     )
  -> RowNo -- ^ Col of
  -> ColNo -- ^ Col of Row
  -> Direction
  -> Isotope a
  -> Isotope a
flipElems getSwap mkSwap fromRow fromCol dir isotope = fromMaybe isotope $ do
  let (toRow, toCol) = dir2delta dir (fromRow, fromCol)
  fromB <- isotopeGet fromRow fromCol isotope >>= getSwap
  toB <- isotopeGet toRow toCol isotope >>= getSwap
  pure $
    isotopeSet toRow toCol (mkSwap (revDir dir) toB fromB) $
    isotopeSet fromRow fromCol (mkSwap dir fromB toB) isotope

-- | Logic is a little naive. Basically, per-element,
-- check in each direction if it is part of any 3-match.
-- If so, apply f to said elem. This implicitly handles
-- more complicated matches (5, 7, L, T) etc
match
  :: forall a b
   . Eq b
  => (a -> Maybe b) -- ^ how should we compare them?
  -> (b -> a) -- ^ what to do next
  -> Isotope a
  -> Isotope a
match matchBy f i@Isotope{..} = Isotope $
  flip IntMap.mapWithKey elements $ \r (Row cols) ->
  Row $ flip Seq.mapWithIndex cols $ \c a ->
  let row = RowNo r
      col = ColNo c
      coord = (row, col)
      getCoords dir = take 2 $ drop 1 $ iterate (dir2delta dir) coord
      horizCoords = [dir2delta LEFT coord, coord, dir2delta RIGHT coord]
      vertCoords = [dir2delta UP coord, coord, dir2delta DOWN coord]
      dirCoords :: [[(RowNo, ColNo)]] = mconcat
        [ getCoords <$> [minBound..maxBound]
        , [horizCoords, vertCoords]
        ]
      dirAs :: [[a]] = mapMaybe (traverse (uncurry (isotopeGet' i))) dirCoords
   in fromMaybe a $ do
    b <- matchBy a
    let dirBs = mapMaybe (traverse matchBy) dirAs
    pure $
      if (any (all (== b)) dirBs) then f b else a

addTop
  :: Row a
  -> Isotope a
  -> Isotope a
addTop newRow Isotope{..} =
  let maxRow = maybe 0 fst $ IntMap.lookupMax elements
   in Isotope $ IntMap.insert (succ maxRow) newRow elements

clearBottom
  :: (Row a -> Bool)
  -> Isotope a
  -> Isotope a
clearBottom f i@Isotope{..} =
  IntMap.lookupMin elements & \case
    Nothing -> i
    Just (rowNo, row) ->
      if f row
      then Isotope (IntMap.delete rowNo elements)
      else i
