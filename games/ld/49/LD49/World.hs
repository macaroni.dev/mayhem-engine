{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
module LD49.World
  ( module LD49.World
  ) where

import Data.Monoid
import Data.Semigroup

import Apecs
import Linear
import CuteC2

import Mayhem.Engine.Utils
import Mayhem.Engine.TH
import Mayhem.Engine.Geometry
import Mayhem.Engine.Frame
import qualified Mayhem.Engine.Animation as Animation

import LD49.Isotope
import LD49.Direction

newtype HighScore = HighScore { score :: Frame }
  deriving stock (Eq, Ord, Show)
  deriving newtype (Enum, Bounded)
  deriving (Semigroup, Monoid) via (Max HighScore)
instance Component HighScore where type Storage HighScore = Global HighScore

newtype PlayTime = PlayTime { clock :: Frame }
  deriving stock (Eq, Ord, Show)
  deriving newtype (Enum, Bounded)
instance Semigroup PlayTime where
  PlayTime x <> PlayTime y = PlayTime (x + y)
instance Monoid PlayTime where
  mempty = PlayTime 0
instance Component PlayTime where type Storage PlayTime = Global PlayTime

newtype PlayingArea = PlayingArea (Box Float) deriving stock (Show)
instance Semigroup PlayingArea where
  pa <> _ = pa
instance Monoid PlayingArea where
  mempty = PlayingArea $ Box (pure 0) (pure 0)
instance Component PlayingArea where type Storage PlayingArea = Global PlayingArea

newtype Position = Position (V2 Float) deriving stock (Show)
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity (V2 Float) deriving stock (Show)
instance Component Velocity where type Storage Velocity = Map Velocity

data Clamp = Clamp deriving stock (Show)
instance Component Clamp where type Storage Clamp = Map Clamp

data Scene =
    Gameplay
  | Gameover
instance Component Scene where type Storage Scene = Global Scene

instance Semigroup Scene where
  a <> _ = a
instance Monoid Scene where
  mempty = Gameplay

data NanoTim = NanoTim
  deriving stock (Show, Eq, Ord, Enum, Bounded)
  deriving (Semigroup, Monoid) via (Min NanoTim)
instance Component NanoTim where type Storage NanoTim = Unique NanoTim

data ShootCharge = ShootCharge { bullet :: Bullet, clock :: Frame }
  deriving stock (Show, Eq, Ord)
instance Component ShootCharge where type Storage ShootCharge = Unique ShootCharge

data Caldwell = Caldwell
  { isotope :: Isotope Tile
  , depth   :: Float
  }
  deriving stock (Show, Eq, Ord)
  --deriving newtype (Semigroup, Monoid)
instance Component Caldwell where type Storage Caldwell = Unique Caldwell

data HalfLife = HalfLife
  { speed :: Float
  , clock :: Float
  }
  deriving stock (Show, Eq, Ord)
instance Component HalfLife where type Storage HalfLife = Unique HalfLife

data Bullet =
    BulletUnstable
  | BulletFlip Direction
  deriving stock (Show, Eq, Ord)
instance Component Bullet where type Storage Bullet = Map Bullet

data BulletCooldown = BulletCooldown { clock :: Frame }
  deriving stock (Show, Eq, Ord)
instance Component BulletCooldown where type Storage BulletCooldown = Unique BulletCooldown

data RxnClock = RxnClock Frame deriving stock (Show, Eq, Ord)
instance Component RxnClock where type Storage RxnClock = Map RxnClock

data FireRxn = FireRxn RowNo ColNo deriving stock (Show, Eq, Ord)
instance Component FireRxn where type Storage FireRxn = Map FireRxn

data IceRxn = IceRxn deriving stock (Show, Eq, Ord) -- Uses Position & Velocity
instance Component IceRxn where type Storage IceRxn = Map IceRxn

data RadioidBullet = RadioidBullet deriving stock (Show, Eq, Ord)
instance Component RadioidBullet where type Storage RadioidBullet = Map RadioidBullet

data MetalRxn = MetalRxn deriving stock (Show, Eq, Ord)
instance Component MetalRxn where type Storage MetalRxn = Map MetalRxn

-- Resets & freezes the HalfLife
-- TODO: Meter - gold-ify it?
data NobleRxn = NobleRxn deriving stock (Show, Eq, Ord)
instance Component NobleRxn where type Storage NobleRxn = Map NobleRxn

-- Frozen for n more frames
data Frozen = Frozen Frame deriving stock (Show, Eq, Ord)
instance Component Frozen where type Storage Frozen = Map Frozen

data Dead = Dead Frame deriving stock (Show, Eq, Ord)
instance Component Dead where type Storage Dead = Unique Dead

-- TH_CODE

mkOpticsLabels ''Caldwell
mkOpticsLabels ''HalfLife

makeWorld "World"
  [ ''Scene
  , ''NanoTim
  , ''Caldwell
  , ''HalfLife
  , ''Bullet
  , ''Position
  , ''Velocity
  , ''Clamp
  , ''PlayingArea
  , ''BulletCooldown
  , ''RxnClock
  , ''FireRxn
  , ''IceRxn
  , ''RadioidBullet
  , ''MetalRxn
  , ''NobleRxn
  , ''Frozen
  , ''Dead
  , ''Animation.State
  , ''ShootCharge
  , ''PlayTime
  , ''HighScore
  ]
