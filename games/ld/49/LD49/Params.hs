{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
-- Import qualified
module LD49.Params where

import Linear.V2
import CuteC2

import Mayhem.Engine.TH
import Mayhem.Engine.Utils
import Mayhem.Engine.Frame

data Main = Main
  { caldwell :: Caldwell
  , halflife :: HalfLife
  , nanotim  :: NanoTim
  , flammable :: Flammable
  , coolant :: Coolant
  , metallic :: Metallic
  , noble   :: Noble
  , radioid :: Radioid
  , score :: Score
  }

data Caldwell = Caldwell
  { tileSize :: Float -- Add a c2aabb?
  , tilePadding :: Float
  , rowWidth :: Int
  , matchLen :: Frame
  , swapLen :: Frame
  , unstableLen :: Frame
  , depthLimit :: Float
  , sideGutter :: Float
  , topGutter  :: Float
  }

data HalfLife = HalfLife
  { initialSpeed :: Float
  , len :: Float
  , locX :: Float
  , locY :: Float
  }

data Score = Score
  { locX :: Float
  , locY :: Float
  }

data NanoTim = NanoTim
  { speed :: Float
  , startLoc :: V2 Float
  , bulletCooldown :: Frame
  , bulletSpeed :: Float
  , bulletRadius :: Float
  , hurtbox :: C2AABB
  , shootCharge :: Frame
  , shootSpriteOffset :: Float
  }

data Flammable = Flammable
  { dur :: Frame
  }

data Coolant = Coolant
  { speed :: Float
  , freezeDur :: Frame
  , hitbox_min :: V2 Float
  , hitbox_max :: V2 Float
  }

data Metallic = Metallic
  { dur :: Frame
  , xoffset :: Float
  , yoffset :: Float
  , hbW :: Float
  , hbH :: Float
  }

data Noble = Noble
  { dur :: Frame
  }

data Radioid = Radioid
  { bulletSpeed :: Float
  , bulletRadius :: Float
  }

hardcoded :: Main
hardcoded = Main
  { nanotim = NanoTim
    { speed = 10.0 -- 10 is maybe a lil too fast?
    , startLoc = V2 700 1000
    , bulletCooldown = 15
    , bulletSpeed = 12.0
    , bulletRadius = 4.0
     -- TODO: Offset both his sprite + hurtbox so his pos is centered
    , hurtbox = C2AABB
      { min = C2V 0 0
      , max = C2V 40 70
      }
    , shootCharge = 11
    , shootSpriteOffset = 117
    }
  , caldwell = let topGutter = 150 in Caldwell
    { tileSize = 90.0
    , tilePadding = 3.0
    , rowWidth = 9
    , matchLen = 60
    , swapLen = 10
    , unstableLen = 30
    , depthLimit = 1050 - topGutter
    , sideGutter = 275.0
    , topGutter  = topGutter
    }
  , halflife = HalfLife
    { initialSpeed = 1.0 -- TODO: We assume 1.0 speed in the halflife display code!
    , len = 420.0 -- 600.0
    , locX = 1225
    , locY = 365
    }
  , flammable = Flammable
    { dur = 90
    }
  , coolant = Coolant
    { speed = 5.0
    , freezeDur = 90
    , hitbox_min = V2 15 15
    , hitbox_max = V2 90 75
    }
  , metallic = Metallic
    { dur = 600 -- TODO: 600
    , xoffset = (-25)
    , yoffset = (-45)
    , hbH = 45
    , hbW = 90
    }
  , noble = Noble
    { dur = 120
    }
  , radioid = Radioid
    { bulletSpeed = 10.0
    , bulletRadius = 4.0
    }
  , score = Score
    { locX = 1225
    , locY = 200
    }
  }

-- TH_CODE
mkOpticsLabels ''Main
mkOpticsLabels ''Caldwell
mkOpticsLabels ''HalfLife
mkOpticsLabels ''NanoTim
mkOpticsLabels ''Score
