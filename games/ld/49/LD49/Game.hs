module LD49.Game where

import Control.Monad
import Control.Concurrent (forkOS, ThreadId)
import Data.Functor
import Data.Monoid
import Data.Foldable (for_, traverse_)
import Data.Maybe
import Text.Printf (printf)
import qualified Data.Set as Set
import System.Environment

import qualified Cute.Sound.C as CS
import qualified SDL.GPU.Simple as Target (TargetF(..))
import qualified SDL.GPU.Utils as GPU
import qualified SDL.GPU.Simple as GPU.C
import qualified SDL.GPU.FC.Simple as FC
import qualified SDL
import qualified SDL.Utils as SDL
import Optics hiding (set, element)
import Memorable
import Linear.V2
import Linear.Metric
import Linear.Vector

import Mayhem.Engine.Loop
import Mayhem.Engine.Assets
import Mayhem.Engine.Effects.Debug
import Mayhem.Engine.Effects.GHCi
import Mayhem.Engine.Effects.Stepper
import Mayhem.Engine.Effects
import Mayhem.Engine.Utils
import Mayhem.Engine.Input
import Mayhem.Engine.Geometry
import Mayhem.Engine.Frame
import Mayhem.Engine.Resolution
import qualified Mayhem.Engine.Animation as Animation

import LD49.World
import qualified LD49.World as Caldwell (Caldwell(..))
import LD49.Isotope
import LD49.Repl
import LD49.Direction
import LD49.Collisions
import qualified LD49.Params as P
import qualified LD49.Assets as A

type CanGame es =
  ([IOE, SDL_GPU, Cute_Sound, Input [SDL.Event], Log, Clock, Debug] :>> es
  , The ApecsE es World
  , The Params es P.Main
  , The Assets es A.Main
  )

type GHCiEffects = '[IOE, Stepper, ApecsE World]

gameRez :: NativeResolution
gameRez = NativeResolution 1440 1080

main :: IO ()
main = main' Nothing

ghciMain :: IO (ThreadId, GHCiPipe GHCiEffects)
ghciMain = do
  ghciPipe <- newGHCiPipe
  tid <- forkOS $ main' $ Just ghciPipe
  pure (tid, ghciPipe)

main' :: Maybe (GHCiPipe GHCiEffects) -> IO ()
main' ghciPipe = runManaged $ do
  stress <- liftIO $ isEnvSet "STRESS"
  let params = if stress then P.hardcoded & #halflife % #len .~ 60.0 else P.hardcoded

  Run console <-
      runLiftIOE
    &   exp'SDL_GPU SDL_GPU'Config {enableVsync = False, nativeRez = gameRez}
    >>= exp'Cute_Sound
    >>= exp'Log
    >>= exp'Apecs initWorld
    >>= exp'Params (pure params)
    >>= exp'Assets @A.Main "assets"
    >>= exp'Debug (theViewAssets $ #fonts % #comicmono % #rawFont)
    >>= exp'Clock
    >>= exp'Stepper
    >>= maybe exp'GHCiDummy exp'GHCiPipe ghciPipe

  console $ do
    initGame
    framed $ (if stress then stressloop else gameloop) >>= \Loop{..} -> do
      tickGHCi
      askGPU >>= liftIO . GPU.clear
      f7ToggleDebug
      f11Fullscreen
      f2Stepper
      adaptToWindowResize

      stepped $ clocked loopTick
      loopDraw

      flush
      liftIO performGC
      askCuteSound >>= liftIO . CS.mix
      askGPU >>= liftIO . GPU.flip

stressloop
  :: CanGame es => Eff es (Game es)
stressloop = gget >>= \case
  Gameplay -> pure gameplayLoop
  Gameover -> resetGameplay $> gameplayLoop

gameloop
  :: CanGame es => Eff es (Game es)
gameloop = gget >>= \case
  Gameplay -> pure gameplayLoop
  Gameover -> pure gameoverLoop

-- TODO: Should Loop also pass params here?
initGame
  :: The Params es P.Main
  => The Assets es A.Main
  => The ApecsE es World
  => [IOE, SDL_GPU] :>> es
  => Eff es ()
initGame = do
  assets <- theAssets

  liftIO $ do
    _ <- GPU.toggleFullscreen
    GPU.centerAnchor (assets ^. #images % #radioidbullet)

    {-
    let initialIsotope = isotopeFromList
          [ replicate 9 TileEmpty
          , fmap TileIdle [Flammable, Coolant, Metallic, Noble, Radioactive]
            ++ replicate 4 TileEmpty
          ]
-}
{-
    let initialIsotope = fmap TileIdle $ isotopeFromList
          [ [Flammable, Coolant, Metallic, Flammable, Flammable]
          , [Flammable, Flammable, Noble, Flammable, Flammable]
          --, [Flammable, Flammable, Flammable, Flammable, Flammable]
          , [Coolant, Coolant, Coolant, Coolant, Coolant]
          --, [Metallic, Metallic, Metallic, Metallic, Metallic]
          , [Radioactive, Radioactive, Radioactive, Radioactive, Radioactive]
          ]
-}
  resetGameplay

gameoverLoop :: CanGame es => Game es
gameoverLoop = Loop
  { loopTick = tickGameOver
  , loopDraw = drawGameOver
  }

tickGameOver
  :: CanGame es => Eff es ()
tickGameOver = do
  es <- input
  let keyPressed = not $ null $ filter (flip Set.notMember SDL.functionKeycodes) $ keypresses es

  PlayTime clk <- gget

  when (clk > 45 && keyPressed) resetGameplay

  gset (PlayTime (succ clk))

gameplayLoop :: CanGame es => Game es
gameplayLoop = Loop
  { loopTick   = tickGameplay
  , loopDraw   = drawGameplay
  }

tickGameplay
  :: CanGame es => Eff es ()
tickGameplay = do
  params <- theParams
  PlayingArea paBox <- gget
  P.Caldwell{..} <- theViewParams #caldwell

--  logCaldwell

  -- Need the type annotation here to get it compiling..
  cboxes :: CBoxes m <- foldA
    [ cfoldMapM $ \(Position p, bullet, eB) -> do
        pure $ cbSing (NanoTimBulletHit bullet) $ CBoxV
          { shape = c2Circle (v22c2v p) (params ^. #nanotim % #bulletRadius)
          , onHitBy = const Nothing
          , onHit = case bullet of
              BulletUnstable -> Nothing -- Keep on going!
              BulletFlip _ -> Just $ freeEntity eB
          }
    , cfoldMapM $ \cw@Caldwell{} -> caldwellLoop cw $ \r c tileX tileY tile -> do
        pure $ if tileEmpty tile then mempty else cbSing IsotopeHurt $ CBoxV
          { shape = C2AABB' $ C2AABB
                    { min = C2V tileX tileY
                    , max = C2V (tileX + tileSize) (tileY + tileSize)
                    }
          , onHitBy = if tileEmpty tile then const Nothing else \case
              NanoTimBulletHit BulletUnstable -> Just $ do
                randE <- randElement
                let newTile = TileUnstable { element = randE, clock = unstableLen }
                cmap $ \Caldwell{..} ->
                  Caldwell{isotope = isotopeSet r c newTile isotope, ..}
              NanoTimBulletHit (BulletFlip dir) -> Just $ do
                cmap $ \Caldwell{..} ->
                  Caldwell
                  {isotope =
                      flipElems swappableElem (mkTileSwap swapLen) r c dir isotope
                  , ..
                  }
              _ -> Nothing
          , onHit = Nothing
          }
    , cfoldMapM $ \(NanoTim, Position p) -> do
        hb <- theViewParams (#nanotim % #hurtbox)
        pure $ cbSing NanoTimHurt $ CBoxV
          { shape = C2AABB' $ C2AABB
            { min = v22c2v $ (c2v2v2 (hb.min)) + p
            , max = v22c2v $ (c2v2v2 (hb.max)) + p
            }
          , onHitBy = \case
              ReactionHit Flammable -> Just $ cmap $ \NanoTim -> Dead 0
              ReactionHit Coolant -> Just $ do
                P.Coolant{..} <- theViewParams #coolant
                cmap $ \NanoTim -> Frozen freezeDur
              RadioidBulletHit -> Just $ do
                cmap $ \NanoTim -> Dead 0
              _ -> Nothing
          , onHit = Nothing
          }
    -- REACTIONS
    , cfoldMapM $ \(FireRxn _ c) -> do
        x <- col2x c
        pure $ cbSing (ReactionHit Flammable) $ CBoxV
          { shape = C2AABB' $ C2AABB
            { min = C2V x 0
            , max = C2V (x + tileSize) (boxH paBox + topGutter)
            }
          , onHitBy = \_ -> Nothing
          , onHit = Nothing
          }
    , cfoldMapM $ \(IceRxn, Position p) -> do
        P.Coolant{..} <- theViewParams #coolant
        pure $ cbSing (ReactionHit Coolant) $ CBoxV
          { shape = C2AABB' $ C2AABB
            { min = v22c2v (p + hitbox_min)
            , max = v22c2v (p + hitbox_max)
            }
          , onHitBy = \_ -> Nothing
          , onHit = Nothing
          }
    , cfoldMapM $ \(RadioidBullet, Position p, eB) -> do
        P.Radioid{..} <- theViewParams #radioid
        pure $ cbSing RadioidBulletHit $ CBoxV
          { shape = c2Circle (v22c2v p) bulletRadius
          , onHitBy = \_ -> Nothing
          , onHit = Just $ freeEntity eB
          }
    , cfoldMapM $ \MetalRxn -> cfoldMapM $ \(NanoTim, Position (V2 ntx nty)) -> do
        P.Metallic{..} <- theViewParams #metallic
        pure $ cbSing ReactionShield $ CBoxV
          { shape = C2AABB' $ C2AABB
            { min = C2V (ntx + xoffset) (nty + yoffset)
            , max = C2V (ntx + xoffset + hbW) (nty + yoffset + hbH)
            }
          , onHitBy = \case
              RadioidBulletHit -> Just $ pure ()
              _ -> Nothing
          , onHit = Nothing
          }
    ]

  handleCollisions cboxes
  debugCBoxes cboxes

  tickPhysics
  tickRxns
  tickNanoTim
  cmapM_ $ \(NanoTim, Not :: Not Dead) -> tickHalfLife
  tickCaldwell
  cmapM_ $ \(NanoTim, Not :: Not Dead) -> checkDepth
  checkForDeath
  cmapM_ $ \(NanoTim, Not :: Not Dead) ->
    gmodify @PlayTime succ
  pure ()

tickRxns
  :: CanGame es => Eff es ()
tickRxns = do
  cmapM_ $ \(RxnClock clk, ety) ->
    if clk == 0 then freeEntity ety else cset ety (RxnClock $ pred clk)

tickNanoTim
  :: CanGame es => Eff es ()
tickNanoTim = cmapM_ $ \(NanoTim, Not :: Not Dead) -> tickNanoTim'

tickNanoTim'
  :: CanGame es => Eff es ()
tickNanoTim' = do
  P.NanoTim{..} <- theViewParams #nanotim
  keyPressed <- SDL.getKeyboardState

  cmap $ \(NanoTim, (Not :: Not Frozen)) ->
    if | keyPressed SDL.ScancodeA -> Velocity (V2 (-speed) 0)
       | keyPressed SDL.ScancodeD -> Velocity (V2 speed 0)
       | otherwise                -> Velocity (V2 0 0)

  cmap $ \BulletCooldown{..} -> BulletCooldown{clock = cntdown clock}
  cmapM_ $ \(NanoTim, Position ntp, ShootCharge{..}, Not :: Not Frozen, eNT) -> do
    let bulletPos = Position ntp -- TODO: Offset to match NanoTim's barrel
        bulletVel = Velocity (V2 0 (-bulletSpeed))
    if clock == 0
      then do
        ctx <- askCuteSound
        sound <- theViewAssets (#sounds % #nanoshot % #sound)
        ret' <- liftIO $ CS.insertSound ctx sound
        when (ret' == 0) $ liftIO $ do
          -- It'll return 0 when the sound is playing
          CS.errorReason >>= \err -> unless (err == nullPtr) $ peekCString err >>= \s -> putStrLn ("cute_sound err: " ++ s)
        destroyC @ShootCharge eNT
        newEntity_ (bullet, bulletVel, bulletPos)
      else cset eNT (ShootCharge{clock = pred clock, ..})

  cmapM_ $ \(NanoTim, BulletCooldown{..}, Not :: Not ShootCharge, Not :: Not Frozen, eNT) -> do
    let mkB b = cset eNT
          ( ShootCharge { bullet = b, clock = shootCharge }
          , BulletCooldown bulletCooldown
          )

    when (clock == 0) $ do
      if | keyPressed SDL.ScancodeI -> mkB $ BulletFlip UP
         | keyPressed SDL.ScancodeJ -> mkB $ BulletFlip LEFT
         | keyPressed SDL.ScancodeK -> mkB $ BulletFlip DOWN
         | keyPressed SDL.ScancodeL -> mkB $ BulletFlip RIGHT
         | keyPressed SDL.ScancodeSpace -> mkB BulletUnstable
         | otherwise -> pure ()

checkDepth :: CanGame es => Eff es ()
checkDepth = do
  P.Caldwell{depthLimit, topGutter} <- theViewParams #caldwell
  debug_ $ ShapeEntry (pure 0) radioGreen (HorizontalLine $ depthLimit + topGutter)
  cmapM_ $ \Caldwell{depth} -> do
    when (depth > depthLimit) $ do
      cmap $ \NanoTim -> Dead 0

checkForDeath :: CanGame es => Eff es ()
checkForDeath = do
  cmapM_ $ \(NanoTim, Dead n, eNT) -> do
    --logString $ "Dead " ++ show n
    if | n == 0 -> do
           gget >>= \(PlayTime clk) -> gmodify (`max` HighScore clk)
           freeEntity eNT
           cset eNT (Dead (succ n))
       | n < 120 -> cset eNT (Dead (succ n))
       | otherwise -> gset (PlayTime 0) >> gset Gameover

tickPhysics
  :: CanGame es => Eff es ()
tickPhysics = do
  PlayingArea box <- gget
  cmap $ \(Position p, Velocity v, mClamp :: Maybe Clamp, mFreeze :: Maybe Frozen) ->
    let p' = p + v
     in if | isJust mFreeze -> Position p
           | isNothing mClamp -> Position p'
           | pointInBox box p' -> Position p'
           | otherwise -> Position p
  cmapM_ $ \(Position p, mClamp :: Maybe Clamp, ety) -> do
    when (isNothing mClamp && not (pointInBox box p)) $ freeEntity ety

  cmap $ \(Frozen n) -> if n == 0 then Nothing else Just (Frozen (pred n))

tickHalfLife
  :: CanGame es => Eff es ()
tickHalfLife = do
  P.Caldwell{rowWidth, depthLimit} <- theViewParams #caldwell
  P.HalfLife{len} <- theViewParams #halflife
  depthExceeded <- cany $ \Caldwell{..} -> depth > depthLimit

  nobleOn <- cany $ \NobleRxn -> True

  when nobleOn $ cmap $ \HalfLife{..} -> HalfLife{clock = 0, ..}

  unless nobleOn $ do
    cmapM_ $ \currHL@HalfLife{} -> do
      when (currHL ^. #clock >= len && not depthExceeded) $ do
        newRow <- fmap TileIdle <$> randRow rowWidth
        cmap $ \c@Caldwell{} -> c & #isotope %~ addTop newRow
        cmap $ \HalfLife{..} -> HalfLife{clock = 0.0, ..}

    cmap $ \HalfLife{..} -> HalfLife{clock = clock + speed, ..}

tickCaldwell
  :: CanGame es => Eff es ()
tickCaldwell = do
  params@P.Caldwell{..} <- theViewParams #caldwell

  let updateDepth Caldwell{..} =
        Caldwell{depth = calcDepth params isotope, ..}

  let mkMatch e = TileMatch { clock = matchLen, element = e }

  cmapM $ \Caldwell{..} -> do
    i' <- isotopeTraverse (\r c -> tickTile (startReaction r c)) isotope
    pure Caldwell{isotope = i', ..}

  cmap $ \caldwell@Caldwell{} ->
    caldwell
    & #isotope %~ clearBottom (all tileEmpty)
    & #isotope %~ match idleElement mkMatch
    & updateDepth

startReaction :: CanGame es => RowNo -> ColNo -> Element -> Eff es ()
startReaction r c = \case
  Flammable -> do
    P.Flammable{..} <- theViewParams #flammable
    newEntity_
      ( FireRxn r c
      , RxnClock dur
      )
  Coolant -> do
    P.Coolant{..} <- theViewParams #coolant
    p <- cfoldMapM $ caldwellLoop' $ \r' c' tileX tileY _ ->
      if r == r' && c == c'
      then pure $ V2 (Sum tileX) (Sum tileY)
      else pure mempty

    newEntity_
      ( IceRxn
      , Position (fmap getSum p)
      , Velocity (V2 0 speed)
      )
  Metallic -> do
    P.Metallic{..} <- theViewParams #metallic
    newEntity_
      ( MetalRxn
      , RxnClock dur
      )
  Noble -> do
    P.Noble{..} <- theViewParams #noble
    newEntity_
      ( NobleRxn
      , RxnClock dur
      )
  Radioactive -> do
    P.Radioid{..} <- theViewParams #radioid
    p <- cfoldMapM $ caldwellLoop' $ \r' c' tileX tileY _ ->
      if r == r' && c == c'
      then pure $ V2 (Sum tileX) (Sum tileY)
      else pure mempty
    cmapM_ $ \(NanoTim, Position ntp) ->
      newEntity_
      ( RadioidBullet
      , Position (fmap getSum p)
      , Velocity ( bulletSpeed *^ (signorm (towards (getSum <$> p) ntp)) )
      )


drawGameOver
  :: CanGame es => Eff es ()
drawGameOver = do
  screen <- askGPU
  Font font <- theViewAssets (#fonts % #comicmono__GAMEOVER)
  gameoverScreen <- theViewAssets (#images % #gameover)
  highScore <- renderHighScore
  let msg = unlines [highScore, "Press any key to play again"]
  liftIO $ GPU.blit gameoverScreen Nothing screen 0 0
  liftIO $ void $
    FC.draw font screen 0 0 msg

drawGameplay
  :: CanGame es => Eff es ()
drawGameplay = do
  screen <- askGPU
  isDead <- cexists @(NanoTim, Dead)
  liftIO $ when isDead $ GPU.setTargetRGB screen 255 0 0
  drawBackground
  drawCaldwell
  drawNanoTim
  drawRxns
  drawBullets
  drawHalflife
  liftIO $ when isDead $ GPU.unsetTargetColor screen

drawBackground
  :: CanGame es => Eff es ()
drawBackground = do
  screen <- askGPU
  bg <- theViewAssets (#images % #background)
  liftIO $ GPU.blit bg Nothing screen 0 0

drawHalflife
  :: CanGame es => Eff es ()
drawHalflife = do
  screen <- askGPU
  PlayTime clk <- gget

  Font normFont <- theViewAssets (#fonts % #comicmono__HL)
  Font nobleFont <- theViewAssets (#fonts % #comicmono__NOBLE)
  nobleOn <- cany $ \NobleRxn -> True
  let font = if nobleOn then nobleFont else normFont

  let clockOn = if | not (nobleOn) -> True
                   | (clk`div` 20) `mod` 2 == 0 -> True
                   | otherwise -> False
  when clockOn $ do
    P.HalfLife{..} <- theViewParams #halflife
    cmapM_ $ \HalfLife{clock} -> do
      let framesLeft = len - clock
          secsLeft = framesLeft / 60.0
          fmtSec :: String = printf "%.1f" secsLeft
      liftIO $ void $ FC.draw font screen locX locY fmtSec

  scoreP <- theViewParams #score
  drawScore (scoreP ^. #locX) (scoreP ^. #locY)

secsStr :: Frame -> String
secsStr fs =
  let secs = fI fs / 60.0 :: Float
   in printf "%.1fs" secs

renderScore
  :: CanGame es => Eff es String
renderScore = do
  HighScore hs  <- gget
  PlayTime clk <- gget
  let fmtSec :: String = secsStr clk
      fmtHs :: String = secsStr hs
      scoreMsg = unlines
                 [ unwords ["Time:", fmtSec]
                 , unwords ["High:", fmtHs]
                 ]
  pure scoreMsg

renderHighScore
  :: CanGame es => Eff es String
renderHighScore = do
  HighScore hs  <- gget
  let fmtHs :: String = secsStr hs
      scoreMsg = unwords ["High Score:", fmtHs]
  pure scoreMsg

drawScore
  :: CanGame es => Float -> Float -> Eff es ()
drawScore x y = do
  screen <- askGPU
  Font font <- theViewAssets (#fonts % #comicmono__SCORE)
  scoreMsg <- renderScore
  liftIO $ void $ FC.draw font screen x y scoreMsg

drawRxns
  :: CanGame es => Eff es ()
drawRxns = do
  screen <- askGPU
  A.Animations{..} <- theViewAssets #animations
  A.Images{..} <- theViewAssets #images
  cmapM_ $ \(FireRxn _ c, RxnClock clk) -> do
    x <- col2x c
    liftIO $ Animation.renderAt clk Animation.Loop'Always firecolumn $
      \img rect (_ :: V2 Int) ->
        GPU.blit img (Just rect) screen x 0
  cmapM_ $ \(IceRxn, Position (V2 x y)) -> do
    PlayTime clk <- gget
    liftIO $ Animation.renderAt clk Animation.Loop'Always coldcolumn $
      \img rect (_ :: V2 Int) ->
        GPU.blit img (Just rect) screen x y
  cmapM_ $ \(MetalRxn, RxnClock clk) ->
    cmapM_ $ \(NanoTim, Position (V2 x y)) -> do
      P.Metallic{..} <- theViewParams #metallic
      liftIO $ Animation.renderAt clk Animation.Loop'Always metalcolumn $
        \img rect (_  :: V2 Int) ->
          GPU.blit img (Just rect) screen (x + xoffset) (y + yoffset)
  cmapM_ $ \(RadioidBullet, Position (V2 x y)) ->
    liftIO $ GPU.blit radioidbullet Nothing screen x y

drawNanoTim
  :: CanGame es => Eff es ()
drawNanoTim = cmapM_ $ \(NanoTim, Position (V2 x y), msc :: Maybe ShootCharge, mfrz :: Maybe Frozen, eNT) -> do
  isDead <-  existsC @Dead eNT
  unless isDead $ do
    screen <- askGPU
    P.NanoTim{..} <- theViewParams #nanotim
    idleTim <- theViewAssets (#images % #nanotim1)
    scTim <- theViewAssets (#animations % #nanotimshoots)
    liftIO $ case msc of
      Nothing | isJust mfrz ->
                GPU.withColor idleTim frozenBlue $
                GPU.blit idleTim Nothing screen x y
      Nothing -> GPU.blit idleTim Nothing screen x y
      Just ShootCharge{..} ->
        Animation.renderAt clock Animation.Loop'Always scTim $ \img rect (_ :: V2 Int) ->
          GPU.blit img (Just rect) screen x (y - shootSpriteOffset)

drawBullets
  :: CanGame es => Eff es ()
drawBullets = cmapM_ $ \(bullet, Position p@(V2 x y)) -> do
  screen <- askGPU
  A.Images{..} <- theViewAssets #images
  let drawB txt img = do
        debug_ $ TextEntry p txt
        liftIO $ GPU.blit img Nothing screen x y
  case bullet of
    BulletUnstable -> drawB "?" unstablebullet
    BulletFlip RIGHT -> drawB ">" rightbullet
    BulletFlip LEFT -> drawB "<" leftbullet
    BulletFlip UP -> drawB "^" upbullet
    BulletFlip DOWN -> drawB "v" downbullet

drawCaldwell
  :: CanGame es => Eff es ()
drawCaldwell = do
  --screen <- askGPU
  P.Caldwell{..} <- theViewParams #caldwell

  cmapM_ $ \c@Caldwell{..} -> do
    caldwellLoop c $ \_ _ tileX tileY tile -> do
--      debug_ $
--        ShapeEntry (V2 tileX tileY) greenGrey (squareFilled tileSize)
      drawTile tileX tileY tile
      debug_ $ TextEntry (V2 tileX tileY) (tileCode tile)
      debug_ $ ShapeEntry (V2 0 0) radioGreen (HorizontalLine (depth + topGutter))
    {-
    let indexed = isotopeToIndexedList isotope
    for_ indexed $ \(ri, irow) -> do
      for_ irow $ \(ci, tile) -> do
        let tileX = 0 + fI ci * (tileSize + 2*tilePadding)
            tileY = depth - (fI (ri + 1) * (tileSize + 2*tilePadding))

        debug_ $
          ShapeEntry (V2 tileX tileY) greenGrey (squareFilled tileSize)

        debug_ $ TextEntry (V2 tileX tileY) comicmono (tileCode tile)
        debug_ $ ShapeEntry (V2 0 0) radioGreen (HorizontalLine depth)
-}

caldwellLoop'
  :: Monoid r
  => CanGame es
  => (RowNo -> ColNo -> Float -> Float -> Tile -> Eff es r)
  -> Caldwell
  -> Eff es r
caldwellLoop' = flip caldwellLoop

caldwellLoop
  :: Monoid r
  => CanGame es
  => Caldwell
  -> (RowNo -> ColNo -> Float -> Float -> Tile -> Eff es r)
  -> Eff es r
caldwellLoop Caldwell{..} f = do
  (PlayingArea (Box boxV1 boxV2)) <- gget
  P.Caldwell{..} <- theViewParams #caldwell
  debug_ $ ShapeEntry (pure 0) radioGreen (Rectangle boxV1 boxV2)
  let indexed = isotopeToIndexedList isotope
  foldForA indexed $ \(ri, r, irow) -> foldForA irow $ \(_, c, a) -> do
    tileX <- col2x c --0 + fI ci * (tileSize + 2*tilePadding) + sideGutter
    let tileY = depth - (fI (ri + 1) * (tileSize + 2*tilePadding)) + topGutter
    f r c tileX tileY a

col2x :: CanGame es => ColNo -> Eff es Float
col2x (ColNo c) = do
  P.Caldwell{..} <- theViewParams #caldwell
  pure $ 0 + fI c * (tileSize + 2*tilePadding) + sideGutter

greenGrey :: GPU.Color
greenGrey = GPU.Color 92 94 92 255

radioGreen :: GPU.Color
radioGreen = GPU.Color 52 250 105 255

white :: GPU.Color
white = GPU.Color 255 255 255 255

frozenBlue :: GPU.Color
frozenBlue = GPU.Color 94 157 224 255

calcDepth :: P.Caldwell -> Isotope Tile -> Float
calcDepth P.Caldwell{..} i =
  fI (isotopeLen i) * (tileSize + (2*tilePadding))

-- worse is better - just nuke all dynamic n-ary components to ensure
-- we aren't leaking
freeEntity :: CanGame es => Entity -> Eff es ()
freeEntity ety = traverse_ ($ ety)
  [ destroyC @Position
  , destroyC @Velocity
  , destroyC @Clamp
  , destroyC @Bullet
  , destroyC @RxnClock
  , destroyC @FireRxn
  , destroyC @IceRxn
  , destroyC @RadioidBullet
  , destroyC @MetalRxn
  , destroyC @NobleRxn
  , destroyC @Frozen
  ]

drawTile :: CanGame es => Float -> Float -> Tile -> Eff es ()
drawTile x y tile = do
  screen <- askGPU
  case tile of
    TileEmpty -> pure ()
    TileIdle e -> do
      elemImg <- getElemImg e
      liftIO $ GPU.blit elemImg Nothing screen x y
    TileSwap{..} -> do
      P.Caldwell{..} <- theViewParams #caldwell
      for_ currElement $ \e -> do
        elemImg <- getElemImg e
        let (RowNo dr, ColNo dc) = dir2delta dir (0, 0)
            delta :: (V2 Float) = V2 (fI dc) (fI (-dr))
            pct :: Float = 1 - (fI clock / fI swapLen)
            V2 swapX swapY = (V2 x y) + tileSize *^ delta ^* pct
        -- TODO: Something is wrong with the swap logic..maybe the gutter?
{-        logString $ unwords
          [ "e:", show e
          , "x:", show x
          , "y:", show y
          , "dir:", show dir
          , "dr:", show dr
          , "dc:", show dc
          , "delta:", show delta
          , "swapX:", show swapX
          , "swapY:", show swapY
          ]
        when (clock == 0) $ logString "--- About to swap! ---" -}
        liftIO $ GPU.blit elemImg Nothing screen swapX swapY
    TileMatch{..} -> do
      decayAnim <- getDecayAnim element
      liftIO $ Animation.renderAt clock Animation.Loop'Always decayAnim $ \img rect (_ :: V2 Int) ->
        GPU.blit img (Just rect) screen x y
    TileUnstable{..} -> do
      elemImg <- getElemImg (fakeRandElement clock)
      liftIO $ GPU.blit elemImg Nothing screen x y

getDecayAnim :: CanGame es => Element -> Eff es Animation.Script
getDecayAnim = \case
  Flammable -> theViewAssets (#animations % #firedecay)
  Coolant -> theViewAssets (#animations % #colddecay)
  Metallic -> theViewAssets (#animations % #metaldecay)
  Radioactive -> theViewAssets (#animations % #radiationdecay)
  Noble -> theViewAssets (#animations % #nobledecay)

getElemImg :: CanGame es => Element -> Eff es (Ptr GPU.Image)
getElemImg = \case
  Flammable -> theViewAssets (#images % #fire_tile1)
  Coolant -> theViewAssets (#images % #cold_tile1)
  Metallic -> theViewAssets (#images % #metal_tile1)
  Radioactive -> theViewAssets (#images % #radiation_tile1)
  Noble -> theViewAssets (#images % #noble_tile1)


resetGameplay
  :: The Params es P.Main
  => The ApecsE es World
  => [IOE, SDL_GPU] :>> es
  => Eff es ()
resetGameplay = do
  screen <- askGPU
  params <- theParams
  screenW <- liftIO (screen *-> Target.w)
  screenH <- liftIO (screen *-> Target.h)

  gset Gameplay

  clearAllC @Position
  clearAllC @Velocity
  clearAllC @Bullet
  clearAllC @BulletCooldown
  clearAllC @RxnClock
  clearAllC @FireRxn
  clearAllC @IceRxn
  clearAllC @RadioidBullet
  clearAllC @MetalRxn
  clearAllC @NobleRxn
  clearAllC @Frozen
  clearAllC @Dead

  gset $ PlayingArea $ Box
    (V2 (params ^. #caldwell % #sideGutter) (params ^. #caldwell % #topGutter))
    (V2 (fI screenW - params ^. #caldwell % #sideGutter) (fI screenH))

  gset (PlayTime 0)

  initialRow <- fmap TileIdle <$> randRow (params ^. #caldwell % #rowWidth)
  let initialIsotope = addTop initialRow mempty

  _ <- newEntity $
       ( NanoTim
       , Position (params ^. #nanotim % #startLoc)
       , BulletCooldown 0
       , Clamp
       )
  _ <- newEntity $ Caldwell
       { isotope = initialIsotope
       , depth = calcDepth (params ^. #caldwell) initialIsotope
       }
  _ <- newEntity $ HalfLife
       { speed = params ^. #halflife % #initialSpeed
       , clock = 0.0
       }
  pure ()
