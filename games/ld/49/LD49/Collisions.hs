module LD49.Collisions
  ( module LD49.Collisions
  , module CuteC2
  ) where

import Control.Monad (unless, when)
import Data.Foldable (for_)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.DList (DList)
import qualified Data.DList as DList

import CuteC2
import Optics
import qualified SDL.GPU.Utils as GPU

import Mayhem.Engine.Effects.Debug
import Mayhem.Engine.Utils
import Mayhem.Engine.Loop hiding (Map)

import LD49.World
import LD49.Isotope

data CBoxK =
    NanoTimHurt
  | NanoTimBulletHit Bullet
  | RadioidHit
  | RadioidHurt
  | RadioidBulletHit
  | IsotopeHurt
  | ReactionHit Element
  | ReactionShield
  deriving (Eq, Ord, Show)

data CBoxV es = CBoxV
  { shape :: C2Shape
  , onHit :: Maybe (Eff es ())
  , onHitBy :: CBoxK -> Maybe (Eff es ())
  }

newtype CBoxes m = CBoxes (Map CBoxK (DList (CBoxV m)))

cbSing :: CBoxK -> CBoxV m -> CBoxes m
cbSing k v = CBoxes $ Map.singleton k (pure v)

instance Semigroup (CBoxes m) where
  (CBoxes x) <> (CBoxes y) = CBoxes $ Map.unionWith (<>) x y

instance Monoid (CBoxes m) where
  mempty = CBoxes mempty

handleCollisions :: [Debug, Log] :>> es => CBoxes es -> Eff es ()
handleCollisions (CBoxes boxes) = do
  let kvs = Map.toList boxes
  for_ kvs $ \(_, vsee) ->
    for_ kvs $ \(ker, vser) -> do
      let didCollide v1 v2 = c2Collided (shape v1) (shape v2)
      let pairs = do
            vee <- DList.toList vsee
            ver <- DList.toList vser
            pure (vee, ver)

      for_ pairs $ \(vee, ver) -> do
        let dc = didCollide vee ver -- laziness baby!
        when dc $ do
{-          when (kee /= ker) $
            logString $ unwords
            ["oi"
            , "kee:", show kee, show $ isJust (onHit ver)
            , "ker:", show ker, show $ isJust (onHitBy vee ker)
            ] -}
          case onHitBy vee ker of
            Just callback -> do
              callback
              sequence_ (onHit ver)
            Nothing -> pure ()

debugCBoxes :: Debug :> es => CBoxes es -> Eff es ()
debugCBoxes (CBoxes kvs) = for_ kvs $ \boxes ->
  for_ boxes $ \CBoxV{shape} -> case shape of
    C2Poly'{} -> error "polys arent ready yet dummy"
    C2Capsule'{} -> error "capsules arent ready yet dummy"
    C2Circle' C2Circle{..} ->
      debug_ $
        ShapeEntry (c2v2v2 p) hbred $
        Circle r
    C2AABB' C2AABB{..} ->
      debug_ $
        ShapeEntry (pure 0) hbred $
        RectangleFilled (c2v2v2 min) (c2v2v2 max)

hbred :: GPU.Color
hbred = GPU.Color 250 52 52 155
