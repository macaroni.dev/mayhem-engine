module LD49.Repl where

import Data.Void
import Data.Function (fix)
import Control.Monad.IO.Class (MonadIO(..))
import Control.Concurrent.MVar
import Control.Concurrent.Async
import Text.Read (readEither)
import qualified System.Console.Haskeline as HL

import LD49.Direction

newtype Repl = Repl
  { cmdVar :: MVar Cmd
  }

withRepl :: (Repl -> IO r) -> IO r
withRepl f = do
  cmdVar <- newEmptyMVar
  let repl = Repl{..}
  withAsync (runRepl repl) $ \_ -> f repl

runRepl :: Repl -> IO ()
runRepl Repl{..} = HL.runInputT HL.defaultSettings $ fix $ \loop -> do
  HL.getInputLine ">>= " >>= \case
    Nothing -> pure ()
    Just line -> case readEither @Cmd line of
      Left err -> do
        HL.outputStrLn err
        loop
      Right cmd -> do
        liftIO $ putMVar cmdVar cmd
        loop

nextCmd :: MonadIO m => Repl -> m (Maybe Cmd)
nextCmd Repl{..} = liftIO $ tryTakeMVar cmdVar

data Cmd =
    CmdFlip RowNo ColNo Direction
  | CmdUnstable RowNo ColNo
  | CmdCaldwell
  deriving (Show, Read)
