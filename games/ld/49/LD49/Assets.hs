{-# LANGUAGE TemplateHaskell #-}

{-# OPTIONS_GHC -ddump-splices -ddump-to-file #-}
module LD49.Assets where

import GHC.Generics

import qualified SDL.GPU.Utils as GPU
import qualified Cute.Sound.C as CS

import qualified Mayhem.Engine.Animation as Animation
import Mayhem.Engine.Assets
import Mayhem.Engine.Utils
import Mayhem.Engine.TH

data Main = Main
  { animations :: Animations
  , fonts :: Fonts
  , images :: Images
  , sounds :: Sounds
  }
  deriving stock Generic
  deriving anyclass Asset

data Sounds = Sounds
  { nanoshot :: CuteSound
  }
  deriving stock Generic
  deriving anyclass Asset

data Fonts = Fonts
  { comicmono :: Font 16 ('Color 255 255 255 255)
  , comicmono__HL :: Font 70 ('Color 255 72 0 255)
  , comicmono__NOBLE :: Font 70 ('Color 255 218 97 255)
  , comicmono__GAMEOVER :: Font 40 ('Color 255 255 255 255)
  , comicmono__SCORE :: Font 20 ('Color 20 59 57 255)
  }
  deriving stock Generic
  deriving anyclass Asset

data Images = Images
  { cold_tile1 :: Ptr GPU.Image
  , fire_tile1 :: Ptr GPU.Image
  , metal_tile1 :: Ptr GPU.Image
  , radiation_tile1 :: Ptr GPU.Image
  , noble_tile1 :: Ptr GPU.Image
  , background :: Ptr GPU.Image
  , gameover :: Ptr GPU.Image
  , downbullet :: Ptr GPU.Image
  , upbullet :: Ptr GPU.Image
  , leftbullet :: Ptr GPU.Image
  , rightbullet :: Ptr GPU.Image
  , radioidbullet :: Ptr GPU.Image
  , unstablebullet :: Ptr GPU.Image
  , nanotim1 :: Ptr GPU.Image
  }
  deriving stock Generic
  deriving anyclass Asset

data Animations = Animations
  { coldcolumn :: Animation.Script
  , colddecay :: Animation.Script
  , firecolumn :: Animation.Script
  , firedecay :: Animation.Script
  , metalcolumn :: Animation.Script
  , metaldecay :: Animation.Script
  , nobledecay :: Animation.Script
  , radiationdecay :: Animation.Script
  , nanotimshoots :: Animation.Script
  }
  deriving stock Generic
  deriving anyclass Asset

-- TH_CODE
mkOpticsLabels ''Main
mkOpticsLabels ''Fonts
mkOpticsLabels ''Images
mkOpticsLabels ''Animations
mkOpticsLabels ''Sounds
