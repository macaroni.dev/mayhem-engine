module LD49.GHCi (module LD49.GHCi, module X) where

import LD49.Game as X (ghciMain)
import LD49.Isotope as X
import LD49.World as X

import Mayhem.Engine.Effects as X
import Mayhem.Engine.Effects.GHCi as X
import Mayhem.Engine.Effects.Stepper as X

-- TODO: foreign-store + GHCiPipe to persist it between reloads.
-- Then we can potentially stuff game logic through for hot-reloading
