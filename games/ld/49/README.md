# Nuclear Puzzle Defense

*(This is a mirror of the [ldjam listing](https://ldjam.com/events/ludum-dare/49/nuclear-puzzle-defense).)*

> 3,000 years ago, a catastrophic event at the Caldwell Reactor initiated an unstoppable nuclear reaction. Thanks to the efforts of a single still-functioning droid, disaster continues to be kept at bay.

![splash.png](splash.png)

Help `NANOTIM` prevent untold destruction by swapping and matching tiles, and watch out for any potential ***reactions***!

## How to Play
![instructions.png](instructions.png)

Matching different tiles yields different effects:

![firedecay.gif](firedecay.gif)
> **Flammable** tiles combust in a pillar of flame.

![colddecay.gif](colddecay.gif)
> **Subzero** tiles rain down a chilling cloud.

![radiationdecay.gif](radiationdecay.gif)
> **Radioactive** tiles emit volatile particles.

![metaldecay.gif](metaldecay.gif)
> **Metallic** tiles summon a shield to protect `NANOTIM` from radioactive particles.

![nobledecay.gif](nobledecay.gif)
> **Noble** tiles reset and temporarily stop the reactor's radioactive decay.

## About Us

We are a husband and wife studio that someday hopes to make games full-time.

**Nuclear Puzzle Defense** was built in a custom Haskell 2D game engine we have been [iterating](https://ldjam.com/events/ludum-dare/47/echoes-of-ouroboros) on as we have been [delving](https://ldjam.com/events/ludum-dare/48/im-definitely-not-scared-of-my-own-basement) into Ludum Dare. The art was hand-drawn and animated in Clip Studio Paint.
