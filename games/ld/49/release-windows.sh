#! /usr/bin/env bash

set -ex

t=$(date --rfc-3339=seconds | sed 's/ /-/g' | sed 's/:/-/g')
g=$(git rev-parse --short HEAD)
outdir="jam-windows-$t-$g"
releasework="releases/jam-windows-$t-$g"
releasedir="$releasework/NuclearPuzzleDefense"

nix-build -A projectCross.mingwW64.hsPkgs.mayhem-engine.components.exes.ld49 -o "$outdir" ../../../default.nix

mkdir -p "$releasedir"/assets

cp "$outdir"/bin/*.exe "$releasedir"
cp "$outdir"/bin/*.dll "$releasedir"

rm "$outdir"

cp -r assets/* "$releasedir/assets/"

# ghc-pkg doesn't help Nix discover the dependencies on these dlls, so we manually copy them
# Bumped haskell.nix and maybe SDL2_ttf is now auto-bundled?
cp -Pn "$(nix-build --no-out-link --expr '(import ../../../nix/pkgs.nix).pkgsCross.mingwW64.SDL2_ttf')/bin"/*.dll "$releasedir"
cp -Pn "$(nix-build --no-out-link --expr '(import ../../../nix/pkgs.nix).pkgsCross.mingwW64.zlib')/bin"/*.dll "$releasedir"

chmod +w -R "$releasedir"

pushd "$releasework"
zip -r NuclearPuzzleDefense.zip NuclearPuzzleDefense
popd

echo "Built $releasedir"
