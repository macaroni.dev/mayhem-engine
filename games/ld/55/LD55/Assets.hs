module LD55.Assets where

import GHC.Generics
import Mayhem.Engine.Assets
import qualified SDL.GPU.Utils as GPU
import Mayhem.Engine.Animation qualified as Animation
import LD55.Summon

data Main = Main
  { fonts :: Fonts
  , gamecontrollerdb :: TextFilePath
  , sprites :: Sprites
  , sfx :: Sfx
  }
  deriving stock Generic
  deriving anyclass Asset

data Fonts = Fonts
  { debug :: Font 24 ('Color 255 255 255 255) -- ComicMono for now
  , hotkeys :: Font 16 ('Color 255 255 255 255) -- ComicMono for now
  }
  deriving stock Generic
  deriving anyclass Asset

data Sfx = Sfx
  { cannot_summon :: CuteSound
  , chewed :: CuteSound
  , missile :: CuteSound
  , summon_bar_hit :: CuteSound
  , summon_bar_miss :: CuteSound
  }
  deriving stock Generic
  deriving anyclass Asset

data Sprites = Sprites
  { -- Summon__FireS.png
    -- Summon__FireM.png
    -- Summon__FireL.png
    --
    -- Summon__ColdS.png
    -- Summon__ColdM.png
    -- Summon__ColdL.png
    --
    -- Summon__CheeseS.png
    -- Summon__CheeseM.png
    -- Summon__CheeseL.png
    _Summon :: Summon -> Ptr GPU.Image
  , _Castle :: Ptr GPU.Image
  , _Maus :: Ptr GPU.Image
  , _Maus2 :: Ptr GPU.Image
  , _MausShoot :: Ptr GPU.Image
  , _MausL :: Ptr GPU.Image
  , _Maus2L :: Ptr GPU.Image
  , _MausShootL :: Ptr GPU.Image
  , _Ant :: Ptr GPU.Image
  , _AntAggro :: Ptr GPU.Image
  , _AntSlow :: Ptr GPU.Image
  , _AntFrozen :: Ptr GPU.Image
  , _AntWeak :: Ptr GPU.Image
  , _Blanket :: Ptr GPU.Image
  , _Grass :: Ptr GPU.Image
  , _Loader :: Ptr GPU.Image
  , _Loader2 :: Ptr GPU.Image
  , _Loader3 :: Ptr GPU.Image
  }
  deriving stock Generic
  deriving anyclass Asset
