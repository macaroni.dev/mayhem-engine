module LD55.Summon where

import Mayhem.Engine.Frame
import Mayhem.Engine.Utils ((//))
import Mayhem.Engine.LinearDotSyntax ()
import Data.List.NonEmpty (nonEmpty, NonEmpty (..))
import Data.Foldable
import Data.List.NonEmpty.Extra (maximum1)
import Data.Function (on)
import Apecs qualified
import Data.Monoid (Sum (..))
import SDL.GPU.Utils qualified as GPU
import Linear.V2
import Control.Monad.IO.Unlift
import Foreign.Ptr

newtype SP = SP Int
  deriving stock (Show, Eq, Ord)
  deriving newtype (Num)
  deriving (Semigroup, Monoid) via (Sum Int)

instance Apecs.Component SP where type Storage SP = Apecs.Global SP

data Summon =
    FireS
  | FireM
  | FireL
  | ColdS
  | ColdM
  | ColdL
  | CheeseS
  | CheeseM
  | CheeseL
  deriving stock (Eq, Ord, Show, Read, Enum, Bounded)

summonCode :: Summon -> String
summonCode smn = take 2 (show smn) ++ take 1 (reverse (show smn))

summonCost :: Summon -> SP
summonCost = \case
  FireS -> 1
  FireM -> 2
  FireL -> 3
  ColdS -> 1
  ColdM -> 2
  ColdL -> 3
  CheeseS -> 1
  CheeseM -> 2
  CheeseL -> 3

data SummonBarTarget = SummonBarTarget | SummonBarMiss | SummonBarHit
  deriving stock (Eq, Show)

data SummonBar = SummonBar
  { targets :: NonEmpty (Frame, Frame)
  , hits :: [(Frame, Frame)]
  , clock :: Frame
  } deriving stock (Eq, Show)

-- Is this the way?
instance Apecs.Component SummonBar where type Storage SummonBar = Apecs.Unique SummonBar

data SummonBarResult =
    SummonBarFailed
  | SummonBarSuccess
  | SummonBarContinue SummonBar
  deriving stock (Eq, Show)

inRange :: Ord a => (a, a) -> a -> Bool
inRange (l,u) x = l <= x && x <= u

summonBarStep
  :: Bool -- ^ click on this frame?
  -> SummonBar
  -> SummonBarResult
summonBarStep clicked SummonBar{..} =
  let (l,u) :| rest = targets in
  if | clicked && inRange (l,u) clock ->
       case nonEmpty rest of
         Nothing -> SummonBarSuccess
         Just nextTargets -> SummonBarContinue $ SummonBar
           { targets = nextTargets
           , hits = (l,u) : hits
           , clock = succ clock
           }
     | clicked && not (inRange (l,u) clock) -> SummonBarFailed
     | clock > u -> SummonBarFailed
     | otherwise -> SummonBarContinue $ SummonBar {clock = succ clock, ..}

debugSummonBar :: MonadUnliftIO m => Ptr GPU.Target -> V2 Float -> Float -> Float -> SummonBar -> m ()
debugSummonBar screen anchor totalLen barHeight SummonBar{..} = do
  let bh = barHeight / 2
  let lth = barHeight / 5
  let barLen = maximum1 $ fmap snd targets
  let sc x = fromIntegral x * (totalLen / fromIntegral barLen) + anchor.x
  let scaledTgts = fmap (\(l,u) -> (sc l, sc u)) targets
  let scaledHits = fmap (\(l,u) -> (sc l, sc u)) hits
  GPU.withLineThickness lth $ GPU.line screen anchor.x anchor.y (anchor.x + totalLen) anchor.y (GPU.greyN 0.5)
  for_ scaledTgts $ \(l,u) ->
    GPU.rectangleFilled screen l (anchor.y - bh) u (anchor.y + bh) GPU.white
  for_ scaledHits $ \(l,u) ->
    GPU.rectangleFilled screen l (anchor.y - bh) u (anchor.y + bh) GPU.green
  GPU.withLineThickness lth $ GPU.line screen (sc clock) (anchor.y - bh) (sc clock) (anchor.y + bh) GPU.red

summonSummonBar :: Summon -> SummonBar
summonSummonBar = \case
  FireS -> easySummonBar
  FireM -> mediumSummonBar
  FireL -> hardSummonBar
  ColdS -> easySummonBar
  ColdM -> mediumSummonBar
  ColdL -> hardSummonBar
  CheeseS -> easySummonBar
  CheeseM -> mediumSummonBar
  CheeseL -> hardSummonBar

easySummonBar :: SummonBar
easySummonBar = SummonBar
  { targets = (20, 40) :| []
  , hits = []
  , clock = 0
  }

mediumSummonBar :: SummonBar
mediumSummonBar = SummonBar
  { targets = (20, 35) :| [(45, 60)]
  , hits = []
  , clock = 0
  }

hardSummonBar :: SummonBar
hardSummonBar = SummonBar
  { targets = (20, 27) :| [(35, 42), (53, 60)]
  , hits = []
  , clock = 0
  }
