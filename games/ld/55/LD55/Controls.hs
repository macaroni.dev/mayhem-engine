module LD55.Controls where

import SDL qualified

import Control.Monad
import Data.Traversable
import Mayhem.Engine.Controller
import Mayhem.Engine.Effects
import Mayhem.Engine.Loop
import Mayhem.Engine.Utils
import Mayhem.Engine.Frame
import Mayhem.Engine.Assets qualified
import Data.Foldable
import Linear (V2 (..), normalize, (^*))
import Linear.Affine (Point(..))
import SDL.GPU.Utils qualified as GPU
import SDL.GPU.CuteC2 qualified as GPU
import SDL.GPU.FC.Simple qualified as FC
import Cleff
import Data.Map.Strict qualified as Map
import CuteC2
import qualified Cute.Sound.C as CS

import LD55.M
import LD55.World
import LD55.Params qualified as P
import LD55.Assets qualified as A
import LD55.Summon
import LD55.Renderer

data C = C
  { loc :: V2 Float
  , selectButton :: Button
  , cancelButton :: Button
  , hotkeys :: Summon -> Button
  } deriving stock (Show)

parseController
  :: [IOE, SDL_GPU] :>> es
  => The Params es P.Main
  => [SDL.Event]
  -> Eff es C
parseController es = gpuly $ paramly $ do
  absLoc <- fmap fromIntegral <$> SDL.getAbsoluteMouseLocation
  P loc <- liftIO $ GPU.screen2virtual ?screen absLoc
  selectButton <- mouseButton SDL.ButtonLeft es
  cancelButton <- mouseButton SDL.ButtonRight es
  hotkeyMapping <- fmap Map.fromList $ for [FireS ..] $ \smn -> (smn,) <$> scancodeButton (?params.hotkeyBindings smn) es
  let hotkeys = \smn -> hotkeyMapping Map.! smn
  pure C{..}

buttonAABB :: C2AABB
buttonAABB = C2AABB (C2V 0 0) (C2V 30 30)

moveAABB :: V2 Float -> C2AABB -> C2AABB
moveAABB (v22c2v -> off) (C2AABB mn mx) = C2AABB (c2Add off mn) (c2Add off mx)

summonButtons :: [(Summon, C2AABB)]
summonButtons = zipWith3 mk [FireS ..] (repeat buttonAABB) offs
  where
    mk smn box off = (smn, moveAABB off box)
    offs = do
      y <- take 3 $ iterate (+40) 355
      x <- take 3 $ iterate (+40) 635
      pure $ V2 x y

debugSummonButtons :: Eff (Framed LD55) ()
debugSummonButtons = gpuly $ assetly $ for_ summonButtons $ \(smn, box) -> do
  render'OnLayer HUD $ do
    GPU.c2AABBFilled ?screen box (GPU.transparent 0.5 GPU.yellow)
    void $ FC.drawScale
      ?assets.fonts.debug.rawFont
      ?screen
      box.min.x
      box.min.y
      (FC.Scale 0.75 0.75)
     (summonCode smn)

trySummon :: Summon -> Eff (Framed LD55) ()
trySummon smn = do
  let cost = summonCost smn
  sp <- gget @SP
  if | cost <= sp -> do
         cmap $ \NotEnoughSP{} -> Nothing :: Maybe NotEnoughSP
         gmodify (subtract cost)
         gset (Input'Summon smn)
     | otherwise -> assetly $ paramly $ do
         cs_ctx <- askCuteSound
         _ <- liftIO $ CS.insertSound cs_ctx ?assets.sfx.cannot_summon.sound
         newEntity_ $ NotEnoughSP $ FrameDown ?params.notEnoughSPDuration

handleControls :: C -> Eff (Framed LD55) ()
handleControls C{..} = paramly $ assetly $ gget @InputMode >>= \case
  Input'Select -> do
    for_ [FireS ..] $ \smn ->
      when (hotkeys smn).wasPressed $ trySummon smn
    when selectButton.wasPressed $ do
      let pressed = flip find summonButtons $ \(_, box) -> c2Collided (c2Circle (v22c2v loc) 1) (C2AABB' box)
      case pressed of
        Just (smn, _) -> trySummon smn
        Nothing -> do
          ucget @MissileCooldown >>= \case
            Nothing -> do
              cs_ctx <- askCuteSound
              _ <- liftIO $ CS.insertSound cs_ctx ?assets.sfx.missile.sound
              newEntity_
                ( Missile
                , Position ?params.castleCenter
                , Velocity $ normalize (loc - ?params.castleCenter) ^* ?params.missileSpeed
                )
              newEntity_ $ MissileCooldown $ FrameDown ?params.missileCooldown
            Just {} -> pure ()
  Input'Summon smn -> do
    -- We use SP before this so....might as well force them to summon
    --when cancelButton.wasPressed $ do
    --gset Input'Select
    when selectButton.wasPressed $ do
      gset $ Input'SummonBar (summonSummonBar smn) smn (Position loc)
  Input'SummonBar bar smn p -> do
    case summonBarStep selectButton.wasPressed bar of
      SummonBarFailed -> do
          cs_ctx <- askCuteSound
          _ <- liftIO $ CS.insertSound cs_ctx ?assets.sfx.summon_bar_miss.sound
          gset Input'Select
      SummonBarSuccess -> do
        cs_ctx <- askCuteSound
        _ <- liftIO $ CS.insertSound cs_ctx ?assets.sfx.summon_bar_hit.sound
        newEntity_
          ( Summoned smn
          , SummonDuration $ FrameDown $ ?params.summonDuration smn
          , p
          )
        gset Input'Select
      SummonBarContinue nextBar -> do
        when (length nextBar.hits /= length bar.hits) $ do
          cs_ctx <- askCuteSound
          _ <- liftIO $ CS.insertSound cs_ctx ?assets.sfx.summon_bar_hit.sound
          pure ()
        gset $ Input'SummonBar nextBar smn p
