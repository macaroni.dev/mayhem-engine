{-# LANGUAGE TemplateHaskell #-}

module LD55.World where

import Apecs
import Linear
import LD55.Summon
import Data.Monoid (Sum (..))
import Mayhem.Engine.Frame

data Screen = Screen'Title | Screen'Tutorial | Screen'Tutorial2 | Screen'Playing | Screen'Gameover deriving stock (Eq, Show)

instance Semigroup Screen where
  x <> _ = x

-- Unlawful
instance Monoid Screen where
  mempty = Screen'Title

instance Component Screen where type Storage Screen = Global Screen

newtype Position = Position { unPosition :: (V2 Float) } deriving stock (Eq, Show)
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity { unVelocity :: (V2 Float) }
  deriving stock (Eq, Show)

instance Component Velocity where type Storage Velocity = Map Velocity

data InputMode = Input'Select | Input'Summon Summon | Input'SummonBar SummonBar Summon Position
  deriving stock (Eq, Show)

instance Semigroup InputMode where
  x <> _ = x
-- Unlawful
instance Monoid InputMode where
  mempty = Input'Select
instance Component InputMode where type Storage InputMode = Global InputMode


data Summoned = Summoned Summon deriving stock (Eq, Show)
instance Component Summoned where type Storage Summoned = Map Summoned

data SummonDuration = SummonDuration FrameDown deriving stock (Eq, Show)
instance Component SummonDuration where type Storage SummonDuration = Map SummonDuration

data Enemy = Enemy deriving stock (Eq, Show)
instance Component Enemy where type Storage Enemy = Map Enemy

data AimedAt = AimedAt Entity deriving stock (Eq, Show)
instance Component AimedAt where type Storage AimedAt = Map AimedAt

data TestBullet = TestBullet { aimedAt :: Entity } deriving stock (Eq, Show)
instance Component TestBullet where type Storage TestBullet = Map TestBullet

data MissileCooldown = MissileCooldown FrameDown deriving stock (Show, Eq)
instance Component MissileCooldown where type Storage MissileCooldown = Unique MissileCooldown

data Missile = Missile deriving stock (Show, Eq)
instance Component Missile where type Storage Missile = Map Missile

data NotEnoughSP = NotEnoughSP FrameDown deriving stock (Show, Eq)
instance Component NotEnoughSP where type Storage NotEnoughSP = Unique NotEnoughSP

data Chewing = Chewing Frame deriving stock (Show, Eq)
instance Component Chewing where type Storage Chewing = Map Chewing

newtype ChewSfx = ChewSfx { nextSfx :: Frame }
  deriving stock (Show, Eq)
  deriving newtype (Num)
  deriving (Semigroup, Monoid) via (Sum Frame)

instance Component ChewSfx where type Storage ChewSfx = Global ChewSfx

newtype Health = Health Int
  deriving stock (Show, Eq)
  deriving newtype (Num)
  deriving (Semigroup, Monoid) via (Sum Int)

instance Component Health where type Storage Health = Map Health

newtype CastleHealth = CastleHealth Int
  deriving stock (Show, Eq)
  deriving newtype (Num)
  deriving (Semigroup, Monoid) via (Sum Int)

instance Component CastleHealth where type Storage CastleHealth = Global CastleHealth


newtype Score = Score Int
  deriving stock (Show, Eq)
  deriving newtype (Num)
  deriving (Semigroup, Monoid) via (Sum Int)

instance Component Score where type Storage Score = Global Score

-- Shots
data ShotType =
    Fireball1
  | Fireball2
  | Fireball3
  | SlowShot
  | AggroShot Position
  | FreezeShot
  deriving stock (Show, Eq)

data Shot = Shot ShotType deriving stock (Show, Eq)
instance Component Shot where type Storage Shot = Map Shot

data ShotCooldown = ShotCooldown FrameDown deriving stock (Show, Eq)
instance Component ShotCooldown where type Storage ShotCooldown = Map ShotCooldown

data Explosion = Explosion FrameDown deriving stock (Show, Eq)
instance Component Explosion where type Storage Explosion = Map Explosion

-- Status effects
data Slowed = Slowed FrameDown deriving stock (Show, Eq)
instance Component Slowed where type Storage Slowed = Map Slowed

data Frozen = Frozen FrameDown deriving stock (Show, Eq)
instance Component Frozen where type Storage Frozen = Map Frozen

data Aggro'd = Aggro'd Position FrameDown deriving stock (Show, Eq)
instance Component Aggro'd where type Storage Aggro'd = Map Aggro'd

data Weakened = Weakened FrameDown deriving stock (Show, Eq)
instance Component Weakened where type Storage Weakened = Map Weakened

makeWorld "World"
  [ ''Position
  , ''Velocity
  , ''InputMode
  , ''Summoned
  , ''SummonDuration
  , ''Enemy
  , ''TestBullet
  , ''MissileCooldown
  , ''Missile
  , ''Chewing
  , ''CastleHealth
  , ''SP
  , ''Health
  , ''AimedAt
  , ''Shot
  , ''ShotCooldown
  , ''Slowed
  , ''Frozen
  , ''Aggro'd
  , ''Weakened
  , ''Explosion
  , ''ChewSfx
  , ''NotEnoughSP
  , ''Score
  , ''Screen
  ]

