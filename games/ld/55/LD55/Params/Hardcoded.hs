module LD55.Params.Hardcoded where

import SDL qualified
import Control.Monad.IO.Class
import Linear.V2
import CuteC2
import SDL.GPU.Simple qualified as GPU
import SDL.GPU.Utils.Colors qualified as GPU

import LD55.Params qualified as P
import LD55.World
import LD55.Summon

hardcoded :: MonadIO m => m P.Main
hardcoded = pure $ P.Main
  { hotkeyBindings = \case
      FireS -> SDL.Scancode1
      FireM -> SDL.Scancode2
      FireL -> SDL.Scancode3
      ColdS -> SDL.ScancodeQ
      ColdM -> SDL.ScancodeW
      ColdL -> SDL.ScancodeE
      CheeseS -> SDL.ScancodeA
      CheeseM -> SDL.ScancodeS
      CheeseL -> SDL.ScancodeD
  , hotkeyColor = \case
      ColdS -> GPU.black
      ColdM -> GPU.black
      ColdL -> GPU.black
      _ -> GPU.black
  , testBulletSpeed = 1
  , castleCenter = V2 400 250
  , castleRadius = 40
  , antSpeed = 0.5
  , missileSpeed = 10
  , missileCooldown = 30
  , antHurtbox = c2Circle (C2V 0 0) 10
  , missileHitbox = c2Circle (C2V 0 0) 10
  , antChewRate = 60
  , maxHealth = 500
  , antMaxHealth = 2
  , summonDuration = \case
      FireS -> 60 * 5
      FireM -> 60 * 5
      FireL -> 60 * 5
      ColdS -> 60 * 20
      ColdM -> 60 * 30
      ColdL -> 60 * 20
      CheeseS -> 60 * 20
      CheeseM -> 60 * 20
      CheeseL -> 60 * 10
  , summonRange = \case
      FireS -> 120
      FireM -> 120
      FireL -> 80
      ColdS -> 100
      ColdM -> 80
      ColdL -> 100
      CheeseS -> 100
      CheeseM -> 80
      CheeseL -> 100
  , bulletSpeed = 1
  , shotCooldown = \case
      Fireball1 -> 45
      Fireball2 -> 45
      Fireball3 -> 120
      _ -> 60
  , shotDamage = \case
      Fireball1 -> 2
      Fireball2 -> 1
      Fireball3 -> 2
      SlowShot -> 0
      FreezeShot -> 0
      AggroShot{} -> 0
  , slowDuration = 60
  , freezeDuration = 60
  , aggroDuration = 60
  , explosionDuration = 60
  , explosionRadius = 40
  , maxSP = 10
  , notEnoughSPDuration = 60 * 5
  }
