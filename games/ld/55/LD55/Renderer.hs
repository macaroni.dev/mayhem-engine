{-# LANGUAGE TemplateHaskell #-}

module LD55.Renderer where

import Cleff
import Data.IORef
import Data.Map (Map)
import Data.Map.Strict qualified as Map
import Data.Foldable

import Mayhem.Engine.Effects.Expansion

data Render :: Effect where
  Render'OnLayer :: Layer -> m () -> Render m ()
  Render'DropLayer :: Layer -> Render m ()
  Render'Flush :: Render m ()

data Layer =
    Background
  | Earth
  | Field Float
  | Air
  | HUD
  | Cursor
  | DebugLayer
  deriving stock (Eq, Ord, Show)

exp'Render :: forall es m. MonadIO m => IOE :> es => Expansion m Render es
exp'Render = \Run{..} -> do
  layerRef <- liftIO $ newIORef (mempty :: Map Layer (IO ()))
  pure $ Run $ run . runRender layerRef

runRender :: IOE :> es => IORef (Map Layer (IO ())) -> Eff (Render : es) ~> Eff es
runRender layerRef = interpret $ \case
  Render'OnLayer layer m -> withToIO $ \toIO -> do
    liftIO $ modifyIORef' layerRef (Map.insertWith (*>) layer (toIO m))
  Render'DropLayer layer -> do
    liftIO $ modifyIORef' layerRef (Map.delete layer)
  Render'Flush -> liftIO $ do
    layers <- readIORef layerRef
    traverse_ snd (Map.toAscList layers)
    writeIORef layerRef mempty

makeEffect ''Render
