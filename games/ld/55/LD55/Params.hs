module LD55.Params where

import LD55.Summon
import SDL qualified
import Mayhem.Engine.Frame
import Linear.V2
import CuteC2
import LD55.World
import SDL.GPU.Simple qualified as GPU

data Main = Main
  { hotkeyBindings :: Summon -> SDL.Scancode
  , hotkeyColor :: Summon -> GPU.Color
  , testBulletSpeed :: Float
  , castleCenter :: V2 Float
  , castleRadius :: Float
  , antSpeed :: Float
  , missileSpeed :: Float
  , missileCooldown :: Frame
  , antHurtbox :: C2Shape
  , missileHitbox :: C2Shape
  , antChewRate :: Frame
  , maxHealth :: Int
  , antMaxHealth :: Int
  , summonDuration :: Summon -> Frame
  , summonRange :: Summon -> Float
  , bulletSpeed :: Float
  , shotCooldown :: ShotType -> Frame
  , shotDamage :: ShotType -> Int
  , slowDuration :: Frame
  , freezeDuration :: Frame
  , aggroDuration :: Frame
  , explosionDuration :: Frame
  , explosionRadius :: Float
  , maxSP :: SP
  , notEnoughSPDuration :: Frame
  }
