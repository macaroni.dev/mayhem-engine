{-# LANGUAGE BlockArguments #-}
module LD55.Game where

import Prelude-- hiding (min, max)

import Control.Monad
import Control.Applicative
import Control.Monad.Extra (unlessM, findM, whenM)
import Data.Foldable
import Data.Ord (clamp)
import Safe (headMay, minimumMay)
import Data.List (sortBy)
import Data.Maybe (isJust, mapMaybe, fromMaybe)
import Data.Function
import System.Random (initStdGen)
import Optics
import Text.Pretty.Simple (pShowNoColor)
import Data.Text.Lazy qualified as TL
import Data.KdMap.Static qualified as Kd
import System.Environment (lookupEnv)
import Foreign.Marshal.Utils qualified as Foreign
import System.Random.Stateful

import Mayhem.Engine.Loop
import Mayhem.Engine.Effects
import Mayhem.Engine.Utils
import Mayhem.Engine.Automaton
import Mayhem.Engine.Diamond
import Mayhem.Engine.Frame
import Mayhem.Engine.LinearDotSyntax ()
import Mayhem.Engine.Geometry
import Mayhem.Engine.Controller
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Console.Jam'22

import SDL qualified
import SDL.GPU.Utils qualified as GPU
import SDL.GPU.C qualified as GPU.C
import SDL.GPU.Utils qualified as GPU.Camera (CameraF(..))
import SDL.GPU.Utils qualified as GPU.Target (TargetF(..))
import Memorable
import SDL.GPU.FC.Simple qualified as FC
import SDL.GPU.CuteC2 qualified as GPU
import Cute.Sound.C qualified as CS
import CuteC2
import Linear

import LD55.M
import LD55.World
import LD55.Summon
import LD55.Assets qualified as A
import LD55.Params qualified as P
import LD55.Controls
import LD55.Renderer

import Debug.RecoverRTTI

game :: Maybe (HotPipe (Framed LD55)) -> Eff LD55 ()
game hotPipe = do
  initGame
  framedHot hotPipe $ gget >>= \case
    Screen'Playing -> gameloop
    Screen'Title -> assetly $ gpuly $ do
      c <- input >>= parseController
      when (c.selectButton.wasPressed) $ gset Screen'Tutorial
      GPU.blit ?assets.sprites._Loader Nothing ?screen 0 0
      askGPU >>= liftIO . GPU.flip
    Screen'Tutorial -> assetly $ gpuly $ do
      c <- input >>= parseController
      when (c.selectButton.wasPressed) $ gset Screen'Tutorial2
      GPU.blit ?assets.sprites._Loader2 Nothing ?screen 0 0
      askGPU >>= liftIO . GPU.flip
    Screen'Tutorial2 -> assetly $ gpuly $ do
      c <- input >>= parseController
      when (c.selectButton.wasPressed) $ gset Screen'Playing
      GPU.blit ?assets.sprites._Loader3 Nothing ?screen 0 0
      askGPU >>= liftIO . GPU.flip
    Screen'Gameover -> gpuly $ assetly $ GPU.withScreenColor ?screen GPU.red $ do
      c <- input >>= parseController
      when (c.selectButton.wasPressed) $ do
        resetGame
        gset Screen'Playing
      draw c
      render'Flush
      void $ FC.drawScale
        ?assets.fonts.debug.rawFont
        ?screen
        200
        100
        (FC.Scale 2 2)
        (unlines ["GAME OVER", "Press to continue"])
      askGPU >>= liftIO . GPU.flip

gameloop :: Eff (Framed LD55) ()
gameloop = do
  tickGHCi
  askGPU >>= liftIO . GPU.clear
  f7ToggleDebug
  f11Fullscreen
  f2Stepper
  adaptToWindowResize

  input >>= parseController >>= \c -> do
    stepped $ clocked $ do
      debugHUD c
      --debugSummonButtons
      gget >>= \case
        Input'SummonBar bar _ _ -> gpuly $ do
          render'OnLayer HUD $ do
            GPU.rectangleFilled ?screen 150 400 550 450 (GPU.transparent 0.5 GPU.black)
            debugSummonBar ?screen (V2 200 425) 300 15 bar
        _ -> gpuly $ assetly $ render'OnLayer HUD $ do
          GPU.rectangleFilled ?screen 150 400 550 450 (GPU.transparent 0.5 GPU.black)
          whenM (cexists @NotEnoughSP) $ do
            void $ FC.draw ?assets.fonts.debug.rawFont ?screen 250 410 "Not enough SP!"

      gpuly $ assetly $ paramly $ render'OnLayer HUD $ do
        SP sp <- gget @SP
        sc <- gget @Score
        CastleHealth hp <- gget
        let hpStart = 20
        let hpLen = 75
        let hpPercent = hp // ?params.maxHealth
        let hpEnd = hpStart + (hpLen * hpPercent)
        GPU.rectangleFilled ?screen 0 400 100 450 (GPU.transparent 0.5 GPU.black)
        GPU.withLineThickness 10 $ GPU.line ?screen hpStart 412 hpEnd 412 GPU.red
        let SP mxsp = ?params.maxSP
        void $ FC.drawScale ?assets.fonts.debug.rawFont ?screen 5 400 (FC.Scale 0.5 0.5) (unlines ["HP", "Soul Pts " ++ show sp ++ "/" ++ show mxsp, show sc])
      tickMissileCooldown
      tickShotCooldown
      tickExplosion
      tickAntChewing
      tickSummonDuration
      tickNotEnoughSP
      --tickAntSpawn
      tickAntWaves
      tickStatusCooldown
      guideAnts
      checkAntChewing
      applyAggro
      handleControls c
      guideTestBullets
      guideAimedAt
      cmap $ \(Position p, Velocity v, mSlow :: Maybe Slowed, mFrozen :: Maybe Frozen, mChew :: Maybe Chewing) ->
        let v' = if | isJust mFrozen -> 0
                    | isJust mChew -> 0
                    | isJust mSlow -> v/2
                    | otherwise -> v
        in Position (p + v')
      checkCollisions
      gcMissiles
      gcShots
      gget >>= \(CastleHealth hp) -> when (hp <= 0) $ gset Screen'Gameover
      draw c
      unlessM Debug.enabled $ do
        render'DropLayer DebugLayer


  liftIO performMinorGC
  askCuteSound >>= liftIO . CS.mix
  askCuteSound8000 >>= liftIO . CS.mix
  render'Flush
  askGPU >>= liftIO . GPU.flip

initGame :: Eff LD55 ()
initGame = paramly $ assetly $ do
  SDL.cursorVisible SDL.$= False
  _ <- liftIO $ SDL.setMouseLocationMode SDL.RelativeLocation
  gset (CastleHealth ?params.maxHealth)
  traverse_ GPU.bottomCenterAnchor $ mconcat
    [ fmap ?assets.sprites._Summon [FireS ..]
    , [ ?assets.sprites._Maus
      , ?assets.sprites._Maus2
      , ?assets.sprites._MausShoot
      , ?assets.sprites._MausL
      , ?assets.sprites._Maus2L
      , ?assets.sprites._MausShootL
      ]
    ]
  traverse_ GPU.centerAnchor
    [ ?assets.sprites._Ant
    , ?assets.sprites._AntSlow
    , ?assets.sprites._AntFrozen
    , ?assets.sprites._AntAggro
    , ?assets.sprites._AntWeak
    ]
  GPU.setAnchor ?assets.sprites._Castle 0.5 0.6
  -- TODO: Remove
  gset (SP 0)
  _ <- liftIO GPU.toggleFullscreen -- TODO: Use a GPU.init flag
  pure ()

guideTestBullets :: Eff (Framed LD55) ()
guideTestBullets = paramly $ cmapM_ $ \(TestBullet{..}, Position p, ety) -> do
  cgetMay @Position aimedAt >>= traverse_ \(Position tgt) -> do
    cset ety $ Velocity $ normalize (tgt - p) ^* ?params.testBulletSpeed

guideAimedAt :: Eff (Framed LD55) ()
guideAimedAt = paramly $ cmapM_ $ \(AimedAt a, Position p, ety) -> do
  cgetMay @Position a >>= traverse_ \(Position tgt) -> do
      cset ety $ Velocity $ normalize (tgt - p) ^* ?params.bulletSpeed

spawnAntAt :: (The Params es P.Main, The ApecsE es World, IOE :> es) => Position -> Eff es ()
spawnAntAt p@(Position pv) = paramly $ do
  newEntity_
    ( Enemy
    , p
    , Velocity $ normalize (?params.castleCenter - pv) ^* ?params.antSpeed
    , Health ?params.antMaxHealth
    )

data SpawnLoc = SpawnLeft | SpawnTop | SpawnRight deriving (Enum, Bounded)

tickAntWaves :: Eff (Framed LD55) ()
tickAntWaves = do
  clk <- getClock
  let spawnRate = 60 * 10
  let waveLevel = (clk `div` spawnRate) + 1
  let maxAnts = 1000
  currAnts <- ccount @Enemy
  let antBudget = fromIntegral $ max 0 (maxAnts - currAnts)
  let antsToSpawn = fromIntegral (waveLevel * 3 `min` antBudget)
  let shouldSpawn = clk `mod` (60 * 10) == 0
  when shouldSpawn $ replicateM_ antsToSpawn $ uniformEnumM globalStdGen >>= \case
    SpawnLeft -> do
      y <- uniformRM (0, 300) globalStdGen
      spawnAntAt $ Position $ V2 0 y
    SpawnRight -> do
      y <- uniformRM (0, 300) globalStdGen
      spawnAntAt $ Position $ V2 848 y
    SpawnTop -> do
      x <- uniformRM (0, 848) globalStdGen
      spawnAntAt $ Position $ V2 x 0

guideAnts :: Eff (Framed LD55) ()
guideAnts = paramly $ cmap $ \(Enemy, Position pv, _ :: Not Slowed, _ :: Not Frozen) ->
  Velocity $ normalize (?params.castleCenter - pv) ^* ?params.antSpeed

tickAntSpawn :: Eff (Framed LD55) ()
tickAntSpawn = do
  f <- getClock
  when (f `mod` 120 == 0) $
    spawnAntAt (Position 0)

gcMissiles :: Eff (Framed LD55) ()
gcMissiles = cmapM_ $ \(Missile, Position (V2 x y), ety) ->
  when (x < 0 || y < 0 || x > 1000 || y > 1000) $ destroyMissile ety

gcShots :: Eff (Framed LD55) ()
gcShots = cmapM_ $ \(Shot{}, Position (V2 x y), ety) ->
  when (x < 0 || y < 0 || x > 1000 || y > 1000) $ destroyShot ety

tickNotEnoughSP :: Eff (Framed LD55) ()
tickNotEnoughSP = cmapM_ $ \(NotEnoughSP fd, ety) -> case cntDown fd of
  Nothing -> destroyC @NotEnoughSP ety
  Just fd' -> cset ety (NotEnoughSP fd')

tickSummonDuration :: Eff (Framed LD55) ()
tickSummonDuration = cmapM_ $ \(SummonDuration fd, ety) -> case cntDown fd of
  Nothing -> destroySummon ety
  Just fd' -> cset ety (SummonDuration fd')

tickMissileCooldown :: Eff (Framed LD55) ()
tickMissileCooldown = cmap $ \(MissileCooldown fd) -> MissileCooldown <$> cntDown fd

tickShotCooldown :: Eff (Framed LD55) ()
tickShotCooldown = cmap $ \(ShotCooldown fd) -> ShotCooldown <$> cntDown fd

tickExplosion :: Eff (Framed LD55) ()
tickExplosion = cmapM_ $ \(Explosion fd, ety) -> case cntDown fd of
  Nothing -> destroyExplosion ety
  Just fd' -> cset ety (Explosion fd')

tickStatusCooldown :: Eff (Framed LD55) ()
tickStatusCooldown = do
  cmap $ \(Slowed fd) -> Slowed <$> cntDown fd
  cmap $ \(Frozen fd) -> Frozen <$> cntDown fd
  cmap $ \(Aggro'd p fd) -> Aggro'd p <$> cntDown fd
  cmap $ \(Weakened fd) -> Weakened <$> cntDown fd

checkAntChewing :: Eff (Framed LD55) ()
checkAntChewing = paramly $ cmapM_ $ \(Enemy, Position p, mchew :: Maybe Chewing, ety) -> do
  can <- canChew ety
  when (can && distance p ?params.castleCenter <= ?params.castleRadius) $ do
    cset ety (fromMaybe (Chewing 0) mchew)

tickAntChewing :: Eff (Framed LD55) ()
tickAntChewing = paramly $ cmapM $ \(Enemy, Chewing f) -> do
  when (f `mod` ?params.antChewRate == 0) $ do
    gmodify @CastleHealth (subtract 1)
    ChewSfx{..} <- gget
    clk <- getClock
    when (clk >= nextSfx) $ assetly $ do
      cs_ctx <- askCuteSound
      _ <- liftIO $ CS.insertSound cs_ctx ?assets.sfx.chewed.sound
      gset (ChewSfx $ nextSfx + 120) -- TODO: hardcoded
  pure $ Chewing (succ f)

type EnemyIdx = Kd.KdMap Float Position Entity

checkCollisions :: Eff (Framed LD55) ()
checkCollisions = do
  enemies <- cList @(Enemy, Position, Entity)
  let pairedEnemies = fmap (\(_, p, ety) -> (p, ety)) enemies
  let enemyKdPt (Position (V2 x y)) = [x, y]
  let enemyIdx :: EnemyIdx = Kd.build enemyKdPt pairedEnemies

  checkMissileCollision enemyIdx
  checkShotCollision enemyIdx
  checkExplosionCollision enemyIdx
  checkSummonRange enemyIdx


checkSummonRange :: EnemyIdx -> Eff (Framed LD55) ()
checkSummonRange kdm = paramly $ do
  cmapM_ $ \(Summoned smn, p@(Position pv), shotCd :: Maybe ShotCooldown, sety) -> do
    let r = ?params.summonRange smn
    let ptsInRange = Kd.inRadius kdm r p
    let etyInRange = fmap snd ptsInRange
    let closest = fmap snd $ sortBy (compare `on` distance pv . unPosition . fst) ptsInRange
    let onCooldown = isJust shotCd
    let shoot ty tgt = unless onCooldown $ for_ tgt $ \ety -> do
          cset sety $ ShotCooldown $ FrameDown $ ?params.shotCooldown ty
          newEntity_
            ( p
            , Shot ty
            , AimedAt ety
            )
    case smn of
      FireS -> shoot Fireball1 $ headMay closest
      FireM -> shoot Fireball2 $ headMay closest
      FireL -> shoot Fireball3 $ headMay closest
      ColdS -> do
        closestUnslowed <- flip findM closest $ \ety -> not <$> existsC @Slowed ety
        shoot SlowShot $ closestUnslowed <|> headMay closest
      ColdM -> for_ etyInRange (`cset` (Slowed $ FrameDown 1))
      ColdL -> do
        closestUnfrozen <- flip findM closest $ \ety -> not <$> existsC @Frozen ety
        shoot FreezeShot $ closestUnfrozen <|> headMay closest
        for_ etyInRange (`cset` (Slowed $ FrameDown 1))
      CheeseS -> do
        closestUnaggro'd <- flip findM closest $ \ety -> not <$> existsC @Aggro'd ety
        shoot (AggroShot p) $ closestUnaggro'd <|> headMay closest
      CheeseM -> for_ etyInRange (`cset` (Weakened $ FrameDown 1))
      CheeseL -> for_ etyInRange (`cset` (Weakened $ FrameDown 1, Aggro'd p $ FrameDown 1))

checkMissileCollision :: EnemyIdx -> Eff (Framed LD55) ()
checkMissileCollision kdm = paramly $ do
  cmapM_ $ \(Missile, mp@(Position mpv), mety) -> unless (Kd.null kdm) $ do
    let (Position epv, eety) = Kd.nearest kdm mp
    let mhb = c2Move (v22c2v mpv) ?params.missileHitbox
    let ehb = c2Move (v22c2v epv) ?params.antHurtbox
    when (c2Collided mhb ehb) $ do
      destroyMissile mety
      damageAnt eety 1 (gmodify @SP $ clampSP . (+1))

checkShotCollision :: EnemyIdx -> Eff (Framed LD55) ()
checkShotCollision kdm = paramly $ do
  cmapM_ $ \(Shot ty, mp@(Position mpv), mety) -> unless (Kd.null kdm) $ do
    let (Position epv, eety) = Kd.nearest kdm mp
    let mhb = c2Move (v22c2v mpv) ?params.missileHitbox
    let ehb = c2Move (v22c2v epv) ?params.antHurtbox
    when (c2Collided mhb ehb) $ do
      unless (shotPierces ty) $
        destroyShot mety
      damageAnt eety (?params.shotDamage ty) $ do
        Score sc <- gget
        when (sc `mod` 7 == 0) $ gmodify @SP $ clampSP . (+1)
      case ty of
        SlowShot -> cset eety (Slowed $ FrameDown ?params.slowDuration)
        FreezeShot -> cset eety (Frozen $ FrameDown ?params.freezeDuration)
        AggroShot p -> cset eety (Aggro'd p $ FrameDown ?params.aggroDuration)
        Fireball3 -> newEntity_ (Explosion $ FrameDown ?params.explosionDuration, mp)
        _ -> pure ()

checkExplosionCollision :: EnemyIdx -> Eff (Framed LD55) ()
checkExplosionCollision kdm = paramly $ do
  cmapM_ $ \(Explosion{}, p :: Position) ->
    for_ (Kd.inRadius kdm ?params.explosionRadius p) $ destroyAnt . snd

applyAggro :: Eff (Framed LD55) ()
applyAggro = cmap $ \(Aggro'd (Position tgt) _, Position p, Velocity v) ->
  (Velocity $ normalize (tgt - p) ^* norm v, Nothing :: Maybe Chewing)

damageAnt :: Entity -> Int -> (Eff (Framed LD55) ()) -> Eff (Framed LD55) ()
damageAnt ety dmg k = do
  mHealth <- cgetMay @(Health, Maybe Weakened) ety
  for_ mHealth $ \(Health n, mWeak) -> do
    let multiplier = if isJust mWeak then 2 else 1
    let n' = n - (multiplier * dmg)
    cset ety (Health n')
    when (n' <= 0) $ do
      gmodify @Score (+1)
      destroyAnt ety
      k

shotPierces :: ShotType -> Bool
shotPierces = \case
  Fireball2 -> True
  _ -> False

clampSP :: (?params :: P.Main) => SP -> SP
clampSP = clamp (0, ?params.maxSP)

draw :: C -> Eff (Framed LD55) ()
draw c = do
  drawGrass
  drawSummonButtons
  drawAnts
  don't drawEnemies
  drawSummons
  drawTestBullets
  drawMissiles
  drawMouse c
  drawCastle
  drawShots
  drawExplosions

debugHUD :: C -> Eff (Framed LD55) ()
debugHUD c = gpuly $ assetly $ do
  don't $ render'OnLayer HUD $ void $
    FC.drawScale
    ?assets.fonts.debug.rawFont
    ?screen
    0
    0
    (FC.Scale 1 1) $
      TL.unpack $ pShowNoColor c

  render'OnLayer DebugLayer $ void $ do
    hp <- gget @CastleHealth
    sp <- gget @SP
    score <- gget @Score
    FC.drawScale
      ?assets.fonts.debug.rawFont
      ?screen
      0
      0
      (FC.Scale 1 1)
      (unlines [show hp, show sp, show score])
  render'OnLayer Cursor $ do
    let debugCursor r = do
          r ?screen c.loc.x c.loc.y 5 (GPU.transparent 0.5 GPU.white)
          liftIO $ GPU.cross ?screen c.loc.x c.loc.y 3 (GPU.light GPU.white)
    gget >>= \case
      Input'Select{} -> debugCursor GPU.circle
      Input'Summon{} -> debugCursor GPU.circleFilled
      Input'SummonBar bar _ _ -> don't $ void $
        FC.drawScale
        ?assets.fonts.debug.rawFont
        ?screen
        0
        0
        (FC.Scale 1 1) $
          TL.unpack $ pShowNoColor bar


drawAnts :: Eff (Framed LD55) ()
drawAnts = gpuly $ assetly $ cmapM_ $ \(Enemy, Position p, Velocity v, ety :: Entity) -> do
  slowed <- isJust <$> cgetMay @Slowed ety
  frozen <- isJust <$> cgetMay @Frozen ety
  aggro'd <- isJust <$> cgetMay @Aggro'd ety
  weakened <- isJust <$> cgetMay @Weakened ety

  let img = if | frozen -> ?assets.sprites._AntFrozen
               | aggro'd -> ?assets.sprites._AntAggro
               | slowed -> ?assets.sprites._AntSlow
               | weakened -> ?assets.sprites._AntWeak
               | otherwise ->  ?assets.sprites._Ant
  -- Ant sprite defaults to face up, so we have to account for that
  let d = radToDeg (unangle v) + 90
  render'OnLayer (Field p.y) $
    GPU.blitRotate img Nothing ?screen p.x p.y d

drawEnemies :: Eff (Framed LD55) ()
drawEnemies = gpuly $ cmapM_ $ \(Enemy, Position p, ety) -> do
  render'OnLayer (Field p.y) $ GPU.circleFilled ?screen p.x p.y 5 GPU.brown
  cgetMay @Slowed ety >>= traverse_ \_ ->
    render'OnLayer Earth $ GPU.circle ?screen p.x p.y 7 GPU.cyan
  cgetMay @Frozen ety >>= traverse_ \_ ->
    render'OnLayer Earth $ GPU.circle ?screen p.x p.y 7 GPU.blue
  cgetMay @Aggro'd ety >>= traverse_ \_ ->
    render'OnLayer Earth $ GPU.circle ?screen p.x p.y 7 GPU.red
  cgetMay @Weakened ety >>= traverse_ \_ ->
    render'OnLayer Earth $ GPU.circle ?screen p.x p.y 7 GPU.yellow

drawSummons :: Eff (Framed LD55) ()
drawSummons = gpuly $ assetly $ paramly $ cmapM_ $ \(Summoned smn, Position p) -> do
  render'OnLayer DebugLayer $ do
    let color = if | smn `elem` [FireS, FireM, FireL] -> GPU.red
                   | smn `elem` [ColdS, ColdM, ColdL] -> GPU.blue
                   | otherwise -> GPU.yellow
    GPU.circleFilled ?screen p.x p.y 10 color
    GPU.circleFilled ?screen p.x p.y (?params.summonRange smn) (GPU.transparent 0.25 color)
    void $ FC.drawScale
      ?assets.fonts.debug.rawFont
      ?screen
      p.x
      p.y
      (FC.Scale 1 1)
      (summonCode smn)
  let drawAura c = render'OnLayer Earth $ do
        GPU.circle ?screen p.x p.y (?params.summonRange smn) c
        GPU.circleFilled ?screen p.x p.y (?params.summonRange smn) (GPU.transparent 0.1 c)
  case smn of
    ColdM -> drawAura GPU.cyan
    ColdL -> drawAura GPU.cyan
    CheeseM -> drawAura GPU.yellow
    CheeseL -> drawAura GPU.orange
    _ -> pure ()

  render'OnLayer (Field p.y) $
    GPU.blit (?assets.sprites._Summon smn) Nothing ?screen p.x p.y

drawMouse :: C -> Eff (Framed LD55) ()
drawMouse c = assetly $ gpuly $ paramly $ render'OnLayer Earth $ do
  let lr l r = if c.loc.x < ?params.castleCenter.x then l else r
  maus <- ucget @MissileCooldown >>= \case
    Just (MissileCooldown (FrameDown f)) ->
      if | f `mod` 120 > 60 -> pure $ lr ?assets.sprites._MausL ?assets.sprites._Maus
         | otherwise -> pure $ lr ?assets.sprites._MausShootL ?assets.sprites._MausShoot
    Nothing -> do
      clk <- getClock
      if | clk `mod` 120 > 60 -> pure $ lr ?assets.sprites._Maus2L ?assets.sprites._Maus2
         | otherwise -> pure $ lr ?assets.sprites._MausL ?assets.sprites._Maus
  GPU.blit maus Nothing ?screen ?params.castleCenter.x (?params.castleCenter.y - 25)

drawCastle :: Eff (Framed LD55) ()
drawCastle = gpuly $ assetly $ paramly $ do
  render'OnLayer DebugLayer $
    GPU.circle ?screen ?params.castleCenter.x ?params.castleCenter.y ?params.castleRadius (GPU.greyN 0.5)
  render'OnLayer Earth $ do
    GPU.blit ?assets.sprites._Castle Nothing ?screen ?params.castleCenter.x ?params.castleCenter.y

drawTestBullets :: Eff (Framed LD55) ()
drawTestBullets = gpuly $ cmapM_ $ \(TestBullet{}, Position p) -> do
  render'OnLayer Air $ do
    GPU.circle ?screen p.x p.y 3 GPU.white

drawShots :: Eff (Framed LD55) ()
drawShots = gpuly $ cmapM_ $ \(Shot ty, Position p) -> do
  render'OnLayer Air $ do
    let c = case ty of
          Fireball1 -> GPU.red
          Fireball2 -> GPU.red
          Fireball3 -> GPU.red
          SlowShot -> GPU.cyan
          FreezeShot -> GPU.blue
          AggroShot{} -> GPU.yellow
    GPU.circleFilled ?screen p.x p.y 3 c

drawExplosions :: Eff (Framed LD55) ()
drawExplosions = gpuly $ paramly $ cmapM_ $ \(Explosion{}, Position p) -> do
  render'OnLayer Air $ GPU.circleFilled ?screen p.x p.y ?params.explosionRadius (GPU.transparent 0.25 GPU.orange)

drawMissiles :: Eff (Framed LD55) ()
drawMissiles = gpuly $ cmapM_ $ \(Missile, Position p) -> do
  render'OnLayer Air $ do
    GPU.circleFilled ?screen p.x p.y 3 GPU.white

drawGrass :: Eff (Framed LD55) ()
drawGrass = gpuly $ assetly $ render'OnLayer Background $
  GPU.blitTile
  ?assets.sprites._Grass
  ?screen
  (GPU.Rect 0 0 1000 1000)

drawSummonButtons :: Eff (Framed LD55) ()
drawSummonButtons = gpuly $ assetly $ paramly $ render'OnLayer HUD $ do

  GPU.blitTile
    ?assets.sprites._Blanket
    ?screen
    (GPU.Rect 630 350 730 430)

  for_ summonButtons $ \(smn, box) -> do
    GPU.c2AABBFilled ?screen box (GPU.transparent 0.5 GPU.black)

    let rect = GPU.Rect box.min.x box.min.y (box.max.x - box.min.x) (box.max.y - box.min.y)
    let smallRect = GPU.Rect rect.x rect.y (rect.w/2) (rect.h/2)
    liftIO $ Foreign.with rect $ \rptr ->
      -- TODO: GPU simple blitRect has the wrong type (no Maybes for rects)
      GPU.C.blitRect (?assets.sprites._Summon smn) nullPtr ?screen rptr
    hk <- SDL.getScancodeName (?params.hotkeyBindings smn)
    GPU.rectangleFilled2 ?screen smallRect (GPU.transparent 0.5 GPU.white)
    FC.drawBoxColor (?assets.fonts.hotkeys.rawFont) ?screen smallRect (?params.hotkeyColor smn) hk

canChew :: Entity -> Eff (Framed LD55) Bool
canChew ety = do
  frz <- existsC @Frozen ety
  agg <- existsC @Aggro'd ety
  pure $ not $ or [frz, agg]

-- destructors

destroyMissile :: Entity -> Eff (Framed LD55) ()
destroyMissile ety = do
  destroyC @Missile ety
  destroyC @Position ety
  destroyC @Velocity ety

destroyAnt :: Entity -> Eff (Framed LD55) ()
destroyAnt ety = do
  destroyC @Enemy ety
  destroyC @Position ety
  destroyC @Velocity ety
  destroyC @Chewing ety
  destroyC @Health ety

destroySummon :: Entity -> Eff (Framed LD55) ()
destroySummon ety = do
  destroyC @Summoned ety
  destroyC @SummonDuration ety
  destroyC @Position ety
  destroyC @Slowed ety
  destroyC @Frozen ety
  destroyC @Aggro'd ety
  destroyC @Weakened ety

destroyShot :: Entity -> Eff (Framed LD55) ()
destroyShot ety = do
  destroyC @Shot ety
  destroyC @ShotCooldown ety
  destroyC @Position ety
  destroyC @Velocity ety

destroyExplosion :: Entity -> Eff (Framed LD55) ()
destroyExplosion ety = do
  destroyC @Explosion ety
  destroyC @Position ety

resetGame :: IOE :> es => The ApecsE es World => The Params es P.Main => Eff es ()
resetGame = paramly $ do
  destroyAllC @Position
  destroyAllC @Velocity
  destroyAllC @Enemy
  gset @InputMode mempty
  destroyAllC @Summoned
  destroyAllC @SummonDuration
  destroyAllC @Enemy
  destroyAllC @TestBullet
  destroyAllC @MissileCooldown
  destroyAllC @Missile
  destroyAllC @Chewing
  gset @CastleHealth (CastleHealth ?params.maxHealth)
  gset @SP mempty
  destroyAllC @Health
  destroyAllC @AimedAt
  destroyAllC @Shot
  destroyAllC @ShotCooldown
  destroyAllC @Slowed
  destroyAllC @Frozen
  destroyAllC @Aggro'd
  destroyAllC @Weakened
  destroyAllC @Explosion
  gset @ChewSfx mempty
  destroyAllC @NotEnoughSP
  gset @Score mempty
