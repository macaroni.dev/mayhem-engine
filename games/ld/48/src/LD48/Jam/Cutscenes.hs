module LD48.Jam.Cutscenes where

import Optics
import qualified SDL.GPU.Simple as Rect (RectF (..))
import Memorable

import qualified Mayhem.Engine.Animation as Animation

import LD48.Jam.Model
import LD48.Jam.Assets qualified as A
import SDL.GPU.Simple qualified as GPU

openingCutscene :: A.Main -> Cutscene'
openingCutscene A.Main{..} =
  Cutscene'
  { next = Gameplay
  , still = Nothing
  , anim =
      Just $ Animation.init (Animation.Loop'Count 0) (animations ^. #cutscenes % #opening)
  , text = Nothing
  , duration = Just (5 * 60)
  , waitForDuration = True
  , clickToAdvance = True
  }

middleCutscene :: A.Main -> Cutscene'
middleCutscene A.Main{..} =
  Cutscene'
  { next = Gameplay
  , still = Nothing
  , anim = Just $ Animation.init (Animation.Loop'Always) (animations ^. #cutscenes % #middle)
  , text = Nothing
  , duration = Just (3 * 60)
  , waitForDuration = True
  , clickToAdvance = True
  }

endingCutscene :: A.Main -> Cutscene'
endingCutscene assets@A.Main{..} =
  Cutscene'
  { next = Cutscene (titleScreen assets)
  , still = Nothing
  , anim = Just $ Animation.init (Animation.Loop'Count 0) (animations ^. #cutscenes % #ending)
  , text = Nothing
  , duration = Just (10 * 60)
  , waitForDuration = True
  , clickToAdvance = True
  }

titleScreen :: A.Main -> Cutscene'
titleScreen assets@A.Main{..} =
  Cutscene'
  { next = Cutscene $ openingCutscene assets
  , still = Just (images ^. #title_screen)
  , anim = Nothing
  , text = Nothing
  , duration = Nothing
  , waitForDuration = False
  , clickToAdvance = True
  }

testCutscene :: A.Main -> Cutscene'
testCutscene A.Main{..} =
  Cutscene'
  { next = Gameplay
  , still = Just (images ^. #test_wall)
  , anim = Nothing
  , text =
      Just
      ( fonts ^. #alagard % #rawFont
      , GPU.Rect { x = 50, y = 300, w = 300, h = 80 }
      , "Puhoy there!"
      )
  , duration = Just (60 * 10)
  , waitForDuration = False
  , clickToAdvance = True
  }

scaredCutscene :: A.Main -> Cutscene'
scaredCutscene A.Main{..} =
  Cutscene'
  { next = Gameplay
  , still = Just (images ^. #test_wall)
  , anim = Nothing
  , text =
      Just
      ( fonts ^. #alagard % #rawFont
      , GPU.Rect { x = 50, y = 300, w = 300, h = 80}
      , "TOO SCARED???? lmaoooo"
      )
  , duration = Just (60 * 10)
  , waitForDuration = False
  , clickToAdvance = True
  }
