module LD48.Jam.Game where

import Prelude hiding (min, max)
import Prelude qualified

import Data.Word
import Control.Monad
import Control.Applicative
import Control.Monad.Extra
import Data.Foldable
import Data.Monoid
import Data.Functor
import Data.Maybe
import Optics hiding (set)
import Linear
import Linear.Affine
import Data.Set (Set)
import qualified Data.Set as Set
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import System.Random (randomRIO)

import qualified SDL.Utils as SDL
import qualified SDL.GPU.Utils as GPU
import qualified SDL.GPU.Simple as Rect (RectF (..))
import qualified SDL.GPU.FC.Simple as FC
import CuteC2
import qualified Cute.Sound.C as CS
import Memorable

import Mayhem.Engine.Effects hiding (Map)
import Mayhem.Engine.Effects.Debug
import Mayhem.Engine.Loop hiding (Map)
import Mayhem.Engine.Frame
import Mayhem.Engine.Input
import Mayhem.Engine.Utils
import Mayhem.Engine.Resolution
import qualified Mayhem.Engine.Animation as Animation

import LD48.Jam.Assets qualified as A
import LD48.Jam.Model
import LD48.Jam.Enemies
import LD48.Jam.Cutscenes
import LD48.Jam.PortraitGrid

type CanGame es =
  ([IOE, SDL_GPU, Cute_Sound, Input [SDL.Event], Log, Clock, Debug] :>> es
  , The ApecsE es World
  , The Assets es A.Main
  )

gameRez :: NativeResolution
gameWidth :: Word16
gameHeight :: Word16
gameRez@NativeResolution{nativeWidth=gameWidth, nativeHeight=gameHeight} = NativeResolution 640 400

main :: IO ()
main = runManaged $ do
  Run console <-
      runLiftIOE
    &   exp'SDL_GPU SDL_GPU'Config {enableVsync = False, nativeRez = gameRez}
    >>= exp'Cute_Sound
    >>= exp'Log
    >>= exp'Apecs initWorld
    >>= exp'Assets @A.Main "assets/jam"
    >>= exp'Clock
    >>= exp'Debug (theViewAssets $ #fonts % #alagard % #rawFont)

  console $ do
    initialize
    framed $ mainLoop >>= \Loop{..} -> do
      askGPU >>= liftIO . GPU.clear
      f7ToggleDebug
      f11Fullscreen
      adaptToWindowResize

      clocked loopTick
      loopDraw

      flush
      liftIO performGC
      askCuteSound >>= liftIO . CS.mix
      askGPU >>= liftIO . GPU.flip



initialize
  :: The ApecsE es World
  => The Assets es A.Main
  => '[IOE, SDL_GPU, Cute_Sound] :>> es
  => Eff es ()
initialize = do
  assets@A.Main{..} <- theAssets
  screen <- askGPU
  cs_ctx <- askCuteSound
  liftIO $ do
    _ <- GPU.toggleFullscreen
    GPU.centerAnchor $ images ^. #flashlight_mask
    GPU.centerAnchor $ images ^. #flashlight_mask_wide
    GPU.centerAnchor $ images ^. #flashlight_mask_wide_dither
    GPU.centerAnchor $ images ^. #flashlight_color

    let blendMask img = GPU.setBlendFunction img GPU.funcZero GPU.funcOne GPU.funcDstColor GPU.funcZero

    for_
      [ view #flashlight_mask
      , view #flashlight_mask_wide
      , view #flashlight_mask_wide_dither
      ] (\f -> blendMask $ f images)

    CS.loopSound (music ^. #bgm % #sound) 1
    CS.pauseSound (music ^. #bgm % #sound) 1

    _ <- liftIO $ CS.insertSound cs_ctx (music ^. #bgm % #sound)
    CS.errorReason >>= \s ->
      unless (s == nullPtr) $ do
        err <- peekCString s
        putStrLn $ "Failed to play bgm: " ++ err

  do
    _ <- newEntity Portrait
    _ <- newEntity
         ( Staircase
         , Position 0
         , Animation.init
           Animation.Loop'Always
           (animations ^. #stairs)
         )
    _ <- newEntity (Flashlight (Charged chargeLen), Nothing @Light)

    gset $ Cutscene $ titleScreen assets

  pure ()

mainLoop :: CanGame es => Eff es (Game es)
mainLoop = gget >>= \case
  Gameplay -> do
    theAssets >>= \A.Main{..} -> liftIO $ CS.pauseSound (music ^. #bgm % #sound) 0
    pure gameplayLoop
  (Cutscene cutscene@Cutscene'{..}) -> do
    theAssets >>= \A.Main{..} -> liftIO $ CS.pauseSound (music ^. #bgm % #sound) 1
    gmodify @CurrentCutscene $ \(CurrentCutscene c) -> CurrentCutscene (c <|> anim)
    pure $ cutsceneLoop cutscene
  PortraitGrid -> pure portraitGridLoop
  Fade{..} | alpha <= 0 -> cleanup prev >> gset next >> mainLoop
  Fade{..} -> do
    let getLoop = \case
          Gameplay -> gameplayLoop
          Cutscene cutscene -> cutsceneLoop cutscene
          PortraitGrid -> portraitGridLoop
          Fade{} -> error "TODO: Nested Fades not supported"
    let Loop{loopDraw=prevDraw} = getLoop prev
        Loop{loopDraw=nextDraw} = getLoop next
    let fadeLoop = Loop
          { loopTick   = gset Fade{ alpha = alpha -. speed, ..}
          , loopDraw = do
              -- TODO: Fade
              screen <- askGPU
              gget >>= \case
                Fade{alpha=currAlpha} -> do
                  nextDraw
                  GPU.withScreenColor screen (fadeColor { GPU.colorA = currAlpha }) prevDraw
                _ -> error "uh oh"
          }
    pure fadeLoop


gameplayLoop :: CanGame es => Game es
gameplayLoop =
  Loop
  { loopTick = do
      es <- input
      screen <- askGPU

      when (SDL.KeycodeF7 `elem` keypresses es) $ gmodify _TOGGLE_DEBUG
      when (SDL.KeycodeF2 `elem` keypresses es) $ portraitGridTransition

      (P (V2 ax ay)) <- SDL.getAbsoluteMouseLocation
      V2 vx vy <- GPU.getVirtualCoords screen (V2 (fromIntegral ax) (fromIntegral ay))

      cmap $ \(Flashlight _) -> Position (V2 vx vy)

      mousePressed <- SDL.getMouseButtons >>= \f -> pure (f SDL.ButtonLeft)
      do
        cmapM_ $ \((Flashlight charge), light, ety) ->
          case (light, charge) of
            (Nothing, Charged n)
              | n > lightStartup && mousePressed-> do
                ety `cset` Flashlight (Charged (n -! lightStartup))
                ety `cset` Just Light
            (Nothing, Charged _) | mousePressed -> ety `cset` Flashlight (Flickering flickerLen)
            (Just Light, _) | not mousePressed-> ety `cset` Nothing @Light
            _ -> pure ()

      when (wasMousePressed SDL.ButtonRight es) $ do
        cmapM_ $ \((Flashlight charge), ety) -> case charge of
          Reloading _ -> pure ()
          Flickering _ -> pure ()
          _ -> do
            ety `cset` Flashlight (Reloading reloadLen)
            ety `cset` Nothing @Light

      assets <- theAssets
      initialFear <- gget @Fear
      ------------------------------------
      -- Tick flashlight
      cmap $ \((Flashlight charge), light) -> Flashlight $ case (light, charge) of
        (Just Light, Charged 0) -> Flickering flickerLen
        (Just Light, Charged n) -> Charged (n -! 1)
        (Nothing, Charged n) -> Charged n
        (_, Reloading 0) -> Charged chargeLen
        (_, Reloading n) -> Reloading (n -! 1)
        (_, Flickering 0) -> OutOfBattery
        (_, Flickering n) -> Flickering (n -! 1)
        (_, OutOfBattery) -> OutOfBattery

      cmap $ \((Flashlight charge), light) -> case charge of
        Charged{} -> light
        _ -> Nothing @Light

      ------------------------------------
      -- Tick Enemy
      cmapM_ $ \case
        (Alive [], ety) -> destroyEnemy ety
        (Alive (EnemyPlan{..} : rest), ety) -> do
            ety `cset` hurtbox
            ety `cmodify` moveEnemy movement
            ety `cset` Alive rest
            for_ attack $ \Attack{..} -> do
              gmodify $ \(Fear fear) -> Fear $ Prelude.min (fear + fearDmg) 100
        (Dead 0, ety) -> destroyEnemy ety
        (Dead n, ety) -> ety `cset` Dead (n -! 1)

      ------------------------------------
      -- Handle Collisions
      cmapM_ $ \(Flashlight{}, fety, fp@Position{}) -> do
        lit <- existsC @Light fety
        cmapM_ $ \case
          (enemy, ety, ep@Position{}, Alive (plan : _)) -> do
            let hurtboxes = fmap (moveShape ep) . getHurtbox . hurtbox $ plan
                flashShape = moveShape fp $ C2Circle' flashlightHB
            when (lit && any (c2Collided flashShape) hurtboxes) $ do
              ety `cset` Dead (enemyDeathLen enemy)
              cmodify ety $ Animation.pause . (Animation.switch (Animation.Loop'Count 0) (enemyLit assets enemy))
          _ -> pure ()

      ------------------------------------
      -- Tick Animations
      -- TODO: This is tricky ..we don't want to step new animations!
      cmap $ \(anim, eState :: Maybe EnemyState) -> do
        case eState of
          Just (Alive (EnemyPlan{attack = Just _ } : _)) -> anim
          _  -> Animation.step anim


      ------------------------------------
      -- Tick GameClock and Depth
      gmodify @GameClock succ
      gget @GameClock >>= gset . clockToDepth

      ------------------------------------
      -- Generate Enemies
      -- Don't generate enemies when too scared
      -- At this pt, we are just playing out animations of enemies on-screen
      genEnemies assets

      ------------------------------------
      -- Check Fear
      -- TODO: Early returns would help here!
      whenM tooScaredM $ do
        fadeTo 5 (Cutscene $ middleCutscene assets)

      -- Fear Portrait
      finalFear <- gget @Fear
      let (tier, idle, emotes) = fearAnimOf assets finalFear
      let (oldTier, _, _) = fearAnimOf assets initialFear
      let idleS = Animation.init (Animation.Loop'Count 0) idle
      let hitS = Animation.init (Animation.Loop'Count 0) (assets ^. #animations % #portraits % #hit)
      cmapM $ \case
        (Portrait, Nothing) -> pure idleS
        _ | finalFear /= initialFear -> pure $ hitS
        (Portrait, Just anim@Animation.State{..}) -> do
          p <- randomRIO (0, 100 :: Int)
          if (p == 0)
            then randomChoice emotes <&> Animation.init (Animation.Loop'Count 0)
            else if clock > 30 then pure idleS else pure anim
      ------------------------------------
      -- Check for end
      gget @GameClock >>= \(GameClock clk) -> do
        when (clk >= 4300) $ do
          fadeTo 255 (Cutscene $ endingCutscene assets)
  , loopDraw = do
      assets@A.Main{..} <- theAssets
      screen <- askGPU

      ------------------------------------
      -- Render Staircase
      cmapM_ $ \(Staircase, anim@Animation.State{}) -> do
        cmapM_ $ \(Flashlight charge, fety, (Position (V2 fx fy))) -> do
          lit <- existsC @Light  fety
          liftIO $ Animation.render @Int anim $ \stairImg rect _ -> do
            -- Lit stairs are the back-most layer
            GPU.blit stairImg (Just rect) screen 0 0


            -- A mask is between the lit stairs and dark stairs
            -- It reveals the underlying lit stairs
            let blitMask = GPU.blit (images ^. #flashlight_mask_wide_dither) Nothing screen fx fy
            case charge of
              Charged _ -> when lit blitMask
              Flickering n ->
                for_ (Map.lookupGE n flickerFrames) $ \(_, flickOn) ->
                when flickOn blitMask
              _ -> pure ()

            GPU.withBlendFunc stairImg
              GPU.funcDstAlpha
              GPU.funcOneMinusDstAlpha
              GPU.funcDstAlpha
              GPU.funcOneMinusDstAlpha $
              GPU.withColor stairImg darkness $
                GPU.blit stairImg (Just rect) screen 0 0

      ------------------------------------
      -- Render Enemies
      cmapM_ $ \(enemy :: Enemy, eState :: EnemyState, p@(Position (V2 x y)), anim) -> do
        liftIO $ Animation.render @Int anim $ \enemyImg rect _ -> do
          let blitEnemy =
                GPU.blit enemyImg (Just rect) screen x y

          let shadowBlit i blt = do
                _ <- blt
                _ <- GPU.withColor i (lightDarkness { GPU.colorA = 125 }) blt
                pure ()


          case eState of
            Dead{} -> blitEnemy --GPU.withColor enemyImg (GPU.Color 251 242 54 255) blitEnemy
            Alive (EnemyPlan{attack = Just _} : _) -> do
              let (hitImg, hitX, hitY) = enemyHit assets enemy
              shadowBlit hitImg (GPU.blit hitImg Nothing screen hitX hitY)
            Alive [] -> pure ()
            _ -> shadowBlit enemyImg blitEnemy

        whenM (ggets _IS_DEBUG) $ case eState of
          Dead{} -> pure ()
          Alive [] -> pure ()
          Alive (EnemyPlan{hurtbox=Hurtbox hbs} : _) -> do
            let yellow = GPU.Color 0 255 255 175
            for_ (moveShape p <$> hbs) $ \case
              C2Circle' C2Circle{..} -> do
                let C2V cX cY = p
                GPU.circleFilled screen cX cY r yellow
              C2AABB' C2AABB{..} -> do
                let C2V minX minY = min
                let C2V maxX maxY = max
                GPU.rectangleFilled screen minX minY maxX maxY yellow
              C2Capsule' C2Capsule{} -> pure () -- Not supported yet
              C2Poly' _ C2Poly{} -> pure () -- Not supported yet
      ------------------------------------
      -- Render Flashlight & Reticle
      cmapM_ $ \(Flashlight charge, ety, (Position pos@(V2 x y))) -> do
        isDebug <- ggets _IS_DEBUG
        lit <- existsC @Light ety

        let red = GPU.Color 255 0 0 175
        when isDebug $ do
          let C2Circle{..} = flashlightHB
          let V2 cX cY = pos + c2v2v2 p
          GPU.circleFilled screen cX cY r red

        let blitLight =
              GPU.blit (images ^. #flashlight_color) Nothing screen x y
        case charge of
          Charged{} -> when lit blitLight
          Flickering n ->
            for_ (Map.lookupGE n flickerFrames) $ \(_, flickOn) ->
            when flickOn blitLight
          _ -> pure ()

        GPU.line screen (x-5) (y-5) (x+5) (y+5) red
        GPU.line screen (x-5) (y+5) (x+5) (y-5) red

      ------------------------------------
      -- Render Hand
      -- TODO: Tweak x coordinate based on frame
      --Clock clk <- gget
      liftIO $ GPU.blit (images ^. #hand) Nothing screen 300 275
      ------------------------------------
      -- Render HUD
      fearPct <- gget <&> \(Fear n) -> "Fear: " ++ show n ++ "%%"
      let textXO = 300
      cmapM_ $ \case
        Flashlight (Reloading n) -> do
          when ((n `div` 20) `mod` 2 == 0) $ do
            liftIO $ FC.draw (fonts ^. #alagard % #rawFont) screen (fromIntegral gameWidth - textXO) 25 $ unlines ["Battery: -%%", fearPct]
            liftIO $
              void $ FC.drawAlign (fonts ^. #alagard__BigRed % #rawFont) screen (gameWidth// (2 :: Int)) (gameHeight// (2 :: Int)) FC.alignCenter "RELOADING..."
        Flashlight OutOfBattery -> do
          liftIO $ FC.draw (fonts ^. #alagard % #rawFont) screen (fromIntegral gameWidth - textXO) 25 $ unlines ["Battery: 0%%", fearPct]
          liftIO $
            void $ FC.drawAlign (fonts ^. #alagard__BigRed % #rawFont) screen (gameWidth// (2 :: Int)) (gameHeight// (2 :: Int)) FC.alignCenter "OUT OF BATTERY!"
        Flashlight (Charged n) -> do
          let pct :: Word64 = fromIntegral $ (100 * n) `div` chargeLen
          liftIO $ void $ FC.draw (fonts ^. #alagard % #rawFont) screen (fromIntegral gameWidth - textXO) 25 $ unlines ["Battery: " ++ show pct ++ "%%", fearPct]
        Flashlight (Flickering _) -> do
          liftIO $ void $ FC.draw (fonts ^. #alagard % #rawFont) screen (fromIntegral gameWidth - textXO) 25 $ unlines ["Battery: 0%%", fearPct]

      cmapM_ $ \(Portrait, anim) -> do
        liftIO $ Animation.render @Int anim $ \portraitImg rect _ -> do
          GPU.blit portraitImg (Just rect) screen (fromIntegral gameWidth - 150) 10
      ------------------------------------
      -- Render DEBUG
      whenM (ggets _IS_DEBUG) $
        cmapM_ $ \(Flashlight charge) -> do
          step <- gget @Depth
          fear <- gget @Fear
          let hud = unlines
                [ show fear
                , show step
                , show charge
                ]
          liftIO $ void $ FC.draw (fonts ^. #alagard % #rawFont) screen 0 0 hud

      pure ()
  }

-- Constants
flickerLen :: Frame
flickerLen = 60

flickerFrames :: Map Frame Bool
flickerFrames = Map.fromList
  [ (60, False)
  , (55, True)
  , (50, False)
  , (45, True)
  , (40, False)
  , (35, True)
  , (30, False)
  , (30, True)
  , (25, False)
  , (20, True)
  , (27, False)
  , (25, True)
  , (22, False)
  , (20, True)
  , (18, False)
  , (15, True)
  , (13, False)
  , (10, True)
  , (8, False)
  , (5, True)
  , (3, False)
  ]

chargeLen :: Frame
chargeLen = 60

lightStartup :: Frame
lightStartup = 6

reloadLen :: Frame
reloadLen = 90 -- TODO: 0 for testing!

-- TODO: We can't use the global clock for this! Oops
clockToDepth :: GameClock -> Depth
clockToDepth (GameClock (Frame n)) = Depth (n `div` 60)

_TOGGLE_DEBUG :: DEBUG -> DEBUG
_TOGGLE_DEBUG (DEBUG d) = DEBUG (not d)

-- hitboxes
flashlightHB :: C2Circle
flashlightHB = C2Circle (C2V 0 0) 40

moveHB :: Position -> Hurtbox -> Hurtbox
moveHB p (Hurtbox shapes) = Hurtbox (fmap (moveShape p) shapes)

moveShape :: Position -> C2Shape -> C2Shape
moveShape (Position pos) = \case
  C2Circle' C2Circle{..} -> do
    let V2 cX cY = pos + c2v2v2 p
    C2Circle' (C2Circle (C2V cX cY) r)
  C2AABB' C2AABB{..} -> do
    let V2 minX minY = pos + c2v2v2 min
    let V2 maxX maxY = pos + c2v2v2 max
    C2AABB' (C2AABB (C2V minX minY) (C2V maxX maxY))
  C2Capsule' C2Capsule{} -> error "moveShape: unsupported shape: capsule"
  C2Poly' _ C2Poly{} -> error "moveShape: unsupported shape: poly"

-- Cutscenes
cutsceneLoop :: CanGame es => Cutscene' -> Game es
cutsceneLoop Cutscene'{..} =
  Loop
  { loopTick = do
      es <- input
      when (SDL.KeycodeF2 `elem` keypresses es) $ portraitGridTransition
      GameClock clk <- gget @GameClock
      when clickToAdvance $ do
        let waitedForDur = maybe True (\d -> not (waitForDuration && d > clk)) duration
        when waitedForDur $ do
          let presses = filter (flip Set.notMember SDL.functionKeycodes) $ keypresses es
          let mousePressed = wasMousePressed SDL.ButtonLeft es || wasMousePressed SDL.ButtonRight es
          when (not (null presses) || mousePressed) $ do
            fadeTo 255 next

      gmodify @GameClock succ
      gmodify @CurrentCutscene $ \(CurrentCutscene s) -> CurrentCutscene (Animation.step <$> s)

  , loopDraw = do
      screen <- askGPU

      liftIO $ for_ still $ \img -> GPU.blit img Nothing screen 0 0

      -- hacky...
      CurrentCutscene currCut <- gget @CurrentCutscene
      liftIO $ for_ currCut $ \animS ->
        Animation.render @Int animS $ \img rect _ ->
          GPU.blit img (Just rect) screen 0 0

      for_ text $ \(font, rect, str) -> do
        liftIO $ do
          let black = GPU.Color 0 0 0 255
          GPU.rectangleFilled2 screen rect black
          let textRect = GPU.Rect { x = rect.x + 7, y = rect.y + 7, h = rect.h - 14, w = rect.w - 14 }
          FC.drawBox font screen textRect str

      GameClock clk <- gget
      A.Main{..} <- theAssets
      liftIO $ for_ duration $ \dur ->
        when (clk > dur) $ do
          void $ FC.draw (fonts ^. #alagard % #rawFont) screen 500 350 "Click to continue"

  }

fearAnimOf :: A.Main -> Fear -> (Int, Animation.Script, [Animation.Script])
fearAnimOf A.Main{..} (Fear n) =
  let p = animations ^. #portraits
  in
    if | n >= 75   -> (3, p ^. #fear3_idle, replicate 2 (p ^. #fear3_blink) ++ [p ^. #fear3_gasp] ++ replicate 2 (p ^. #fear3_look))
       | n >= 50   -> (2, p ^. #fear2_idle, replicate 2 (p ^. #fear2_blink) ++ [p ^. #fear2_gasp] ++ replicate 2 (p ^. #fear2_look))
       | n >= 25   -> (1, p ^. #fear1_idle, replicate 2 (p ^. #fear1_blink) ++ [p ^. #fear1_gasp] ++ replicate 2 (p ^. #fear1_look))
       | otherwise -> (0, p ^. #normal_idle, replicate 2 (p ^. #normal_blink) ++ [p ^. #normal_gasp] ++ replicate 2 (p ^. #normal_look))

darkness :: GPU.Color
darkness =
  GPU.Color
  --55 28 82 255
  87 37 138 255
  --139 61 217 255

lightDarkness :: GPU.Color
lightDarkness =
  GPU.Color
  --55 28 82 255
  --87 37 138 255
  139 61 217 255

tooScaredM :: CanGame es => Eff es Bool
tooScaredM = gget @Fear <&> (>= 100)

doneAttackingM :: CanGame es => Eff es Bool
doneAttackingM = fmap getAll $ cfoldMap $ \case
  Alive (EnemyPlan{..} : _) ->
    All (isNothing attack)
  _ -> All True

--
cleanup :: CanGame  es => Scene -> Eff es ()
cleanup = \case
  Gameplay -> do
    assets <- theAssets
    gset (Fear 0)
    gset (GameClock 0)
    cmap $ \Flashlight{} -> (Flashlight $ Charged chargeLen, Nothing @Light)
    cmapM_ $ \(_ :: Enemy, ety) -> destroyEnemy ety
    cmap $ \Portrait ->
             Animation.init (Animation.Loop'Count 0)
             (assets ^. #animations % #portraits % #normal_idle)
  Cutscene _ -> do
    gset (CurrentCutscene Nothing)
    gset (GameClock 0)
  PortraitGrid -> do
    cmapM_ $ \(_ :: Animation.State, Not :: Not Staircase, ety) -> destroyC @Animation.State ety
  _ -> pure ()

fadeTo :: CanGame es => Word8 -> Scene -> Eff es ()
fadeTo spd s = do
  curr <- gget @Scene
  gset Fade
    { next = s
    , prev = curr
    , speed = spd
    , alpha = 255
    }


fadeColor :: GPU.Color
fadeColor = GPU.Color 255 0 0 255
