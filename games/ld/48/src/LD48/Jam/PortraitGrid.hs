module LD48.Jam.PortraitGrid where

import Optics
import Control.Monad
import Data.Foldable

import qualified SDL
import qualified SDL.GPU.Utils as GPU
import qualified SDL.GPU.Simple as GPU.C
import Memorable

import qualified Mayhem.Engine.Animation as Animation
import Mayhem.Engine.Loop
import Mayhem.Engine.Utils
import Mayhem.Engine.Input

import LD48.Jam.Assets qualified as A
import LD48.Jam.Model
import LD48.Jam.Cutscenes

type CanPortraitGrid es =
  ( The Assets es A.Main
  , The ApecsE es World
  , '[Input [SDL.Event], IOE, SDL_GPU] :>> es
  )
portraitGridTransition :: CanPortraitGrid es => Eff es ()
portraitGridTransition = do
  A.Portraits{..} <- view (#animations % #portraits) <$> theAssets

  let anims = fmap (Animation.pad 15 15)
        [ fear1_idle
        , Animation.pad 10 0 fear1_blink
        , Animation.pad 0 10 fear1_gasp
        , Animation.pad 5 5 fear1_look
        , fear2_idle
        , Animation.pad 7 3 fear2_blink
        , Animation.pad 3 7 fear2_gasp
        , Animation.pad 5 5 fear2_look
        , fear3_idle
        , Animation.pad 12 0 fear3_blink
        , Animation.pad 0 12 fear3_gasp
        , Animation.pad 8 4 fear3_look
        , normal_idle
        , Animation.pad 7 4 normal_blink
        , Animation.pad 3 8 normal_gasp
        , Animation.pad 4 6 normal_look
        , hit
        ]
  for_ anims (newEntity . Animation.init Animation.Loop'Always)
  prev <- gget @Scene
  gset Fade
    { next = PortraitGrid
    , prev = prev
    , speed = 255
    , alpha = 0
    }

tile :: Ptr GPU.Target -> [Animation.State] -> IO ()
tile screen = go 0 0 0
  where
    go :: Float -> Float -> Float -> [Animation.State] -> IO ()
    go tallest currX currY = \case
      [] -> pure ()
      Animation.State{..} : rest -> do
        screenW <- screen *-> (.w)

        let anims = Animation.ssAnimations (Animation.unScript script)
        let img = Animation.ssImage (Animation.unScript script)
        let Animation.SpriteClip{..} = Animation.currentLocation anims pos

        let scHf :: Float = fromIntegral scH
        let scWf :: Float = fromIntegral scW
        let won'tFit = currX + scWf > fromIntegral screenW
        let thisX = if won'tFit then 0 else currX
        let thisY = if won'tFit then currY + tallest else currY
        let nextTallest = if won'tFit then scHf else max scHf tallest
        let nextX = thisX + scWf
        let nextY = thisY

        let scRect = GPU.Rect { x = fromIntegral scX, y = fromIntegral scY, w = fromIntegral scW, h = fromIntegral scH }
        GPU.blit img (Just scRect) screen thisX thisY
        go nextTallest nextX nextY rest

portraitGridLoop :: CanPortraitGrid es => Game es
portraitGridLoop =
  Loop
  { loopTick = do
      es <- input
      assets <- askAssets
      when (SDL.KeycodeF2 `elem` keypresses es) $ do
        gset Fade
          { next = (Cutscene (titleScreen assets))
          , prev = PortraitGrid
          , speed = 255
          , alpha = 0
          }
      cmap Animation.step
  , loopDraw = do
      screen <- askGPU
      anims <- cfoldMap (\(anim :: Animation.State, Not :: Not Staircase) -> [anim])
      liftIO $ tile screen anims
  }
