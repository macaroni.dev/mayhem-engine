# I'm Definitely Not Scared of My Own Basement
*(This is a mirror of the [ldjam listing](https://ldjam.com/events/ludum-dare/48/im-definitely-not-scared-of-my-own-basement).)*

![FinalTitle.png](FinalTitle.png)

> Oh no, the power’s out! Better go down to the basement to fix it. It’s totally fine, it’s not like there’s anything scary or dangerous down there!

## How to Play

![basement-too-scared-ld48.gif](basement-too-scared-ld48.gif)

* Any key to advance cutscenes/menus.
* Click or hold the left mouse to use the flashlight.
* The flashlight has limited battery - don't run out! Click right mouse at any time to recharge it.
* Your remaining battery life and fear level are in the top-right corner. Don't get too scared or else you'll run away!
* Brave the darkness and make it to the end :)

## About

Please enjoy our second ever submission to a game jam. We're a husband-and-wife team, and our game is built in a custom SDL2+Haskell engine (also built during the jam) with hand drawn art made in Clip Studio Paint.

## Installation
On Windows, just unzip the zip file and play jam.exe :)

Mac and Linux ports coming soon! If you have (64-bit) Wine installed, it should be able to play the Windows exe fine.

## [**CREDITS**](https://gitlab.com/macaroni.dev/gamedev-scratch/-/blob/LD48_jam/ludum-dare/48/CREDITS.md)
