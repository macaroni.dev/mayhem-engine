# Credits

## External Assets
- [`Alagard` font](https://www.dafont.com/alagard.font) by Hewett Tsoi

## Tools
- [Clip Studio Paint](https://www.clipstudio.net/en/) for art
- [FamiStudio](https://famistudio.org/) for music

## Software
- [Haskell](https://www.haskell.org/) (GHC 8.10.4)
- Developed on [NixOS](https://nixos.org/)
  - Cross-compiled to Windows with [`haskell.nix`](https://github.com/input-output-hk/haskell.nix) and [mingw-w64](http://mingw-w64.org/doku.php)
- [SDL2](https://www.libsdl.org/) ([Haskell bindings](https://hackage.haskell.org/package/sdl2))
- [sdl-gpu](https://github.com/grimfang4/sdl-gpu) for graphics ([Haskell bindings](https://gitlab.com/macaroni.dev/sdl-gpu-hs))
- [SDL_FontCache](https://github.com/grimfang4/SDL_FontCache) for text rendering ([Haskell bindings](https://gitlab.com/macaroni.dev/sdl-gpu-hs/-/blob/master/src/SDL/GPU/FC/C.hsc))
- [cute_sound.h](https://github.com/RandyGaul/cute_headers/blob/master/cute_sound.h) for sound ([Haskell bindings](https://gitlab.com/macaroni.dev/cute-sound-hs))
- [cute_c2.h](https://github.com/RandyGaul/cute_headers/blob/master/cute_c2.h) for collisions ([Haskell bindings](https://gitlab.com/macaroni.dev/cute-c2-hs))
- [apecs](https://hackage.haskell.org/package/apecs) ECS
- [animate](https://hackage.haskell.org/package/animate)
