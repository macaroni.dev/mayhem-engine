# Echoes of Ouroboros
*(This is a mirror of the [ldjam listing](https://ldjam.com/events/ludum-dare/47/echoes-of-ouroboros).)*

![TitleScreen.png](TitleScreen.png)

Echoes of Ouroboros is a 2D puzzle game. Play as a thief who must finish a job... under *loopy* circumstances!

![ld47-blog.gif](ld47-blog.gif)

* Game written from scratch in Haskell ([with `gloss`](https://hackage.haskell.org/package/gloss)) & developed on NixOS
* Original art made in Clip Studio Paint

Please enjoy our first ever submission to a game jam!

## Installation
### Windows
Unzip the zip and run `EchoesOfOuroboros.exe`

Some users have run into the zip file being corrupted. In that case, [try this download instead](https://macaroni-static.sfo2.digitaloceanspaces.com/EchoesOfOuroborosWINDOWS_2.zip)

### Mac
Unzip the zip and run `EchoesOfOuroboros`. You will have to approve it in System Preferences > Security and Privacy > General. Under "Allow apps downloaded from:" (MacOS should also prompt you to do this when you try to run the game.)

### Linux
Unzip the zip and run `EchoesOfOuroboros`

If it fails with something like `No such file or directory`, then it's an issue with how we packaged it w.r.t `libc`.

In that case, we recommend building from source. The easiest way is to use the [Nix package manager](https://nixos.org/download.html) and clone the source. In `ludum-dare/47-stuck-in-a-loop`, just `nix-build` and then run `./result/EchoesOfOuroboros`.

You can also try installing Haskell tooling (`ghc` and `cabal`) and build from source that way. [`ghcup` is probably the easiest way.](https://www.haskell.org/ghcup/) You can then run `USE_PWD_ASSETS=1 cabal v2-run` in the `ludum-dare/47-stuck-in-a-loop` subdirectory.

### Post-Jam Updates

The game now has a few Post-Jam updates:

* Background Music
* Rebalanced puzzles
* Falling animation
* Rewind animation
* Better asset discovery

[Download the post-jam version here!](https://macaroni-static.sfo2.digitaloceanspaces.com/LD/EchoesOfOuroboros.zip)
