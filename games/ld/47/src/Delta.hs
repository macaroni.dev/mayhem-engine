module Delta where

data Delta a = Delta
  { before :: a
  , after  :: a
  } deriving (Eq, Ord, Show, Read, Functor, Foldable, Traversable)

(-/-) :: a -> a -> Delta a
(-/-) = Delta

(///) :: Delta a -> a -> Delta a
Delta{..} /// now = Delta { before = after, after = now }
