module Model (module Model, module Model.Types, Frame(..))where

import LD47
import Model.Types
import Grid
import Frame

switchSpikeState :: Bool -> SpikeState -> SpikeState
switchSpikeState shouldSwitch spikeState =
  if shouldSwitch then
    case spikeState of
      ON -> OFF
      OFF -> ON
  else spikeState


spikesOn :: Spikes -> Bool
spikesOn Spikes{..} = currentState == ON


isTripped :: ArrowState -> Bool
isTripped = \case
  Untripped -> False
  Tripped{} -> True

isn'tTripped :: ArrowState -> Bool
isn'tTripped = not . isTripped


holeBreakTime :: Int
holeBreakTime = 45


isDead :: ActionState -> Bool
isDead = \case
  Dead{} -> True
  _ -> False

dirToCoords :: Direction -> Coords
dirToCoords = \case
  UP -> V2 0 (-1)
  DOWN -> V2 0 1
  LEFT -> V2 (-1) 0
  RIGHT -> V2 1 0

playerPushing :: Coords -> Player -> Maybe Direction
playerPushing coords player =
  case player ^. #actionSeq % #actionState of
    Walking{..} -> if coords == (player ^. #location) + dirToCoords dir then Just dir else Nothing
    _ -> Nothing

actionToPush :: Action -> Action
actionToPush = \case
  Walk dir -> Push dir
  Push dir -> Push dir

isBlockFall :: BlockState -> Bool
isBlockFall = \case
  BlockFall _ -> True
  _ -> False

isBlockFalling :: Block -> Bool
isBlockFalling Block{..} = isBlockFall blockState

secondsLeft :: Ruins -> Int
secondsLeft w =
  let timeLimit = w ^. #room % #timeLimit
      timeElapsed = w ^. #clock
      framesLeft :: Frame = timeLimit - timeElapsed
      framesLeftF :: Float = fromIntegral framesLeft
   in ceiling @Float @Int $ framesLeftF / 60
