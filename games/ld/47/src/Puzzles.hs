{-# LANGUAGE OverloadedLists #-}

module Puzzles (puzzleFor, puzzles, baseTerrain) where

import LD47
import Model

{-
COMBINATORS:
- Row & colum (length + starting position)
-}

puzzleFor :: PuzzleKey -> Maybe Room
puzzleFor (PuzzleKey k) = puzzles !? k

baseTerrain :: Grid Terrain
baseTerrain =
  gridFill Floor 20 12
  & gridSets Wall
  (mconcat
    [ v2xs (V2 0  0) 19
    , v2xs (V2 0  1) 19
    , v2ys (V2 0  0) 13
    , v2ys (V2 1  0) 13
    , v2ys (V2 18 0) 13
    , v2ys (V2 19 0) 13
--    , [V2 10 5]
    ]
  )

baseDoor :: Door
baseDoor = Door
  { location = V2 10 1
  , switch  = V2 0 0
  , open = False
  }

-- TODOs:
-- * Decouple dialog from Room number
-- * Put each room in its own named variable
-- * Put second spikes puzzle first since it's easier
puzzles :: Vector Room
puzzles =
  [ Room
    { portrait = PortraitGrin
    , timeLimit = 6000
    , ghostLimit = 1
    , start = V2 3 10
    , terrain = baseTerrain
    , door = baseDoor & #open .~ True & #switch .~ V2 3 11
    , traps =
        [ TSpikes $ Spikes
          { spikeLocs = [ V2 6 6 ]
          , switchLocs = [ V2 5 5 ]
          , currentState = OFF
          , defaultState = OFF
          }
        , TBlock $ Block
          { initialLocation = V2 4 4
          , location = V2 4 4
          , blockState = BlockWait
          }
        , TArrows $ Arrows
          { tripWires =
              [ TripWire
                { start = V2 3 11
                , len = 15
                }
              ]
          , arrows =
              [ Arrow
                { loc = V2 3 9
                , len = 10
                }
              ]
          , arrowState = Untripped
          }
        , THole $ Hole
          { location = V2 11 11
          , holeState = Unbroken
          }
        , THole $ Hole
          { location = V2 5 4
          , holeState = Unbroken
          }
        ]
    }
  , Room
    { portrait = PortraitShock
    , timeLimit = 600
    , ghostLimit = 1
    , start = V2 3 10
    , terrain =
        baseTerrain
    , door = baseDoor & #switch .~ V2 10 6
    , traps = []
    }
  , Room
    { portrait = PortraitNormal
    , timeLimit = 600
    , ghostLimit = 1
    , start = V2 7 10
    , terrain =
        baseTerrain
    , door = baseDoor & #switch .~ V2 15 4
    , traps =
        [ TSpikes $ Spikes
          { spikeLocs =
            [ V2 13 2
            , V2 13 3
            , V2 13 4
            , V2 13 5
            , V2 13 6
            , V2 14 6
            , V2 15 6
            , V2 16 6
            , V2 17 6
            , V2 17 5
            , V2 17 4
            , V2 17 3
            , V2 17 2
            , V2 16 2
            , V2 15 2
            , V2 14 2
            ]
          , switchLocs =
            [ V2 4 9
            ]
          , currentState = ON
          , defaultState = ON
          }
        ]
    }
  , Room
    { portrait = PortraitMad
    , timeLimit = 600
    , ghostLimit = 1
    , start = V2 10 11
    , terrain =
      baseTerrain
      & gridSets Wall
      (mconcat
       [ v2xs (V2 2 8) 7
       , v2xs (V2 2 6) 7
       , v2xs (V2 2 4) 7
       , v2xs (V2 2 2) 7
       , v2xs (V2 12 8) 6
       , v2xs (V2 12 6) 6
       , v2xs (V2 12 4) 6
       , v2xs (V2 12 2) 6
       ]
      )
    , door = baseDoor & #switch .~ V2 10 10
    , traps =
      [ TSpikes $ Spikes
        { spikeLocs = setOf $ v2xs (V2 9 9) 3
        , switchLocs = [ V2 6 10 ]
        , currentState = ON
        , defaultState = ON
        }
      , TSpikes $ Spikes
        { spikeLocs = setOf $ v2xs (V2 9 7) 3
        , switchLocs = [ V2 7 10 ]
        , currentState = ON
        , defaultState = ON
        }
      , TSpikes $ Spikes
        { spikeLocs = setOf $ v2xs (V2 9 5) 3
        , switchLocs = [ V2 8 10 ]
        , currentState = ON
        , defaultState = ON
        }
      , TSpikes $ Spikes
        { spikeLocs = setOf $ v2xs (V2 9 3) 3
        , switchLocs = [ V2 9 10 ]
        , currentState = ON
        , defaultState = ON
        }
      ]
    }
  , Room
    { portrait = PortraitShock
    , timeLimit = 600
    , ghostLimit = 1
    , start = V2 10 11
    , terrain =
      baseTerrain
    , door = baseDoor & #switch .~ V2 2 2
    , traps =
      [ TBlock $ Block
        { initialLocation = V2 10 9
        , location = V2 10 9
        , blockState = BlockWait
        }
      , TArrows $ Arrows
        { tripWires =
            [ TripWire
              { start = V2 2 10
              , len = 16
              }
            ]
        , arrows =
            [ Arrow
              { loc = V2 2 2
              , len = 16
              }
            , Arrow
              { loc = V2 2 6
              , len = 16
              }
            ]
        , arrowState = Untripped
        }
      ]
    }
  , Room
    { portrait = PortraitGrin
    , timeLimit = 600
    , ghostLimit = 1
    , start = V2 2 11
    , terrain =
      baseTerrain
      & gridSets Wall (v2xs (V2 3 10) 15)
    , door = baseDoor & #switch .~ V2 17 11
    , traps =
      fmap
      (\loc -> THole $ Hole { holeState = Unbroken, location = loc })
      (v2xs (V2 2 2) 16)
      ++
      [ THole $ Hole { holeState = Unbroken, location = V2 3 11 }
      , THole $ Hole { holeState = Unbroken, location = V2 2 10 }
      , TBlock $ Block
        { initialLocation = V2 4 11
        , location = V2 4 11
        , blockState = BlockWait
        }
      , TSpikes $ Spikes
        { spikeLocs =
          setOf $ mconcat
          [ v2xs (V2 2 3) 16
          , v2xs (V2 2 4) 16
          , v2xs (V2 2 5) 16
          , v2xs (V2 2 6) 16
          , v2xs (V2 2 7) 16
          , v2xs (V2 2 8) 16
          , v2xs (V2 2 9) 16
          ]
        , switchLocs =
          [ V2 16 11
          ]
        , currentState = OFF
        , defaultState = OFF
        }
      ]
    }
  ]
