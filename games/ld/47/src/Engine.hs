{-# LANGUAGE StrictData #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}

module Engine
  ( engine
  , EngineM
  , MonadEngine (..)
  , MonadSound (..)
  -- * Graphics
  , RenderM
  -- ** Sound
  , Sound (..)
  , Source
  , withAudio
  , loadSource
  , loadLoopingSource
  -- ** Re-exports
  , InputState
  , module Delta
  , MonadState(..)
  , modify
  , module Engine.Types
  ) where

import Engine.Types

import Prelude
import Optics
import Optics.State.Operators
import Control.Monad (unless)
import Control.Monad.Trans.Maybe (MaybeT)
import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import qualified Graphics.Gloss.Interface.Environment as Gloss
import Control.Monad.State.Strict (StateT, execStateT, modify, MonadState(..), lift)
import Control.Monad.IO.Class (MonadIO(..))
import qualified System.Exit
import Sound.ALUT (Source)
import qualified Sound.ALUT as ALUT
import Debug.Trace (traceIO)

import Render
import Input
import Delta

-- | Desired Features:
--
-- * RenderM context for resolution-unaware drawing + top-left origin
--    * Also include `assets`?
-- * State monad interface
--    * Maybe mtl it? Then we can expose more capabilities to these handlers
-- * Keyboard/Mouse state tracking
-- * Global Font (via gloss-sdl2-surface & sdl-ttf)
--
-- Nice-to-have:
--
-- * Font caching from SDL_FontCache
-- * Dynamic font (using custom in-text markup)
-- * Sound handling (with OpenAL + ALUT)
-- * Animation handling
--
-- Stretch abstractions:
--
-- * Extensible Picture ADT (a la Dalek) for plugin-like architecture
-- * Composable ticking machines
-- * Asset (images, sound, animation) loading via HKD
engine
  :: String -- ^ Title
  -> (Int, Int) -- ^ Window size
  -> (Int, Int) -- ^ Window initial position
  -> Gloss.Color
  -> Int
  -> world
  -> (world -> RenderM Gloss.Picture)
  -> (forall m. MonadSound m => Delta world -> m ())
  -> (Gloss.Event -> EngineM world ())
  -> (Float -> InputState -> EngineM world ())
  -> IO ()
engine
  title
  windowSize
  windowPos
  bgColor
  fps
  initWorld
  draw
  doSound
  handleEvent
  tick = do
    Gloss.playIO
      display
      bgColor
      fps
      (Engine initWorld initWorld initialInputState)
      ioDraw
      ioHandle
      ioTick

  where
    display = Gloss.InWindow title windowSize windowPos

    ioDraw Engine{world} = do
      let (width, height) = windowSize
      let ctx = Context {..}
      pure $ runRenderM ctx $ draw world

    ioHandle e eng = flip execStateT eng $ unEngineM $ do
      #inputState %= stepInputState e
      zoom #world $ handleEvent e

    ioTick t Engine{..} = flip execStateT Engine{..} $ unEngineM $ do
      zoom #world $ tick t inputState
      delta <- Delta <$> use #previous <*> use #world
      doSound delta
      use #world >>= assign #previous


newtype EngineM world a = EngineM { unEngineM :: StateT world IO a }
  deriving stock (Functor)
  deriving newtype (Applicative, Monad, MonadState world)

instance Zoom (EngineM s) (EngineM t) s t where
  zoom o (EngineM mc) = EngineM $ zoom o mc
  zoomMaybe o (EngineM mc) = EngineM $ zoomMaybe o mc
  zoomMany o (EngineM mc) = EngineM $ zoomMany o mc

class (MonadSound m, MonadState world m) => MonadEngine world m where
  info :: String -> m ()
  closeGame :: m ()

class Monad m => MonadSound m where
  execSound :: Sound -> m ()

instance MonadEngine world (EngineM world) where
  info = EngineM . liftIO . traceIO
  closeGame = EngineM $ liftIO $ System.Exit.exitSuccess

instance MonadSound (EngineM world) where
  execSound = EngineM . \case
    SoftPlay src -> do
      srcState <- ALUT.get (ALUT.sourceState src)
      unless (srcState == ALUT.Playing) $ ALUT.play [src]
    HardPlay src -> ALUT.play [src]
    Stop src     -> ALUT.stop [src]
    Pause src -> ALUT.pause [src]

instance MonadEngine world m => MonadEngine world (MaybeT m) where
  info = lift . info
  closeGame = lift closeGame

instance MonadSound m => MonadSound (MaybeT m) where
  execSound = lift . execSound

data Sound =
    SoftPlay Source
  | HardPlay Source
  | Stop Source
  | Pause Source

loadSource :: FilePath -> IO Source
loadSource path = do
    buf <- ALUT.createBuffer (ALUT.File path)
    source <- ALUT.genObjectName
    ALUT.buffer source ALUT.$= Just buf
    return source

loadLoopingSource :: FilePath -> IO Source
loadLoopingSource path = do
  source <- loadSource path
  ALUT.loopingMode source ALUT.$= ALUT.Looping
  pure source

withAudio :: IO a -> IO a
withAudio ioa = ALUT.withProgNameAndArgs ALUT.runALUT $ \_ _ -> ioa
