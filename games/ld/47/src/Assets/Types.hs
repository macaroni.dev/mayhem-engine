{-# LANGUAGE TemplateHaskell #-}

module Assets.Types where

import LD47
import SpliceImport
import qualified Graphics.Gloss as Gloss

-- I'm just gonna stringly-type all this stuff
-- TODO: Use IsString + lenses to do offsets
data Asset = Asset
  { name :: String
  , mods :: Gloss.Picture -> Gloss.Picture
  }

instance IsString Asset where
  fromString s = Asset s id

-- TH_CODE
mkOpticsLabels ''Asset
