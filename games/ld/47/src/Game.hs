module Game where

import LD47
import Engine
import Model
import Render
import Puzzles
import Assets

import qualified Ease
import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss

rewindSpeed :: Num a => a
rewindSpeed = 5

tileSize :: Num a => a
tileSize = 32

tileSizeF :: Float
tileSizeF = tileSize

stepSpeed :: Num a => a
stepSpeed = 10

stepSpeedF :: Float
stepSpeedF = stepSpeed

pushSpeed :: Num a => a
pushSpeed = 30

fallSpeed :: Num a => a
fallSpeed = 120

fallSpeedF :: Float
fallSpeedF = fallSpeed

main :: IO ()
main = withAudio $ do
  assets <- loadAssets
  sounds <- loadSounds
  let initialWorld = WTitleMenu
  engine
    "Echoes Of Ouroboros"
    (640, 480)
    (0, 0)
    Gloss.black
    60
    initialWorld
    (renderWorld assets)
    (soundWorld sounds)
    handleWorld
    (tickWorld sounds)

handleWorld :: Gloss.Event -> EngineM World ()
handleWorld = \case
  Gloss.EventKey k _ _ _ -> case k of
    Gloss.SpecialKey Gloss.KeyEsc -> closeGame
    _ -> pure ()
  _ -> pure ()

tickWorld :: Sounds -> Float -> InputState -> EngineM World ()
tickWorld sounds dt input = do
  res <- runMaybeT $ asum
    [ zoomMaybeT _WRuins (tickRuins sounds dt input)
    , zoomMaybeT _WTitleMenu (tickTitleMenu dt input)
     -- TODO: Tick countdown -> Every n, tail samples, at 0, switch to destination
    , zoomMaybeT _WRewind (tickRewind sounds)
    ]

  traverse_ put $ join res

tickTitleMenu :: Monad m => Float -> InputState -> m (Maybe World)
tickTitleMenu _ input =
  if (anyOf (#keys % each) (== Gloss.Down) input)
  then pure $ Just $ ruinsFor 1
  else pure Nothing

tickRewind :: Sounds -> EngineM RewindState (Maybe World)
tickRewind _ = use #samples >>= \case
  [] -> Just . WRuins <$> use #destination
  _ : rest -> do
    #samples .= rest
    pure Nothing

ruinsFor :: PuzzleKey -> World
ruinsFor pk = case puzzleFor pk of
  Nothing -> WTheEnd
  Just room ->
    WRuins $ fix $ \this -> Ruins
    { player = Player
      { location = room ^. #start
      , actionSeq = ActionSeq
        { actionState = Waiting
        , clock = 0
        }
      }
    , ghosts = []
    , samples = []
    , actions = mempty
    , clock = 0
    , puzzleKey = pk
    , room = room
    , puzzle = this
    }

tickRuins :: Sounds -> Float -> InputState -> EngineM Ruins (Maybe World)
tickRuins _ _ input = do
  pk <- use #puzzleKey
  clock <- #clock <<%= succ

  -- Players affect Door & Traps
  doorSwitch <- use (#room % #door % #switch)

  -- TODO: Block locs should be here too!
  players    <- (:) <$> use #player <*> use (#ghosts % mapping #player)
  let playerLocs = fmap (view #location) players
  let playerLocSet = setOf playerLocs

  blockLocs <- toListOf (#room % #traps % folded % _TBlock % filtered (not . isBlockFalling) % #location) <$> get
  let blockLocSet = setOf blockLocs
  let allLocSet = playerLocSet <> blockLocSet

  (#room % #door % #open) .= elem doorSwitch allLocSet

{-  (#room % #traps % each % _TSpikes) %= \Spikes{..} ->
    Spikes -- TODO: Spikes still spawn under blocks. Not worth fixing now!
    { currentState = switchSpikeState (any (`elem` switchLocs) allLocSet) defaultState
    , ..
    }
-}
  zoomMany (#room % #traps % each % _TSpikes) $ do
    Spikes{..} <- get
    let switched = any (`elem` switchLocs) allLocSet
    let newState = switchSpikeState switched defaultState
    #currentState .= newState

  (#room % #traps % each % _TArrows) %= \Arrows{..} ->
    let tripLocs = foldMap (\TripWire{..} -> foldMap setSing $ v2xs start (Coord len)) tripWires
     in Arrows
        { arrowState =
          if isTripped arrowState || any (`elem` tripLocs) allLocSet
          then Tripped (arrows >>= \Arrow{..} -> takeUntil (not . (`elem` allLocSet)) $ v2xs loc (Coord len) ) (unFrame clock) -- TODO: Allow tripping after-the-fact
          else arrowState
        , ..
        }

  {- BLOCKS
   * Find any player pushing in the blocks' direction (fn for next to + dir)
   * If any and we're BlockWait, enter BlockPush
   * If we're BlockPush in that direction, tick
   * If we're BlockPush in another direction , reset BlockPUsh in that dir
   * TODO: Need initialLocation in Block type
  -}

  (#room % #traps % each % _TBlock) %= \Block{..} ->
    let pushingDirection = asum $ fmap (playerPushing location) players
    in case (pushingDirection, blockState) of
      (_, BlockFall{}) -> Block{..}
      (Just pushDir, BlockWait) -> Block{blockState = BlockPush pushDir 0, ..}
      (Just pushDir, BlockPush dir _) | pushDir == dir -> Block{..}
      _ -> Block{blockState = BlockWait, ..}

  (#room % #traps % each % _THole) %= \Hole{..} ->
    case holeState of
      Unbroken | location `elem` allLocSet -> Hole{holeState=Breaking 0, ..}
      Breaking n | n == holeBreakTime -> Hole{holeState = Broken, ..}
      Breaking n -> Hole{holeState = Breaking (succ n), ..}
      _ -> Hole{..}

  trapHitboxes <- foldMap getTrapHitboxes <$> use (#room % #traps)

  let checkForDeath = do
        pLoc <- use #location
        let (mHit :: Maybe Hitbox) = find (\Hitbox{..} -> pLoc == loc) trapHitboxes
        unlessM (isDead <$> use (#actionSeq % #actionState)) $
          for_ mHit $ \Hitbox{..} -> #actionSeq .= ActionSeq (Dead hitType) 0

  zoom #player checkForDeath
  zoomMany (#ghosts % each % #player) checkForDeath

  -- Step Players w.r.t. new Trap/Player state
  nextTerrain <- getNextTerrain

  pendingAction <- getNextAction input

  -- TICK BLOCKS
  -- TODO: We can have the player move a little tighter alongside the block
  (#room % #traps % each % _TBlock) %= \Block{..} ->
    case blockState of
      BlockFall n -> Block{blockState = BlockFall (succ n), ..}
      _ | Just Hitbox{hitType=HOLE} <- find (\Hitbox{..} -> location == loc) trapHitboxes ->
          Block{blockState=BlockFall 0, ..}
      BlockPush dir n | location + dirToCoords dir `elem` playerLocs ->
                        Block{blockState=BlockWait, ..}
                      | Just Wall <- gridGet (location + dirToCoords dir) nextTerrain ->
                        Block{blockState=BlockWait, ..}
                      | n == pushSpeed -> Block{blockState=BlockWait, location = location + dirToCoords dir, ..}
                      | otherwise -> Block{blockState=BlockPush dir (succ n), ..}
      BlockWait -> Block{..}


  processedAction <- zoom #player (tickPlayer nextTerrain pendingAction)

  (#actions % at' clock) .= processedAction

  zoomMany (#ghosts % each) $ do
    actionRecord <- use $ #actions % at' clock
    _ <- zoom #player $ tickPlayer nextTerrain actionRecord
    pure ()

  {-
  Tick & move block here (checking for collisions with nextTerrain)

  -}

  when (clock `mod` rewindSpeed == 0) $ do
    sample <- get
    #samples %= (sample :)

  -- Check for clock reset
  runMaybeT $ asum
    [ do
        timeLimit <- use $ #room % #timeLimit
        guard (timeLimit == clock)
        player <- use (#puzzle % #player)
        actions <- use #actions
        ghostLimit <- use $ #room % #ghostLimit

        ghosts <- #ghosts <%= take ghostLimit . (Ghost{..} :)

        destination <- set #ghosts ghosts <$> use #puzzle

        samples <- use #samples
        pure $ WRewind $ RewindState{..}
    , do
        guardM $ andM
          [ (==) <$> use (#player % #location) <*> use (#room % #door % #location)
          , use (#room % #door % #open)
          ]
        info "succ pk"
        pure $ ruinsFor (succ pk)
    ]

getNextTerrain :: EngineM Ruins (Grid Terrain)
getNextTerrain = do
  base <- use (#room % #terrain)

  door <- use $ #room % #door

  traps <- use $ #room % #traps

  let wallLocs = fold @_ @(Set Coords)
        [ if door ^. #open then mempty else setOf [door ^. #location]
        , foldOf (folded % _TSpikes % filtered spikesOn % #spikeLocs) traps
        , foldMapOf (folded % _TBlock % filtered (not . isBlockFalling) % #location) setSing traps
        ]


  pure
    $ (gridSets Wall wallLocs)
    $ (if door ^. #open then gridSets Floor [door ^. #location] else id)
    $ base

getNextAction :: InputState -> EngineM Ruins (Maybe Action)
getNextAction input = pure $ inputToAction input

getTrapHitboxes :: Trap -> Set Hitbox
getTrapHitboxes = \case
  TSpikes Spikes{..} ->
    if currentState == ON
    then setMap (Hitbox SPIKE) spikeLocs
    else mempty
  TArrows Arrows{..} -> case arrowState of
    Tripped coords _ -> setOf $ fmap (Hitbox ARROW) coords
    Untripped -> mempty
  TBlock Block{..}   -> mempty
  THole Hole{..} -> case holeState of
    Broken -> setSing (Hitbox HOLE location)
    _ -> mempty

inputToAction :: InputState -> Maybe Action
inputToAction input = let
  up    = input ^? #keys % at' (Gloss.Char 'w') % _Just % filtered (== Gloss.Down)
  down  = input ^? #keys % at' (Gloss.Char 's') % _Just % filtered (== Gloss.Down)
  left  = input ^? #keys % at' (Gloss.Char 'a') % _Just % filtered (== Gloss.Down)
  right = input ^? #keys % at' (Gloss.Char 'd') % _Just % filtered (== Gloss.Down)
  in case (up, down) of
       (Just _, Nothing) -> Just $ Walk UP
       (Nothing, Just _) -> Just $ Walk DOWN
       _ -> case (left, right) of
         (Just _, Nothing) -> Just $ Walk LEFT
         (Nothing, Just _) -> Just $ Walk RIGHT
         _ -> Nothing

-- TODO: Don't do one frame of Waiting after walking is up. It makes it chunky.
-- Maybe have this function return "Maybe Action" if it processed that action? Then
-- put it in the history..Or just save all Actions and don't de-dupe
tickPlayer :: Grid Terrain -> Maybe Action -> EngineM Player (Maybe Action)
tickPlayer t mAction = do
--  info $ "Grid Terrain = " ++ show t
--  info $ "Maybe Action = " ++ show mAction
--  get >>= \p -> info $ "Player = " ++ show p
  loc <- use #location
  -- Handle action
  #actionSeq %= \case
    ActionSeq Waiting _ | Just (Walk dir) <- mAction ->
      ActionSeq (Walking dir False) 0
    ActionSeq Waiting _ | Just (Push dir) <- mAction ->
      ActionSeq (Walking dir True) 0
    x -> x

  -- Handle being blocked
  #actionSeq % #actionState %= \case
    Walking dir _ ->
      case gridGet (loc + dirToCoords dir) t of
        Just Wall -> Walking dir True
        Nothing -> Walking dir True
        _ -> Walking dir False
    x -> x

  -- Handle movement
  use #actionSeq >>= \case
    ActionSeq Walking{..} clock | clock == stepSpeed ->
      do
        #location %= if blocked then id else (+dirToCoords dir)
        #actionSeq % #actionState .= (case mAction of
                                        Just (Walk inputDir) -> Walking inputDir blocked
                                        Just (Push inputDir) -> Walking inputDir True
                                        Nothing -> Waiting)
        #actionSeq % #clock .= 0
    _ -> pure ()

  #actionSeq % #clock %= succ

  ifM (orFalse <$> preuse (#actionSeq % #actionState % #blocked)) (pure $ fmap actionToPush mAction) (pure mAction)

resetTrap :: Trap -> Trap
resetTrap trap =
  trap
  & _TSpikes %~ resetSpikes
  & _TArrows %~ resetArrows
  & _THole   %~ resetHole
  & _TBlock  %~ resetBlock
  where
    resetSpikes Spikes{..} = Spikes{currentState = defaultState, ..}
    resetArrows Arrows{..} = Arrows{arrowState = Untripped, ..}
    resetHole Hole{..}     = Hole{holeState = Unbroken, ..}
    resetBlock Block{..}   = Block{location = initialLocation, blockState = BlockWait, ..}

renderWorld :: Assets -> World -> RenderM Gloss.Picture
renderWorld assets = \case
  WTheEnd -> renderAt (V2 @Float 0 0) $ assets "TheEnd"
  WTitleMenu -> renderAt (V2 @Float 0 0) $ assets "TitleScreen"
  WRuins ruins -> renderRuins assets ruins
  WRewind RewindState{..} -> renderRuins assets $ set #ghosts [] $ headDef destination samples

data RuinsLayer =
    LBase
  | LProne
  | LUpright
  | LAir
  | LHUD
  deriving (Eq, Ord, Show)

renderRuins :: Assets -> Ruins -> RenderM Gloss.Picture
renderRuins assets w = renderScene <$> mconcat
  [ renderBackground
  , renderTerrain
  , renderDoor
  , renderTraps
  , renderGhosts
  , renderPlayer
  , renderHUD
  ]
  where
    -- TODO: Render level number
    renderDevice = assets $ case secondsLeft w of
      x | x > 20 || x < 1-> "DeviceNone"
      x -> "Device" ++ show x

    renderHUD =
        sceneAt LHUD ((V2 0 12) * pure (tileSize :: Float))
      $ mconcat
      [ assets "HUD"
      , assets (show $ w ^. #room % #portrait)
      , Gloss.translate (4 * tileSize) 0 $ assets $ "Dialogue"++ show (w ^. #puzzleKey)
      , Gloss.translate (17 * tileSize) 0 renderDevice
      , assets "HUDBorder"
      ]

    dirToCompass = \case
      UP -> "N"
      DOWN -> "S"
      RIGHT -> "E"
      LEFT -> "W"

    getPlayerSprite prefix p =
      case (p ^. #actionSeq % #actionState) of
        Dead HOLE ->
          let fallFrame = p ^. #actionSeq % #clock
              factor = (fallSpeedF - fromIntegral fallFrame) / fallSpeedF
              eased = Ease.quadIn factor
              offset = ((1 - eased) * tileSizeF) / 2
          in if fallFrame > fallSpeed
             then mempty
             else
                 Gloss.translate offset (-offset)
               $ Gloss.scale eased eased
               $ assets
               $ prefix ++ "Fall"
        Dead SPIKE -> assets $ prefix ++ "DeadSpikes"
        Dead ARROW -> assets $ prefix ++ "Corpse"
        Waiting -> assets $ prefix ++ "StandS" -- TODO: Track which way he's facing
        Walking dir False -> assets $ prefix ++ "Step" ++ dirToCompass dir
        Walking dir True -> assets $ prefix ++ "Push" ++ dirToCompass dir

    renderPlayer =
      renderPlayer'like
      (getPlayerSprite "Thief" (w ^. #player))
      (w ^. #player)

    renderPlayer'like sprite Player{location,..} =
      let tileLoc = (fromIntegral <$> location) * pure (tileSize :: Float)
{-          overlay = if isDead (actionSeq ^. #actionState)
                    then Gloss.color Gloss.red $ thickCircle 5 (tileSize/2)
                    else mempty-}
          dirV = case actionSeq of
            ActionSeq{actionState=Walking dir False, clock} ->
                (fromIntegral <$> dirToCoords dir)
              * pure (fromIntegral clock / stepSpeed :: Float)
              * pure tileSize
            _ -> pure 0
      in sceneAt LUpright (tileLoc + dirV) (sprite)

    renderGhosts =
      foldMap
      (\p ->
         renderPlayer'like (getPlayerSprite "Ghost" p) p
      )
      (w ^. #ghosts % mapping #player)

    renderTraps = foldFor (w ^. #room % #traps) $ \case
      TSpikes Spikes{..} -> mconcat
        [ foldMap (\c ->
                     sceneAt (if currentState == ON then LUpright else LProne) (c * pure tileSize)
                   $ if (currentState == ON) then assets "SpikesUp" else assets "SpikesDown"
                  )
          spikeLocs
        , foldMap (\c ->
                     sceneAt LProne (c * pure tileSize)
                   $ if (currentState /= defaultState) then assets "PressurePlateDown" else assets "PressurePlateUp"
                ) switchLocs
        ]
      TArrows Arrows{..} ->
        let tripSprite = assets $ if arrowState == Untripped then "TripwireActive" else "TripwireTripped"
        in mconcat
           [ foldFor tripWires $ \TripWire{..} -> foldFor [0..Coord (len-1)] $ \loc ->
               sceneAt LProne ((start + (V2 loc 0)) * pure tileSize) tripSprite
           , case arrowState of
               Tripped coords clock ->
                 let arrowAsset = assets $ if (clock `mod` 15 < 7) then "ArrowsShort1" else "ArrowsShort2" in
                 foldFor coords $ \c -> sceneAt LAir (c * pure tileSize) arrowAsset
               _ -> mempty
           ]
      TBlock Block{..} ->
        sceneAt LUpright (location * pure tileSize)
        $ if isBlockFall blockState then mempty else assets "Block"
      THole Hole{..} -> sceneAt LProne (location * pure tileSize) $ case holeState of
        Broken -> assets "WeakFloorBroken"
        _ -> assets "WeakFloor"

    renderDoor =
      let Door{..} = w ^. #room % #door
       in mconcat
          [ sceneAt LUpright (location * pure tileSize) $ if open then assets "DoorOpen" else assets "DoorClosed"
          , sceneAt LUpright (switch * pure tileSize) $ if open then assets "ButtonActivated" else assets "ButtonDeactivated"
          ]

    renderTerrain = ifoldForGrid @Coord (gridCrop (V2 2 2) (V2 17 11) (w ^. #room % #terrain)) $ \tv t ->
      case t of
        Floor -> mempty
        Wall -> sceneAt LUpright ((tv + (V2 2 2)) * pure tileSize) (assets "Wall")

    renderBackground = sceneAt LBase (V2 @Coord 0 0) $ assets "Room"

renderRuins_OLD :: Ruins -> RenderM Gloss.Picture
renderRuins_OLD w = mconcat
  [ renderTerrain
  , renderDoor
  , renderTraps
  , renderGhosts
  , renderPlayer
  , renderHUD
  ]
  where
    timeRemaining = (w ^. #room % #timeLimit) - (w ^. #clock)

    -- TODO: Render level number
    renderHUD =
        renderAt ((V2 0 12) * pure (tileSize :: Float))
      $ Gloss.color brown
      $ mconcat
      [ rectangleSolid (20 * tileSize) (3 * tileSize)
      ,   tileOffset 0 0
        $ Gloss.color Gloss.white
        $ Gloss.text
        $ show
        $ unFrame timeRemaining
      ,   tileOffset 15 0
        $ Gloss.color (Gloss.greyN 0.5)
        $ Gloss.text
        $ show
        $ w ^. #room % #ghostLimit
      ]

    renderPlayer =
      renderPlayer'like
      (Gloss.color Gloss.cyan $ squareSolid tileSize)
      (w ^. #player)

    renderPlayer'like sprite Player{location,..} =
      let tileLoc = (fromIntegral <$> location) * pure (tileSize :: Float)
          overlay = if isDead (actionSeq ^. #actionState)
                    then Gloss.color Gloss.red $ thickCircle 5 (tileSize/2)
                    else mempty
          dirV = case actionSeq of
            ActionSeq{actionState=Walking dir False, clock} ->
                (fromIntegral <$> dirToCoords dir)
              * pure (fromIntegral clock / stepSpeed :: Float)
              * pure tileSize
            _ -> pure 0
      in renderAt (tileLoc + dirV) (sprite <> overlay)

    renderGhosts =
      foldMap
      (renderPlayer'like
        (Gloss.color (Gloss.withAlpha 0.5 Gloss.aquamarine) $ squareSolid tileSize)
      )
      (w ^. #ghosts % mapping #player)

    renderTraps = foldFor (w ^. #room % #traps) $ \case
      TSpikes Spikes{..} -> mconcat
        [ foldMap (\c ->
                     renderAt (c * pure tileSize)
                   $ Gloss.color (Gloss.withAlpha (if (currentState == ON) then 1 else 0.5) Gloss.orange)
                   $ circleSolid (tileSize/2)
                  )
          spikeLocs
        , foldMap (\c ->
                     renderAt (c * pure tileSize)
                   $ Gloss.color Gloss.yellow
                   $ circleSolid (tileSize/2)
                ) switchLocs
        ]
      _ -> mempty -- TODO: nah

    renderDoor =
      let Door{..} = w ^. #room % #door
          doorColor = if open then Gloss.rose else Gloss.white
       in mconcat
          [ renderAt (location * pure tileSize) $ Gloss.color doorColor $ thickCircle 5 (tileSize/2)
          , renderAt (switch * pure tileSize) $ Gloss.color Gloss.red $ circleSolid (tileSize/2)
          ]

    renderTerrain =
      ifoldForGrid @Coord (w ^. #room % #terrain) $ \tv t ->
        renderAt (tv * pure tileSize) $ case t of
          Floor      -> Gloss.color Gloss.white $ squareWire tileSize
          Wall       -> Gloss.color Gloss.green $ squareSolid tileSize

-- 160,82,45
brown :: Gloss.Color
brown = Gloss.makeColorI 160 82 45 255

tileOffset :: Float -> Float -> Gloss.Picture -> Gloss.Picture
tileOffset x y = Gloss.translate (x * tileSizeF) (y * tileSizeF)

soundWorld :: forall m. MonadSound m => Sounds -> Delta World -> m ()
soundWorld sounds dw = do
  let start_bgm = execSound @m $ SoftPlay $ sounds "bgm"
  let start_rewind_bgm = execSound @m $ SoftPlay $ sounds "rewind-bgm"
  let stop_bgm = execSound @m $ Stop $ sounds "bgm"
  let stop_rewind_bgm = execSound @m $ Stop $ sounds "rewind-bgm"
  case dw of
    Delta{before = WRuins{}, after = WRuins{}} -> pure ()
    Delta{before = WRewind{}, after = WRuins{}} -> stop_rewind_bgm >> start_bgm
    Delta{before = WRuins{}, after = WRewind{}} -> stop_bgm >> start_rewind_bgm
    Delta{before = _, after = WRuins{}} -> start_bgm
    _ -> pure ()

