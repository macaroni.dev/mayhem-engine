{-# LANGUAGE TemplateHaskell #-}

module Grid where

import SpliceImport

import Optics hiding (ifoldl')
import Optics.Coerce

import Linear (V2(..))
import Data.Vector (Vector, ifoldl', (!?))
import qualified Data.Vector
import qualified Data.Foldable
--------------------------------------------------------------------
newtype Coord = Coord Int
  deriving stock   (Show, Read)
  deriving newtype (Enum, Bounded, Eq, Ord, Num, Real, Integral)


-- TODO: Use Ix for all this?
newtype Grid a = Grid
  { tiles :: Vector (Vector a)
  }
  deriving stock (Eq, Show, Read)
  deriving newtype (Semigroup, Monoid)

type instance Index (Grid a) = V2 Coord
type instance IxValue (Grid a) = a
instance Ixed (Grid a) where
  ix (V2 (Coord x) (Coord y)) =
      coerceS @(Vector (Vector a)) @(Grid a)
    $ coerceT @(Vector (Vector a)) @(Grid a)
    $ ix @(Vector (Vector a)) y % ix @(Vector a) x

gix :: forall a. Coord -> Coord -> Optic' An_AffineTraversal NoIx (Grid a) a
gix x y = ix @(Grid a) (V2 x y)

gridOf :: [[a]] -> Grid a
gridOf = Grid . Data.Vector.fromList . fmap Data.Vector.fromList

gridFill :: a -> Int -> Int -> Grid a
gridFill a x y = Grid $ Data.Vector.replicate y $ Data.Vector.replicate x a

gridGet :: V2 Coord -> Grid a -> Maybe a
gridGet (V2 (Coord x) (Coord y)) Grid{..} = do
  row <- tiles !? y
  row !? x

gridSets :: Foldable f => a -> f (V2 Coord) -> Grid a -> Grid a
gridSets newVal coords grid = Data.Foldable.foldl' (\g c -> g & ix c .~ newVal) grid coords

-- TODO: Write gridPinch in terms of gridCrop
gridPinch :: V2 Coord -> Grid a -> Grid a
gridPinch (V2 (Coord x) (Coord y)) (Grid rows) =
  Grid $ Data.Vector.slice y (length rows - y) $
  fmap (\row -> Data.Vector.slice x (length row - x) row) rows

-- TODO: Property test (smth with dimensions & crop should be id)
gridCrop :: V2 Coord -> V2 Coord -> Grid a -> Grid a
gridCrop (V2 (Coord x1) (Coord y1)) (V2 (Coord x2) (Coord y2)) (Grid rows) =
  Grid $ Data.Vector.slice y1 (y2 - y1 + 1) $
  fmap (Data.Vector.slice x1 (x2 - x1 + 1)) rows

-- TODO: Abstract over Ix
ifoldMapGrid :: forall n a b. Num n => Monoid b => (V2 n -> a -> b) -> Grid a -> b
ifoldMapGrid f Grid{..} = ifoldl'
  (\acc1 y as -> ifoldl' (\acc2 x a -> f (V2 (fromIntegral x) (fromIntegral y)) a <> acc2) acc1 as
  ) (mempty :: b) tiles

ifoldForGrid :: forall n a b. Num n => Monoid b => Grid a -> (V2 n -> a -> b) -> b
ifoldForGrid = flip ifoldMapGrid

-- TH_CODE
Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels ''Grid
