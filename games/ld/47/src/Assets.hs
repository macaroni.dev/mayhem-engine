{-# LANGUAGE OverloadedStrings #-}
module Assets (module Assets, module Assets.Types) where

import LD47
import Assets.Types
import Render
import Engine (Source, loadSource, loadLoopingSource)

import System.Environment
import System.FilePath
import qualified Graphics.Gloss as Gloss
import qualified Data.Map.Strict as Map
import Debug.Trace (traceIO)

thiefOffset :: Asset -> Asset
thiefOffset = #mods .~ Gloss.translate 0 8

spriteNames :: [Asset]
spriteNames =
  [ "TitleScreen"
  , "TheEnd"
  , "ArrowsShort1"
  , "ArrowsShort2"
  , "HUD"
  , "HUDBorder"
  , "PortraitGrin"
  , "PortraitNormal"
  , "PortraitMad"
  , "PortraitShock"
  , "Block" & #mods .~ Gloss.translate 0 32
  , "Wall" & #mods .~ Gloss.translate 0 32
  , "ButtonActivated"
  , "ButtonDeactivated"
  , "DoorClosed" & #mods .~ Gloss.translate (-32) 32
  , "DoorOpen" & #mods .~ Gloss.translate (-32) 32
  , "GhostFall" & #mods .~ Gloss.translate 0 32
  , "GhostDeadSpikes" & #mods .~ Gloss.translate 0 32
  , "GhostCorpse" -- TODO: 2 wide 1 tall
  , "GhostPushE" & thiefOffset
  , "GhostPushN" & thiefOffset
  , "GhostPushS" & thiefOffset
  , "GhostPushW" & thiefOffset
  , "GhostStandE" & thiefOffset
  , "GhostStandN" & thiefOffset
  , "GhostStandS" & thiefOffset
  , "GhostStandW" & thiefOffset
  , "GhostStepE" & thiefOffset
  , "GhostStepN" & thiefOffset
  , "GhostStepS" & thiefOffset
  , "GhostStepW" & thiefOffset
  , "PressurePlateDown"
  , "PressurePlateUp"
  , "Room"
  , "SpikesDown" & #mods .~ Gloss.translate 0 32
  , "SpikesUp" & #mods .~ Gloss.translate 0 32
  , "ThiefFall" & #mods .~ Gloss.translate 0 32
  , "ThiefDeadSpikes" & #mods .~ Gloss.translate 0 32
  , "ThiefCorpse" -- TODO: 2 wide 1 tall
  , "ThiefPushE" & thiefOffset
  , "ThiefPushN" & thiefOffset
  , "ThiefPushS" & thiefOffset
  , "ThiefPushW" & thiefOffset
  , "ThiefStandE" & thiefOffset
  , "ThiefStandN" & thiefOffset
  , "ThiefStandS" & thiefOffset
  , "ThiefStandW" & thiefOffset
  , "ThiefStepE" & thiefOffset
  , "ThiefStepN" & thiefOffset
  , "ThiefStepS" & thiefOffset
  , "ThiefStepW" & thiefOffset
  , "TripwireActive"
  , "TripwireTripped"
  , "WeakFloorBroken"
  , "WeakFloor"
  , "DeviceNone"
  ] ++ (fmap (fromString . ("Device"++) . show) [1..20 :: Int])
    ++ (fmap (fromString . ("Dialogue"++) . show) [1..5 :: Int])

-- TODO: No step N/S

type Sounds = String -> Source
type Assets = String -> Gloss.Picture

getAssetPrefix :: IO String
getAssetPrefix = lookupEnv "USE_PWD_ASSETS" >>= \case
  Just "1" -> pure ""
  _ -> takeDirectory <$> getExecutablePath

loadAssets :: IO Assets
loadAssets = do
  assetPrefix <- getAssetPrefix
  let loadSprite Asset{..} = (name,) . mods <$> loadPng (assetPrefix </> "assets" </> name ++ ".png")
  !assets <- Map.fromList <$> traverse loadSprite spriteNames
  pure $ \name -> assets Map.! name

data Sound = Sound
  { name :: String
  , loop :: Bool
  }

instance IsString Sound where
  fromString s = Sound s False

soundNames :: [Sound]
soundNames =
  [ "bgm" { loop = True }
  , "rewind-bgm" { loop = True }
  ]

loadSounds :: IO Sounds
loadSounds = do
  assetPrefix <- getAssetPrefix
  let loadSound Sound{..} = (name,) <$> ((if loop then loadLoopingSource else loadSource) (assetPrefix </> "assets" </> name ++ ".wav"))
  !sounds <- Map.fromList <$> traverse loadSound soundNames
  pure $ \name -> sounds Map.! name
