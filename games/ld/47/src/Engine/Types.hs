{-# LANGUAGE TemplateHaskell #-}

module Engine.Types where

import Input
import SpliceImport

import Optics

data Engine world = Engine
  { world :: world
  , previous :: world
  , inputState :: InputState
  } deriving (Eq, Show)

-- TH_CODE
Optics.makeFieldLabelsWith Optics.noPrefixFieldLabels ''Engine
