{-# LANGUAGE TemplateHaskell #-}

module Model.Types where

import LD47
import Grid
import Frame
import SpliceImport

data Portrait =
    PortraitGrin
  | PortraitNormal
  | PortraitMad
  | PortraitShock
  deriving (Eq, Show, Read)

type Coords = V2 Coord

data HitType = SPIKE | ARROW | HOLE
  deriving (Eq, Ord, Show, Read)

data Hitbox = Hitbox
  { hitType :: HitType
  , loc :: V2 Coord
  }
  deriving (Eq, Ord, Show, Read)


newtype PuzzleKey = PuzzleKey Int
  deriving newtype (Show, Read, Eq, Ord, Enum, Bounded, Num)

-- Future: Hole?
data Terrain =
    Floor
  | Wall
  deriving (Eq, Show, Read)

data Direction =
    UP
  | DOWN
  | LEFT
  | RIGHT
  deriving (Eq, Show, Read)

data Action =
    Walk Direction
  | Push Direction
  deriving (Eq, Show, Read)

-- TODO: Trap/door state is conflated with their defs..
-- TODO: Individual switch presses
-- OBJECTS
data SpikeState = ON | OFF deriving (Eq, Show, Read)

data Spikes = Spikes
  { spikeLocs :: Set Coords
  , switchLocs :: Set Coords
  , currentState :: SpikeState
  , defaultState :: SpikeState
  }
  deriving (Eq, Show, Read)

-- ARROWS ALWAYS GO LEFT TO RIGHT
data Arrow = Arrow
  { loc :: Coords
  , len :: Int
  }
  deriving (Eq, Show, Read)

data Orientation = HORIZONTAL | VERTICAL
  deriving (Eq, Show, Read)

data TripWire = TripWire
  { start :: Coords
  , len :: Int
  }
  deriving (Eq, Show, Read)

data ArrowState =
    Untripped
  | Tripped [Coords] Int
  deriving (Eq, Show, Read)

data Arrows = Arrows
  { arrows :: [Arrow]
  , tripWires :: [TripWire]
  , arrowState :: ArrowState
  }
  deriving (Eq, Show, Read)

data BlockState =
    BlockWait
  | BlockFall Int
  | BlockPush Direction Int
  deriving (Eq, Show, Read)

data Block = Block
  { location :: Coords
  , blockState :: BlockState
  , initialLocation :: Coords
  }
  deriving (Eq, Show, Read)

data HoleState =
    Unbroken
  | Breaking Int
  | Broken
  deriving (Eq, Show, Read)

data Hole = Hole
  { location :: Coords
  , holeState :: HoleState
  }
  deriving (Eq, Show, Read)

-- Traps can modify the base terrain (with new walls & holes)
data Trap =
    TSpikes Spikes
  | TArrows Arrows
  | TBlock Block
  | THole Hole
  deriving (Eq, Show, Read)

data Door = Door
  { location :: Coords
  , switch   :: Coords
  , open     :: Bool
  }
  deriving (Eq, Show, Read)

data Room = Room
  { terrain :: Grid Terrain
  , start   :: Coords
  , door    :: Door
  , traps :: [Trap]
  , ghostLimit :: Int
  , timeLimit   :: Frame
  , portrait :: Portrait
  }
  deriving (Eq, Show, Read)

data ActionState =
    Waiting
  | Walking { dir :: Direction, blocked :: Bool }
  | Dead HitType -- TODO: Cause of death
  deriving (Eq, Show, Read)

data ActionSeq =
  ActionSeq
  { actionState :: ActionState
  , clock :: Frame
  }
  deriving (Eq, Show, Read)

data Player = Player
  { location :: V2 Coord
  , actionSeq :: ActionSeq
  } deriving (Eq, Show, Read)

data Ghost = Ghost
  { player :: Player
  , actions :: FrameMap Action
  }
  deriving (Eq, Show, Read)

data Ruins = Ruins
  { player      :: Player
  , ghosts      :: [Ghost]
  , actions     :: FrameMap Action
  , clock       :: Frame
  , puzzleKey   :: PuzzleKey
  , puzzle      :: ~Ruins -- This always refers to ourself on creation, so we need laziness
  , room        :: Room
  , samples     :: [Ruins]
  } deriving (Eq, Show, Read)

data Sample a = Sample
  { duration :: Frame
  , value :: a
  } deriving (Eq, Show, Read)

data RewindState = RewindState
  { samples :: [Ruins]
  , destination :: Ruins
  }
  deriving (Eq, Show, Read)

data World =
    WRuins Ruins
  | WRewind RewindState
  | WTitleMenu
  | WTheEnd
  deriving (Eq, Show, Read)

-- TH_CODE
makePrisms ''HitType
mkOpticsLabels ''Hitbox
mkOpticsLabels ''Spikes
mkOpticsLabels ''Arrow
makePrisms ''Orientation
mkOpticsLabels ''TripWire
makePrisms ''ArrowState
mkOpticsLabels ''Arrows
mkOpticsLabels ''BlockState
mkOpticsLabels ''Block
mkOpticsLabels ''HoleState
mkOpticsLabels ''Hole
makePrisms ''Trap
mkOpticsLabels ''Door
mkOpticsLabels ''Room
mkOpticsLabels ''ActionState
makePrisms ''ActionState
mkOpticsLabels ''ActionSeq
mkOpticsLabels ''Player
mkOpticsLabels ''Ghost
mkOpticsLabels ''Ruins
mkOpticsLabels ''Sample
mkOpticsLabels ''RewindState
makePrisms ''World
