module LD51.M where

import SDL qualified

import Mayhem.Engine.Effects
import Mayhem.Engine.Effects.Clock
import Mayhem.Engine.Effects.Stepper
import Mayhem.Engine.Effects.Debug
import Mayhem.Engine.Effects.GHCi

import LD51.World
import LD51.Params qualified as P
import LD51.Assets qualified as A

type CanGame es =
  ( '[IOE, Clock, Cute_Sound, Cute_Sound8000, SDL_GPU, Stepper, Debug, GHCi, Log] :>> es
  , The ApecsE es World
  , The Params es P.Main
  , The Assets es A.Main
  )

type CanLoop es =
  ( CanGame es
  , '[Input [SDL.Event]] :>> es
  )
