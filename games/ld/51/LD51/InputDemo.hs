module LD51.InputDemo where

import Data.Ord (clamp)

import SDL.GPU.Utils qualified as GPU
import SDL.GPU.FC.Simple qualified as FC
import Linear
import Foreign.C.String
import Foreign.Ptr

import Mayhem.Engine.Effects
import Mayhem.Engine.Assets
import Mayhem.Engine.Utils (foldA)

import LD51.M
import LD51.Draw
import LD51.World
import LD51.Control
import LD51.Assets qualified as A
import LD51.Params qualified as P

init :: CanGame es => Eff es ()
init = paramly $ do
  clearAllC @StickActive
  clearAllC @Player
  resetClock
  newEntity_
    ( Player
    , Position ?params.player.startLoc
    , Velocity 0
    )

loop :: CanLoop es => Eff es ()
loop = clocked $ gpuly $ assetly $ paramly $ clockly $ do
  controller <- foldA [mouseAndKeyboard, gameController]

  applyController controller

  -- NOTE: This all was copied from Game.hs instead of put in a function
  -- | Handle Movement
  cmap $ \(Velocity v, Movement{..}) ->
    let vdiff = unVelocity targetVelocity - v
        vdir = if nearZero vdiff then 0 else signorm vdiff
    in if nearZero v && nearZero vdiff
    then Velocity 0
    else Velocity $ v + vdir ^* rate

  -- | Handle Velocity
  (maxX, maxY) <- askNativeRezF
  cmap $ uncurry $ \(Position p, Not :: Not Cow) -> \case
    Velocity v -> Position $ V2 (clamp (0, maxX)) (clamp (0, maxY)) <*> (p + v)

  -- | Tick Yips
  cmap $ \case
    Yip{..} -> Just $
      if yipUntil <= ?clock
      then YipCooldown{cooldownUntil = ?clock + ?params.player.yipCooldown}
      else Yip{..}
    YipCooldown{..} -> if cooldownUntil <= ?clock then Nothing else Just YipCooldown{..}

  -- Draw
  liftIO $ do
    let txt = unlines
          [ "Press F1 to return to Title Screen"
          , "---"
          , "Hold Space/Left Mouse Button to \"grip\" the stick."
          , "While \"gripped,\" move your mouse to \"tilt\" the stick."
          , "Release Space/Left Mouse Button to \"let go of\" the stick, returning it to neutral."
          , "The overlayed arrow corresponds to your direction & velocity."
          ]
    FC.draw ?assets.fonts.main.rawFont ?screen 0 0 txt
  do
    (x, y) <- askNativeRezF
    let r = y/4
    liftIO $ drawControlStick'pseudoRealistic ?screen controller.movementStick (V2 (x/2) (y/2)) r
  drawPlayer
  cmapM_ $ \(Player, Position p) ->
    liftIO $ drawControlStick'arrow ?screen controller.movementStick p 30 3 GPU.white { GPU.colorA = 100 }
