{-# LANGUAGE BlockArguments #-}

module LD51.Control where

import Control.Monad
import Control.Applicative
import Control.Monad.Extra (ifM)
import Data.Foldable
import Data.Maybe
import Data.Functor
import Data.Function
import Data.Monoid.Generic
import GHC.Generics
import Data.Int

import SDL qualified
import SDL.Input.GameController qualified as SDL
import SDL.Vect
import SDL.GPU.Utils qualified as GPU
import qualified Cute.Sound.C as CS

import Mayhem.Engine.Assets
import Mayhem.Engine.Effects
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Engine.Loop
import Mayhem.Engine.Input
import Mayhem.Engine.Geometry (radToDeg)
import Mayhem.Engine.Utils (foldForA, allocaWith, guarded)

import LD51.M
import LD51.Params qualified as P
import LD51.Assets qualified
import LD51.World

----------------------------------------------------
-- Generic virtual controller components:
data ControlStick = ControlStick
  { -- | Vector with magnitude between 0 and 1 representing
    -- location relative to stick origin
    location :: V2 Float
  }
  deriving (Show)

instance Semigroup ControlStick where
  x <> y =
    let raw = x.location + y.location
        normed = norm raw
        updated = if | nearZero raw -> V2 0 0
                     | normed > 1 -> (/normed) <$> raw
                     | otherwise -> raw
    in ControlStick updated

instance Monoid ControlStick where
  mempty = ControlStick 0

data Button = Button
  { state :: ButtonState
  , event :: Maybe ButtonEvent
  } deriving (Show)

instance Semigroup Button where
  x <> y = Button
    { state = case (x.state, y.state) of
        (Button'Held, _) -> Button'Held
        (_, Button'Held) -> Button'Held
        _ -> Button'Unheld
    , event = x.event <|> y.event
    }

instance Monoid Button where
  mempty = Button Button'Unheld Nothing

data ButtonState = Button'Held | Button'Unheld deriving (Show)
data ButtonEvent = Button'Press | Button'Release deriving (Eq, Show)

----------------------------------------------------

-- This could go HKD for diffing
data Controller = Controller
  { movementStick :: ControlStick
  , yipButton :: Button
  }
  deriving stock (Show, Generic)
  deriving Semigroup via GenericSemigroup Controller
  deriving Monoid via GenericMonoid Controller

mouseAndKeyboard :: CanLoop es => Eff es Controller
mouseAndKeyboard = paramly $ do
  _ <- liftIO $ SDL.setMouseLocationMode SDL.RelativeLocation
  events <- input @[SDL.Event]
  isMousePressed <- SDL.getMouseButtons
  isScanPressed <- SDL.getKeyboardState
  firstEdge [mouseButtonEdge SDL.ButtonLeft events, scancodeEdge SDL.ScancodeSpace events] & \case
    Just SDL.Pressed -> do
      newEntity_ (StickActive 0)
    Just SDL.Released -> do
      _ <- liftIO SDL.getRelativeMouseLocation -- throw it away to keep it moving
      destroyUC @StickActive
    Nothing -> unless (isMousePressed SDL.ButtonLeft || isScanPressed SDL.ScancodeSpace) $ do
      _ <- liftIO SDL.getRelativeMouseLocation -- throw it away to keep it moving
      pure ()

  movementStick <- do
    location <- ucget @StickActive >>= \case
      Just (StickActive prevLoc) -> do
        offset <- liftIO SDL.getRelativeMouseLocation
        let normedOffset = fmap ((/ ?params.controls.movementStickRadius) . fromIntegral) offset
        let updatedLoc = prevLoc + normedOffset
        let updatedNorm = norm updatedLoc
        let normedLoc = if | nearZero updatedLoc -> 0
                           | updatedNorm > 1 -> (/ updatedNorm) <$> updatedLoc
                           | otherwise -> updatedLoc

        cmap $ \(StickActive _) -> StickActive normedLoc
        pure normedLoc
      Nothing -> do
        pure 0
    pure ControlStick{..}

  yipButton <- do
    let event = scancodeEdge SDL.ScancodeF events <&> \case
          SDL.Pressed -> Button'Press
          SDL.Released -> Button'Release
    let state = if isScanPressed SDL.ScancodeF then Button'Held else Button'Unheld
    pure Button{..}

  pure Controller{..}

-- TODO: This really doesn't work..try logging its return value in InputDemo?
gameController :: CanLoop es => Eff es Controller
gameController = do
  events <- input @[SDL.Event]
  for_ events \e -> case e.eventPayload of
    SDL.ControllerAxisEvent SDL.ControllerAxisEventData{..} -> do
      let maxInt16F = fromIntegral (maxBound :: Int16) :: Float
      let axisF = fromIntegral controllerAxisEventValue :: Float
      let toZero x = if abs x < 0.1 then 0 else x
      -- See https://wiki.libsdl.org/SDL_GameControllerAxis
      --
      -- sdl2's Haskell bindings only expose these in the Raw interface - and they are
      -- the wrong type anyways
      let dv = if | controllerAxisEventAxis == 0 -> V2 (Just (axisF / maxInt16F)) Nothing
                  | controllerAxisEventAxis == 1 -> V2 Nothing (Just (axisF / maxInt16F))
                  | otherwise -> V2 Nothing Nothing
      gmodify $ \(GameControllerStick v) ->
        GameControllerStick (fmap toZero . fromMaybe <$> v <*> dv)
    _ -> pure ()
  stickCtl <- do
    GameControllerStick loc <- gget
    pure mempty { movementStick = ControlStick loc }

  buttonCtl <- foldForA events \e -> case e.eventPayload of
    SDL.ControllerButtonEvent SDL.ControllerButtonEventData{..} ->
      case (controllerButtonEventButton, controllerButtonEventState) of
        (SDL.ControllerButtonA, SDL.ControllerButtonPressed) ->
          pure mempty { yipButton = Button Button'Held (Just Button'Press) }
        (SDL.ControllerButtonA, SDL.ControllerButtonReleased) ->
          pure mempty { yipButton = Button Button'Unheld (Just Button'Release) }
        _ -> pure mempty
    _ -> pure mempty

  pure (stickCtl <> buttonCtl)

applyController :: CanLoop es => Controller -> Eff es ()
applyController Controller{..} = paramly $ do
  let ControlStick{location=dir} = movementStick
  let target = dir ^* ?params.player.maxSpeed
  cmapM $ \(Player, Position p) -> do
    unless (nearZero dir) $ do
      let lineLen = dir ^* 10
      debug_ $ Debug.ShapeEntry 0 GPU.white $ Debug.Line p (p + lineLen)

    pure Movement
      { targetVelocity = Velocity target
      , rate = if nearZero target then ?params.player.deceleration else ?params.player.acceleration
      }

  let Button{event=yipEvent} = yipButton
  when (yipEvent == Just Button'Press) $ do
    yipUntil <- getClock <&> (+ ?params.player.yipDuration)
    cmapM $ \(Player, Not :: Not Yip) -> assetly $ do
      cs_ctx <- askCuteSound8000
      _ <- liftIO $ CS.insertSound cs_ctx ?assets.sounds.yip.sound
      pure $ Yip yipUntil

drawControlStick'dot
  :: Ptr GPU.Target
  -> ControlStick
  -> V2 Float -- ^ Center
  -> Float -- ^ Gate Radius
  -> Float -- ^ Stick Radius
  -> GPU.Color -- ^ Gate color
  -> GPU.Color -- ^ Stick color
  -> IO ()
drawControlStick'dot
  screen
  ControlStick {..}
  stickCenter@(V2 centerX centerY)
  gateRadius
  stickRadius
  gateColor
  stickColor = do
    GPU.circle screen centerX centerY gateRadius gateColor
    GPU.cross screen centerX centerY (stickRadius/2) stickColor
    let V2 stickX stickY = stickCenter + (gateRadius *^ location)
    GPU.circleFilled screen stickX stickY stickRadius stickColor

    -- TODO: Draw an X at the center always

drawControlStick'pseudoRealistic
  :: Ptr GPU.Target
  -> ControlStick
  -> V2 Float -- ^ Center
  -> Float -- ^ Reference Radius
  -> IO ()
drawControlStick'pseudoRealistic
  screen
  stick
  center
  r =
  let mkGrey val = GPU.Color val val val 255
  in drawControlStick'pseudo3D screen stick center 0.7 r (r/2) (r/1.5) GPU.white (mkGrey 100) (mkGrey 160)

drawControlStick'pseudo3D
  :: Ptr GPU.Target
  -> ControlStick
  -> V2 Float -- ^ Center
  -> Float -- ^ "Tilt factor"
  -> Float -- ^ Gate Radius
  -> Float -- ^ Stick Shaft Radius
  -> Float -- ^ Stick Head Radius
  -> GPU.Color -- ^ Gate color
  -> GPU.Color -- ^ Stick Shaft color
  -> GPU.Color -- ^ Stick Head color
  -> IO ()
drawControlStick'pseudo3D
  screen
  ControlStick{..}
  stickCenter@(V2 centerX centerY)
  tiltFactor
  gateRadius
  stickShaftRadius
  stickHeadRadius
  gateColor
  stickShaftColor
  stickHeadColor = do
    GPU.circle screen centerX centerY gateRadius gateColor
    let V2 stickX stickY = stickCenter + (gateRadius *^ location)

    GPU.circleFilled screen centerX centerY stickShaftRadius stickShaftColor
    GPU.withLineThickness (stickShaftRadius*2) $ GPU.line screen centerX centerY stickX stickY stickShaftColor

    let drx  = norm location * tiltFactor * stickHeadRadius
    let rx = stickHeadRadius - drx
    let ry = stickHeadRadius
    let a = if nearZero location then 0 else radToDeg $ unangle location
    GPU.ellipseFilled screen stickX stickY rx ry a stickHeadColor

drawControlStick'arrow
  :: Ptr GPU.Target
  -> ControlStick
  -> V2 Float -- ^ Anchor point
  -> Float -- ^ Max arrow length
  -> Float -- ^ Arrow thickness
  -> GPU.Color
  -> IO ()
drawControlStick'arrow
  screen
  ControlStick{..}
  anchorPoint@(V2 anchorX anchorY)
  arrowLen
  arrowThickness
  arrowColor = do
  let tipV@(V2 tipX tipY) = anchorPoint + arrowLen *^ location
  GPU.withLineThickness arrowThickness $
    GPU.line screen anchorX anchorY tipX tipY arrowColor
  when (not $ nearZero location) $ do
    let dir = signorm location
    let V2 p1x p1y = tipV + (dir ^* (arrowThickness * 2))
    let V2 p2x p2y = tipV + (perp dir ^* (arrowThickness * 2))
    let V2 p3x p3y = tipV + (perp dir * -1 ^* (arrowThickness * 2))
    GPU.triFilled screen p1x p1y p2x p2y p3x p3y arrowColor
  GPU.circleFilled screen anchorX anchorY (arrowThickness/2) arrowColor
