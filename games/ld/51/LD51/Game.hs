module LD51.Game where

import Prelude hiding (min, max)

import Control.Monad
import Data.Foldable
import Data.Functor
import Data.Traversable
import Data.Fixed (mod')
import Data.Maybe
import Data.Map qualified as Map
import Data.Ord (clamp)
import System.Random
import System.Random.Stateful
import Foreign.Marshal.Utils (toBool)

import SDL.GPU.Utils qualified as GPU
import SDL.GPU.FC.Simple qualified as FC
import SDL qualified
import qualified Cute.Sound.C as CS
import Linear
import Ease
import CuteC2

import Optics

import Mayhem.Engine.Loop
import Mayhem.Engine.Effects
import Mayhem.Engine.Effects.GHCi
import Mayhem.Engine.Effects.Stepper
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Engine.Utils
import Mayhem.Engine.Input (scanpresses)
import Mayhem.Engine.Resolution
import Mayhem.Engine.Frame
import Mayhem.Engine.Assets

import LD51.M
import LD51.World
import LD51.Params qualified as P
import LD51.Assets qualified as A
import LD51.Control
import LD51.Draw
import LD51.InputDemo qualified

main :: CanGame es => Eff es ()
main = do
  initGame
  framed $ do
    tickGHCi
    askGPU >>= liftIO . GPU.clear
    f7ToggleDebug
    f11Fullscreen
    f2Stepper
    adaptToWindowResize

    -- NOTE: I don't quite love 'clocked' but it works
    do
      gget >>= \case
        Screen'Title -> do
          isScanPressed <- SDL.getKeyboardState
          events <- input
          when (isScanPressed SDL.ScancodeReturn) $ do
            resetGameplay
            gset Screen'Playing
          when (SDL.ScancodeF1 `elem` scanpresses events) $ do
            LD51.InputDemo.init
            gset Screen'InputDemo
          (foldA [mouseAndKeyboard, gameController]) >>= drawTitle
        Screen'Gameover -> do
          isScanPressed <- SDL.getKeyboardState
          when (isScanPressed SDL.ScancodeReturn) $ do
            resetGameplay
            gset Screen'Playing
          drawGameover
        Screen'InputDemo -> do
          events <- input
          when (SDL.ScancodeF1 `elem` scanpresses events) $ do
            gset Screen'Title
          LD51.InputDemo.loop
        Screen'Playing -> do
          stepped $ clocked $ do
            Debug.reset
            handleInput -- NOTE (hack): we don't buffer paused input so this doesn't work in step mode
            cowGC
            weedGC
            spawnCows
            spawnWeed
            planCows
            handleHerding
            respectSkyline
            handleTick
            handleCollisions
            checkForGameover
            playBGSFX
          -- draw depends on the clock ticking above..nasty but it works to allow for stepping
          draw
          Debug.render


    liftIO performMinorGC
    askCuteSound >>= liftIO . CS.mix
    askCuteSound8000 >>= liftIO . CS.mix
    askGPU >>= liftIO . GPU.flip

initGame :: CanGame es => Eff es ()
initGame = assetly $ do
  liftIO $ CS.setVolume ?assets.sounds.yip.sound 0.5 0.5
  SDL.cursorVisible SDL.$= False -- TODO: Remove
  _ <- liftIO GPU.toggleFullscreen -- TODO: Use a GPU.init flag
  resetGameplay

ten :: Frame
ten = 10 * 60

resetGameplay :: CanGame es => Eff es ()
resetGameplay = (resetClock >>) $ clockly $ do
  params <- theParams

  clearAllC @Grass
  clearAllC @Tumbleweed
  clearAllC @Position
  clearAllC @Velocity
  clearAllC @Player
  clearAllC @Yip
  clearAllC @Movement
  clearAllC @Cow
  clearAllC @Barn
  clearAllC @StickActive

  gset (Score 0)
  gset (LostCows 0)

  newEntity_
    ( Player
    , Position (params ^. #player % #startLoc)
    , Velocity 0
    )
  newEntity_
    ( Cow (Cow'Grazing []) Cow1
    , Position (V2 200 200)
    )

  newEntity_
    ( Mothership'Idle
    , MothershipTimer (?clock + ten)
    )

  for_ params.barns.locs $ \p ->
    newEntity_
    ( Barn
    , Position p
    )

  genGrass

handleInput :: CanLoop es => Eff es ()
handleInput = foldA [mouseAndKeyboard, gameController] >>= applyController

cowGC :: CanLoop es => Eff es ()
cowGC = do
  cmapM $ \cowStuff@(cow :: Cow, Velocity _, Position (V2 x y)) -> do
    (maxX, maxY) <- askNativeRezF
    let addScore = gmodify (succ @Score) $> Nothing
        loseCow = gmodify (succ @LostCows) $> Nothing
        keepCow = pure $ Just cowStuff
    case cow.state of
      Cow'Abducted ->
        if y < 0 then addScore else keepCow
      _ ->
        if | x < 0 -> loseCow
           | x > maxX -> loseCow
           | y > maxY -> loseCow
           | otherwise -> keepCow


handleCollisions :: CanLoop es => Eff es ()
handleCollisions = clockly $ paramly $ do
  params <- theParams
  let P.Player{herdHitbox, yipHitbox} = params ^. #player
  let debugShapeAt color loc = \case
        C2Circle' C2Circle{..} ->
          debug_ $ Debug.ShapeEntry (loc + c2v2v2 p) color $ Debug.Circle r
        C2AABB' C2AABB{..} ->
          debug_ $ Debug.ShapeEntry loc color $ Debug.Rectangle (c2v2v2 min) (c2v2v2 max)
        unsupported -> error $ "[debugShapeAt]: unsupported shape: " ++ show unsupported

  -- TODO: It would be nice to have some sort of :+: combinator to allow for un-nesting cmapM_'s
  cmapM_ $ \(_ :: Mothership, abduction :: Maybe Abduction) -> do
    abductionHitboxes <- for abduction $ \Abduction{..} -> do
      let hbs = fmap (offsetC2Shape ?params.mothership.abductionLocation) hitboxes
      for_ hbs $ debugShapeAt GPU.white 0
      pure hbs

    cmapM_ $ \(Player, Position playerP, Velocity playerV, mYip :: Maybe Yip) -> do
      debugShapeAt GPU.orange playerP herdHitbox
      let isYipping = case mYip of
            Nothing -> False
            Just Yip{} -> True
            Just YipCooldown{} -> False
      when isYipping $ debugShapeAt GPU.red playerP yipHitbox

      cmapM $ \(cow@Cow{}, Position cowP) -> do
        let herdable = case cow.state of
              Cow'Yipped{} -> False
              Cow'Abducted{} -> False
              _ -> True
        let cowHurtbox = offsetC2Shape cowP $ params ^. #cows % #hurtbox
        debugShapeAt GPU.yellow cowP (params ^. #cows % #hurtbox)
        if | Just ahbs <- abductionHitboxes
           , any (c2Collided cowHurtbox) ahbs -> do
               pure $ cow & #state .~ Cow'Abducted
           | not herdable -> pure cow -- If a Cow is not herdable, it's busy!
           | isYipping && c2Collided cowHurtbox (offsetC2Shape playerP yipHitbox) -> do
               let yippedDir = if nearZero playerV then 0 else signorm playerV
               let yippedUntil = ?clock + ?params.cows.yipDuration
               pure $ cow & #state .~ Cow'Yipped {..}
           | c2Collided cowHurtbox (offsetC2Shape playerP herdHitbox) -> do
               pure $ cow & #state .~ Cow'Herded
           | otherwise -> pure $ cow & #state %~ \case
               s@Cow'Grazing{} -> s
               _ -> Cow'Grazing []

handleHerding :: CanLoop es => Eff es ()
handleHerding = paramly $ cmapM_ $ \(_ :: Mothership, Position motherP) -> cmapM_ $ \(Player, Position playerP) -> do
  cmapM $ \(Cow cowState _, Position cowP) -> do
    pure $ case cowState of
      Cow'Abducted ->
        let diffP = motherP - cowP
            V2 _ motherY = motherP
            V2 _ cowY = cowP
        in if | nearZero diffP -> Velocity 0
              -- HACK: Stop when we get higher than the ship to help
              -- avoid shaking when the ship is not moving.
              --
              -- Maybe we can stop using Velocity and just have it track
              -- the ship's position after a certain point?
              | cowY < motherY -> Velocity 0
              | otherwise -> Velocity $ signorm diffP ^* ?params.cows.abductedSpeed
      Cow'Grazing [] -> Velocity 0
      Cow'Grazing (goal : _) -> case goal of
        CowGoal'Idle{} -> Velocity 0
        CowGoal'Walk{..} -> Velocity $ walkDir ^* ?params.cows.walkSpeed
      Cow'Herded ->
        let diffP = cowP - playerP
            dir = if nearZero diffP then V2 1 0 else signorm diffP
        in Velocity $ dir ^* ?params.cows.herdSpeed
      Cow'Yipped {..} -> Velocity $ yippedDir ^* ?params.cows.yipSpeed

planCows :: CanLoop es => Eff es ()
planCows = clockly $ paramly $ do
  debug_ $ Debug.ShapeEntry 0 GPU.green $ Debug.HorizontalLine ?params.field.skyline
  cmapM $ \(cow, Position (V2 _ py)) -> case cow of
    Cow{state=Cow'Grazing [], ..} -> do
      goalRoll :: Float <- uniformFloat01M globalStdGen
      goal <-
        if | goalRoll < ?params.cows.goalSplit -> do
               idleDur <- Frame <$> uniformRM ?params.cows.idleRange globalStdGen
               let idleUntil = ?clock + idleDur
               pure CowGoal'Idle{..}
           | otherwise -> do
               let nearSkyline = py - ?params.field.skyline < 20 -- further than respectSkyline
               -- IDEA: Make them more likely to try to leave when near edge?
               walkDur <- Frame <$> uniformRM ?params.cows.walkRange globalStdGen
               let angRange =
                     if nearSkyline
                     then (0, pi /2)
                     else (0, 2 * pi)
               walkAng <- uniformRM angRange globalStdGen
               let walkDir = angle walkAng
               let walkUntil = ?clock + walkDur
               pure CowGoal'Walk{..}
      pure Cow{state = Cow'Grazing [goal], ..}
    x@Cow{state=Cow'Grazing (goal : rest), ..} -> case goal of
      CowGoal'Idle{..} -> pure $
        if ?clock == idleUntil then Cow{state=Cow'Grazing rest, ..} else x
      CowGoal'Walk{..} -> pure $
        if ?clock == walkUntil then Cow{state=Cow'Grazing rest, ..} else x
    x -> pure x

respectSkyline :: CanLoop es => Eff es ()
respectSkyline = paramly $ cmap $ \(cow@Cow{}, Position (V2 _ py), Velocity v@(V2 vx vy)) ->
  let nearSkyline = py - ?params.field.skyline < 5
      movingUp = vy < 0
      beingAbducted = case cow.state of
        Cow'Abducted -> True
        _ -> False
  in if nearSkyline && movingUp && not beingAbducted then Velocity $ V2 vx 0 else Velocity v

spawnCows :: CanLoop es => Eff es ()
spawnCows = clockly $ paramly $ do
  cmapM_ $ \(Barn, Position p) ->
    debug_ $ Debug.ShapeEntry p GPU.red $ Debug.Circle 20

  numCows <- ccount @Cow
  unless (numCows >= ?params.barns.maxCows) $
    when (?clock `mod` ?params.barns.spawnRate == 0) $ do
      barns <- cList @(Barn, Position)
      pickedBarn <- uniformRM (0, length barns - 1) globalStdGen
      let (_, Position spawnP)  = barns !! pickedBarn
      let allCowTypes :: [CowType] = [minBound..maxBound]
      pickedCowType <- uniformRM (0, length allCowTypes - 1) globalStdGen
      newEntity_
        ( Cow (Cow'Grazing []) (allCowTypes !! pickedCowType)
        , Position spawnP
        )
handleTick :: CanLoop es => Eff es ()
handleTick = clockly $ paramly $ do
  (maxX, maxY) <- askNativeRezF
  -- | Handle Movement
  cmap $ \(Velocity v, Movement{..}) ->
    let vdiff = unVelocity targetVelocity - v
        vdir = if nearZero vdiff then 0 else signorm vdiff
    in if nearZero v && nearZero vdiff
    then Velocity 0
    else Velocity $ v + vdir ^* rate

  -- | Handle Velocity
  cmap $ uncurry $ \(Position p, Not :: Not Cow) -> \case
    Velocity v -> Position $ V2 (clamp (0, maxX)) (clamp (0, maxY)) <*> (p + v)

  cmap $ uncurry $ \(_ :: Cow, Position p) -> \case
    Velocity v -> Position $ p + v

  cmap $ uncurry $ \(Tumbleweed, Position p) -> \case
    Velocity v -> Position $ p + v

  -- | Tick Yips
  cmap $ \case
    Yip{..} -> Just $
      if yipUntil <= ?clock
      then YipCooldown{cooldownUntil = ?clock + ?params.player.yipCooldown}
      else Yip{..}
    YipCooldown{..} -> if cooldownUntil <= ?clock then Nothing else Just YipCooldown{..}

  cmap $ \(cow :: Cow) -> case cow.state of
    Cow'Yipped{..} | yippedUntil == ?clock -> cow & #state .~ Cow'Grazing []
    _ -> cow

  -- | Tick Mothership

  -- Float it in
  cmapM @Mothership $ \_ ->
    -- This is a lot of easing math, but it works. I wonder if more combinators would make
    -- this more declarative
    let V2 abx _ = ?params.mothership.abductionLocation
        pathLen = ?params.mothership.abductionHeight - ?params.mothership.startHeight
        ef = quadOut
        splitTime = ?clock `mod` ten
        abductTime = ?params.mothership.abductionDuration
        idleTime = ten - ?params.mothership.abductionDuration
        upTime = idleTime `div` 2
        downTime = idleTime `div` 2
        yoffset = if | ?clock < ten -> easeBetween ef ?clock ten * pathLen
                     | splitTime < abductTime -> pathLen
                     | splitTime - abductTime < upTime -> easeBetween (invEase ef) (splitTime - abductTime) upTime * pathLen
                     | otherwise -> easeBetween ef (splitTime - upTime - abductTime) downTime * pathLen
        currv2 = V2 abx (yoffset + ?params.mothership.startHeight)
    in do
      debug_ $ Debug.ShapeEntry currv2 GPU.blue $ Debug.CircleFilled 20
      pure $ Position currv2

  cmap $ \case
    Mothership'Idle -> Nothing
    Mothership'Abducting _ -> Just Abduction
      { loc = Position ?params.mothership.abductionLocation
      , hitboxes = ?params.mothership.abductionHitboxes
      }

  cmapM $ \(ms :: Mothership, MothershipTimer mt) -> do
    --debug_ $ Debug.TextEntry (V2 300 0) $ "TTA: " ++ show (mt - ?clock)
    case ms of
      Mothership'Idle ->
        if ?clock == mt
        then assetly $ do
          cs_ctx <- askCuteSound
          _ <- liftIO $ CS.insertSound cs_ctx ?assets.sounds.abduction.sound
          pure $ Mothership'Abducting (?clock + ?params.mothership.abductionDuration)
        else pure Mothership'Idle
      Mothership'Abducting af ->
        pure $ if ?clock == af
        then Mothership'Idle
        else Mothership'Abducting af

  cmap $ \(MothershipTimer mt) ->
    if mt == ?clock
    then MothershipTimer (?clock + ten)
    else MothershipTimer mt

draw :: forall es. CanLoop es => Eff es ()
draw = assetly $ paramly $ clockly $ do
  screen <- askGPU
  liftIO $ GPU.blit ?assets.images.background Nothing screen 0 0

  cmapM $ \case
    Mothership'Abducting f -> do
      let ef = pieceEase 0.5 quadIn (invEase quadIn)
      let alpha = round $ easeBetween ef (f - ?clock) ?params.mothership.abductionDuration * (64 :: Float)
      let abductionGreen = ?params.mothership.abductionColor { GPU.colorA = alpha }
      let V2 x y = ?params.mothership.abductionLocation
      liftIO $
        GPU.ellipseFilled screen x y
        ?params.mothership.abductionRadiusX
        ?params.mothership.abductionRadiusY
        0
        abductionGreen
    _ -> pure ()

  do
    let V2 x y = ?params.mothership.abductionLocation
    let roffset = ?params.mothership.targetPulseDelta *
                  easePeriodically (pieceEase 0.5 linear (invEase linear))
                  ?params.mothership.targetPulsePeriod ?clock
    liftIO $ GPU.withLineThickness 3.0 $
      GPU.ellipse screen x y
      (?params.mothership.abductionRadiusX + roffset)
      (?params.mothership.abductionRadiusY + roffset)
      0
      ?params.mothership.targetColor

  cowBlits <- cfoldMap $ \(Cow cowState cowType, Position (V2 x y)) -> do
    let cowSprite = ?assets.images.cow cowType
    let blitCow = liftIO $ GPU.blit cowSprite Nothing screen x y
    case cowState of
      Cow'Abducted -> Aligned $ Map.singleton y $ GPU.withColor cowSprite GPU.green blitCow
      _ -> Aligned $ Map.singleton y blitCow

  barnBlits <- cfoldMap $ \(Barn, Position (V2 x y)) -> do
    let blitBarn =
          liftIO $
          GPU.withAnchor ?assets.images.barn 0.5 1 $
          GPU.blit ?assets.images.barn Nothing screen x y
    Aligned $ Map.singleton y blitBarn

  weedBlits <- cfoldMap $ \(Tumbleweed, Position (V2 x y)) -> do
    let yOffset =
          ?params.flora.weedBounceDelta *
          easePeriodically (pieceEase 0.5 quadIn (invEase quadOut)) ?params.flora.weedBouncePeriod ?clock -
          (?params.flora.weedBounceDelta/2)
    let blitWeed =
          liftIO $
          GPU.withAnchor ?assets.images.weed 0.5 0.5 $
          GPU.blit ?assets.images.weed Nothing screen x (y + yOffset)
    Aligned $ Map.singleton y blitWeed

  grassBlits <- cfoldMap $ \(Grass{..}, Position (V2 x y)) -> do
    let grassSprite = ?assets.images.grass type_
    let blitGrass =
          liftIO $
          GPU.withAnchor grassSprite 0.5 0.5 $
          GPU.blit grassSprite Nothing screen x y
    Aligned $ Map.singleton y blitGrass

  traverse_ snd $ Map.toAscList $ unAligned $ mconcat
    [ cowBlits
    , barnBlits
    , weedBlits
    , grassBlits
    ]

  drawPlayer

  cmapM_ $ \(_ :: Mothership, Position (V2 x y)) -> do
    let sprite = ?assets.images.mothership
    liftIO $ GPU.withAnchor sprite 0.5 0.5 $
      GPU.blit sprite Nothing screen x y

  liftIO $ GPU.blit ?assets.images.boss Nothing screen 0 0
  let bossMsg = unlines
        [ "We need cattle STAT!"
        , "Standby for pickup\nevery 10 Earth secs."
        , "Don't you lose any.."
        ]
  liftIO $ void $ FC.draw ?assets.fonts.main.rawFont screen 90 10 bossMsg
  do
    Score score <- gget
    LostCows lostCows <- gget
    let ui = unlines
          [ unwords [ "Score =", show score]
          , unwords [ "Lost Cows =", show lostCows]
          ]
    liftIO $ void $ FC.draw ?assets.fonts.main.rawFont screen 700 0 ui

drawTitle :: CanLoop es => Controller -> Eff es ()
drawTitle controller = assetly $ do
  (x, y) <- askNativeRezF
  screen <- askGPU
  let title = "ALIEN CATTLE RUSTLERS"
  let pressEnter = "Press ENTER to begin"
  let instructions = unlines
        [ "CONTROLS:"
        , "---"
        , "Hold Space/Left Mouse Button and drag the mouse to fly"
        , "Press F to holler at the cows"
        , "---"
        , "Try out the flight controls and see how\nthe control stick in the cockpit responds below:"
        ]
  let f1practice = "Press F1 to enter flight practice mode"
  liftIO $ do
    FC.drawAlign ?assets.fonts.main__Title.rawFont screen (x/2) (y/2 - 100) FC.alignCenter title
    FC.drawAlign ?assets.fonts.main.rawFont screen (x/2) (y/2 - 60) FC.alignCenter pressEnter
    FC.drawAlign ?assets.fonts.main.rawFont screen (x/2) (y/2) FC.alignCenter instructions

    let gateRadius = 30
    let stickCenter = V2 (x/2) (y - 100)
    drawControlStick'pseudoRealistic screen controller.movementStick stickCenter gateRadius

    void $ FC.drawAlign ?assets.fonts.main.rawFont screen (x/2) (y-50) FC.alignCenter f1practice

checkForGameover :: CanLoop es => Eff es ()
checkForGameover =
  paramly $ gget >>= \(LostCows n) ->
    if n >= ?params.numLives
    then gset Screen'Gameover
    else gset Screen'Playing

drawGameover :: CanLoop es => Eff es ()
drawGameover = assetly $ paramly $ do
  screen <- askGPU
  GPU.withScreenColor screen ?params.gameoverColor draw
  (x, y) <- askNativeRezF
  let msg = "Game Over!\n\nToo many cows lost - you're fired!\n\nPress ENTER to try again"
  liftIO $ void $
    FC.drawAlign ?assets.fonts.main.rawFont screen (x/2) (y/2 - 50) FC.alignCenter msg

playBGSFX :: CanLoop es => Eff es ()
playBGSFX = assetly $ paramly $ do
  -- GC done sounds
  cmapM $ \BGSFX{..} -> do
    isPlaying <- liftIO $ CS.isActive sound.sound
    pure $ if (not $ toBool isPlaying) then Nothing else Just BGSFX{..}

  hasBGSFX <- cexists @BGSFX
  unless (hasBGSFX) $ do
    roll'shouldPlay <- uniformFloat01M globalStdGen
    let shouldPlay = roll'shouldPlay < ?params.bgsfxRate
    when shouldPlay $ do
      let allBGSFX =
            [ ?assets.sounds.moo
            , ?assets.sounds.riff1
            , ?assets.sounds.riff2
            , ?assets.sounds.riff3
            , ?assets.sounds.riff4
            ]
      whichBGSFX <- uniformRM (0, length allBGSFX - 1) globalStdGen
      let pickedBGSFX = allBGSFX !! whichBGSFX
      cs_ctx <- askCuteSound8000
      _ <- liftIO $ CS.insertSound cs_ctx pickedBGSFX.sound
      newEntity_ BGSFX{sound = pickedBGSFX}

genGrass :: CanGame es => Eff es ()
genGrass = paramly $ do
  grass :: [GrassType] <- replicateM ?params.flora.numGrass (uniformEnumM globalStdGen)
  locs :: [Position] <- replicateM ?params.flora.numGrass $ do
    x <- uniformRM (10, 838) globalStdGen
    y <- uniformRM (100, 400) globalStdGen
    pure $ Position $ V2 x y
  for_ (fmap Grass grass `zip` locs) newEntity_

weedGC :: CanLoop es => Eff es ()
weedGC = do
  (maxX, _) <- askNativeRezF
  cmap $ \weed@(Tumbleweed, Position (V2 x _), Velocity _) ->
    if | x < 0 -> Nothing
       | x > maxX -> Nothing
       | otherwise -> Just weed

spawnWeed :: CanLoop es => Eff es ()
spawnWeed = paramly $ do
  (maxX, maxY) <- askNativeRezF
  alreadyTumbling <- cexists @Tumbleweed
  unless alreadyTumbling $ do
    roll'should <- uniformFloat01M globalStdGen
    when (roll'should < ?params.flora.weedRate) $ do
      shouldGoRight :: Bool <- uniformM globalStdGen
      let dir = if shouldGoRight then V2 1 0 else V2 (-1) 0
      let x = if shouldGoRight then 0 else maxX
      y <- uniformRM (?params.field.skyline, maxY) globalStdGen
      newEntity_
        ( Tumbleweed
        , Position $ V2 x y
        , Velocity $ dir ^* ?params.flora.weedSpeed
        )
