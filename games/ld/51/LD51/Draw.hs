module LD51.Draw where

import Foreign.Ptr
import SDL.GPU.Utils qualified as GPU
import Ease
import Linear

import Mayhem.Engine.Utils
import Mayhem.Engine.Effects

import LD51.M
import LD51.World
import LD51.Assets qualified as A
import LD51.Params qualified as P

drawPlayer :: CanLoop es => Eff es ()
drawPlayer = paramly $ assetly $ gpuly $ cmapM_ $ \(Player, Position (V2 x y), Movement{}) -> do
  do
    clk <- getClock
    let bobOffset =
          ?params.player.bobAnimationDelta *
          easePeriodically (pieceEase 0.5 quadIn (invEase quadOut)) ?params.player.bobAnimationPeriod clk -
          (?params.player.bobAnimationDelta / 2)
    let alienSprite = ?assets.images.alien
    liftIO $ GPU.withAnchor alienSprite 0.5 0.5 $
      GPU.blit alienSprite Nothing ?screen x (y + bobOffset)
