{-# LANGUAGE TemplateHaskell #-}

module LD51.Params where

import Data.Word
import Linear.V2
import CuteC2
import SDL.GPU.Utils qualified as GPU

import Mayhem.Engine.TH
import Mayhem.Engine.Utils
import Mayhem.Engine.Frame

data Main = Main
  { player :: Player
  , controls :: Controls
  , cows :: Cows
  , mothership :: Mothership
  , field :: Field
  , barns :: Barns
  , numLives :: Int
  , gameoverColor :: GPU.Color
  , bgsfxRate :: Float
  , flora :: Flora
  }

data Barns = Barns
  { locs :: [V2 Float]
  , spawnRate :: Frame
  , maxCows :: Int
  }

data Field = Field
  { skyline :: Float
  }

data Player = Player
  { startLoc :: V2 Float
  , acceleration :: Float
  , deceleration :: Float
  -- IDEA: Different value for when we accelerate at top speed for turning tuning
  , maxSpeed :: Float
  , bobAnimationPeriod :: Frame
  , bobAnimationDelta :: Float
  , yipDuration :: Frame
  , yipCooldown :: Frame
  -- x,y is relative to Player loc (center of alien)
  , herdHitbox :: C2Shape
  , yipHitbox :: C2Shape
  }

data Cows = Cows
  { hurtbox :: C2Shape
  , herdSpeed :: Float
  , yipSpeed :: Float
  , walkSpeed :: Float
  , abductedSpeed :: Float
  , yipDuration :: Frame
  , goalSplit :: Float
  , idleRange :: (Word64, Word64)
  , walkRange :: (Word64, Word64) -- In Frames
  }

data Mothership = Mothership
  { abductionRadiusX :: Float
  , abductionRadiusY :: Float
  , abductionHitboxes :: [C2Shape] -- Centered around location
  , abductionDuration :: Frame
  , abductionLocation :: V2 Float
  , abductionColor :: GPU.Color
  , targetColor :: GPU.Color
  , targetPulseDelta :: Float
  , targetPulsePeriod :: Frame
  , descentDuration :: Frame
  , leavingDuration :: Frame
  , startHeight :: Float
  , abductionHeight :: Float
  }

data Flora = Flora
  { numGrass :: Int
  , weedRate :: Float
  , weedSpeed :: Float
  , weedBouncePeriod :: Frame
  , weedBounceDelta :: Float
  , weedSpinPeriod :: Frame
  }

data Controls = Controls
  { movementStickRadius :: Float
  }

hardcoded :: Main
hardcoded = Main
  { numLives = 3
  , gameoverColor = GPU.red
  , bgsfxRate = let everySecond = 1/60 in everySecond / 5
  , flora = Flora
    { numGrass = 20
    , weedRate = let everySecond = 1/60 in everySecond / 5
    , weedSpeed = 1
    , weedBouncePeriod = 60
    , weedBounceDelta = 2
    , weedSpinPeriod = 60
    }
  , field = Field
    { skyline = 90
    }
  , barns = Barns
    { locs =
        [ V2 200 200
        , V2 600 300
        ]
    , spawnRate = 180
    , maxCows = 15
    }
  , player = Player
    { startLoc = V2 442 400
    , acceleration = 0.1
    , deceleration = 0.03
    , maxSpeed = 3
    , bobAnimationPeriod = 60
    , bobAnimationDelta = 3
    , yipDuration = 10
    , yipCooldown = 90
    , herdHitbox = c2Circle (C2V 0 0) 35
    , yipHitbox = c2Circle (C2V 0 0) 70
    }
  , cows = Cows
    { hurtbox = c2Circle (C2V 15 10) 10
    , herdSpeed = 3
    , yipSpeed = 3
    , walkSpeed = 1
    , abductedSpeed = 5
    , yipDuration = 40
    , goalSplit = 0.75
    , idleRange = (60, 180)
    , walkRange = (60, 120)
    }
  , mothership = Mothership
    { abductionRadiusX = 70
    , abductionRadiusY = 40
    , abductionHitboxes = -- TODO: tune this and make it depend on rx, ry
      [ c2Circle (C2V 0 0) 40
      , c2Circle (C2V 40 0) 25
      , c2Circle (C2V -40 0) 25
      ]
    , abductionDuration = 60
    , abductionLocation = V2 424 270
    , abductionColor = GPU.green
    , targetColor = GPU.Color 184 22 135 64
    , targetPulseDelta = 7
    , targetPulsePeriod = 90
    , descentDuration = 3 * 60
    , leavingDuration = 60
    , startHeight = -100
    , abductionHeight = 100
    }
  , controls = Controls
    { movementStickRadius = 120
    }
  }

-- TH_CODE
mkOpticsLabels ''Main
mkOpticsLabels ''Player
mkOpticsLabels ''Controls
mkOpticsLabels ''Cows
