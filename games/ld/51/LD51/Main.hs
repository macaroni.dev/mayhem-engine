module Main where

import Data.Bool (bool)
import Data.Functor (void)
import Data.Foldable
import Control.Monad
import Control.Concurrent (forkOS)
import Control.Monad.Managed (runManaged)
import Optics
import SDL qualified
import SDL.Raw qualified

import Mayhem.Engine.Effects
import Mayhem.Engine.Effects.GHCi
import Mayhem.Engine.Effects.Debug
import Mayhem.Engine.Effects.Stepper
import Mayhem.Engine.Loop
import Mayhem.Engine.Assets
import Mayhem.Engine.Resolution
import Mayhem.Engine.Utils
import Mayhem.Console.Jam'22

import LD51.Game qualified as Game
import LD51.Assets qualified as A
import LD51.Params qualified as P
import LD51.World (World, initWorld)

main :: IO ()
main = main' Nothing

ghciMain :: IO (GHCiPipe (Jam'22'GHCi A.Main P.Main World))
ghciMain = do
  ghciPipe <- newGHCiPipe
  _ <- forkOS $ main' $ Just ghciPipe
  pure ghciPipe

gameRez :: NativeResolution
gameRez = edtv

main' :: Maybe (GHCiPipe (Jam'22'GHCi A.Main P.Main World)) -> IO ()
main' ghciPipe = runManaged $ do
  devmode <- liftIO $ isEnvSet "DEVMODE"

  Run run <- runJam'22 @A.Main @P.Main @World
             devmode
             (theViewAssets $ #fonts % #debug % #rawFont)
             ghciPipe
             gameRez
             initWorld
             (pure P.hardcoded)

  A.Main{gamecontrollerdb} <- run theAssets
  liftIO $ withCString gamecontrollerdb.path $ \db -> do
    ret <- SDL.Raw.gameControllerAddMappingsFromFile db
    when (ret < 0) $ error "Unable to initialize controller db"

  liftIO SDL.availableJoysticks >>= \joys -> for_ joys $ \j -> do
    _ <- managed (bracket (SDL.openJoystick j) SDL.closeJoystick)
    _ <- managed (bracket (SDL.Raw.gameControllerOpen j.joystickDeviceId) SDL.Raw.gameControllerClose)
    pure ()
  run Game.main
