{-# LANGUAGE TemplateHaskell #-}

module LD51.Assets where

import GHC.Generics

import Foreign.Ptr
import SDL.GPU.Simple qualified as GPU

import Mayhem.Engine.Assets
import Mayhem.Engine.TH
import Mayhem.Engine.Utils

import LD51.World

data Main = Main
  { fonts :: Fonts
  , images :: Images
  , sounds :: Sounds
  , gamecontrollerdb :: TextFilePath
  }
  deriving stock Generic
  deriving anyclass Asset

data Fonts = Font
  { debug :: Font 24 ('Color 255 255 255 255) -- https://squaregear.net/fonts/tiny.html
  , main :: Font 13 ('Color 255 255 255 255) -- https://ninjikin.itch.io/a-font-for-the-future-that-never-existed-80s-retro-future-font
  , main__Title :: Font 26 ('Color 255 255 255 255)
  }
  deriving stock Generic
  deriving anyclass Asset

data Images = Images
  { alien :: Ptr GPU.Image
  , cow :: CowType -> Ptr GPU.Image
  , background :: Ptr GPU.Image
  , mothership :: Ptr GPU.Image
  , barn :: Ptr GPU.Image
  , boss :: Ptr GPU.Image
  , grass :: GrassType -> Ptr GPU.Image
  , weed :: Ptr GPU.Image
  }
  deriving stock Generic
  deriving anyclass Asset

data Sounds = Sounds
  { yip :: CuteSound
  , moo :: CuteSound
  , abduction :: CuteSound
  , riff1 :: CuteSound
  , riff2 :: CuteSound
  , riff3 :: CuteSound
  , riff4 :: CuteSound
  }
  deriving stock Generic
  deriving anyclass Asset

-- TH_CODE
mkOpticsLabels ''Main
mkOpticsLabels ''Fonts
mkOpticsLabels ''Images
mkOpticsLabels ''Sounds
