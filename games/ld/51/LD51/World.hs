{-# LANGUAGE TemplateHaskell #-}
module LD51.World where

import Data.Monoid

import Apecs
import Linear
import System.Random
import CuteC2

import Mayhem.Engine.Frame
import Mayhem.Engine.Assets
import Mayhem.Engine.TH
import Mayhem.Engine.Utils

data Screen = Screen'Title | Screen'Playing | Screen'Gameover | Screen'InputDemo deriving stock (Show)

instance Semigroup Screen where
  x <> _ = x
instance Monoid Screen where
  mempty = Screen'Title
instance Component Screen where type Storage Screen = Global Screen

newtype Score = Score Int
  deriving stock (Eq, Ord, Show)
  deriving newtype (Enum, Bounded)
  deriving (Semigroup, Monoid) via Sum Int
instance Component Score where type Storage Score = Global Score

newtype BGSFX = BGSFX { sound :: CuteSound }
instance Component BGSFX where type Storage BGSFX = Unique BGSFX

newtype LostCows = LostCows Int
  deriving stock (Eq, Ord, Show)
  deriving newtype (Enum, Bounded)
  deriving (Semigroup, Monoid) via Sum Int
instance Component LostCows where type Storage LostCows = Global LostCows

newtype Position = Position { unPosition :: (V2 Float) } deriving stock (Eq, Show)
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity { unVelocity :: (V2 Float) }
  deriving stock (Eq, Show)

instance Component Velocity where type Storage Velocity = Map Velocity

data Movement = Movement
  { targetVelocity :: Velocity
  , rate :: Float
  } deriving stock (Show)
instance Component Movement where type Storage Movement = Map Movement

data Player = Player deriving stock (Show)
instance Component Player where type Storage Player = Unique Player

data Yip =
    Yip { yipUntil :: Frame }
  | YipCooldown { cooldownUntil :: Frame }
  deriving stock (Show)
instance Component Yip where type Storage Yip = Unique Yip

data Cow = Cow
  { state :: CowState
  , type_ :: CowType
  } deriving stock (Show)
instance Component Cow where type Storage Cow = Map Cow

data CowType = Cow1 | Cow2 | Cow3 deriving stock (Show, Eq, Ord, Enum, Bounded)
data CowState =
    Cow'Grazing [CowGoal]
  | Cow'Herded
  | Cow'Yipped { yippedDir :: (V2 Float), yippedUntil :: Frame }
  | Cow'Abducted deriving stock (Show)

data CowGoal =
    CowGoal'Idle { idleUntil :: Frame }
  | CowGoal'Walk { walkDir :: V2 Float, walkUntil :: Frame }
  deriving stock (Show)

data Barn = Barn deriving stock (Show)
instance Component Barn where type Storage Barn = Map Barn

data Mothership =
    Mothership'Idle
  | Mothership'Abducting Frame
  deriving stock (Show)
instance Component Mothership where type Storage Mothership = Unique Mothership

data MothershipTimer = MothershipTimer Frame deriving stock (Show)
instance Component MothershipTimer where type Storage MothershipTimer = Unique MothershipTimer

data Abduction = Abduction { loc :: Position, hitboxes :: [C2Shape] } deriving stock (Show)
instance Component Abduction where type Storage Abduction = Unique Abduction

data GrassType =
    Grass1
  | Grass2
  | Grass3
  | Grass4
  deriving stock (Show, Eq, Ord, Enum, Bounded)

data Grass = Grass
  { type_ :: GrassType
  } deriving stock (Show)
instance Component Grass where type Storage Grass = Map Grass

data Tumbleweed = Tumbleweed
instance Component Tumbleweed where type Storage Tumbleweed = Unique Tumbleweed

-- Input handling
-- This could technically even be another "World"

data StickActive = StickActive (V2 Float) deriving stock (Show)
instance Component StickActive where type Storage StickActive = Unique StickActive

newtype GameControllerStick = GameControllerStick { unGameControllerStick :: (V2 Float) }
  deriving stock (Show)
  deriving newtype (Num)
  deriving (Semigroup, Monoid) via (Sum GameControllerStick)

instance Component GameControllerStick where type Storage GameControllerStick = Global GameControllerStick

-- TH_CODE

makeWorld "World"
  [ ''Position
  , ''Velocity
  , ''Movement
  , ''Player
  , ''Yip
  , ''Cow
  , ''Mothership
  , ''MothershipTimer
  , ''Abduction
  , ''Score
  , ''LostCows
  , ''Barn
  , ''Screen
  , ''BGSFX
  , ''Grass
  , ''Tumbleweed
  -- Input handling
  , ''StickActive
  , ''GameControllerStick
  ]

mkOpticsLabels ''Cow
