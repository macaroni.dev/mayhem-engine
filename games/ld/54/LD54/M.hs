module LD54.M where

import Mayhem.Console.Jam'22
import Mayhem.Engine.Effects.GameController

import LD54.Assets as A
import LD54.Params as P
import LD54.World
import LD54.Renderer

-- TODO: This is a little ugly - any expansion I have doesn't get picked up by ghci..
-- Remove GHCi from Jam'22? Or have Jam'22 take a list of additional expansions
type LD54 = Render : GameController : Jam'22 A.Main P.Main World
type LD54'GHCi = Jam'22'GHCi A.Main P.Main World
