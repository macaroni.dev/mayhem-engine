module LD54.Assets where

import GHC.Generics
import Mayhem.Engine.Assets

data Main = Main
  { fonts :: Fonts
  , gamecontrollerdb :: TextFilePath
  }
  deriving stock Generic
  deriving anyclass Asset

data Fonts = Fonts
  { debug :: Font 24 ('Color 255 255 255 255) -- ComicMono for now
  }
  deriving stock Generic
  deriving anyclass Asset
