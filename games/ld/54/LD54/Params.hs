module LD54.Params where

data Main = Main
  { playerRadius :: Float
  , playerSpeed :: Float
  , crowdRadius :: Float
  }
