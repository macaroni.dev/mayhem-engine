module LD54.Params.Hardcoded where

import Control.Monad.IO.Class

import LD54.Params qualified as P

hardcoded :: MonadIO m => m P.Main
hardcoded = pure $ P.Main
  { playerRadius = 10
  , playerSpeed = 0.5
  , crowdRadius = 10
  }
