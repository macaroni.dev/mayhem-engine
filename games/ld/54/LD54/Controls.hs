module LD54.Controls where

import SDL qualified

import Mayhem.Engine.Controller
import Mayhem.Engine.Effects
import Mayhem.Engine.Loop
import Linear ((^*))

import LD54.M
import LD54.World
import LD54.Params qualified

data C = C
  { movestick :: ControlStick
  } deriving stock (Show)

parseController :: [SDL.Event] -> Eff (Framed LD54) C
parseController = fmap (C . arcadeControlStick . dpadArcadeStick2) . wasd

handleControls :: C -> Eff (Framed LD54) ()
handleControls C{..} = paramly $ do
  cmap $ \Player -> Velocity (movestick.location ^* ?params.playerSpeed)
