{-# LANGUAGE BlockArguments #-}
module LD54.Game where

import Prelude hiding (min, max)

import Control.Monad
import Control.Monad.Extra (whenM)
import Data.Foldable
import Safe (minimumMay)
import Data.Maybe (mapMaybe)
import Data.Function
import System.Random (initStdGen)
import Optics

import Mayhem.Engine.Loop
import Mayhem.Engine.Effects
import Mayhem.Engine.Utils
import Mayhem.Engine.Automaton
import Mayhem.Engine.Diamond
import Mayhem.Engine.LinearDotSyntax ()
import Mayhem.Engine.Geometry
import Mayhem.Engine.Controller
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Console.Jam'22

import SDL.GPU.Utils qualified as GPU
import SDL.GPU.Utils qualified as GPU.Camera (CameraF(..))
import SDL.GPU.FC.Simple qualified as FC
import SDL.GPU.CuteC2 qualified as GPU
import Cute.Sound.C qualified as CS
import CuteC2
import Linear

import LD54.M
import LD54.World
import LD54.Assets qualified as A
import LD54.Params qualified as P
import LD54.Controls
import LD54.Renderer

import Debug.RecoverRTTI

game :: Maybe (HotPipe (Framed LD54)) -> Eff LD54 ()
game hotPipe = do
  initGame
  framedHot hotPipe gameloop

gameloop :: Eff (Framed LD54) ()
gameloop = do
  tickGHCi
  askGPU >>= liftIO . GPU.clear
  f7ToggleDebug
  f11Fullscreen
  f2Stepper
  adaptToWindowResize

  input >>= parseController >>= \c -> do
    stepped $ clocked $ do
      gpuly $ assetly $ do
        render'OnLayer HUD $ void $
          FC.draw
          ?assets.fonts.debug.rawFont
          ?screen
          0
          0
          (show c.movestick.location)

      handleControls c
      cmap $ \(Position p, Velocity v) -> Position (p + v)
      checkCollisions
      draw
    whenM (Debug.enabled) $ pure ()

  liftIO performMinorGC
  askCuteSound >>= liftIO . CS.mix
  askCuteSound8000 >>= liftIO . CS.mix
  render'Flush
  askGPU >>= liftIO . GPU.flip

initGame :: Eff LD54 ()
initGame = do
  newEntity_ (Player, Position (V2 0 50), Velocity 0)
  traverse_ (\p -> newEntity_ (Crowd, p, Velocity 0)) $ mconcat $ flip fmap [0..1000] $ \n ->
    [ Position (V2 n n)
    ]

checkCollisions :: Eff (Framed LD54) ()
checkCollisions = paramly $ gpuly $ assetly $ do
  cmapM_ $ \(Player, Position p) -> cmapM_ $ \(Crowd, Position c) -> do
    let pbox = c2Circle (v22c2v p) ?params.playerRadius
        cbox = c2Circle (v22c2v c) ?params.crowdRadius
    when (c2Collided pbox cbox) $ do
      render'OnLayer HUD $ void $
        FC.draw
        ?assets.fonts.debug.rawFont
        ?screen
        300
        0
        "COLLIDED!"


draw :: Eff (Framed LD54) ()
draw = gpuly $ do
  cmapM_ $ \(Player, Position p) -> render'OnLayer (Field p.y) $ do
    liftIO $ GPU.circleFilled ?screen p.x p.y 10 GPU.yellow

  cmapM_ $ \(Crowd, Position p) -> render'OnLayer (Field p.y) $ do
    liftIO $ GPU.circleFilled ?screen p.x p.y 10 GPU.red
