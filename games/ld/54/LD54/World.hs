{-# LANGUAGE TemplateHaskell #-}

module LD54.World where

import Apecs
import Linear

newtype Position = Position { unPosition :: (V2 Float) } deriving stock (Eq, Show)
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity { unVelocity :: (V2 Float) }
  deriving stock (Eq, Show)

instance Component Velocity where type Storage Velocity = Map Velocity

data Player = Player
  deriving stock (Eq, Show)
instance Component Player where type Storage Player = Unique Player

data Crowd = Crowd
  deriving stock (Eq, Show)
instance Component Crowd where type Storage Crowd = Map Crowd

data Camera = Camera
  { pos :: V2 Float
  , offset :: V2 Float
  , zoom :: Float
  }

-- Unlawful
instance Semigroup Camera where
  x <> _ = x

instance Monoid Camera where
  mempty = Camera { pos = 0, offset = 0, zoom = 1 }
instance Component Camera where type Storage Camera = Global Camera

makeWorld "World"
  [ ''Position
  , ''Velocity
  , ''Camera
  , ''Player
  , ''Crowd
  ]
