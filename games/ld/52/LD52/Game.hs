{-# LANGUAGE BlockArguments #-}
module LD52.Game where

import Prelude hiding (min, max)

import Control.Monad
import Data.Foldable
import Safe (minimumMay)
import Data.Maybe (mapMaybe)
import Data.Function

import Mayhem.Engine.Loop
import Mayhem.Engine.Effects
import Mayhem.Engine.Utils
import Mayhem.Engine.Diamond
import Mayhem.Engine.Geometry
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Engine.LinearDotSyntax ()
import Mayhem.Console.Jam'22
import LD52.M

import SDL.GPU.Utils qualified as GPU
import SDL.GPU.Utils qualified as GPU.Camera (CameraF(..))
import SDL.GPU.FC.Simple qualified as FC
import SDL.GPU.CuteC2 qualified as GPU
import Cute.Sound.C qualified as CS
import CuteC2
import Linear hiding (trace)

import LD52.World
import LD52.Assets qualified as A
import LD52.Params qualified as P
import LD52.Timer
import LD52.Control

import Debug.Trace

game :: Maybe (HotPipe (Framed LD52)) -> Eff LD52 ()
game hotPipe = do
  initGame
  framedHot hotPipe gameloop

gameloop :: Eff (Framed LD52) ()
gameloop = do
    tickGHCi
    controller <- keyboardController
    askGPU >>= liftIO . GPU.clear
    f7ToggleDebug
    f11Fullscreen
    f2Stepper
    adaptToWindowResize

    gset Screen'Playing
    gget >>= \case
      Screen'Title -> askGPU >>= \s -> GPU.circleFilled s 50 50 50 (GPU.white)
      Screen'Playing -> playing'main controller
      Screen'Gameover -> pure ()
      Screen'InputDemo -> pure ()
    liftIO performMinorGC
    askCuteSound >>= liftIO . CS.mix
    askCuteSound8000 >>= liftIO . CS.mix
    askGPU >>= liftIO . GPU.flip

initGame :: Eff LD52 ()
initGame = gpuly $ do
  GPU.enableCamera ?screen False
  _ <- newEntity
    ( Player
    , tickForever Player'Idle
    , Velocity 0
    , Facing'Left
    , Position $ V2 400 0
    )
  _ <- newEntity
    ( SproutSpawn True
    , Position $ V2 500 1000
    )
  _ <- newEntity
    ( SproutSpawn True
    , Position $ V2 777 1000
    )
  pure ()

playing'main :: Controller -> Eff (Framed LD52) ()
playing'main Controller{..} = do
  stepped $ clocked $ paramly $ do
    Debug.reset

    debug_ $ Debug.FreeForm $ \s -> do
      GPU.arcFilled s 200 1000 20 0 -180 (GPU.Color 0 0 255 255)
      GPU.arcFilled s 240 1000 20 0 180 (GPU.Color 0 255 255 255)
      let cap = c2Capsule (C2V 200 900) (C2V 300 900) 10
      GPU.c2Shape s cap (GPU.Color 0 0 255 255)

    -- Handle time
    tickerMapM $ \case
      Player'Dash -> pure $ tickForever Player'Idle
      Player'JumpSquat -> do
        -- TODO: hacky and unprincipled
        let js = if jump.isHeld then ?params.player.jumpSpeed else ?params.player.shorthopSpeed
        cmap $ \(Player, Velocity v) -> Velocity (v + V2 0 (negate js))
        pure $ tickForever Player'Jump
      Player'Land -> pure $ tickForever Player'Idle
      Player'Grab -> pure $ tickForever Player'Idle
      Player'Grabbing -> pure $ tickForever Player'Idle

    tickerMapME_ $ \ety -> \s -> case s of
      Sprout'Pulled -> do
        cset ety (tickDown Sprout'Charge ?params.sprouts.chargeFrames, Sprout'Held)
      Sprout'Charge -> do
        cset ety (tickDown Sprout'Scream ?params.sprouts.screamFrames, Nothing @Sprout'Held)
      Sprout'Scream -> do
        Sprout{..} <- cget ety -- Technically unsafe - extend tickerMap to allow other components as well
        destroyC @Position ety
        destroyC @Velocity ety
        destroyC @Sprout ety
        destroyC @Sprout'Held ety
        destroyC @SproutBombStateT ety
        cset spawnedFrom SproutSpawn { hasSprout = True }

    ----
    handleInput Controller{..}

    surfaces <- mkSurfaces


    -- Follow slopes w/velocity when grounded:
    -- TODO:
    -- - Can we generalize this + the ground check for all points/surfaces?
    --   A general-purpose slide-along + FP-error-reducing thing?
{-    cmapM_ $ \(Player, Position (mkDiamond -> d), Velocity v@(V2 v'x v'y), tps, ety :: Entity) -> do
      when (isGrounded tps) $ do
        for_ (diamondAlongGrounds v d surfaces) $ cset ety . Velocity
        --for_ (diamondAlongGrounds2 v d surfaces) $ \dv ->
-}
    cmapM $ \(Player, Position p, Velocity v, tps, ety :: Entity) -> do
      let diamond'from = mkDiamond p
      let diamond'to = mkDiamond (p + v)
      let relevantSurfaces = -- UNUSED - is it necessary?
            if isGrounded tps then filter (not . diamondGrounded diamond'from) surfaces else surfaces
      let res =
            if isGrounded tps then
              groundedPointSweep v diamond'from surfaces
            else
                 maybe (GroundedSweep'Unimpeded v) GroundedSweep'Impeded
               $ pointSweep diamond'from diamond'to surfaces
      case res of
        GroundedSweep'Unimpeded{..} -> do
          pure $ Position (p + sweep)
        GroundedSweep'Impeded{collision = pc@PointCollision{..}} -> do
          logString $ unwords ["collision =", show pc]
          tickerCase' tps $ \case
            Player'Jump -> do
              when (surface.ty == Ground && point == BOTTOM) $ cset ety (tickDown Player'Land ?params.player.landFrames)
              cset ety $ Velocity $ case surface.ty of
                Ground -> V2 v.x 0
                Ceiling -> V2 v.x 0
                RightWall -> V2 0 v.y
                LeftWall -> V2 0 v.y
            _ -> pure ()
          pure $ Position (p + sweep)

    debug_ $ Debug.FreeForm $ \screen -> for_ surfaces $ \Surface{..} -> do
      let Seg (V2 x1 y1) (V2 x2 y2) = line
      let color = case ty of
            Ground -> GPU.Color 255 255 255 255
            Ceiling -> GPU.Color 255 0 0 255
            RightWall -> GPU.Color 0 255 0 255
            LeftWall -> GPU.Color 0 0 255 255
      GPU.withLineThickness 3.0 $ GPU.line screen x1 y1 x2 y2 color

    -- Apply gravity
    cmap $ \case
      (tps, Velocity v) -> tickerCase' tps $ \case
        Player'Jump -> Velocity (v + V2 0 ?params.player.gravity)
        _ -> Velocity v

    cmap $ \(sbs :: SproutBombStateT, Not :: Not Sprout'Held, Velocity v) ->
      tickerCase' @Velocity sbs $ \case
        Sprout'Pulled -> Velocity v
        Sprout'Scream -> Velocity 0
        Sprout'Charge -> Velocity (v + V2 0 ?params.player.gravity)

    -- Check for ground
    cmapM_ $ \(Player, Position (mkDiamond -> d), tps, ety) -> do
      when (isGrounded tps) $
        unless (any (diamondGrounded d) surfaces) $
          cset ety $ tickForever Player'Jump

    -- Handle interactions
    cmapM_ $ tickerCase $ \case
      Player'Grab -> handleGrabCollisions
      _ -> pure ()
    pure ()

    cmapM_ $ \(Player, Position p) -> gset $ Camera p
  playing'draw


playing'draw :: Eff (Framed LD52) ()
playing'draw = paramly $ assetly $ gpuly $ withCamera $ do
  rez <- askNativeRez
  -- Debug b/g
  for_ (takeWhile (<= ?params.playArea.h) [0, 16..]) $ \y -> debug_ $ Debug.FreeForm $ \s ->
    GPU.horizontalLine s 0 y ?params.playArea.w
  for_ (takeWhile (<= ?params.playArea.w) [0, 16..]) $ \x -> debug_ $ Debug.FreeForm $ \s ->
    GPU.verticalLine s x 0 ?params.playArea.h

  cmapM_ $ \(SproutSpawn{..}, p :: Position) -> do
    let c = if hasSprout then GPU.Color 0 255 0 255 else GPU.Color 20 128 20 255
    debugC2Shape c hasSprout p ?params.sprouts.spawnbox

  cmapM_ $ \(Player, tps, facing :: Facing, p@(Position pv@(V2 px py))) -> do
{-    let halfScreenW = rez.nativeWidth // (2 :: Int)
        halfScreenH = rez.nativeHeight // (2 :: Int)
    GPU.getDefaultCamera >>= \cam ->
      -- TODO: Consider zooming - I think without albertvaka's patch it will be a pain tho
      GPU.setCamera ?screen cam
      { GPU.Camera.x = min (px - halfScreenW) (?params.playArea.w - fromIntegral rez.nativeWidth)
      , GPU.Camera.y = min (py - halfScreenH) (?params.playArea.h - fromIntegral rez.nativeHeight)
      } -}

    let color = tickerCase' @GPU.Color tps $ \case
          Player'Idle -> GPU.Color 0 255 0 255
          Player'Walk -> GPU.Color 255 0 255 255
          Player'Dash -> GPU.Color 255 255 0 255
          Player'Land -> GPU.Color 0 255 255 255
          Player'JumpSquat -> GPU.Color 0 255 255 255
          Player'Jump -> GPU.Color 255 255 255 255
          Player'Grab -> GPU.Color 255 255 255 255
          Player'Grabbing -> GPU.Color 255 255 255 255
    let diamond = mkDiamond pv
    GPU.polygonFilled ?screen
      [ diamond.at BOTTOM
      , diamond.at LEFT
      , diamond.at TOP
      , diamond.at RIGHT
      ] color

    debugC2Shape (GPU.Color 50 255 50 255) False p $ flipC2Shape facing ?params.player.grabbox

  cmapM_ $ \(Sprout{}, p :: Position, sbst :: SproutBombStateT, Not :: Not Sprout'Stashed) -> do
    let c = tickerCase' sbst $ \case
          Sprout'Pulled -> GPU.Color 0 0 255 255
          Sprout'Charge -> GPU.Color 0 255 0 255
          Sprout'Scream -> GPU.Color 255 255 255 255
    debugC2Shape c False p ?params.sprouts.screambox

  withoutCamera $ do
    stashSize <- ccount @Sprout'Stashed
    void $ FC.draw ?assets.fonts.debug.rawFont ?screen 0 0 ("STASH: " ++ replicate stashSize 'x')
    cmapM_ $ \(Player, Position p, Velocity v) ->
      void $ FC.drawScale ?assets.fonts.debug.rawFont ?screen 300 0 (FC.Scale 0.5 0.5) $
        unlines ["POS: " ++ show p, "VEL: " ++ show v]

  Debug.render

withCamera :: Eff (Framed LD52) r -> Eff (Framed LD52) r
withCamera k = gpuly $ GPU.withFreshMatrix ?screen GPU.model $ do
  Camera (V2 x y) <- gget
  GPU.translate (negate x) (negate y) 0
  NativeResolution{..} <- askNativeRez
  let halve = (// (2 :: Int))
  GPU.translate (halve nativeWidth) (halve nativeHeight) 0
  k

withoutCamera :: Eff (Framed LD52) r -> Eff (Framed LD52) r
withoutCamera k = gpuly $ GPU.withFreshMatrix ?screen GPU.model k

flipC2Shape :: Facing -> C2Shape -> C2Shape
flipC2Shape Facing'Left = id -- convention used in Params (max is (0,0))
flipC2Shape Facing'Right = \case
  x@C2Circle'{} -> x
  C2AABB' C2AABB{..} ->
    -- We must do it this way to ensure min and max are still valid
    c2AABB (C2V max.x min.y) (C2V (max.x + (max.x - min.x)) max.y)
  x@C2Capsule'{} -> x -- unsupported rn
  x@C2Poly'{} -> x -- unsupported rn

debugC2Shape :: '[IOE, Debug] :>> es => GPU.Color -> Bool -> Position -> C2Shape -> Eff es ()
debugC2Shape color filled (Position v) shape = debug_ $ Debug.FreeForm $ \screen -> do
  case offsetC2Shape v shape of
    C2Circle' C2Circle{..} ->
      let f = if filled then GPU.circleFilled else GPU.circle
      in f screen p.x p.y r color
    C2AABB' C2AABB{..} ->
      let f = if filled then GPU.rectangleFilled else GPU.rectangle
      in f screen min.x min.y max.x max.y color
    C2Capsule' C2Capsule{..} -> pure () -- TODO: do this on saturday
    C2Poly' _ C2Poly{} -> pure () -- TODO

handleGrabCollisions :: Eff (Framed LD52) ()
handleGrabCollisions = paramly $ cmapM_ $ \(Player, facing :: Facing, Position pv, pEty) -> do
  let grabbox = offsetC2Shape pv $ flipC2Shape facing ?params.player.grabbox
  debugC2Shape (GPU.Color 255 0 0 255) True (Position 0) grabbox
  collision <- cfind $ \(SproutSpawn{..}, Position sv, _ :: Entity) ->
    let spawnbox = offsetC2Shape sv ?params.sprouts.spawnbox
    in hasSprout && c2Collided grabbox spawnbox
  for_ collision $ \(_, _, sEty) -> do
    _ <- newEntity
      ( Sprout sEty
      , tickDown Sprout'Pulled ?params.player.grabbingFrames
      , Position pv
      )
    cset sEty SproutSpawn { hasSprout = False }
    cset pEty (tickDown Player'Grabbing ?params.player.grabbingFrames, Velocity 0)

handleInput :: Controller -> Eff (Framed LD52) ()
handleInput ctrl@Controller {..} = paramly $
  cmapM_ $ \(pstate :: PlayerStateT, facing :: Facing, ety :: Entity) -> tickerCase' pstate $ \case
  Player'Idle | jump.wasPressed -> cset ety (tickDown Player'JumpSquat ?params.player.jumpSquatFrames)
  Player'Idle | left.isHeld ->
                  cset ety
                  ( tickForever Player'Walk
                  , Facing'Left
                  , Velocity $ facingV2 Facing'Left ^* ?params.player.walkSpeed
                  )
  Player'Idle | right.isHeld ->
                  cset ety
                  ( tickForever Player'Walk
                  , Facing'Right
                  , Velocity $ facingV2 Facing'Right ^* ?params.player.walkSpeed
                  )
  Player'Idle | dash.wasPressed -> do
                  cset ety
                    ( tickDown Player'Dash ?params.player.dashFrames
                    , Velocity $ facingV2 facing ^* ?params.player.dashSpeed
                    )
  Player'Idle | action.wasPressed -> handleAction ctrl ety facing
  Player'Idle -> cset ety $ Velocity 0 -- Redundant - points to the abstraction being off somehow..maybe we need to couple velocity with transitions?

  Player'Walk | dash.wasPressed -> do
                  cset ety
                    ( tickDown Player'Dash ?params.player.dashFrames
                    , Velocity $ facingV2 facing ^* ?params.player.dashSpeed
                    )
  Player'Walk | jump.wasPressed ->
                  cset ety (tickDown Player'JumpSquat ?params.player.jumpSquatFrames)
  Player'Walk | action.wasPressed -> handleAction ctrl ety facing
  Player'Walk | left.isHeld -> cset ety (tickForever Player'Walk, Facing'Left, Velocity $ facingV2 Facing'Left ^* ?params.player.walkSpeed)
  Player'Walk | right.isHeld -> cset ety (tickForever Player'Walk, Facing'Right, Velocity $ facingV2 Facing'Right ^* ?params.player.walkSpeed)
  Player'Walk -> cset ety (tickForever Player'Idle, Velocity 0)

  Player'Dash -> do
    if | dash.wasPressed ->
           case facing of
             Facing'Left | right.isHeld ->
                             cset ety
                             ( tickDown Player'Dash ?params.player.dashFrames
                             , Facing'Right
                             , Velocity $ facingV2 Facing'Right ^* ?params.player.dashSpeed
                             )
             Facing'Right | left.isHeld ->
                              cset ety
                              ( tickDown Player'Dash ?params.player.dashFrames
                              , Facing'Left
                              , Velocity $ facingV2 Facing'Left ^* ?params.player.dashSpeed
                              )
             _ -> pure ()
       | jump.wasPressed ->
           cset ety (tickDown Player'JumpSquat ?params.player.jumpSquatFrames)
       | otherwise -> pure ()

  Player'Jump | action.wasPressed -> handleAction ctrl ety facing
  Player'Jump -> pure () -- TODO: Handle drift

  Player'JumpSquat -> pure ()
  Player'Land -> pure () -- No inputs during land
  Player'Grab -> pure () -- No inputs during grab
  Player'Grabbing -> pure () -- No inputs during grabbing

handleAction :: Controller -> Entity -> Facing -> Eff (Framed LD52) ()
handleAction Controller {..} player facing = paramly $ ucgetEC >>= \case
  Nothing -> do
    if | up.isHeld -> do
           cfind (const True) >>= traverse_ \(Sprout'Stashed, t :: SproutBombStateT, ety) -> do
             destroyC @Sprout'Stashed ety
             cset ety (Sprout'Held, tickUnpause t)
       | otherwise -> cset player (tickDown Player'Grab ?params.player.grabFrames)
  Just (Sprout'Held, (Velocity v, t :: SproutBombStateT), sprout) -> do
    if | down.isHeld -> cset sprout
         ( Nothing @Sprout'Held
         , Velocity 0
         )
       | up.isHeld -> do
           stashSize <- ccount @Sprout'Stashed
           when (stashSize < ?params.player.maxStash) $
             cset sprout
             ( Nothing @Sprout'Held
             , Sprout'Stashed
             , tickPause t
             , Nothing @Velocity
             , Nothing @Position
             )
       | otherwise -> cset sprout
         ( Nothing @Sprout'Held
         , Velocity $ v + facingV2' facing * ?params.player.throwVelocity
         )

checkForGround :: Eff (Framed LD52) ()
checkForGround = paramly $ do
  cmapM_ $ \case
    (Player, tps :: Ticker PlayerState, Position (V2 x y), ety :: Entity) -> do
      tickerCase' tps $ \case
        Player'Jump -> do
          when (y > ?params.playArea.h) $
            cset ety
            ( tickDown Player'Land ?params.player.landFrames
            , Position (V2 x ?params.playArea.h)
            , Velocity 0
            )
        _ -> pure ()

  cmapM_ $ \case
    (Sprout{}, Position (V2 x y), ety :: Entity) -> do
      when (y > ?params.playArea.h) $
        cset ety
        ( Position (V2 x ?params.playArea.h)
        , Velocity 0
        )

mkDiamond :: V2 Float -> Diamond
mkDiamond loc = groundedDiamond loc 20 40

-- The level

theGround :: The Params es P.Main => Eff es (Surface ())
theGround = paramly $ pure $
  Surface
  { tag = ()
  , ty = Ground
  , line = Seg (V2 0 ?params.playArea.h) (V2 ?params.playArea.w ?params.playArea.h)
  }

slope :: The Params es P.Main => Eff es (Surface ())
slope = paramly $ pure $
  Surface
  { tag = ()
  , ty = Ground
  , line = Seg (V2 150 (?params.playArea.h - 150)) (V2 600 (?params.playArea.h - 400))
  }

platforms :: The Params es P.Main => Eff es [Surface ()]
platforms = paramly $ slope >>= \sl -> pure $
  [ Surface
    { tag = ()
    , ty = Ground
    , line = Seg (V2 100 (?params.playArea.h - 100)) (V2 300 (?params.playArea.h - 100))
    }
  , Surface
    { tag = ()
    , ty = Ground
    , line = Seg (V2 200 (?params.playArea.h - 50)) (V2 500 (?params.playArea.h - 50))
    }
  , Surface
    { tag = ()
    , ty = Ceiling
    , line = Seg (V2 200 (?params.playArea.h - 50)) (V2 500 (?params.playArea.h - 50))
    }
  , Surface
    { tag = ()
    , ty = Ceiling
    , line = Seg (V2 500 (?params.playArea.h - 50)) (V2 700 (?params.playArea.h-200))
    }
    -- Slopes & connected surfaces
  , sl
  , Surface
    { tag = ()
    , ty = Ground
    , line = Seg sl.line.seg'to (sl.line.seg'to + V2 150 0)
    }
  , Surface
    { tag = ()
    , ty = Ground
    , line = Seg (sl.line.seg'to + V2 150 0) (sl.line.seg'to + V2 (150 + 100) 100)
    }
  ] ++
  (flip fmap (take 20 [-50, -100..]) \ht ->
      let (V2 _ fullht) = sl.line.seg'to + V2 0 ht
      in if fullht < 0 then
        Surface
        { tag = ()
        , ty = Ground
        , line = Seg (V2 0 fullht) (V2 ?params.playArea.w fullht)
        }
      else
        Surface
        { tag = ()
        , ty = Ground
        , line = Seg (sl.line.seg'to + V2 50 ht) (sl.line.seg'to + V2 100 ht)
        }
  )
  ++
  [ Surface
    { tag = ()
    , ty = Ground
    , line = Seg sl.line.seg'from (sl.line.seg'from - V2 100 0)
    }
  ]

walls :: The Params es P.Main => Eff es [Surface ()]
walls = paramly $ slope >>= \sl -> pure $
  [ Surface
    { tag = ()
    , ty = RightWall
    , line = Seg (V2 700 (?params.playArea.h)) (V2 700 (?params.playArea.h - 10))
    }
  , Surface
    { tag = ()
    , ty = LeftWall
    , line = Seg (V2 0 0) (V2 0 ?params.playArea.h)
    }
  , Surface
    { tag = ()
    , ty = RightWall
    , line = Seg (V2 ?params.playArea.w 0) (V2 ?params.playArea.w ?params.playArea.h)
    }
    -- On the up slope:
  ] ++
  -- We can't have them perfect overlap due to a bug (see diamond.md 2023-04-28 notes)
  (flip fmap [(0, RightWall), (0.1, LeftWall)] $ \(x, ty) ->
  Surface
  { tag = ()
  , line = Seg (sl.line.seg'to + V2 (x + 10) -10) (sl.line.seg'to + V2 (x + 10) 0)
  , ..
  })


mkSurfaces :: The Params es P.Main => Eff es [Surface()]
mkSurfaces = mconcat <$> sequence [(:[]) <$> theGround, platforms, walls]
