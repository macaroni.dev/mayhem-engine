module LD52.Assets where

import GHC.Generics

import Foreign.Ptr
import SDL.GPU.Simple qualified as GPU

import Mayhem.Engine.Assets
--import Mayhem.Engine.TH
import Mayhem.Engine.Utils

import LD52.World

data Main = Main
  { fonts :: Fonts
  }
  deriving stock Generic
  deriving anyclass Asset

data Fonts = Fonts
  { debug :: Font 24 ('Color 255 255 255 255) -- ComicMono for now
  }
  deriving stock Generic
  deriving anyclass Asset
