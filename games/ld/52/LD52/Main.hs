module Main where

import Mayhem.Console.Jam'22
import Mayhem.Engine.Loop (Framed, HotPipe, newHotPipe, hotReload)
import Mayhem.Engine.Utils (isEnvSet)

import Control.Concurrent (forkOS, ThreadId, killThread)
import Foreign.Store qualified as FS
import Data.Typeable

import LD52.M
import LD52.Game as Game
import LD52.Assets as A
import LD52.Params as P
import LD52.World

import SDL.GPU.CuteC2 qualified as GPU

main :: IO ()
main = main' Nothing Nothing

dev :: IO ()
dev = do
  ghciPipe <- newGHCiPipe
  FS.writeStore ghciStore ghciPipe

  hotPipe <- newHotPipe
  FS.writeStore hotStore hotPipe

  tid <- forkOS $ main' (Just ghciPipe) (Just hotPipe)
  FS.writeStore threadStore tid

ghciStore :: FS.Store (GHCiPipe LD52'GHCi)
ghciStore = FS.Store 0

threadStore :: FS.Store ThreadId
threadStore = FS.Store 1

hotStore :: FS.Store (HotPipe (Framed LD52))
hotStore = FS.Store 52

ghci :: Typeable a => Eff LD52'GHCi a -> IO a
ghci e = do
  ghciPipe <- FS.readStore ghciStore
  sendGHCi ghciPipe e

reload :: IO ()
reload = do
  hotPipe <- FS.readStore hotStore
  hotReload hotPipe gameloop
  _ <- ghci (reloadParams P.hardcoded)
  pure ()

kill :: IO ()
kill = do
  tid <- FS.readStore threadStore
  killThread tid

gameRez :: NativeResolution
gameRez = edtv

main' :: Maybe (GHCiPipe LD52'GHCi) -> Maybe (HotPipe (Framed LD52)) -> IO ()
main' ghciPipe hotPipe = runManaged $ do
  devmode <- liftIO $ isEnvSet "DEVMODE"

  Run run <- runJam'22 @A.Main @P.Main @World
             devmode
             (theAssetss (.fonts.debug.rawFont))
             ghciPipe
             gameRez
             initWorld
             (pure P.hardcoded)
  run $ Game.game hotPipe
