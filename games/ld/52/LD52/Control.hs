module LD52.Control where

import Data.Functor
import Data.Monoid.Generic
import GHC.Generics
import GHC.Records
import Control.Applicative
import Mayhem.Engine.Input
import Mayhem.Engine.Loop
import SDL qualified

data Button = Button
  { state :: ButtonState
  , event :: Maybe ButtonEvent
  } deriving (Show)

instance Semigroup Button where
  x <> y = Button
    { state = case (x.state, y.state) of
        (Button'Held, _) -> Button'Held
        (_, Button'Held) -> Button'Held
        _ -> Button'Unheld
    , event = x.event <|> y.event
    }

instance Monoid Button where
  mempty = Button Button'Unheld Nothing

data ButtonState = Button'Held | Button'Unheld deriving (Eq, Show)
data ButtonEvent = Button'Press | Button'Release deriving (Eq, Show)

instance HasField "isHeld" Button Bool where
  getField Button{..} = case state of
    Button'Held -> True
    Button'Unheld -> False

instance HasField "wasPressed" Button Bool where
  getField Button{..} = case event of
    Just Button'Press -> True
    _ -> False

instance HasField "wasReleased" Button Bool where
  getField Button{..} = case event of
    Just Button'Release -> True
    _ -> False

data Controller = Controller
  { left :: Button
  , right :: Button
  , up :: Button
  , down :: Button
  , jump :: Button
  , action :: Button
  , dash :: Button
  }
  deriving stock (Show, Generic)
  deriving Semigroup via GenericSemigroup Controller
  deriving Monoid via GenericMonoid Controller

scancodeButton :: [IOE, Input [SDL.Event]] :>> es => SDL.Scancode -> Eff es Button
scancodeButton code = do
  events <- input @[SDL.Event]
  isScanPressed <- SDL.getKeyboardState
  let event = scancodeEdge code events <&> \case
          SDL.Pressed -> Button'Press
          SDL.Released -> Button'Release
  let state = if isScanPressed code then Button'Held else Button'Unheld
  pure Button{..}

keyboardController :: [IOE, Input [SDL.Event]] :>> es => Eff es Controller
keyboardController = do
  left <- scancodeButton SDL.ScancodeA
  right <- scancodeButton SDL.ScancodeD
  up <- scancodeButton SDL.ScancodeW
  down <- scancodeButton SDL.ScancodeS
  jump <- scancodeButton SDL.ScancodeJ
  action <- scancodeButton SDL.ScancodeK
  dash <- scancodeButton SDL.ScancodeL
  pure Controller{..}
