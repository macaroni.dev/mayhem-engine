
{-# OPTIONS_GHC -Wno-unticked-promoted-constructors #-}
module LD52.Timer where

import Control.Monad
import Apecs.Core (Elem)
import Data.Kind
import Mayhem.Engine.Frame
import Mayhem.Engine.Effects

data Timer = Timer FrameDown
  deriving stock (Eq, Show)

instance Component Timer where type Storage Timer = Map Timer

timerMap
  :: forall cx cy w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es Timer
  => Get w es cx
  => Set w es cy
  => (cx -> cy)
  -> Eff es ()
timerMap f = cmapM_ $ \(Timer fdown, c :: cx, ety :: Entity) -> case cntDown fdown of
    Nothing -> do
      destroyC @Timer ety
      cset ety (f c)
    Just fdown' -> cset ety (Timer fdown')

timerMapM
  :: forall cx cy w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es Timer
  => Get w es cx
  => Set w es cy
  => (cx -> Eff es cy)
  -> Eff es ()
timerMapM f = cmapM_ $ \(Timer fdown, c :: cx, ety :: Entity) -> case cntDown fdown of
    Nothing -> do
      destroyC @Timer ety
      fc <- f c
      cset ety fc
    Just fdown' -> cset ety (Timer fdown')

timerMapM_
  :: forall cx w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es Timer
  => Get w es cx
  => (cx -> Eff es ())
  -> Eff es ()
timerMapM_ f = cmapM_ $ \(Timer fdown, c :: cx, ety :: Entity) -> case cntDown fdown of
    Nothing -> do
      destroyC @Timer ety
      f c
    Just fdown' -> cset ety (Timer fdown')

timerMapME
  :: forall cx cy w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es Timer
  => Get w es cx
  => Set w es cy
  => (Entity -> cx -> Eff es cy)
  -> Eff es ()
timerMapME f = cmapM_ $ \(Timer fdown, c :: cx, ety :: Entity) -> case cntDown fdown of
    Nothing -> do
      destroyC @Timer ety
      fc <- f ety c
      cset ety fc
    Just fdown' -> cset ety (Timer fdown')

timerMapME_
  :: forall cx w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es Timer
  => Get w es cx
  => (Entity -> cx -> Eff es ())
  -> Eff es ()
timerMapME_ f = cmapM_ $ \(Timer fdown, c :: cx, ety :: Entity) -> case cntDown fdown of
    Nothing -> do
      destroyC @Timer ety
      f ety c
    Just fdown' -> cset ety (Timer fdown')

--------

data IsTimed = Timed | Untimed

data Ticker' (c :: IsTimed -> Type) where
  TickDown :: c Timed -> FrameDown -> Ticker' c
  TickForever :: c Untimed -> Frame -> Ticker' c

data Ticker c = Ticker { paused :: Bool, ticker :: Ticker' c }

tickForever :: c Untimed -> Ticker c
tickForever c = Ticker { paused = False, ticker = TickForever c 0 }

tickDown :: c Timed -> FrameDown -> Ticker c
tickDown c fd = Ticker { paused = False, ticker = TickDown c fd }

tickPause :: Ticker c -> Ticker c
tickPause t = t { paused = True }

tickUnpause :: Ticker c -> Ticker c
tickUnpause t = t { paused = False }

instance Component (Ticker c) where type Storage (Ticker c) = Map (Ticker c)
-- TODO: CPS for Ticker handling

tickerCase :: (forall t. c t -> r) -> Ticker c -> r
tickerCase k t = case t.ticker of
  TickDown c _ -> k c
  TickForever c _ -> k c

tickerCase' :: forall r c. Ticker c -> (forall (t :: IsTimed). c t -> r) -> r
tickerCase' = flip tickerCase

tickerFor_ :: forall c m. Ticker c -> (forall (t :: IsTimed). c t -> m ()) -> m ()
tickerFor_ = tickerCase'

tickerMap
  :: forall c w es
   . IOE :> es
  => The ApecsE es w
  => Modify w es Timer
  => Get w es (Ticker c)
  => Set w es (Ticker c)
  => (c Timed -> Ticker c)
  -> Eff es ()
tickerMap f = cmapM_ $ \(t :: Ticker c, ety :: Entity) ->
  unless t.paused $ case t.ticker of
    TickDown c fdown -> case cntDown fdown of
      Nothing -> do
        destroyC @(Ticker c) ety
        cset ety (f c)
      Just fdown' -> cset ety (t { ticker = TickDown c fdown' })
    TickForever c frm -> cset ety (t { ticker = TickForever c (succ frm) })

tickerMapM
  :: forall c w es
   . IOE :> es
  => The ApecsE es w
  => Get w es (Ticker c)
  => Set w es (Ticker c)
  => (c Timed -> Eff es (Ticker c))
  -> Eff es ()
tickerMapM f = cmapM_ $ \(t :: Ticker c, ety :: Entity) ->
  unless t.paused $ case t.ticker of
    TickDown c fdown -> case cntDown fdown of
      Nothing -> do
        destroyC @(Ticker c) ety
        f c >>= cset ety
      Just fdown' -> cset ety (t { ticker = TickDown c fdown' })
    TickForever c frm -> cset ety (t { ticker = TickForever c (succ frm) })

tickerMapME
  :: forall c w es
   . IOE :> es
  => The ApecsE es w
  => Get w es (Ticker c)
  => Set w es (Ticker c)
  => (Entity -> c Timed -> Eff es (Ticker c))
  -> Eff es ()
tickerMapME f = cmapM_ $ \(t :: Ticker c, ety :: Entity) ->
  unless t.paused $ case t.ticker of
    TickDown c fdown -> case cntDown fdown of
      Nothing -> do
        destroyC @(Ticker c) ety
        f ety c >>= cset ety
      Just fdown' -> cset ety (t { ticker = TickDown c fdown' })
    TickForever c frm -> cset ety (t { ticker = TickForever c (succ frm) })

tickerMapME_
  :: forall c w es
   . IOE :> es
  => The ApecsE es w
  => Get w es (Ticker c)
  => (Entity -> c Timed -> Eff es ())
  -> Eff es ()
tickerMapME_ f = cmapM_ $ \(t :: Ticker c, ety :: Entity) ->
  unless t.paused $ case t.ticker of
    TickDown c fdown -> case cntDown fdown of
      Nothing -> do
        destroyC @(Ticker c) ety
        f ety c
      Just fdown' -> cset ety (t { ticker = TickDown c fdown' })
    TickForever c frm -> cset ety (t { ticker = TickForever c (succ frm) })
