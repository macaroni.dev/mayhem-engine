module LD52.Params where

import Mayhem.Engine.Frame
import CuteC2
import Linear

data Main = Main
  { player :: Player
  , playArea :: PlayArea
  , sprouts :: Sprouts
  }

data Player = Player
  { dashFrames :: FrameDown
  , jumpSquatFrames :: FrameDown
  , landFrames :: FrameDown
  , grabFrames :: FrameDown
  , grabbingFrames :: FrameDown
  , hurtbox :: C2Shape
  , feetOffset :: Float
  , grabbox :: C2Shape
  , jumpSpeed :: Float
  , shorthopSpeed :: Float
  , gravity :: Float
  , walkSpeed :: Float
  , dashSpeed :: Float
  , fallSpeed :: Float
  , throwVelocity :: V2 Float
  , maxStash :: Int
  }

data PlayArea = PlayArea
  { w :: Float
  , h :: Float
  }

data Sprouts = Sprouts
  { spawnbox :: C2Shape
  , chargeFrames :: FrameDown
  , screamFrames :: FrameDown
  , screambox :: C2Shape
  , gravity :: Float
  }

hardcoded :: Main
hardcoded = Main
  { player = Player
    { dashFrames = 11
    , jumpSquatFrames = 5
    , landFrames = 4
    , grabFrames = 1
    , grabbingFrames = 7
    , hurtbox = c2Circle (C2V 0 0) 20
    , feetOffset = 15
    , grabbox = c2AABB (C2V -40 -40) (C2V 0 0)
    , jumpSpeed = 4.1 * 1.25
    , shorthopSpeed = 2.1 * 1.25
    , gravity = 0.23 / 1.25
    , walkSpeed = 2
    , dashSpeed = 5
    , fallSpeed = 3
    , throwVelocity = V2 5 -5
    , maxStash = 2
    }
  , playArea = PlayArea
    { w = 1000
    , h = 1000
    }
  , sprouts = Sprouts
    { spawnbox = c2AABB (C2V -20 -20) (C2V 0 0)
    , chargeFrames = 90
    , screamFrames = 120
    , screambox = c2Circle (C2V 0 0) 40
    , gravity = 0.08
    }
  }
