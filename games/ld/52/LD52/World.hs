{-# LANGUAGE TemplateHaskell #-}
module LD52.World where

import Linear
import Apecs
import Data.Monoid.Generic

import Mayhem.Engine.TH

import LD52.Timer

data Screen = Screen'Title | Screen'Playing | Screen'Gameover | Screen'InputDemo deriving stock (Show)

instance Semigroup Screen where
  x <> _ = x
instance Monoid Screen where
  mempty = Screen'Playing
instance Component Screen where type Storage Screen = Global Screen

newtype Position = Position { unPosition :: (V2 Float) } deriving stock (Eq, Show)
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity { unVelocity :: (V2 Float) }
  deriving stock (Eq, Show)

instance Component Velocity where type Storage Velocity = Map Velocity

data Player = Player
  deriving stock (Eq, Show)
instance Component Player where type Storage Player = Unique Player

data Facing = Facing'Left | Facing'Right
  deriving stock (Eq, Show)

facingV2 :: Facing -> V2 Float
facingV2 = \case
  Facing'Left -> V2 -1 0
  Facing'Right -> V2 1 0

facingV2' :: Facing -> V2 Float
facingV2' = \case
  Facing'Left -> V2 -1 1
  Facing'Right -> V2 1 1

instance Component Facing where type Storage Facing = Map Facing

data OnGround = OnGround
  deriving stock (Eq, Show)

instance Component OnGround where type Storage OnGround = Map OnGround

data PlayerState (t :: IsTimed) where
  Player'Idle :: PlayerState Untimed
  Player'Walk :: PlayerState Untimed
  Player'Dash :: PlayerState Timed
  Player'JumpSquat :: PlayerState Timed
  Player'Land :: PlayerState Timed
  Player'Jump :: PlayerState Untimed
  Player'Grab :: PlayerState Timed
  Player'Grabbing :: PlayerState Timed
  -- TODO: Throwing/dropping states

type PlayerStateT = Ticker PlayerState

isGrounded :: PlayerStateT -> Bool
isGrounded tps = tickerCase' tps $ \case
  Player'Jump -> False
  _ -> True

data Platform = HorizontalPlatform Float -- length
  deriving stock (Eq, Show)

instance Component Platform where type Storage Platform = Map Platform

data SproutSpawn = SproutSpawn { hasSprout :: Bool }
  deriving stock (Eq, Show)

instance Component SproutSpawn where type Storage SproutSpawn = Map SproutSpawn

data SproutBombState (t :: IsTimed) where
  Sprout'Charge :: SproutBombState Timed
  Sprout'Scream :: SproutBombState Timed
  Sprout'Pulled :: SproutBombState Timed
--  Sprout'Paused :: SproutBombState Timed -> SproutBombState Untimed

type SproutBombStateT = Ticker SproutBombState

data Sprout'Held = Sprout'Held
instance Component Sprout'Held where type Storage Sprout'Held = Unique Sprout'Held

data Sprout'Stashed = Sprout'Stashed
instance Component Sprout'Stashed where type Storage Sprout'Stashed = Map Sprout'Stashed

data Sprout = Sprout { spawnedFrom :: Entity }
  deriving stock (Eq, Show)
instance Component Sprout where type Storage Sprout = Map Sprout

data Camera = Camera
  { pos :: V2 Float
  } deriving stock (Eq, Show)

-- Unlawful
instance Semigroup Camera where
  x <> _ = x

instance Monoid Camera where
  mempty = Camera 0
instance Component Camera where type Storage Camera = Global Camera

-- TH_CODE

makeWorld "World"
  [ ''Position
  , ''Velocity
  , ''Player
  , ''PlayerStateT
  , ''Facing
  , ''Platform
  , ''Screen
  , ''SproutSpawn
  , ''SproutBombStateT
  , ''Sprout'Held
  , ''Sprout'Stashed
  , ''Sprout
  , ''Camera
  ]

makePrisms ''PlayerState
