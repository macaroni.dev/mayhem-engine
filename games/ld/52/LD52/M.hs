module LD52.M where

import Mayhem.Console.Jam'22

import LD52.Assets as A
import LD52.Params as P
import LD52.World

type LD52 = Jam'22 A.Main P.Main World
type LD52'GHCi = Jam'22'GHCi A.Main P.Main World
