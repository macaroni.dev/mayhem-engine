#! /usr/bin/env bash

set -ex

game_name="AlienCattleRustlers"
t=$(date --rfc-3339=seconds | sed 's/ /-/g' | sed 's/:/-/g')
g=$(git rev-parse --short HEAD)
outdir="jam-windows-$t-$g"
releasework="releases/jam-windows-$t-$g"
releasedir="$releasework/$game_name"

nix-build -A projectCross.mingwW64.hsPkgs.mayhem-engine.components.exes.ld52 -o "$outdir" ../../../default.nix

mkdir -p "$releasedir"/assets

cp "$outdir"/bin/*.exe "$releasedir"
cp "$outdir"/bin/*.dll "$releasedir"

rm "$outdir"

# We use -L here since we symlink gamecontroller.db
cp -rL assets/* "$releasedir/assets/"

# ghc-pkg doesn't help Nix discover the dependencies on these dlls, so we manually copy them
cp -Pn "$(nix-build --no-out-link --expr '(import ../../../nix/pkgs.nix).pkgsCross.mingwW64.SDL2_ttf')/bin"/*.dll "$releasedir"
cp -Pn "$(nix-build --no-out-link --expr '(import ../../../nix/pkgs.nix).pkgsCross.mingwW64.zlib')/bin"/*.dll "$releasedir"

chmod +w -R "$releasedir"

pushd "$releasework"
zip -r "$game_name.zip" "$game_name"
popd

echo "Built $releasedir"

echo "PUTTY COMMAND:"
echo "pscp $PUTTY_PROFILE:$(pwd)/$releasework/$game_name.zip ./$outdir-$game_name.zip"
