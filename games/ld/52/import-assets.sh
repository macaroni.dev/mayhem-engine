#! /usr/bin/env bash

prefixes=$(fd .png --max-depth=1 --search-path "$LD52_ASSET_WORK_DIR"/animations --exec echo {/.} | sed 's/[0-9]//g' | sort | uniq)

echo -e "$prefixes" | xargs -rn 1 -I{} cabal run compile-animations -- --prefix {} --src "$LD52_ASSET_WORK_DIR"/animations --dst ./assets/animations

cp "$LD52_ASSET_WORK_DIR"/images ./assets/images
