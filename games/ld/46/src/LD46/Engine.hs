{-# LANGUAGE StrictData #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE FlexibleContexts #-}

module LD46.Engine
  ( engine
  , MonadEngine (..)
  -- ** Sound
  , Sound (..)
  , Source
  , withAudio
  , loadSource
  , loadLoopingSource
  -- ** Re-exports
  , InputState
  , MonadState(..)
  , modify
  ) where

import Control.Lens ((%=), zoom)
import Control.Monad (unless)
import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import qualified Graphics.Gloss.Interface.Environment as Gloss
import Control.Monad.State.Strict (StateT, execStateT, modify, MonadState(..))
import Control.Monad.IO.Class (MonadIO)
import GHC.Generics (Generic)
import Data.Generics.Labels ()
import qualified SDL.Font as SDL
import Sound.ALUT (Source)
import qualified Sound.ALUT as ALUT

import LD46.Render
import LD46.Input

data Engine world = Engine
  { world :: world
  , inputState :: InputState
  } deriving (Eq, Show, Generic)

-- | Desired Features:
-- * RenderM context for resolution-unaware drawing + top-left origin
--    * Also include `assets`?
-- * State monad interface
--    * Maybe mtl it? Then we can expose more capabilities to these handlers
-- * Keyboard/Mouse state tracking
-- * Global Font (via gloss-sdl2-surface & sdl-ttf)
--
-- Nice-to-have:
-- * Font caching from SDL_FontCache
-- * Dynamic font (using custom in-text markup)
-- * Sound handling (with OpenAL + ALUT)
-- * Animation handling
--
-- Stretch abstractions:
-- * Extensible Picture ADT (a la Dalek) for plugin-like architecture
-- * Composable ticking machines
-- * Asset (images, sound, animation) loading via HKD
engine
  :: String -- ^ Title
  -> (Int, Int) -- ^ Window size
  -> (Int, Int) -- ^ Window initial position
  -> Gloss.Color
  -> Maybe SDL.Font
  -> Int
  -> world
  -> (world -> RenderM Gloss.Picture)
  -> (forall m. MonadEngine world m => Gloss.Event -> m ())
  -> (forall m. MonadEngine world m => Float -> InputState -> m ())
  -> IO ()
engine
  title
  windowSize
  windowPos
  bgColor
  font
  fps
  initWorld
  draw
  handleEvent
  tick = do
    Gloss.playIO
      display
      bgColor
      fps
      (Engine initWorld initialInputState)
      ioDraw
      ioHandle
      ioTick

  where
    display = Gloss.InWindow title windowSize windowPos

    ioDraw Engine{world} = do
      let (width, height) = windowSize
      let ctx = Context {..}
      let pic = runRenderM ctx $ draw world
      maybe (pure pic) (\f -> fontify f pic) font

    ioHandle e eng = flip execStateT eng $ do
      #inputState %= stepInputState e
      zoom #world $ handleEvent e

    ioTick t Engine{..} =
        flip execStateT Engine{..}
      $ zoom #world
      $ tick t inputState

-- There's probably a way to create an sound equivalent of Gloss.Picture
-- Including things like speeding it up, etc
--
-- It's trickier because sounds are inherently longer than 1 frame & more mutable
--
-- LinearTypes may help enforcing single-thread Source usage?
data Sound =
    SoftPlay Source
  | HardPlay Source
  | Stop Source
  | Pause Source

loadSource :: FilePath -> IO Source
loadSource path = do
    buf <- ALUT.createBuffer (ALUT.File path)
    source <- ALUT.genObjectName
    ALUT.buffer source ALUT.$= Just buf
    return source

loadLoopingSource :: FilePath -> IO Source
loadLoopingSource path = do
  source <- loadSource path
  ALUT.loopingMode source ALUT.$= ALUT.Looping
  pure source

withAudio :: IO a -> IO a
withAudio ioa = ALUT.withProgNameAndArgs ALUT.runALUT $ \_ _ -> ioa

class MonadState world m => MonadEngine world m where
  execSound :: Sound -> m ()

instance MonadIO m => MonadEngine world (StateT world m) where
  execSound = \case
    SoftPlay src -> do
      srcState <- ALUT.get (ALUT.sourceState src)
      unless (srcState == ALUT.Playing) $ ALUT.play [src]
    HardPlay src -> ALUT.play [src]
    Stop src     -> ALUT.stop [src]
    Pause src -> ALUT.pause [src]
