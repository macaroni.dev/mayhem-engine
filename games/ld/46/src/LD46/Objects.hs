{-# LANGUAGE StrictData #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DeriveGeneric #-}

module LD46.Objects where

import Control.Lens ((&), (^.), (.~), (%~))
import CuteC2
import qualified Graphics.Gloss as Gloss
import Data.Generics.Labels ()
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Monoid (Endo(..))
import Control.Monad (guard)
import GHC.Generics

import LD46.Types
import LD46.Assets
import LD46.Render

import Debug.Trace

data Interaction = Hand | Shovel | Scissors | Net deriving (Eq, Ord, Show)

data Hitbox world = Hitbox
  { hitShape :: C2AABB
  , collisionShape :: Maybe C2AABB
  , triggers :: Map Interaction (F world world)
  } deriving (Show, Generic)

aabbAt :: (Float, Float) -> C2AABB -> C2AABB
aabbAt (x, y) C2AABB{min=C2V{x=minX, y=minY}, max=C2V{x=maxX, y=maxY}} =
  let minX' = minX+x
      maxX' = maxX+x
      minY' = minY+y
      maxY' = maxY+y
  in
    C2AABB
    { min = C2V{x=min minX' maxX', y=min minY' maxY'}
    , max = C2V{x=max minX' maxX', y = max minY' maxY'}
    }

data Object world = Object
  { hitbox   :: Hitbox world
  , location :: (Float, Float)
  , sprite   :: F Assets Gloss.Picture
  } deriving (Show, Generic)

data ObjectPicture = ObjectPicture
  { location :: (Float, Float)
  , rank     :: Float
  , picture  :: Gloss.Picture
  } deriving Show

emptyObject :: Object world
emptyObject = Object
  { hitbox = Hitbox
    { hitShape =C2AABB
      { min = C2V{x=0, y=0}
      , max = C2V{x=0, y = 0}
      }
    , collisionShape = Nothing
    , triggers = mempty
    }
  , location = (0, 0)
  , sprite = F (const mempty)
  }

mkObject :: (Assets -> Gloss.Picture) -> (Float, Float) -> C2AABB -> Object world
mkObject sprite loc aabb =
  emptyObject
  & #sprite .~ F sprite
  & #hitbox . #hitShape .~ aabb
  & #location .~ loc

addTrigger :: Interaction -> (world -> world) -> Object world -> Object world
addTrigger inter trigger obj = obj & #hitbox . #triggers %~ Map.insert inter (F trigger)

aabbDimensions :: C2AABB -> (Float, Float)
aabbDimensions C2AABB{min=C2V{x=minX, y=minY}, max=C2V{x=maxX, y=maxY}} =
  (abs (maxX - minX), abs (maxY - minY))

blocksMovementAt :: C2AABB -> Object world -> Object world
blocksMovementAt aabb obj = obj & #hitbox . #collisionShape .~ Just aabb

blocksMovement :: Object world -> Object world
blocksMovement obj = blocksMovementAt (obj ^. #hitbox . #hitShape) obj

-- TODO: Use Render.Scene here instead & use hitboxes to determine yPos
-- TODO: Maybe instead of using collision/hitbox for rank, have an explicit
--       "horizon" value that determines it
renderObject :: Assets -> Object world -> ObjectPicture
renderObject assets Object{hitbox=Hitbox{..},..} =
  ObjectPicture
  { location = location
  , rank = let C2AABB{..} = aabbAt location (maybe hitShape id collisionShape) in min.y
  , picture =  unF sprite assets
  }

renderHitAABB :: C2AABB -> Gloss.Picture
renderHitAABB aabb =
  let (w, h) = aabbDimensions aabb
   in Gloss.translate (w/2) (-h/2) $
      Gloss.Color (Gloss.withAlpha 0.5 Gloss.red) $
      Gloss.rectangleSolid w h

renderCollisionAABB :: C2AABB -> Gloss.Picture
renderCollisionAABB aabb =
  let (w, h) = aabbDimensions aabb
   in Gloss.translate (w/2) (-h/2) $
      Gloss.Color Gloss.yellow $
      Gloss.rectangleWire w h

mkAABB :: Float -> Float -> C2AABB
mkAABB x y = C2AABB (C2V 0 0) (C2V x y)


-- No broad phase. We'll just loop through all N objects in a room always
movementBlocked :: (Float, Float) -> C2AABB -> [Object world] -> Bool
movementBlocked loc shape =
  any $ \Object{hitbox=Hitbox{..}, location=hitLoc} ->
          case collisionShape of
            Nothing -> False
            Just cs -> c2Collided (C2AABB' $ aabbAt loc shape) (C2AABB' $ aabbAt hitLoc cs)

objectInteract :: Interaction -> (Float, Float) -> C2AABB -> [Object world] -> world -> world
objectInteract interaction loc shape =
  trace "----objectInteract----" . appEndo . foldMap (
    \obj@Object{hitbox=Hitbox{..}, location=hitLoc} -> Endo $ \world -> maybe world id $ do
      -- We look up if there's a trigger first to short-circuit if we don't need to
      -- check for collision
      F trigger <- Map.lookup interaction triggers

      let aabb1 = C2AABB' $ aabbAt loc shape
      let aabb2 = C2AABB' $ aabbAt hitLoc hitShape
      let collided = c2Collided aabb1 aabb2
      traceM $ unwords ["Loc =", show loc, "Shape =", show shape, "Object =", show obj]
      traceM $ unwords ["collided =", show collided, "aabb1 =", show aabb1, "aabb2 =", show aabb2]
      guard collided
      pure $ trigger world
    )
