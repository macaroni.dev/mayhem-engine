{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}

module LD46.Assets where

import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Juicy as Gloss
import GHC.Generics (Generic)

-- Assets are consolidated here so we can easily
-- scaleAssets :: Float -> Assets -> Assets for screen sizes
-- or different rooms..maybe scaleAssets just goes in World?
data Assets = Assets
  { apprenticeLeft :: Gloss.Picture
  , apprenticeRight :: Gloss.Picture
  , apprenticeUp :: Gloss.Picture
  , apprenticeDown :: Gloss.Picture
  , libraryFloor :: Gloss.Picture
  , libraryWall :: Gloss.Picture
  , graveyardFloor :: Gloss.Picture
  , graveyardWall :: Gloss.Picture
  , gardenFloor :: Gloss.Picture
  , gardenWall :: Gloss.Picture
  , offmap :: Gloss.Picture
  , door :: Gloss.Picture
  , transmuter :: Gloss.Picture
  , deadMind :: Gloss.Picture
  , deadBody :: Gloss.Picture
  , deadSoul :: Gloss.Picture
  , deadSplat :: Gloss.Picture
  , tl1Balanced :: Gloss.Picture
  , tl1Body :: Gloss.Picture
  , tl1Mind :: Gloss.Picture
  , tl1Soul :: Gloss.Picture
  , tl2Balanced :: Gloss.Picture
  , tl2Body :: Gloss.Picture
  , tl2Mind :: Gloss.Picture
  , tl2Soul :: Gloss.Picture
  , tl2MindBody :: Gloss.Picture
  , tl2MindSoul :: Gloss.Picture
  , tl2SoulBody :: Gloss.Picture
  , td3Body :: Gloss.Picture
  , td3Mind :: Gloss.Picture
  , td3Soul :: Gloss.Picture
  , bookshelf :: Gloss.Picture
  , table :: Gloss.Picture
  , otherWizard :: Gloss.Picture
  , bush :: Gloss.Picture
  , grave :: Gloss.Picture
  , pond :: Gloss.Picture
  , pondShadow :: Gloss.Picture
  , filingCabinetOpen :: Gloss.Picture
  , filingCabinetClosed :: Gloss.Picture
  , seals :: Gloss.Picture
  , bigDoor :: Gloss.Picture
  }

loadAssets :: IO Assets
loadAssets = do
  apprenticeLeft <- moveSprite 32 32 <$> loadPng "assets/ApprenticeLeft.png"
  apprenticeRight <- moveSprite 32 32 <$> loadPng "assets/ApprenticeRight.png"
  apprenticeUp <- moveSprite 32 32 <$> loadPng "assets/ApprenticeBack.png"
  apprenticeDown <- moveSprite 32 32 <$> loadPng "assets/ApprenticeFront.png"
  libraryFloor <- moveSprite 32 32 <$> loadPng "assets/LibraryFloorTile.png"
  libraryWall <- moveSprite 32 32 <$> loadPng "assets/LibraryWallTile.png"
  bookshelf <- moveSprite 64 64 <$> loadPng "assets/Bookshelf.png"
  table <- moveSprite 92 32 <$> loadPng "assets/Table.png"
  otherWizard <- moveSprite 64 64 <$> loadPng "assets/OtherWizard.png"
  seals <- moveSprite 32 32 <$> loadPng "assets/Seals.png"
  bigDoor <- moveSprite 64 64 <$> loadPngReal "assets/door.png"

  gardenFloor <- moveSprite 32 32 <$> loadPng "assets/GardenFloorTile.png"
  gardenWall <- moveSprite 32 32 <$> loadPng "assets/GardenWallTile.png"
  let graveyardFloor = gardenFloor
      graveyardWall = gardenWall
  bush <- moveSprite 64 64 <$> loadPng "assets/Bush.png"
  grave <- moveSprite 64 64 <$> loadPng "assets/Grave.png"
  pond <- moveSprite 192 64 <$> loadPng "assets/Pond.png"
  pondShadow <- moveSprite 32 32 <$> loadPng "assets/PondShadow.png"

  filingCabinetOpen <- moveSprite 32 32 <$> loadPng "assets/FilingCabinetOpen.png"
  filingCabinetClosed <- moveSprite 32 32 <$> loadPng "assets/FilingCabinetClosed.png"
  door <- moveTile <$> loadPng "assets/Doorway.png"

  -- transmutation
  transmuter   <- moveSprite 96 96 <$> loadPng "assets/Transmuter.png"
  deadMind     <- moveSprite 96 128 <$> loadPng "assets/DeadMind.png"
  deadBody     <- Gloss.scale 0.8 0.8 . moveSprite 128 156 <$> loadPng "assets/DeadBody.png"
  deadSoul     <- moveSprite 110 96 <$> loadPng "assets/DeadSoul.png"
  deadSplat    <- moveSprite 110 120 <$> loadPng "assets/DeadSplat.png"
  tl1Balanced  <- moveSprite 96 128 <$> loadPng "assets/Lvl1Balanced.png"
  tl1Body      <- moveSprite 125 110 <$> loadPng "assets/Lvl1Body.png"
  tl1Mind      <- moveSprite 96 128 <$> loadPng "assets/Lvl1Mind.png"
  tl1Soul      <- moveSprite 110 85 <$> loadPng "assets/Lvl1Soul.png"
  tl2Balanced  <- moveSprite 110 100 <$> loadPng "assets/Lvl2Balanced.png"
  tl2Body      <- moveSprite 96 100 <$> loadPng "assets/Lvl2Body.png"
  tl2Mind      <- moveSprite 96 110 <$> loadPng "assets/Lvl2Mind.png"
  tl2Soul      <- moveSprite 96 100 <$> loadPng "assets/Lvl2Soul.png"
  tl2MindBody  <- moveSprite 115 100 <$> loadPng "assets/Lvl2MindBody.png"
  tl2MindSoul  <- moveSprite 120 100 <$> loadPng "assets/Lvl2MindSoul.png"
  tl2SoulBody  <- moveSprite 96 110 <$> loadPng "assets/Lvl2SoulBody.png"
  td3Body      <- Gloss.scale 1.2 1.2 . moveSprite 115 40 <$> loadPng "assets/Lvl3Body.png"
  td3Mind      <- Gloss.scale 1.2 1.2 . moveSprite 100 40 <$> loadPng "assets/Lvl3Mind.png"
  td3Soul      <- Gloss.scale 1.2 1.2 . moveSprite 100 40 <$> loadPng "assets/Lvl3Soul.png"

  let offmap = mempty
  pure Assets{..}

moveTile :: Gloss.Picture -> Gloss.Picture
moveTile = Gloss.translate (32) (-32)

moveSprite :: Float -> Float -> Gloss.Picture -> Gloss.Picture
moveSprite x y = Gloss.translate (x) (-(y))

loadPng :: String -> IO Gloss.Picture
loadPng s = maybe (error $ "Unable to find file " ++ s) (Gloss.scale 2 2) <$> Gloss.loadJuicy s

loadPngReal :: String -> IO Gloss.Picture
loadPngReal s = maybe (error $ "Unable to find file " ++ s) id <$> Gloss.loadJuicy s

loadPng2 :: String -> IO Gloss.Picture
loadPng2 s = maybe (error $ "Unable to find file " ++ s) (Gloss.scale 1.5 1.5) <$> Gloss.loadJuicy s
