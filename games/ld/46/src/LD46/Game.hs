{-# LANGUAGE StrictData #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE DuplicateRecordFields #-}

module LD46.Game where

import CuteC2
import qualified Data.List
import qualified Data.Char
import Data.Functor (($>))
import Data.List.NonEmpty(NonEmpty(..))
import Control.Lens ((^.), (.~), (&), (%~), _1, _2, use)
import qualified Control.Lens as Lens
import Control.Monad (unless)
import Data.Generics.Labels ()
import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Juicy as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import Data.Vector (Vector)
import qualified Data.Vector as Vector
import Debug.Trace (trace)
import qualified System.IO
import qualified Data.Set as Set
import Debug.Trace
import qualified Data.Map.Strict as Map

import LD46.Dialog
import LD46.Model
import LD46.Types
import LD46.Objects
import LD46.Transmutation
import LD46.Assets
import LD46.Render
import LD46.Engine
import qualified LD46.Defs as Defs

renderHitAABBat :: (Float, Float) -> C2AABB -> RenderM Gloss.Picture
renderHitAABBat loc aabb =
  let aabb'@C2AABB{..} = aabbAt loc aabb
  in renderAt (min.x) (min.y) (renderHitAABB aabb')

renderCollisionAABBat :: (Float, Float) -> C2AABB -> RenderM Gloss.Picture
renderCollisionAABBat loc aabb =
  let aabb'@C2AABB{..} = aabbAt loc aabb
  in renderAt (min.x) (min.y) (renderCollisionAABB aabb')


renderWorld :: Assets -> World -> RenderM Gloss.Picture
renderWorld assets@Assets{..} w@World{..} =
  let Room{..} = currRoom w
      (apprenticeX, apprenticeY) = apprentice ^. #location
      apprenticeDir = apprentice ^. #facing
   in renderScene <$>
    (
      -- Render grid
      (
        ifoldForGrid grid $ \x y tile -> mkScene tilePriority <$> renderAt (x*64) (y*64) (tileAsset assets tile)
      ) <>
      -- Render objects
      (
        foldFor objects $ \obj ->
          let ObjectPicture{location=(x,y),..} = renderObject assets obj
           in fmap (objSceneAt rank) (renderAt x y picture)
           <> mwhen gfxDebug (
            fmap (mkScene gfxDebugPriority) (
                renderHitAABBat (obj ^. #location) (obj ^. #hitbox . #hitShape) <>
                maybe mempty (renderCollisionAABBat (obj ^. #location)) (obj ^. #hitbox . #collisionShape)
                )
            )
      ) <>
      -- Render transmutation
      -- TODO: Move this to transmuter rendering (object rendering should be world -> Assets -> Picture)
      -- TODO: This will remove this magic number hack
      (
        mwhen (room == Main) $ (fmap (objSceneAt 350) $ renderAt 200 200 $ renderTransmutation assets transmutation)
      ) <>
      -- Render apprentice
      (
        (fmap (apprenticeScene apprentice) $ renderAt apprenticeX apprenticeY $ apprenticeAsset assets apprentice) <> -- NOTE: y - 64??
        (mwhen gfxDebug $ pure $ mkScene gfxDebugPriority $ Gloss.Color Gloss.red $ Gloss.circle 32)
      ) <>
      (
        mwhen gfxDebug $ fmap (mkScene gfxDebugPriority) $ mconcat
        [ renderHitAABBat (interactionLoc apprentice) (interactionAABB apprentice)
        , renderCollisionAABBat (apprenticeX, apprenticeY) apprenticeAABB
        ]
      ) <>
      -- Render dialog
      (
        fmap (mkScene dialogPriority) $ maybe mempty id $ do
          DialogPicture{location=(x,y), picture} <- dialog >>= renderDialog
          pure $ renderAt x y picture
      )
    )

fixTranslate :: Float -> Float -> Gloss.Picture -> Gloss.Picture
fixTranslate x y =
  Gloss.translate
  (x + ((-screenWidthF) / 2))
  (((screenHeightF) / 2) - y)

debugCoord :: Show a => a -> a -> Gloss.Picture
debugCoord x y = Gloss.scale 0.1 0.1 $ Gloss.color Gloss.white $ Gloss.text $ show (x,y)

main :: IO ()
main = withAudio $ withFont "assets/alagard.ttf" 200 $ \alagard -> do
  System.IO.hSetBuffering System.IO.stdout System.IO.NoBuffering
  assets <- loadAssets
  bgmSrc <- loadLoopingSource "assets/dark-theme.wav"
  walkingSrc <- loadSource "assets/walking.wav"
  -- TODO: Use a functional approach to room building
  let initialWorld = World
        { room = Graveyard
        , doors = Defs.doors
        , roomMap = F $ \case
            Main -> Defs.mainRoom
            Library -> Defs.libraryRoom
            Graveyard -> Defs.graveyardRoom
            Garden -> Defs.gardenRoom
        , apprentice = Apprentice
          { location = (500, 500)
          , direction = (0, 0)
          , facing = DirDown
          , inventory = emptyInventory
            { ingredients = allIngredients
            , fetalRemains = True
            }
          , interaction = Nothing
          }
        , transmutation = SomeTransmutation Nothing
        , gfxDebug = False
        , dialog = Nothing
        }

      debugEvent e w = do
        putStr $ "\r" ++ show e
        pure w
  -- GO GO GO!
  engine
    "A Mage's Thesis"
    (screenWidth, screenHeight)
    (0, 0)
    Gloss.orange
    (Just alagard)
    60
    initialWorld
    (renderWorld assets)
    handleWorld
    (\dt inputState -> do -- TODO: Sound should be managed in the loop rather than here, but this works for now :)
        execSound (SoftPlay bgmSrc)
        tickWorld dt inputState
        currentMovement <- use (#apprentice . #direction)
        if (currentMovement /= (0, 0))
          then execSound (SoftPlay walkingSrc)
          else execSound (Stop walkingSrc)
    )

screenHeight :: Int
screenWidth :: Int
screenHeight = 768
screenWidth = 1024

screenCtx :: LD46.Render.Context
screenCtx = Context { width = screenWidth, height = screenHeight }

screenHeightF :: Float
screenWidthF :: Float
screenHeightF = fromIntegral screenHeight
screenWidthF = fromIntegral screenWidth
foldFor :: Foldable f => Monoid b => f a -> (a -> b) -> b
foldFor = flip foldMap

-- TODO: Abstract over tilesize
mungeCenter :: Gloss.Picture -> Gloss.Picture
mungeCenter = Gloss.translate (fromIntegral (64 - screenWidth) / 2) (fromIntegral (screenHeight - 64) / 2)

---

-- TODO: Move sanitize event to Engine?
handleWorld :: MonadState World m => Gloss.Event -> m ()
handleWorld (sanitizeEvent -> e) = modify $ \case
  world@World{dialog=Nothing} -> handleMenu e $ handleDebug e $ handleApprenticeInteraction e $ handleApprenticeMovement e world
  world@World{dialog=Just dialog} -> handleApprenticeMovement e $ case handleDialog e dialog of
    d' -> world & #dialog .~ (Just d')

handleMenu :: Gloss.Event -> World -> World
handleMenu e w@World{..} =
  let Inventory{..} = apprentice ^. #inventory
  in case e of
  Gloss.EventKey (Gloss.SpecialKey Gloss.KeyEnter) Gloss.Down _ _ ->
    World { dialog = Just $ DialogListView
            { items = mconcat
                      [ [DialogListItem "Net [ J ] Shovel [ K ] Scissors [ L ]" Gloss.white]
                      , [DialogListItem "--------------------------------" Gloss.white]
                      , [DialogListItem "Inventory:" Gloss.white]
                      , [DialogListItem "" Gloss.white]
                      , mwhen fetalRemains [DialogListItem "FETAL REMAINS" Gloss.red]
                      , mwhen strangeTechnology [DialogListItem "STRANGE TECHNOLOGY" Gloss.green]
                      , mwhen soulStone [DialogListItem "SOUL STONE" Gloss.blue]
                      , flip fmap (Set.toList ingredients) $ \i -> DialogListItem (humanIngredient i) Gloss.white
                      ]
            , done = False
            , pageSize = 19
            , pageStart = 0
            }, ..
          }
  _ -> w

handleDebug :: Gloss.Event -> World -> World
handleDebug e w@World{..} = case e of
  Gloss.EventKey (Gloss.Char 'm') Gloss.Down ms _ | secretModifiers ms ->
    traceShowId w
  Gloss.EventKey (Gloss.Char 'g') Gloss.Down ms _ | secretModifiers ms ->
    World{gfxDebug = not gfxDebug, ..}
  Gloss.EventKey (Gloss.SpecialKey Gloss.KeyF1) Gloss.Down _ _ ->
    World{room = Main, ..}
  Gloss.EventKey (Gloss.SpecialKey Gloss.KeyF2) Gloss.Down _ _ ->
    World{room = Library, ..}
  Gloss.EventKey (Gloss.SpecialKey Gloss.KeyF3) Gloss.Down _ _ ->
    World{room = Garden, ..}
  Gloss.EventKey (Gloss.SpecialKey Gloss.KeyF4) Gloss.Down _ _ ->
    World{room = Graveyard, ..}
  Gloss.EventKey (Gloss.Char 'o') Gloss.Down ms _ | secretModifiers ms ->
    World{ dialog = Just $ DialogSequence $
             dialogText Nothing Nothing "hello this is a mage's thesis" :|
           [ dialogText Nothing Nothing "Please enjoy the game, and remember..."
           , DialogEffect $ DialogDone $ F $ \world -> world & #apprentice . #location .~ (500, 500)
           , dialogText (Just Gloss.red) Nothing "KEEP    IT     ALIVE"
           ]
         , .. }
  _ -> w

handleApprenticeInteraction :: Gloss.Event -> World -> World
handleApprenticeInteraction e world = case e of
  Gloss.EventKey k ks _ _ -> case (k, ks) of
    (Gloss.SpecialKey Gloss.KeySpace, Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Hand
    (Gloss.Char 'j', Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Net
    (Gloss.Char 'k', Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Shovel
    (Gloss.Char 'l', Gloss.Down) ->
      world
      & #apprentice . #interaction .~ Just Scissors
    _ -> world
  _ -> world


handleApprenticeMovement :: Gloss.Event -> World -> World
handleApprenticeMovement e world = case e of
  Gloss.EventKey k ks _ _ -> case (k, ks) of
    (Gloss.Char 'a', Gloss.Down) ->
      world
      & #apprentice . #direction . _1 %~ pred
      & #apprentice . #facing .~ DirLeft
    (Gloss.Char 'a', Gloss.Up) ->
      world
      & #apprentice . #direction . _1 %~ succ
      & #apprentice %~ resetFacing

    (Gloss.Char 'd', Gloss.Down) ->
      world
      & #apprentice . #direction . _1 %~ succ
      & #apprentice . #facing .~ DirRight
    (Gloss.Char 'd', Gloss.Up) ->
      world
      & #apprentice . #direction . _1 %~ pred
      & #apprentice %~ resetFacing

    (Gloss.Char 'w', Gloss.Down) ->
      world
      & #apprentice . #direction . _2 %~ pred
      & #apprentice . #facing .~ DirUp
    (Gloss.Char 'w', Gloss.Up) ->
      world
      & #apprentice . #direction . _2 %~ succ
      & #apprentice %~ resetFacing

    (Gloss.Char 's', Gloss.Down) ->
      world
      & #apprentice . #direction . _2 %~ succ
      & #apprentice . #facing .~ DirDown
    (Gloss.Char 's', Gloss.Up) ->
      world
      & #apprentice . #direction . _2 %~ pred
      & #apprentice %~ resetFacing

    _ -> world
  _ -> world

tickWorld :: MonadState World m => Float -> InputState -> m ()
tickWorld _ _ = modify $ \case
  world@World{dialog=Nothing} -> tickApprentice world
  world@World{dialog=Just dialog} -> case tickDialog dialog of
    (d', f) -> f world & #dialog .~ d'

tickApprentice :: World -> World
tickApprentice world =
  let (dirx, diry) = world ^. #apprentice . #direction
      (dx, dy) = (fromIntegral dirx * apprenticeVelocity, fromIntegral diry * apprenticeVelocity)
   in
       world
       & #apprentice . #location %~ moveAndClamp (dx, dy)
       & tickInteraction
       & #apprentice . #interaction .~ Nothing
  where
    tickInteraction :: World -> World
    tickInteraction w@World{..} = case apprentice of
      Apprentice{interaction=Nothing} -> w
      Apprentice{interaction=Just interaction, ..} ->
        nothingHappened interaction $
        objectInteract interaction (interactionLoc apprentice) (interactionAABB apprentice) (currRoom world ^. #objects) w
    moveAndClamp :: (Float, Float) -> (Float, Float) -> (Float, Float)
    moveAndClamp (dx, dy) (currX, currY) =
      let nextLoc@(nextX, nextY) = (currX + dx, currY + dy)
          yBoundary = if dy < 0 then nextY + (0.75 * apprenticeHeight) else nextY+apprenticeHeight
          xBoundary = if dx < 0 then nextX else nextX + apprenticeWidth
          nextTileLoc = (floor (xBoundary / 64), floor (yBoundary / 64))
          grid = currRoom world ^. #grid
       in case gridGet nextTileLoc grid of -- TODO: If nextTileLoc = door tile, do door instead of this
        Just (Floor _) ->
          if movementBlocked nextLoc apprenticeAABB (currRoom world ^. #objects)
           then (currX, currY)
           else nextLoc
        _ -> (currX, currY)

putWords :: [String] -> IO ()
putWords = putStr . unwords

putWordsLn :: [String] -> IO ()
putWordsLn = putStrLn . unwords

apprenticeVelocity :: Float
apprenticeVelocity = 5.0

-- Idr where these size numbers come from
apprenticeWidth :: Float
apprenticeWidth = 48

apprenticeHeight :: Float
apprenticeHeight = 96

-- The engine currently projects AABB from origin (top-left)
-- which has the unfortunate side-effect of making it so the
-- apprentice's head must have a hitbox -_-..we can fix this tho
--
-- also this size is very hard-code :O
apprenticeAABB :: C2AABB
apprenticeAABB = C2AABB
  { min = C2V (0.1*apprenticeWidth) (apprenticeHeight * 0.75)
  , max = C2V (0.9*apprenticeWidth) apprenticeHeight
  }

interactionAABB :: Apprentice -> C2AABB
interactionAABB Apprentice{facing} = case facing of
  DirLeft ->
    C2AABB
    { min = C2V 0 0
    , max = C2V (-apprenticeWidth/2) apprenticeHeight
    }
  DirRight ->
    C2AABB
    { min = C2V apprenticeWidth 0
    , max = C2V (apprenticeWidth + apprenticeWidth/2) apprenticeHeight
    }
  DirUp ->
    C2AABB
    { min = C2V 0 (apprenticeHeight/4)
    , max = C2V apprenticeWidth (apprenticeHeight/2)
    }
  DirDown ->
    C2AABB
    { min = C2V 0 apprenticeHeight
    , max = C2V apprenticeWidth (apprenticeHeight + apprenticeHeight/4)
    }

interactionSize :: Float
interactionSize = 32

interactionLoc :: Apprentice -> (Float, Float)
interactionLoc Apprentice{location=(x, y)} = (x, y)

mwhen :: Monoid a => Bool -> a -> a
mwhen True a = a
mwhen False _ = mempty

munless :: Monoid a => Bool -> a -> a
munless False a = a
munless True _ = mempty

apprenticeAsset :: Assets -> Apprentice -> Gloss.Picture
apprenticeAsset Assets{..} Apprentice{facing} = case facing of
  DirDown -> apprenticeDown
  DirUp -> apprenticeUp
  DirLeft -> apprenticeLeft
  DirRight -> apprenticeRight

tileAsset :: Assets -> Tile -> Gloss.Picture
tileAsset Assets{..} = \case
  Floor Library -> libraryFloor
  Wall Library -> libraryWall
  Floor Main -> libraryFloor
  Wall Main -> libraryWall
  Floor Graveyard -> graveyardFloor
  Wall Graveyard -> graveyardWall
  Floor Garden -> gardenFloor
  Wall Garden -> gardenWall
  OffMap -> offmap

nothingHappened :: Interaction -> World -> World
nothingHappened i = \case
  w@World{dialog=Just{}} -> w
  w@World{dialog=Nothing} -> w & #dialog .~ Just (dialogText Nothing Nothing $ unwords ["("++show i++")", "Nothing happened..."])

renderTransmutation :: Assets -> SomeTransmutation -> Gloss.Picture
renderTransmutation Assets{..} = \case
  SomeTransmutation Nothing -> mempty
  SomeTransmutation (Just TDeath{death}) -> case death of
    TDMythic Body -> td3Body
    TDMythic Mind -> td3Mind
    TDMythic Soul -> td3Soul
    TDDominant1 Body -> deadBody
    TDDominant1 Mind -> deadMind
    TDDominant1 Soul -> deadSoul
    _ -> deadSplat
  SomeTransmutation (Just TLife{life}) -> case life of
    TLBalanced1 -> tl1Balanced
    TLDominant1 Mind -> tl1Mind
    TLDominant1 Body -> tl1Body
    TLDominant1 Soul -> tl1Soul
    TLBalanced2 -> tl2Balanced
    TLDominant2 Mind -> tl2Mind
    TLDominant2 Body -> tl2Body
    TLDominant2 Soul -> tl2Soul
    TLDual2 Mind Mind -> tl2Mind
    TLDual2 Mind Body -> tl2MindBody
    TLDual2 Mind Soul -> tl2MindSoul

    TLDual2 Body Body -> tl2Body
    TLDual2 Body Mind -> tl2MindBody
    TLDual2 Body Soul -> tl2SoulBody

    TLDual2 Soul Soul -> tl2Mind
    TLDual2 Soul Body -> tl2SoulBody
    TLDual2 Soul Mind -> tl2MindSoul

data Layer = TileLayer | ObjectLayer | DebugLayer | DialogLayer deriving (Eq, Ord)

tilePriority :: Priority Layer
tilePriority = Priority TileLayer 0

objSceneAt :: Float -> Gloss.Picture -> Scene Layer
objSceneAt y = mkScene Priority { layer = ObjectLayer, rank = y }

gfxDebugPriority :: Priority Layer
gfxDebugPriority = Priority DebugLayer 0

dialogPriority :: Priority Layer
dialogPriority = Priority DialogLayer 0

apprenticeScene :: Apprentice -> Gloss.Picture -> Scene Layer
apprenticeScene apprentice pic =
  let C2AABB{..} = aabbAt (apprentice ^. #location) apprenticeAABB
   in objSceneAt (min.y) pic

fmag :: Float -> Float
fmag n = if n < 0 then -1 else 1

sanitizeEvent :: Gloss.Event -> Gloss.Event
sanitizeEvent = \case
  Gloss.EventKey (Gloss.Char c) x y z -> Gloss.EventKey (Gloss.Char (sanitizeChar c)) x y z
  e -> e

sanitizeChar :: Char -> Char
sanitizeChar c = if Data.Char.isControl c then Data.Char.chr $ Data.Char.ord c + 96 else Data.Char.toLower c

secretModifiers :: Gloss.Modifiers -> Bool
secretModifiers Gloss.Modifiers{..} = and $ fmap isDown [alt,shift]
  where
    isDown = \case
      Gloss.Down -> True
      _ -> False


