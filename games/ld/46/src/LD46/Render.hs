{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE StrictData #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE RecordWildCards #-}

module LD46.Render where

import qualified Graphics.Gloss as Gloss
import qualified Graphics.Gloss.SDL.Surface as Gloss
import qualified SDL.Font as SDL
import qualified SDL.Video.Renderer as SDL

import Control.Exception (bracket)
import Linear.V4
import qualified Data.Map.Strict as Map
import qualified Data.Text as Text
import Control.Monad.Identity(Identity(..))
import Control.Monad.Reader (ReaderT(..), runReaderT, ask)
import Data.Monoid (Ap(..))

data Priority layer = Priority
  { layer :: layer
  , rank :: Float
  } deriving (Eq, Ord, Show)

--sceneAt :: Float -> Gloss.Picture -> Scene
--sceneAt y pic = Scene $ Map.singleton (Priority Nothing y) pic

mkScene :: Ord layer => Priority layer -> Gloss.Picture -> Scene layer
mkScene prio pic = Scene $ Map.singleton prio pic

renderScene :: Ord layer => Scene layer -> Gloss.Picture
renderScene = mconcat . fmap snd . Map.toAscList . pictures

-- TODO : Maybe fixTranslate could be moved here.
-- We probably want to describe Scenes in our logical coordinates,
-- and then let this module handle transforming them to absolute Screen
-- coordinates


data Scene layer = Scene { pictures :: Map.Map (Priority layer) Gloss.Picture }
  deriving (Eq, Show)

instance Ord layer => Semigroup (Scene layer) where
  s1 <> s2 = Scene $ Map.unionWith (mappend @Gloss.Picture) (pictures s1) (pictures s2)

instance Ord layer => Monoid (Scene layer) where
  mempty = Scene Map.empty

fontify :: SDL.Font -> Gloss.Picture -> IO Gloss.Picture
fontify font pic = go (V4 255 255 255 255) pic
  where
    go color = \case
      Gloss.Text txt -> bracket (SDL.blended font color (Text.pack txt)) SDL.freeSurface $ \s -> do
        (txtW, txtH) <- SDL.size font (Text.pack txt)
        (_, p') <- Gloss.bitmapOfSurface Gloss.NoCache s
        pure $ Gloss.translate ((fromIntegral txtW)/2) (-(fromIntegral txtH)/2) $ p'
      Gloss.Color c p -> Gloss.Color c <$> go (glossColorToSDL c) p
      Gloss.Translate x y p -> Gloss.Translate x y <$> go color p
      Gloss.Rotate d p -> Gloss.Rotate d <$> go color p
      Gloss.Scale x y p -> Gloss.Scale x y <$> go color p
      Gloss.Pictures ps -> Gloss.Pictures <$> traverse (go color) ps
      p -> pure p

glossColorToSDL :: Gloss.Color -> SDL.Color
glossColorToSDL gcolor =
  let (rf, gf, bf, af) = Gloss.rgbaOfColor gcolor
   in (round . (255*)) <$> V4 rf gf bf af

withFont :: FilePath -> SDL.PointSize -> (SDL.Font -> IO r) -> IO r
withFont path size = bracket (SDL.initialize >> SDL.load path size) (\f -> SDL.free f >> SDL.quit)


data Context = Context
  { width  :: Int
  , height :: Int
  -- TODO: Offsets for relative positioning? Maybe that's not needed
  -- TODO: `local` equivalent
  -- TODO: nativeWidth & nativeHeight? For scaling? Eh
  -- TODO: assets type variable? or general reader type
  } deriving (Show)

newtype RenderT m a = RenderT { unRenderT :: ReaderT Context m a }
  deriving newtype (Functor, Applicative, Monad)
  deriving (Semigroup, Monoid) via (Ap (RenderT m) a)

type RenderM = RenderT Identity

runRenderT :: Context -> RenderT m a -> m a
runRenderT ctx = flip runReaderT ctx . unRenderT

runRenderM :: Context -> RenderM a -> a
runRenderM ctx = runIdentity . runRenderT ctx

renderAt :: Float -> Float -> Gloss.Picture -> RenderM Gloss.Picture
renderAt x y p = RenderT $ do
  Context{..} <- ask
  let (widthF, heightF) = (fromIntegral width, fromIntegral height)
  pure $ Gloss.translate (x + ((-widthF) / 2)) ((heightF / 2) - y) p

