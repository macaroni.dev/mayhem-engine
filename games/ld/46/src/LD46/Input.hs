{-# LANGUAGE StrictData #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}

module LD46.Input where

import Control.Lens ((&), (^.), (.~), (%~))
import Graphics.Gloss as Gloss
import qualified Graphics.Gloss.Interface.IO.Game as Gloss
import Linear (V2(..))
import Data.Generics.Labels ()
import qualified Data.Map.Strict as Map
import GHC.Generics (Generic)

data InputState = InputState
  { mouseLoc   :: V2 Float
  , keys       :: Map.Map Gloss.Key Gloss.KeyState
  } deriving (Eq, Show, Generic)

initialInputState :: InputState
initialInputState =
  InputState
  { mouseLoc = V2 0 0
  , keys = mempty
  }

-- TODO: Modifiers
-- TODO: EventKey location
stepInputState :: Gloss.Event -> InputState -> InputState
stepInputState e s = case e of
  Gloss.EventResize _ -> s
  Gloss.EventMotion (x, y) -> s & #mouseLoc .~ V2 x y
  Gloss.EventKey key keyState _ _ -> s & #keys %~ Map.insert key keyState
