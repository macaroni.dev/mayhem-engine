{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}

module LD46.Transmutation where

import qualified Data.Maybe
import  Data.Set (Set)
import qualified Data.Set as Set
import qualified Data.List
import qualified Data.Map.Strict as Map
import qualified System.Console.Haskeline   as HL
import Text.Read (readMaybe)
import qualified GHC.TypeLits as GHC

-- Transmutation
data IngredientType = Mind | Body | Soul deriving (Eq, Ord, Show, Read)

data Ingredient =
    MindI MindIngredient
  | BodyI BodyIngredient
  | SoulI SoulIngredient
  deriving (Show, Eq, Ord)

humanIngredient :: Ingredient -> String
humanIngredient = \case
  MindI i -> show i
  BodyI i -> show i
  SoulI i -> show i

ingredientType :: Ingredient -> IngredientType
ingredientType = \case
  MindI{} -> Mind
  BodyI{} -> Body
  SoulI{} -> Soul

data MindIngredient =
  -- Graveyard
    Epitaph
  | SheetMusic
  | LostGlasses
  -- Library
  | Grimoire
  | Abacus
  | Astrolabe
  | Encyclopedia
  -- Garden
  | Bee
  | Ants
  | Almanac
  deriving (Show, Eq, Ord, Enum, Bounded)

data BodyIngredient =
  -- Graveyard
    Bones
  | DeadRat
  | Mushrooms
  | Brambles
  -- Library
  | WizardsBeard
  | WizardsRobe
  | Necronomicon
  | AnatomyBook
  -- Garden
  | Mud
  | Veggies
  | Snail
  deriving (Show, Eq, Ord, Enum, Bounded)

data SoulIngredient =
  -- Graveyard
    Ectoplasm
  | FamilyRing
  | Flowers
  -- Library
  | ReligiousText
  | Seals
  | Diary
  -- Garden
  | Frog
  | Fish
  | Butterfly
  | GardenGnome
  deriving (Show, Eq, Ord, Enum, Bounded)

allIngredients :: Set Ingredient
allIngredients = Set.fromList $ mconcat
  [ MindI <$> [minBound..maxBound]
  , BodyI <$> [minBound..maxBound]
  , SoulI <$> [minBound..maxBound]
  ]


data SomeTransmutation = forall tier. SomeTransmutation (Maybe (Transmutation tier))
deriving instance Show SomeTransmutation

data Transmutation (tier :: TransmutationTier) =
    TLife  { state :: (TransmutationState tier), life :: (TransmutationLife tier) }
  | TDeath { death :: (TransmutationDeath tier) }
  deriving (Show)

data TransmutationLife (tier :: TransmutationTier) where
  TLDual2     :: IngredientType -> IngredientType -> TransmutationLife 'T2
  TLDominant2 :: IngredientType -> TransmutationLife 'T2
  TLBalanced2 :: TransmutationLife 'T2
  TLDominant1 :: IngredientType -> TransmutationLife 'T1
  TLBalanced1 :: TransmutationLife 'T1

data TransmutationDeath (tier :: TransmutationTier) where
  TDMythic :: IngredientType -> TransmutationDeath 'T3
  TDDominant3 :: IngredientType -> TransmutationDeath 'T3
  TDMissing3 :: IngredientType -> TransmutationDeath 'T3
  TDMedley3 :: TransmutationDeath 'T3
  TDBalanced3 :: TransmutationDeath 'T3
  TDDominant2 :: IngredientType -> TransmutationDeath 'T2
  TDMedley2 :: TransmutationDeath 'T2
  TDDominant1 :: IngredientType -> TransmutationDeath 'T1
  TDGeneric :: String -> TransmutationDeath t

data TransmutationTier = T0 | T1 | T2 | T3

type family NextTier (tier :: TransmutationTier) :: TransmutationTier where
  NextTier 'T0 = 'T1
  NextTier 'T1 = 'T2
  NextTier 'T2 = 'T3
  NextTier 'T3 = GHC.TypeError ('GHC.Text "Cannot transmute beyond T3 - time to die!")

data TransmutationState (tier :: TransmutationTier) where
  TState0 :: TransmutationState 'T0
  TState1 :: IngredientType -> IngredientType -> IngredientType -> TransmutationState 'T1
  TState2 :: IngredientType -> IngredientType -> IngredientType -> TransmutationState 'T1 -> TransmutationState 'T2

newtype IngredientCounts = IngredientCounts { unIngredientCounts :: (Map.Map IngredientType Int) } deriving Show

instance Semigroup IngredientCounts where
  (IngredientCounts c1) <> (IngredientCounts c2) = IngredientCounts $ Map.unionWith (+) c1 c2

instance Monoid IngredientCounts where
  mempty = IngredientCounts $ Map.fromList $ [Mind, Body,Soul] `zip` [0,0..]
  mappend = (<>)

countIngredients :: [IngredientType] -> IngredientCounts
countIngredients = foldMap $ \ty -> IngredientCounts $ Map.singleton ty 1

data IngredientCount = IC IngredientType Int deriving Show

rankCounts :: IngredientCounts -> (IngredientCount, IngredientCount, IngredientCount)
rankCounts = packRanks . Data.List.reverse . Data.List.sortOn snd . Map.toList . unIngredientCounts . (<> mempty)
  where
    packRanks [x, y, z] =
      ( uncurry IC x
      , uncurry IC y
      , uncurry IC z
      )
    packRanks xs = error $ "I messed up " ++ show xs

stateIngredients :: TransmutationState tier -> [IngredientType]
stateIngredients = \case
  TState0 -> []
  TState1 i1 i2 i3 -> [i1, i2, i3]
  TState2 i1 i2 i3 ts1 -> i1 : i2 : i3 : stateIngredients ts1

transmuteRepl :: IO ()
transmuteRepl = HL.runInputT HL.defaultSettings $ go TState0
  where
    go :: forall tier. TransmutationState tier -> HL.InputT IO ()
    go s = do
      line <- HL.getInputLine ">>> "
      let tys = Data.Maybe.mapMaybe (readMaybe @IngredientType) $ maybe [] words line
      case tys of
        [i1, i2, i3] -> do
          case transmute i1 i2 i3 s of
            TLife {..} -> do
              HL.outputStrLn $ unwords ["It lives!", show life]
              go state
            TDeath{..} -> do
              HL.outputStrLn $ unwords ["It died...", show death]
              go TState0
        _ -> HL.outputStrLn "Please provide 3 of Mind | Body | Soul" >> go s


transmute :: IngredientType -> IngredientType -> IngredientType -> TransmutationState tier -> Transmutation (NextTier tier)
transmute  i1 i2 i3 = \case
  TState0 ->
    let state = TState1 i1 i2 i3
     in case rankCounts $ countIngredients [i1, i2, i3] of
      (IC ty 3, _, _) -> TDeath { death = TDDominant1 ty }
      (IC ty 2, _, _) -> TLife  { life = TLDominant1 ty, .. }
      (IC _ 1, IC _ 1, IC _ 1) -> TLife { life = TLBalanced1, .. }
      _ -> TDeath { death = TDGeneric "Tier 1 logic error" }
  ts1@TState1{} ->
    let state = TState2 i1 i2 i3 ts1
     in case rankCounts $ countIngredients $ i1 : i2 : i3 : stateIngredients ts1 of
      (IC ty 5, _, _) -> TDeath { death = TDDominant2 ty }
      (IC ty 4, _, _) -> TLife  { life = TLDominant2 ty, ..}
      (IC ty1 3, IC ty2 3, _) ->
        TLife { life = TLDual2 ty1 ty2, .. }
      (IC _ 3, _, _) -> TDeath { death = TDMedley2 }
      (IC _ 2, IC _ 2, IC _ 2) -> TLife { life = TLBalanced2, .. }
      _ -> TDeath { death = TDGeneric "Tier 2 logic error" }
  ts2@TState2{} ->
    case rankCounts $ countIngredients $ i1 : i2 : i3 : stateIngredients ts2 of
      (IC ty 7, _, _) -> TDeath { death = TDDominant3 ty }
      (_, _, IC ty 0) -> TDeath { death = TDMissing3 ty }
      (IC ty 6, _, _) -> TDeath { death = TDMythic ty }
      (IC ty 5, _, _) -> TDeath { death = TDMythic ty }
      (IC _ 4, _, _)  -> TDeath { death = TDMedley3 }
      (IC _ 3, IC _ 3, IC _ 3) -> TDeath { death = TDBalanced3 }
      _ -> TDeath { death = TDGeneric "Tier 3 logic error" }

-- Show instances

instance Show (TransmutationLife tier) where
  show = \case
    TLDual2 ty1 ty2 -> unwords ["TLDual 2", show ty1, show ty2]
    TLDominant2 ty -> unwords ["TLDominant2", show ty]
    TLBalanced2 -> "TLBalanced2"
    TLDominant1 ty -> unwords ["TLDominant1", show ty]
    TLBalanced1 -> "TLBalanced1"

instance Show (TransmutationDeath tier) where
  show = \case
    TDMythic ty -> unwords ["TDMythic", show ty]
    TDDominant3 ty -> unwords ["TDDominant3", show ty]
    TDMissing3 ty -> unwords ["TDMissing3", show ty]
    TDMedley3 -> "TDMedley3"
    TDBalanced3 -> "TDBalanced3"
    TDDominant2 ty -> unwords ["TDDominant2", show ty]
    TDMedley2 -> "TDMedley2"
    TDDominant1 ty -> unwords ["TDDominant1", show ty]
    TDGeneric txt -> unwords ["TDGeneric", show txt]

parenShow :: Show a => a -> String
parenShow a = "(" ++ show a ++ ")"

instance Show (TransmutationState tier) where
  show = \case
    TState0 -> "TState0"
    TState1 i1 i2 i3 -> unwords ["TState1", show i1, show i2, show i3]
    TState2 i1 i2 i3 ts1 -> unwords ["TState2", show i1, show i2, show i3, parenShow ts1]

