# Ludum Dare 53: Delivery

This is an unfinished game for LD53. It is a 2D platformer using `diamond`.

The game has a few things going on:

- The character is a diamond with their action state printed.
- `A` and `D` move left and right. `S` crouches. The controls may feel strange, but they are inspired by Melee so once you get the hang you can get shmoving.
- `J` will jump. Release it quick and you will do a short jump.
- Press `K` to do a rapid weak attack. If you hold it, it will keep going. If you are airborn, it makes you float.
- Hold `L` to charge a "heavy" attack.
- Hit `S` airborn to fall quickly.
- Attacks will display their red hitboxes.
- Yellow circles are "enemies." They will knock you back if you touch them.
- White lines are platforms. To the left, there are some slopes (`diamond` follows slopes smoothly).
- Randomly, a few blue circles and platforms will appear.

## ghci

```
λ dev
-- The game will launch in a separate thread.

-- Query game state
λ ghci $ cList @(LD53.World.Player, Position, Velocity)
[(Player,Position {unPosition = V2 (-93.759766) 0.0},Velocity {unVelocity = V2 0.0 0.0})]

-- Make the player jump
λ import LD53.Params.Utils
λ ghci $ paramly $ cmap $ \LD53.World.Player -> mkPlayerAuto Player'JumpSquat
```

`:hot` performs hot reloading. It will both reload any "param" changes in `LD53.Params` (which are in a `Reader`-like context) but it will also reload any code change (including changes to `mayhem-engine`-the-library due to the cabal config).
