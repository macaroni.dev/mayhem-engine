{-# LANGUAGE BlockArguments #-}
module LD53.Game where

import Prelude hiding (min, max)

import Control.Monad
import Control.Monad.Extra (whenM)
import Data.Foldable
import Safe (minimumMay)
import Data.Maybe (mapMaybe)
import Data.Function
import System.Random (initStdGen)
import Optics

import Mayhem.Engine.Loop
import Mayhem.Engine.Effects
import Mayhem.Engine.Utils
import Mayhem.Engine.Automaton
import Mayhem.Engine.Diamond
import Mayhem.Engine.Geometry
import Mayhem.Engine.Effects.Debug qualified as Debug
import Mayhem.Console.Jam'22

import SDL.GPU.Utils qualified as GPU
import SDL.GPU.Utils qualified as GPU.Camera (CameraF(..))
import SDL.GPU.FC.Simple qualified as FC
import SDL.GPU.CuteC2 qualified as GPU
import Cute.Sound.C qualified as CS
import CuteC2
import Linear

import LD53.M
import LD53.Draw
import LD53.Player
import LD53.World
import LD53.Diamond
import LD53.Camera
import LD53.Controls
import LD53.Enemies
import LD53.Jokes
import LD53.Collisions
import LD53.Params.Utils
import LD53.Assets qualified as A
import LD53.Params qualified as P

import Debug.RecoverRTTI

game :: Maybe (HotPipe (Framed LD53)) -> Eff LD53 ()
game hotPipe = do
  initGame
  framedHot hotPipe gameloop

gameloop :: Eff (Framed LD53) ()
gameloop = do
  tickGHCi
  askGPU >>= liftIO . GPU.clear
  f7ToggleDebug
  f11Fullscreen
  f2Stepper
  adaptToWindowResize

  input >>= parseController >>= \c -> do
    stepped $ clocked $ do
      tickHitlag
      tickPlayer c
      tickEnemies
      tickSpotlight
      tickJokes
      checkForDelivery
      flickerSpotlight
      levelEnemyGen
      handleInput c
      handleFastFall
      handleDiamond
      handleCollisions
      moveSpotlight
      updateCamera c
    whenM (Debug.enabled) $ debugDraw c

  liftIO performMinorGC
  askCuteSound >>= liftIO . CS.mix
  askCuteSound8000 >>= liftIO . CS.mix
  askGPU >>= liftIO . GPU.flip

initGame :: Eff LD53 ()
initGame = gpuly $ paramly $ do
  gset ?params.level
  GPU.enableCamera ?screen False
  gmodify @Camera $ #zoom .~ 1.5
  _ <- newEntity
    ( Player
    , tickForever Player'Idle
    , Velocity 0
    , Facing'Left
    , Position 0
    , Player'DJ
    )
  _ <- newEntity $ mkSpotlightAuto (Spotlight'Off 60)

  initStdGen >>= gset . GRNG
  pure ()
