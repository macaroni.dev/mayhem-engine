module LD53.Params.Utils where

import Data.Ord (clamp)

import Mayhem.Engine.Automaton
import Mayhem.Engine.Loop (Framed)
import Mayhem.Engine.Effects

import Linear

import LD53.M
import LD53.World
import LD53.World.Enemies
import LD53.Params qualified as P

mkPlayerAuto :: (?params :: P.Main) => Player'State Timed -> Automaton Player'State
mkPlayerAuto t = TickDown t (?params.player.movementFrames t)

mkButterflyAuto :: (?params :: P.Main) => Butterfly'State Timed -> Automaton Butterfly'State
mkButterflyAuto t = TickDown t (?params.enemies.butterflyFrames t)

mkSpotlightAuto :: (?params :: P.Main) => Spotlight'State Timed -> Automaton Spotlight'State
mkSpotlightAuto t = TickDown t (?params.jokes.spotlightFrames t)

mkJokeAuto :: (?params :: P.Main) => Joke'State Timed -> Automaton Joke'State
mkJokeAuto t = TickDown t (?params.jokes.jokeFrames t)

walkVel :: (?params :: P.Main) => Facing -> Velocity
walkVel facing = Velocity $ facingDir facing ^* ?params.player.walkSpeed

runVel :: (?params :: P.Main) => Facing -> Velocity
runVel facing = Velocity $ facingDir facing ^* ?params.player.runSpeed

dashVel :: (?params :: P.Main) => Facing -> Velocity
dashVel facing = Velocity $ facingDir facing ^* ?params.player.dashSpeed

-- https://www.reddit.com/r/smashbros/comments/isttkv/im_looking_for_information_about_dash_speed/
jumpMomentum
  :: (?params :: P.Main)
  => Float -- 0.0 to 1.0 - analog jump
  -> Float -- Momentum to transfer
  -> Float
jumpMomentum analog momentum = min ((?params.player.jumpSpeedX * analog) + (momentum * ?params.player.jumpSpeedXMul)) ?params.player.jumpSpeedXMax
