module LD53.Params.Hardcoded where

import Control.Monad.IO.Class
import Data.List.NonEmpty (NonEmpty (..))
import Data.Map qualified as Map

import CuteC2
import Linear

import Mayhem.Engine.Diamond
import Mayhem.Engine.Frame
import Mayhem.Engine.Utils
import Mayhem.Engine.Automaton

import LD53.Params qualified as P
import LD53.World
import LD53.Level

hardcoded :: MonadIO m => m P.Main
hardcoded = do
  pure P.Main
    { camera = P.Camera
      { yOffset = 50
      , xOffset = 30
      , posLerpRate = 0.2
      , offsetLerpRate = 0.05
      }
    , player = P.Player
      { movementFrames = \case
          Player'Dash -> 11
          Player'CrouchSquat -> 8
          Player'StandUp -> 10
          Player'JumpSquat -> 3
          Player'Land -> 4
          Player'Skid -> 12
          Player'SlowTurn -> 15
          Player'Hit{..} -> FrameDown dur
          Player'StrongCharge -> 45
          Player'StrongAttack -> 10
          Player'WeakWindup{} -> 2
          Player'WeakAttack{} -> 2
          Player'WeakRecovery{} -> 30
          Player'WeakLand -> 10
      , hurtboxes = autoEsac $ \case
          _ -> [groundedCapsule 0 40 10]
      , strongHitboxes = \(facingScale->fd) ->
        [ c2Capsule (v22c2v $ fd * V2 30 -50) (v22c2v $ fd * V2 30 -10) 10
        ]
      , weakHitboxes = \_ ->
          [ c2Circle (C2V 0 -20) 20
          ]
      , attackHitlag = 8
      , jumpSpeed = 3.68
      , djSpeed = 4.416
      , shorthopSpeed = 2.1
      , gravity = 0.23
      , weakGravity = 0.08
      , maxFallSpeed = 2.8
      , fastFallSpeed = 3.4
      , airFriction = 0.02
      , airAcceleration = 0.08
      , maxAirSpeed = 1.35
      , traction = 0.08
      , jumpSpeedX = 2
      , jumpSpeedXMul = 0.72
      , jumpSpeedXMax = 1.7
      , walkSpeed = 1.5
      , dashSpeed = 3
      , runSpeed = 3
      , sdiDistance = 6
      , diamond = groundedDiamond 0 20 40
      }
    , level = theLevel
    , enemies = P.Enemies
      { butterflyFrames = \case
          Butterfly'Wait f -> FrameDown f
          Butterfly'Patrol (Butterfly'Path{..} :| _) -> FrameDown dur
      , hitboxes = \Enemy{..} -> case plan of
          Butterfly{} -> [c2Circle (C2V 0 0) 10]
      , knockback'angle = normalize (V2 -1 -1)
      , knockback'speed = 5
      , knockback'dur = 30
      , hitlag = 12
      }
    , jokes = P.Jokes
      { spotlightFrames = \case
          Spotlight'On f -> FrameDown f
          Spotlight'Flicker{} -> FrameDown 120
          Spotlight'Off f -> FrameDown f
      , jokeFrames = \case
          Joke'Patrol (Joke'Path{..} :| _) -> FrameDown dur
      , jokeHitbox = c2Circle (C2V 0 0) 10
      , spotlightFlicker = Map.fromList $ mconcat
        [ ([120 :: Frame,110..70] `zip` cycle [True, False, False, False])
        , [ (60, False)
          , (55, True)
          , (50, False)
          , (45, True)
          , (40, False)
          , (35, True)
          , (30, False)
          , (30, True)
          , (25, False)
          , (20, True)
          , (27, False)
          , (25, True)
          , (22, False)
          , (20, True)
          , (18, False)
          , (15, True)
          , (13, False)
          , (10, True)
          , (8, False)
          , (5, True)
          , (3, False)
          ]
        ]
      }
    }

groundedCapsule
  :: V2 Float
  -> Float -- h
  -> Float -- r
  -> C2Shape
groundedCapsule loc h r =
  c2Capsule (v22c2v (loc - V2 0 r)) (v22c2v (loc - V2 0 (h - r))) r
