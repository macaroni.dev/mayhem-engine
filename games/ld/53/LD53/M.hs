module LD53.M where

import Mayhem.Console.Jam'22
import Mayhem.Engine.Effects.GameController

import LD53.Assets as A
import LD53.Params as P
import LD53.World

-- TODO: This is a little ugly - any expansion I have doesn't get picked up by ghci..
type LD53 = GameController : Jam'22 A.Main P.Main World
type LD53'GHCi = Jam'22'GHCi A.Main P.Main World
