module LD53.Destroy where

import Mayhem.Engine.Effects
import Mayhem.Engine.Loop

import LD53.M
import LD53.World

destroyEnemy :: Entity -> Eff (Framed LD53) ()
destroyEnemy ety = do
  destroyC @Enemy ety
  destroyC @Position ety
  destroyC @Velocity ety
  destroyC @Butterfly'Automaton ety
  destroyC @Joke'Flickering ety

destroyJokePlatform :: Entity -> Eff (Framed LD53) ()
destroyJokePlatform ety = do
  destroyC @Joke'Platform ety
  destroyC @Position ety
  destroyC @Joke'Flickering ety

destroyJoke :: Entity -> Eff (Framed LD53) ()
destroyJoke ety = do
  destroyC @Joke'Automaton ety
  destroyC @Joke'Order ety
  destroyC @Position ety
  destroyC @Velocity ety
  destroyC @Joke'Flickering ety
