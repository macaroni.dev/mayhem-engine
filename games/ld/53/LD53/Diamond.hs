module LD53.Diamond where

import Control.Monad

import Linear

import Mayhem.Engine.Effects
import Mayhem.Engine.Automaton
import Mayhem.Engine.Diamond
import Mayhem.Engine.Loop
import Mayhem.Engine.LinearDotSyntax
import Mayhem.Engine.Utils

import LD53.M
import LD53.World
import LD53.Utils
import LD53.Params.Utils
import LD53.Params qualified as P

handleDiamond :: Eff (Framed LD53) ()
handleDiamond = paramly $ do
  level <- gget @Level
  jokeSurfaces <- fmap (.surface) . fmap fst <$> cList @(Joke'Platform, Not Joke'Flickering)
  let surfaces = mconcat [level.surfaces, jokeSurfaces]
  cmapM $ \(Player, NotHitlag, Position p, Velocity v, ps, ety :: Entity) -> do
    let d'from = ?params.player.diamond.offset p
    let d'to = d'from.offset v
    let res =
          if | isGrounded ps -> groundedPointSweep v d'from surfaces
             | otherwise ->
                 maybe (GroundedSweep'Unimpeded v) GroundedSweep'Impeded
               $ pointSweep d'from d'to surfaces
    case res of
      GroundedSweep'Unimpeded{..} -> pure $ Position (p + sweep)
      GroundedSweep'Impeded{..} -> do
        autoCase ps $ \case
          s | Just landAuto <- landPlayer s -> do
            when (collision.surface.ty == Ground && collision.point == BOTTOM) $
              cset ety $ (landAuto, Player'DJ, Nothing @(Player'Drifting, Player'FastFall))

            cset ety $ Velocity $ case collision.surface.ty of
              Ground -> V2 v.x 0
              Ceiling -> V2 v.x 0
              RightWall -> V2 0 v.y
              LeftWall -> V2 0 v.y
          _ -> pure ()
        pure $ Position $ p + collision.sweep

  -- Apply gravity
  cmap $ \case
    (tps, NotHitlag, Velocity v@(V2 vx vy)) -> autoCase tps $ \case
      ps | Just g <- affectedByGravity ps -> Velocity $ V2 vx (min ?params.player.maxFallSpeed (vy + g))
      _ -> Velocity v

  -- Apply traction
  autoCmap $ \(NotHitlag, v@Velocity{}) -> \case
    ps | canApplyTraction ps -> applyTraction v
    _ -> v

  -- Apply drift
  autoCmap $ \(Player'Drifting driftFacing, NotHitlag, Velocity v) -> \case
    ps | canApplyAirAcceleration ps ->
         let xspeed = abs v.x in
           if xspeed > ?params.player.maxAirSpeed then
             -- We can't drift in the same direction when we are past our max speed
             if | sameSign v.x (facingX driftFacing) -> Velocity v
                | otherwise ->
                  Velocity $ V2 (v.x + facingX driftFacing * ?params.player.airAcceleration) v.y
           else
             let vx' = min ?params.player.maxAirSpeed (xspeed + ?params.player.airAcceleration)
             in Velocity $ V2 (facingX driftFacing * vx') v.y
       | otherwise -> Velocity v

  -- Apply air friction
  autoCmap $ \(NotHitlag, v@Velocity{}) -> \case
    ps | canApplyAirFriction ps -> applyAirFriction v
    _ -> v

  -- Check for ground
  cmapM_ $ \(Player, NotHitlag, Position (?params.player.diamond.offset -> d), ps, ety) -> do
    when (canFall ps) $
      unless (any (diamondGrounded d) surfaces) $
      cset ety $ (Velocity 0, tickForever Player'Jump)

canApplyTraction :: forall t. Player'State t -> Bool
canApplyTraction = \case
  Player'Idle -> True
  Player'Dash -> False
  Player'Walk -> False
  Player'Run -> False
  Player'CrouchSquat -> True
  Player'Crouch -> True
  Player'StandUp -> True
  Player'Skid -> True
  Player'SlowTurn -> True
  Player'JumpSquat -> True
  Player'Jump -> False
  Player'Land -> True
  Player'Hit{} -> False
  Player'StrongCharge -> True
  Player'StrongCharged -> True
  Player'StrongAttack -> True
  Player'WeakWindup a -> a == Attack'Grounded
  Player'WeakAttack a -> a == Attack'Grounded
  Player'WeakRecovery a -> a == Attack'Grounded
  Player'WeakLand -> True

applyTraction :: (?params :: P.Main) => Velocity -> Velocity
applyTraction (Velocity v) =
  let vx' = if | nearZero v.x -> 0
               | v.x < 0 -> min 0 (v.x + ?params.player.traction)
               | otherwise -> max 0 (v.x - ?params.player.traction)
  in Velocity $ V2 vx' v.y

landPlayer :: (?params :: P.Main) => forall t. Player'State t -> Maybe Player'Automaton
landPlayer = \case
  Player'Jump -> Just $ mkPlayerAuto Player'Land
  Player'WeakWindup Attack'Airborne -> Just $ mkPlayerAuto $ Player'WeakLand
  Player'WeakAttack Attack'Airborne -> Just $ mkPlayerAuto $ Player'WeakLand
  Player'WeakRecovery Attack'Airborne -> Just $ mkPlayerAuto Player'WeakLand
  _ -> Nothing

canApplyAirFriction :: forall t. Player'State t -> Bool
canApplyAirFriction = \case
  Player'Jump -> True
  _ -> False

applyAirFriction :: (?params :: P.Main) => Velocity -> Velocity
applyAirFriction (Velocity v) =
  let vx' = if | nearZero v.x -> 0
               | v.x < 0 -> min 0 (v.x + ?params.player.airFriction)
               | otherwise -> max 0 (v.x - ?params.player.airFriction)
  in Velocity $ V2 vx' v.y

canApplyAirAcceleration :: forall t. Player'State t -> Bool
canApplyAirAcceleration = \case
  Player'Jump -> True
  Player'WeakWindup Attack'Airborne -> True
  Player'WeakAttack Attack'Airborne -> True
  Player'WeakRecovery Attack'Airborne -> True
  _ -> False

affectedByGravity :: forall t. (?params :: P.Main) => Player'State t -> Maybe Float
affectedByGravity = \case
  Player'Jump -> Just ?params.player.gravity
  Player'Hit{} -> Just ?params.player.gravity
  Player'WeakWindup Attack'Airborne -> Just ?params.player.weakGravity
  Player'WeakAttack Attack'Airborne -> Just ?params.player.weakGravity
  Player'WeakRecovery Attack'Airborne -> Just ?params.player.gravity
  _ -> Nothing
