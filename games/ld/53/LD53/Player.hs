module LD53.Player where

import Data.Functor

import Mayhem.Engine.Effects
import Mayhem.Engine.Automaton
import Mayhem.Engine.Loop
import Mayhem.Engine.Controller

import Linear

import LD53.M
import LD53.World
import LD53.Utils
import LD53.Params.Utils
import LD53.Params qualified as P
import LD53.Controls

tickPlayer :: C -> Eff (Framed LD53) ()
tickPlayer C{..} = paramly $ tickMapM $ \(Player{}, NotHitlag, facing, ety) ->
  let isFacing = eqFacing facing
      fv2 = facingDir facing
  in \case
    Player'Dash ->
      if | stick.left.isHeld && isFacing Facing'Left || stick.right.isHeld && isFacing Facing'Right -> do
           cset ety $ Velocity $ fv2 ^* ?params.player.runSpeed
           pure $ tickForever Player'Run
         | otherwise -> pure $ tickForever Player'Idle
    -- TODO: Something is off with skid
    Player'Skid -> pure $ tickForever Player'Idle
    Player'SlowTurn ->
      if | stick.left.isHeld && isFacing Facing'Left || stick.right.isHeld && isFacing Facing'Right -> do
           cset ety $ Velocity $ fv2 ^* ?params.player.runSpeed
           pure $ tickForever Player'Run
         | otherwise -> pure $ tickForever Player'Idle
    Player'CrouchSquat ->
      if | stick.down.isHeld -> pure $ tickForever Player'Crouch
         | otherwise -> pure $ tickForever Player'Idle
    Player'StandUp -> pure $ tickForever Player'Idle
    Player'JumpSquat -> do
      -- TODO: Use jumpMomentum here
      if | jumpButton.isHeld -> do
             cmodify ety $ \(Velocity v) -> Velocity $ v + V2 0 (negate ?params.player.jumpSpeed)
             pure $ tickForever Player'Jump -- FH
         | otherwise -> do
             cmodify ety $ \(Velocity v) -> Velocity $ v + V2 0 (negate ?params.player.shorthopSpeed)
             pure $ tickForever Player'Jump -- SH
    Player'Land -> pure $ tickForever Player'Idle
    Player'Hit{} -> pure $ tickForever Player'Jump -- We stay airborne
    Player'StrongCharge -> pure $ tickForever Player'StrongCharged
    Player'StrongAttack -> pure $ tickForever Player'Idle
    Player'WeakWindup a -> pure $ mkPlayerAuto $ Player'WeakAttack a
    Player'WeakAttack a -> pure $
      if | weakButton.isHeld -> mkPlayerAuto $ Player'WeakWindup a
         | otherwise -> mkPlayerAuto $ Player'WeakRecovery a
    Player'WeakRecovery a ->
      pure $ if a == Attack'Airborne
             then tickForever Player'Jump
             else tickForever Player'Idle
    Player'WeakLand -> pure $ tickForever Player'Idle

tickHitlag :: Eff (Framed LD53) ()
tickHitlag = tickMapM_ $ \() -> \case
  Hitlag -> pure ()

handleFastFall :: Eff (Framed LD53) ()
handleFastFall = paramly $ cmap $ \(Player'FastFall, Velocity v) -> Velocity $ V2 v.x ?params.player.fastFallSpeed
