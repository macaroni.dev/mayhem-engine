module LD53.Level.Enemies where

import Control.Monad.Reader qualified as MTL
import Data.Set qualified as Set

import Mayhem.Engine.Utils

import LD53.World.Enemies

isSpawned :: SpawnTag -> EnemyGenM Bool
isSpawned t = MTL.asks $ Set.member t . (.currentlySpawned)

spawnedCount :: SpawnTag -> EnemyGenM Int
spawnedCount t = MTL.asks $ lookupSpawn t . (.spawnHistory)

keepSpawned :: EnemyGroup -> EnemyGen
keepSpawned eg = do
  b <- isSpawned eg.spawnTag
  unlessm (b) $ pure eg.enemies

spawnOnce :: EnemyGroup -> EnemyGen
spawnOnce eg = do
  n <- spawnedCount eg.spawnTag
  unlessm (n > 0) $ pure eg.enemies
