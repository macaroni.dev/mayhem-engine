module LD53.Enemies where

import Data.Functor
import Data.List.NonEmpty (NonEmpty (..))
import Data.Foldable
import Data.Monoid (Sum (..))
import Data.Set qualified as Set

import Mayhem.Engine.Effects
import Mayhem.Engine.Loop
import Mayhem.Engine.Utils
import Mayhem.Engine.Automaton

import Control.Monad.Random.Class
import Control.Monad.Trans.Random
import Control.Monad.Trans.Maybe
import Control.Monad.Reader qualified as MTL

import LD53.M
import LD53.World
import LD53.World.Jokes
import LD53.Params.Utils

levelEnemyGen :: Eff (Framed LD53) ()
levelEnemyGen = gget >>= \Level{..} -> runEnemyGen enemyGen

runEnemyGen :: EnemyGen -> Eff (Framed LD53) ()
runEnemyGen eg = do
  clock <- getClock

  currentlySpawned <- cfoldMap $ Set.singleton . (.spawnTag) . fst . id @(Enemy, Not Joke'Flickering)

  spawnHistory <- gget @EnemySpawnHist

  GRNG g <- gget

  case flip MTL.runReader EnemyGenP{..} $ flip runRandT g $ runMaybeT $ unEnemyGen eg of
    (Nothing, g') -> gset (GRNG g')
    (Just enemies, g') -> do
      gset (GRNG g')
      gmappend $ foldFor enemies $ recordSpawn . (.spawnTag) . id @Enemy
      -- Actually spawn them:
      traverse_ spawnEnemy enemies

spawnEnemy :: Enemy -> Eff (Framed LD53) Entity
spawnEnemy e@Enemy{..} = paramly $ case plan of
  Butterfly Butterfly'Plan{..} -> do
    let auto = either mkButterflyAuto tickForever state
    newEntity (e, Position pos, auto)

tickEnemies :: Eff (Framed LD53) ()
tickEnemies = paramly $ do
  tickMap $ \() -> \case
    Butterfly'Wait{} -> tickForever Butterfly'Idle
    Butterfly'Patrol (path :| rest) -> mkButterflyAuto $ Butterfly'Patrol (rest |: path)

  autoCmap @Velocity $ \() -> \case
    Butterfly'Wait{} -> Velocity 0
    Butterfly'Patrol (Butterfly'Path{..} :| _) -> Velocity vel
    Butterfly'Idle -> Velocity 0

  autoCmap @Position @Butterfly'State $ \(Position p, Velocity v) -> const $ Position $ p + v
