module LD53.Knockback where

import Linear

import Mayhem.Engine.Frame

newtype KnockbackDamage = KnockbackDamage Float deriving stock (Show)
newtype KnockbackPercent = KnockbackPercent Float deriving stock (Show)
newtype KnockbackWeight = KnockbackWeight Float deriving stock (Show)
newtype KnockbackScaling = KnockbackScaling Float deriving stock (Show)
newtype BaseKnockback = BaseKnockback Float deriving stock (Show)
newtype Knockback = Knockback Float deriving stock (Show)

launchSpeed :: Knockback -> Float
launchSpeed (Knockback kb) = 0.03 * kb

hitstun :: Knockback -> Frame
hitstun (Knockback kb) = round (0.4 * kb)

calculateKnockback
  :: KnockbackPercent
  -> KnockbackDamage
  -> KnockbackWeight
  -> KnockbackScaling
  -> BaseKnockback
  -> Knockback
calculateKnockback
  (KnockbackPercent p)
  (KnockbackDamage d)
  (KnockbackWeight w)
  (KnockbackScaling s)
  (BaseKnockback b)
  =
  Knockback ((((((p/10) + ((p*d)/20)) * w * 1.4) + 18) * s) + b)

directKnockbackAngle
  :: V2 Float -- knockback angle
  -> V2 Float -- n
  -> V2 Float
directKnockbackAngle a n =
  if (n `dot` a) > 0
  then a * V2 -1 1
  else a
