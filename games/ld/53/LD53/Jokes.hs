module LD53.Jokes where

import Control.Monad
import Safe (minimumByMay)
import Data.Foldable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Map qualified as Map
import Data.Maybe (fromMaybe, mapMaybe)
import Data.Function (on)

import Linear.V2

import Mayhem.Engine.Effects
import Mayhem.Engine.Automaton
import Mayhem.Engine.Loop
import Mayhem.Engine.Utils
import Mayhem.Engine.Diamond
import Mayhem.Engine.LinearDotSyntax
import Mayhem.Engine.Geometry (intersectSegVertLine)

import LD53.M
import LD53.World
import LD53.Utils
import LD53.Rand
import LD53.Enemies
import LD53.Destroy
import LD53.Params.Utils
import LD53.Params qualified as P

tickSpotlight :: Eff (Framed LD53) ()
tickSpotlight = paramly $ tickMapM $ \(spotlight'ety :: Entity) -> \case
  Spotlight'On{} -> do
    remainingJokes <- ccount @Joke'Automaton
    -- Should maybe move this to somewhere else - the lives check would go here
    -- Score check should happen constantly so it catches the moment the targets
    -- are broken
    when (remainingJokes == 0) $ gmodify @Joke'Score succ

    cmapM_ $ \(_ :: Joke'Platform, ety) -> destroyJokePlatform ety
    cmapM_ $ \(_ :: Joke'Automaton, ety) -> destroyJoke ety

    pure $ mkSpotlightAuto (Spotlight'Off 600)
  Spotlight'Flicker{} -> do
    cmap $ \Joke'Flickering -> Nothing @Joke'Flickering
    pure $ mkSpotlightAuto (Spotlight'On 600)
  Spotlight'Off{} -> do
    Level{..} <- gget
    cmapM_ $ \(Player, Position p) -> do
      let spawnY =
              maybe p.y (.y)
            $ minimumByMay (compare `on` (.y))
            $ flip mapMaybe surfaces $ \s -> do
              iv <- intersectSegVertLine s.line p.x
              guard (iv.y >~ p.y)
              pure iv
      let jokePos = V2 p.x spawnY
      cset spotlight'ety (Position jokePos) -- TODO: confirm the spotlight always focuses ground
      Jokes{..} <- effRand $ (unJokeGen jokeGen) (V2 p.x spawnY)

      for_ enemies $ \e -> do
        ety <- spawnEnemy e
        cset ety Joke'Flickering

      for_ ([1..] `zip` jokes) $ \(i, j) -> do
        newEntity
          (either mkJokeAuto tickForever j.state
          , Position j.pos
          , Joke'Flickering
          , Joke'Order i
          )

      for_ platforms $ \jp -> do
        newEntity (jp, Joke'Flickering)

    pure $ mkSpotlightAuto (Spotlight'Flicker False)

-- Is there a way to avoid the Entity annotation here?
checkForDelivery :: Eff (Framed LD53) ()
checkForDelivery = paramly $ autoCmapM_ @Entity $ \spotlight'ety -> \case
  Spotlight'On{} -> do
    remainingJokes <- ccount @Joke'Automaton
    when (remainingJokes == 0) $ do
      gmodify @Joke'Score succ
      cmapM_ $ \(_ :: Joke'Platform, ety) -> destroyJokePlatform ety
      cset spotlight'ety $ mkSpotlightAuto (Spotlight'Off 600)
  _ -> pure ()

moveSpotlight :: Eff (Framed LD53) ()
moveSpotlight = cmapM_ $ \(Player, Position pp) ->
  autoCmap $ \(sp :: Maybe Position) -> \case
    Spotlight'Off{} -> Just $ Position pp -- Follow the player while it's off
    _ -> sp -- Don't move otherwise

flickerSpotlight :: Eff (Framed LD53) ()
flickerSpotlight = paramly $ cmap $ autoFrameMap @Spotlight'State $ \f -> \case
  Spotlight'Flicker _ ->
    let stillOn = maybe False snd $ Map.lookupGE f ?params.jokes.spotlightFlicker
    in  Spotlight'Flicker stillOn
  x -> x

tickJokes :: Eff (Framed LD53) ()
tickJokes = paramly $ do
  tickMap @_ @Joke'State $ \() -> \case
    Joke'Patrol (path :| rest) -> mkJokeAuto $ Joke'Patrol (rest |: path)

  autoCmap @Velocity $ \() -> \case
    Joke'Idle -> Velocity 0
    Joke'Patrol (Joke'Path{..} :| _) ->Velocity vel

  autoCmap @Position @Joke'State $ \(Position p, Velocity v) -> const $ Position $ p + v
