module LD53.Params where

import Mayhem.Engine.Frame
import Mayhem.Engine.Automaton
import Mayhem.Engine.Diamond

import Data.Map qualified as Map
import CuteC2
import Linear
import GHC.Compact

import LD53.World (Level (..), Player'State (..), Player'Automaton, Facing)
import LD53.World.Enemies (Enemy (..), Butterfly'State (..), Butterfly'Path (..))
import LD53.World.Jokes (Spotlight'State (..), Joke'State (..))

data Main = Main
  { player :: Player
  , level :: Level
  , enemies :: Enemies
  , jokes :: Jokes
  , camera :: Camera
  }

data Camera = Camera
  { yOffset :: Float
  , xOffset :: Float
  , posLerpRate :: Float
  , offsetLerpRate :: Float
  }

data Player = Player
  { movementFrames :: Player'State Timed -> FrameDown
  , hurtboxes :: Player'Automaton -> [C2Shape]
  , strongHitboxes :: Facing -> [C2Shape]
  , weakHitboxes :: Facing -> [C2Shape]
  , attackHitlag :: FrameDown
  , jumpSpeed :: Float
  , djSpeed :: Float
  , shorthopSpeed :: Float
  , gravity :: Float
  , weakGravity :: Float
  , maxFallSpeed :: Float
  , fastFallSpeed :: Float
  , airAcceleration :: Float
  , jumpSpeedX :: Float
  , jumpSpeedXMax :: Float
  , jumpSpeedXMul :: Float
  , maxAirSpeed :: Float
  , airFriction :: Float
  , traction :: Float
  , walkSpeed :: Float
  , dashSpeed :: Float
  , runSpeed :: Float
  , sdiDistance :: Float
  , diamond :: Diamond
  }

data Enemies = Enemies
  { butterflyFrames :: Butterfly'State Timed -> FrameDown
  , hitboxes :: Enemy -> [C2Shape]
  , knockback'angle :: V2 Float
  , knockback'speed :: Float
  , knockback'dur :: Frame
  , hitlag :: FrameDown
  }

data Jokes = Jokes
  { spotlightFrames :: Spotlight'State Timed -> FrameDown
  , jokeFrames :: Joke'State Timed -> FrameDown
  , jokeHitbox :: C2Shape
  , spotlightFlicker :: Map.Map Frame Bool
  }
