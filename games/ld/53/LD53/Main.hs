module Main where

import Mayhem.Console.Jam'22
import Mayhem.Engine.Loop (Framed, HotPipe, newHotPipe, hotReload)
import Mayhem.Engine.Utils (isEnvSet)

import UnliftIO (bracket_)
import Control.Concurrent (forkOS, ThreadId, killThread)
import Foreign.Store qualified as FS
import Data.Typeable

import LD53.M
import LD53.Game as Game
import LD53.Assets as A
import LD53.Params as P
import LD53.Params.Hardcoded as P
import LD53.World

import SDL.GPU.CuteC2 qualified as GPU

main :: IO ()
main = do
  guardDev
  main' Nothing Nothing

dev :: IO ()
dev = do
  ghciPipe <- newGHCiPipe
  FS.writeStore ghciStore ghciPipe

  hotPipe <- newHotPipe
  FS.writeStore hotStore hotPipe

  tid <- forkOS $ do
    bracket_ (guardDev >> FS.writeStore devStore ()) (FS.deleteStore devStore) $ main' (Just ghciPipe) (Just hotPipe)
  FS.writeStore threadStore tid

ghciStore :: FS.Store (GHCiPipe LD53'GHCi)
ghciStore = FS.Store 0

threadStore :: FS.Store ThreadId
threadStore = FS.Store 1

hotStore :: FS.Store (HotPipe (Framed LD53))
hotStore = FS.Store 52

devStore :: FS.Store ()
devStore = FS.Store 2

guardDev :: IO ()
guardDev = let FS.Store x = devStore in FS.lookupStore x >>= \case
  Nothing -> pure ()
  Just _ -> error "Dev currently running in a thread!"

ghci :: Typeable a => Eff LD53'GHCi a -> IO a
ghci e = do
  ghciPipe <- FS.readStore ghciStore
  sendGHCi ghciPipe e

reload :: IO ()
reload = do
  hotPipe <- FS.readStore hotStore
  hotReload hotPipe gameloop
  _ <- ghci (P.hardcoded >>= reloadParams)
  pure ()

kill :: IO ()
kill = do
  tid <- FS.readStore threadStore
  killThread tid

gameRez :: NativeResolution
gameRez = edtv

main' :: Maybe (GHCiPipe LD53'GHCi) -> Maybe (HotPipe (Framed LD53)) -> IO ()
main' ghciPipe hotPipe = runManaged $ do
  devmode <- liftIO $ isEnvSet "DEVMODE"

  run22 <- runJam'22 @A.Main @P.Main @World
             devmode
             (theAssetss (.fonts.debug.rawFont))
             ghciPipe
             gameRez
             initWorld
             P.hardcoded
  Run run <- exp'GameController (theAssetss (.gamecontrollerdb.path)) run22
  run $ Game.game hotPipe
