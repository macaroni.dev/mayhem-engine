{-# LANGUAGE TemplateHaskell #-}
module LD53.World
  ( module LD53.World
  , module LD53.World.Enemies
  , module LD53.World.Jokes
  ) where

import Linear
import Apecs
import GHC.Generics
import Data.Monoid.Generic
import System.Random

import Mayhem.Engine.Automaton
import Mayhem.Engine.Frame
import Mayhem.Engine.Diamond
import Mayhem.Engine.Utils (mkOpticsLabels)
import Mayhem.Engine.TH

import LD53.World.Enemies
import LD53.World.Jokes

data Screen = Screen'Title | Screen'Playing | Screen'ButtonCheck deriving stock (Show)

instance Semigroup Screen where
  x <> _ = x

instance Monoid Screen where
  mempty = Screen'Playing
instance Component Screen where type Storage Screen = Global Screen

newtype Position = Position { unPosition :: (V2 Float) } deriving stock (Eq, Show)
instance Component Position where type Storage Position = Map Position

newtype Velocity = Velocity { unVelocity :: (V2 Float) }
  deriving stock (Eq, Show)

instance Component Velocity where type Storage Velocity = Map Velocity

data Facing = Facing'Left | Facing'Right deriving stock (Eq, Show)
instance Component Facing where type Storage Facing = Map Facing

facingDir :: Facing -> V2 Float
facingDir = \case
  Facing'Left -> V2 -1 0
  Facing'Right -> V2 1 0

facingScale :: Facing -> V2 Float
facingScale = \case
  Facing'Left -> V2 -1 1
  Facing'Right -> V2 1 1

facingX :: Facing -> Float
facingX = \case
  Facing'Left -> -1
  Facing'Right -> 1

data Player = Player
  deriving stock (Eq, Show)
instance Component Player where type Storage Player = Map Player

data Attack'Airborne = Attack'Grounded | Attack'Airborne deriving stock (Eq, Show)

type Player'State :: IsTimed -> Type
data Player'State t where
  Player'Idle :: Player'State Untimed
  Player'Dash :: Player'State Timed
  Player'Walk :: Player'State Untimed
  Player'Run :: Player'State Untimed
  Player'CrouchSquat :: Player'State Timed
  Player'Crouch :: Player'State Untimed
  Player'StandUp :: Player'State Timed
  Player'Skid :: Player'State Timed
  Player'SlowTurn :: Player'State Timed
  Player'JumpSquat :: Player'State Timed
  Player'Jump :: Player'State Untimed
  Player'Land :: Player'State Timed
  -- TODO: Track dir here and modify it when lag expires for DI
  Player'Hit :: { dur :: Frame, hitBy :: [Entity] } -> Player'State Timed
  Player'StrongCharge :: Player'State Timed
  Player'StrongCharged :: Player'State Untimed
  Player'StrongAttack :: Player'State Timed
  Player'WeakWindup :: Attack'Airborne -> Player'State Timed
  Player'WeakAttack :: Attack'Airborne -> Player'State Timed
  Player'WeakRecovery :: Attack'Airborne -> Player'State Timed
  Player'WeakLand :: Player'State Timed

type Player'Automaton = Automaton Player'State

data Player'DJ = Player'DJ
  deriving stock (Eq, Show)
instance Component Player'DJ where type Storage Player'DJ = Map Player'DJ

data Player'Drifting = Player'Drifting Facing
  deriving stock (Eq, Show)
instance Component Player'Drifting where type Storage Player'Drifting = Map Player'Drifting

data Player'FastFall = Player'FastFall
  deriving stock (Eq, Show)
instance Component Player'FastFall where type Storage Player'FastFall = Map Player'FastFall

data Hitlag t where
  Hitlag :: Hitlag Timed

type Hitlag'Automaton = Automaton Hitlag

data Level = Level
  { surfaces :: [Surface ()] -- Eventually, make it more abstract than this:
  , enemyGen :: EnemyGen
  , jokeGen :: JokeGen
  } deriving stock (Generic)
  deriving Semigroup via GenericSemigroup Level
  deriving Monoid via GenericMonoid Level

instance Component Level where type Storage Level = Global Level

newtype CRNG = CRNG StdGen deriving stock (Eq, Show)
instance Component CRNG where type Storage CRNG = Map CRNG

newtype GRNG = GRNG StdGen deriving stock (Eq, Show)
instance Component GRNG where type Storage GRNG = Global GRNG

instance Monoid GRNG where
  mempty = GRNG (mkStdGen 0)

instance Semigroup GRNG where
  x <> _ = x
------------------------------------------------------------------------------------

data Camera = Camera
  { pos :: V2 Float
  , offset :: V2 Float
  , zoom :: Float
  }

-- Unlawful
instance Semigroup Camera where
  x <> _ = x

instance Monoid Camera where
  mempty = Camera { pos = 0, offset = 0, zoom = 1 }
instance Component Camera where type Storage Camera = Global Camera

-- TH_CODE

makeWorld "World"
  [ ''Screen
  , ''Position
  , ''Velocity
  , ''Player
  , ''Player'Automaton
  , ''Player'DJ
  , ''Player'Drifting
  , ''Player'FastFall
  , ''Level
  , ''Camera
  , ''Facing
  , ''Enemy
  , ''EnemySpawnHist
  , ''Butterfly'Automaton
  , ''CRNG
  , ''GRNG
  , ''Spotlight'Automaton
  , ''Joke'Automaton
  , ''Joke'Order
  , ''Joke'Platform
  , ''Joke'Flickering
  , ''Joke'Score
  , ''Hitlag'Automaton
  ]

mkOpticsLabels ''Camera
