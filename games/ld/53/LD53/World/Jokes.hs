{-# LANGUAGE TemplateHaskell #-}
module LD53.World.Jokes where

import Apecs
import Linear.V2

import GHC.Generics
import Data.Monoid.Generic
import System.Random
import Control.Monad.Trans.Random
import Control.Monad.Reader qualified as MTL
import Control.Monad.Random.Class
import Data.Monoid (Sum (..))
import Data.List.NonEmpty (NonEmpty)

import Mayhem.Engine.Frame
import Mayhem.Engine.Diamond
import Mayhem.Engine.Automaton
import Mayhem.Engine.Utils
import Mayhem.Engine.TH

import LD53.World.Enemies

newtype Joke'Score = Joke'Score
  { score :: Int }
  deriving stock (Eq, Ord, Show)
  deriving newtype (Num, Enum)
  deriving Semigroup via Sum Int
  deriving Monoid via Sum Int

instance Component Joke'Score where type Storage Joke'Score = Global Joke'Score

data Spotlight'State t where
  Spotlight'Off :: Frame -> Spotlight'State Timed
  Spotlight'Flicker :: Bool -> Spotlight'State Timed
  Spotlight'On :: Frame -> Spotlight'State Timed

-- There really should only be one..but it will be fine.
type Spotlight'Automaton = Automaton Spotlight'State

data Jokes = Jokes
  { enemies :: [Enemy]
  , platforms :: [Joke'Platform]
  , jokes :: [Joke] -- Order is implicit
  } deriving stock (Generic)
  deriving Semigroup via GenericSemigroup Jokes
  deriving Monoid via GenericMonoid Jokes

data Joke = Joke
  { state :: Either (Joke'State Timed) (Joke'State Untimed)
  , pos :: V2 Float
  }

newtype JokeGenM a = JokeGen { unJokeGen :: V2 Float -> Rand StdGen a }
  deriving stock (Functor)

deriving via MTL.ReaderT (V2 Float) (Rand StdGen) instance Applicative JokeGenM
deriving via MTL.ReaderT (V2 Float) (Rand StdGen) instance Monad JokeGenM
deriving via MTL.ReaderT (V2 Float) (Rand StdGen) instance MonadRandom JokeGenM
deriving via MTL.ReaderT (V2 Float) (Rand StdGen) instance MonadSplit StdGen JokeGenM
deriving via MTL.ReaderT (V2 Float) (Rand StdGen) instance MonadInterleave JokeGenM
deriving via MTL.ReaderT (V2 Float) (Rand StdGen) instance MTL.MonadReader (V2 Float) JokeGenM

instance Semigroup a => Semigroup (JokeGenM a) where
  f <> g = do
    x <- interleave f
    y <- interleave g
    pure (x <> y)

instance Monoid a => Monoid (JokeGenM a) where
  mempty = pure mempty

type JokeGen = JokeGenM Jokes
data Joke'State t where
  Joke'Idle :: Joke'State Untimed
  Joke'Patrol :: NonEmpty Joke'Path -> Joke'State Timed

data Joke'Path = Joke'Path
  { dur :: Frame
  , vel :: V2 Float
  }
type Joke'Automaton = Automaton Joke'State

newtype Joke'Order = Joke'Order Int deriving stock (Eq, Ord, Show)

instance Component Joke'Order where type Storage Joke'Order = Map Joke'Order

newtype Joke'Platform = Joke'Platform
  { surface :: Surface ()
  }

instance Component Joke'Platform where type Storage Joke'Platform = Map Joke'Platform

data Joke'Flickering = Joke'Flickering
instance Component Joke'Flickering where type Storage Joke'Flickering = Map Joke'Flickering

-- TH_CODE
mkOpticsLabels ''Jokes
mkOpticsLabels ''Joke'Path
