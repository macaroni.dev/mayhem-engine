module LD53.World.Enemies where

import Linear
import Apecs
import Data.String (IsString (..))
import Data.Set qualified as Set
import Data.Map qualified as Map
import Data.Monoid (Sum (..))

import System.Random
import Control.Applicative
import Data.List.NonEmpty
import Control.Monad.Random.Class
import Control.Monad.Trans.Random
import Control.Monad.Trans.Maybe
import Control.Monad.Reader (MonadReader)
import Control.Monad.Reader qualified as MTL

import Mayhem.Engine.Frame
import Mayhem.Engine.Automaton
import Mayhem.Engine.Utils

newtype SpawnTag = SpawnTag String
  deriving stock (Eq, Show, Ord)
  deriving newtype (IsString)

data EnemyGroup = EnemyGroup
  { spawnTag :: SpawnTag
  , enemies :: [Enemy]
  }

mkEnemyGroup :: SpawnTag -> [EnemyPlan] -> EnemyGroup
mkEnemyGroup spawnTag plans = EnemyGroup{enemies = Enemy spawnTag <$> plans, ..}

data Enemy = Enemy
  { spawnTag :: SpawnTag
  , plan :: EnemyPlan
  }

data EnemyPlan =
    Butterfly Butterfly'Plan

data Butterfly'Plan = Butterfly'Plan
  { pos :: V2 Float
  , state :: Either (Butterfly'State Timed) (Butterfly'State Untimed)
  }

instance Component Enemy where type Storage Enemy = Map Enemy

data Butterfly'State (t :: IsTimed) where
  Butterfly'Idle :: Butterfly'State Untimed
  Butterfly'Wait :: Frame -> Butterfly'State Timed
  Butterfly'Patrol :: NonEmpty Butterfly'Path -> Butterfly'State Timed
  --Butterfly'Hunt :: Butterfly'State Timed
  --Butterfly'Attack :: Butterfly'State Untimed

type Butterfly'Automaton = Automaton Butterfly'State

data Butterfly'Path = Butterfly'Path
  { dur :: Frame
  , vel :: V2 Float
  }

data EnemyGenP = EnemyGenP
  { clock :: Frame
  , currentlySpawned :: Set.Set SpawnTag
  , spawnHistory :: EnemySpawnHist
  } deriving stock (Eq, Show)

newtype EnemySpawnHist = EnemySpawnHist (Map.Map SpawnTag Int)
  deriving stock (Eq, Show)

deriving via (UnionMap SpawnTag (Sum Int)) instance Semigroup EnemySpawnHist
deriving via (UnionMap SpawnTag (Sum Int)) instance Monoid EnemySpawnHist

instance Component EnemySpawnHist where type Storage EnemySpawnHist = Global EnemySpawnHist

recordSpawn :: SpawnTag -> EnemySpawnHist
recordSpawn = EnemySpawnHist . flip Map.singleton 1

lookupSpawn :: SpawnTag -> EnemySpawnHist -> Int
lookupSpawn t (EnemySpawnHist mp) = maybe 0 id $ Map.lookup t mp

newtype EnemyGenM a = EnemyGen { unEnemyGen :: MaybeT (RandT StdGen (MTL.Reader EnemyGenP)) a }
  deriving stock (Functor)
  deriving newtype
  ( Applicative
  , Alternative
  , Monad
  , MonadRandom
  , MonadReader EnemyGenP
  , MonadSplit StdGen
  , MonadInterleave
  )

type EnemyGen = EnemyGenM [Enemy]

instance Semigroup a => Semigroup (EnemyGenM a) where
  f <> g = do
    x <- interleave f
    y <- interleave g
    pure (x <> y)

instance Monoid a => Monoid (EnemyGenM a) where
  mempty = pure mempty
