module LD53.Camera where

import Linear

import Mayhem.Engine.Effects
import Mayhem.Engine.Loop
import Mayhem.Engine.Controller

import LD53.M
import LD53.World
import LD53.Controls
import LD53.Params qualified as P


updateCamera :: C -> Eff (Framed LD53) ()
updateCamera C{..} = paramly $ cmapM_ $ \(Player, Position p, facing :: Facing) -> do
  let P.Camera{..} = ?params.camera
  let y = if | cameraButton.isHeld && stick.upish.isHeld -> negate yOffset
             | cameraButton.isHeld && stick.downish.isHeld -> yOffset
             | otherwise -> 0
      x = facingX facing * xOffset
  -- lerp'ing pos and offset separately inspired by this godot forum comment:
  -- https://godotforums.org/d/19994-how-to-further-control-camera2d-with-code/3
  gmodify $ \Camera{..} -> Camera
    { pos = lerp posLerpRate p pos
    , offset = lerp offsetLerpRate (V2 x y) offset
    , ..
    }
