module LD53.Utils where

import Linear
import System.IO.Unsafe
import GHC.Compact

import Mayhem.Engine.Automaton
import Mayhem.Engine.Effects

import LD53.World

globalCompact :: forall a. a -> a
globalCompact = getCompact . unsafePerformIO . compact

-- Inference helper via ViewPatterns
eqFacing :: Facing -> (Facing -> Bool)
eqFacing = (==)

isGrounded :: Player'Automaton -> Bool
isGrounded = autoEsac $ \case
  Player'Jump -> False
  -- In theory we could allow this. But if we don't do _something_ we go
  -- into immediate fall after getting hit and it cancels the knockback
  Player'Hit{} -> False
  Player'WeakWindup a -> a == Attack'Grounded
  Player'WeakAttack a -> a == Attack'Grounded
  Player'WeakRecovery a -> a == Attack'Grounded
  _ -> True

canFall :: Player'Automaton -> Bool
canFall = autoEsac $ \case
  Player'Hit{} -> False
  Player'StrongCharge -> False
  Player'StrongCharged -> False
  Player'StrongAttack -> False
  Player'WeakWindup Attack'Airborne -> False
  Player'WeakAttack Attack'Airborne -> False
  Player'WeakRecovery Attack'Airborne -> False
  Player'Jump -> False
  _ -> True

pattern NotHitlag :: Not Hitlag'Automaton
pattern NotHitlag = Not
{-# COMPLETE NotHitlag #-}
