{-# LANGUAGE OverloadedStrings #-}

module LD53.Level where

import Control.Monad (join)
import Data.Foldable (toList)
import Data.List.NonEmpty (NonEmpty (..))
import Data.Semigroup (Semigroup (..))
import Control.Monad.Reader qualified as MTL
import Control.Monad.Random.Class
import Optics

import Mayhem.Engine.Geometry
import Mayhem.Engine.Diamond
import Linear

import LD53.Utils
import LD53.World
import LD53.Level.Enemies

mkSurface :: SurfaceType -> Seg Float -> Surface ()
mkSurface ty line = Surface { tag = (), .. }

{-# NOINLINE theLevel #-}
theLevel :: Level
theLevel = Level
  { surfaces = globalCompact $ mconcat
    [ [mainGround],
      miscPlatforms
    ]
  , enemyGen = mconcat
    [ spawnOnce $ mkEnemyGroup "test_again" $
      [ Butterfly $ Butterfly'Plan
        { pos = V2 0 -50
        , state = Right Butterfly'Idle
        }
      , Butterfly $ Butterfly'Plan
        { pos = V2 -10 -60
        , state = Right Butterfly'Idle
        }
      , Butterfly $ Butterfly'Plan
        { pos = V2 10 -75
        , state = Right Butterfly'Idle
        }
      ]
    , spawnOnce $ mkEnemyGroup "test_patrol" $
      [ Butterfly $ Butterfly'Plan
        { pos = V2 400 -200
        , state =
             Left
           $ Butterfly'Patrol
           $ Butterfly'Path{dur = 90, vel = V2 -4 0}
          :| [ Butterfly'Path
               { dur = 90
               , vel = V2 4 0
               }
             ]
        }
      , Butterfly $ Butterfly'Plan
        { pos = V2 -400 -100
        , state =
             Left
           $ Butterfly'Patrol
           $ Butterfly'Path{dur = 90, vel = V2 -3 -1}
          :| [ Butterfly'Path
               { dur = 90
               , vel = 0
               }
             , Butterfly'Path
               { dur = 90
               , vel = V2 3 1
               }
             , Butterfly'Path
               { dur = 90
               , vel = 0
               }
             ]
        }
      ]
    ]
  , jokeGen = join $ uniform [joke1, joke2]
  }
  where
    mainGround = mkSurface Ground $
      Seg (V2 -2000 0) (V2 2000 0)

    miscPlatforms =
      [ mkSurface Ground $
        Seg (V2 -300 -100) (V2 -400 -110)
      , mkSurface Ground $
        Seg (V2 -350 -30) (V2 -420 -40)
      ]

joke1 :: JokeGen
joke1 = do
  baseLoc <- MTL.ask
  mconcat
    [ do
      let plat = Joke'Platform $ Surface
                 { tag = ()
                  , ty = Ground
                  , line = Seg (baseLoc + V2 -25 -50) (baseLoc + V2 25 -50)
                  }
      pure $ mempty & #platforms .~ [plat]
    , pure $ mempty & #jokes .~
      [ Joke (Right Joke'Idle) (baseLoc + V2 25 -100)
      , Joke (Right Joke'Idle) (baseLoc + V2 -25 -100)
      , Joke (Right Joke'Idle) (baseLoc + V2 0 -100)
      ]
    ]

joke2 :: JokeGen
joke2 = do
  baseLoc <- MTL.ask
  let up'down'patrol =
        Joke'Path
        { dur = 60
        , vel = V2 0 -2
        } :|
        [ Joke'Path
          { dur = 60
          , vel = V2 0 2
          }
        ]
  pure $ mempty & #jokes .~
    [ Joke (Left $ Joke'Patrol up'down'patrol) (baseLoc + V2 50 -25)
    , Joke (Left $ Joke'Patrol $ fmap (& #vel %~ (* -1)) up'down'patrol) (baseLoc + V2 -50 -100)
    ]

---------------

data LevelBuilder =
  LVB'Many [LevelBuilder]
  deriving stock (Show, Eq, Ord)

instance Semigroup LevelBuilder where
  x <> y = LVB'Many [x, y]
  sconcat = LVB'Many . toList

instance Monoid LevelBuilder where
  mempty = LVB'Many []
