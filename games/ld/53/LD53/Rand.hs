module LD53.Rand where

import System.Random
import Control.Monad.Trans.Random

import Mayhem.Engine.Effects
import Mayhem.Engine.Loop

import LD53.M
import LD53.World

effRandT :: RandT StdGen (Eff (Framed LD53)) a -> Eff (Framed LD53) a
effRandT rt = do
  GRNG g <- gget
  (a, g') <- runRandT rt g
  gset (GRNG g')
  pure a

effRand :: Rand StdGen a -> Eff (Framed LD53) a
effRand rt = do
  GRNG g <- gget
  let (a, g') = runRand rt g
  gset (GRNG g')
  pure a
