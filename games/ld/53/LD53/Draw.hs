module LD53.Draw where

import Control.Monad
import Data.Functor
import Data.Foldable
import Foreign.Ptr
import UnliftIO

import SDL.GPU.FC.Simple qualified as FC
import SDL.GPU.Utils qualified as GPU
import SDL.GPU.CuteC2 qualified as GPU
import SDL.GPU.Utils.Colors qualified as GPU
import SDL.GPU.Utils qualified as GPU.Camera (CameraF(..))

import Linear
import CuteC2

import Mayhem.Engine.Loop
import Mayhem.Engine.Automaton
import Mayhem.Engine.Geometry
import Mayhem.Engine.Diamond
import Mayhem.Engine.Resolution
import Mayhem.Engine.LinearDotSyntax ()
import Mayhem.Engine.Utils
import Mayhem.Engine.Assets

import LD53.M
import LD53.World
import LD53.Controls
import LD53.Params qualified as P
import LD53.Assets qualified as A

debugDraw :: C -> Eff (Framed LD53) ()
debugDraw _ = gpuly $ paramly $ assetly $ mconcat $
  [ withCamera $ liftIO $ do
      for_ (takeWhile (<= 2000) [0, 16..]) $ \y -> do
        GPU.horizontalLine ?screen -2000 y 4000
        GPU.horizontalLine ?screen -2000 (negate y) 4000
      for_ (takeWhile (<= 2000) [0, 16..]) $ \x -> do
        GPU.verticalLine ?screen x -2000 4000
        GPU.verticalLine ?screen (negate x) -2000 4000
  ,withCamera $ do
      gget >>= drawLevel ?screen
      cmapM_ $ \(Player, Position p, ps, facing :: Facing) -> do
        let diamond = ?params.player.diamond.offset p
        drawDiamond ?screen (playerDebugColor ps) diamond

        for_ (?params.player.hurtboxes ps) $ \hb ->
          GPU.c2Shape ?screen (c2Move (v22c2v p) hb) GPU.yellow

        autoCase ps $ \case
          Player'StrongAttack -> do
            for_ (?params.player.strongHitboxes facing) $ \hb ->
              GPU.c2Shape ?screen (c2Move (v22c2v p) hb) GPU.red
          Player'WeakAttack{} -> do
            for_ (?params.player.weakHitboxes facing) $ \hb ->
              GPU.c2Shape ?screen (c2Move (v22c2v p) hb) GPU.red
          _ -> pure ()

        let (line1, line2) = (p + facingScale facing * V2 20 -30, p + facingScale facing * V2 20 -10)
        GPU.line ?screen line1.x line1.y line2.x line2.y GPU.white
        void $
          FC.drawEffect
          ?assets.fonts.debug.rawFont
          ?screen
          (diamond.at TOP).x ((diamond.at TOP).y - 17)
          (FC.Effect
           { alignment = FC.alignCenter
           , scale = FC.Scale 0.5 0.5
           , color = GPU.white
           }
          )
          (playerDebugCode ps)

  , withCamera $ do
      autoCmapM_ $ \(Position p) -> \case
        Spotlight'Off{} -> pure ()
        Spotlight'Flicker isOn -> do
          when isOn $ GPU.circle ?screen p.x p.y 20 GPU.yellow
        Spotlight'On{} -> do
          GPU.circle ?screen p.x p.y 20 GPU.yellow

  , withCamera $ do
      cmapM_ $ \(e@Enemy{}, Position p, mjf :: Maybe Joke'Flickering, ety :: Entity) -> flickerDraw mjf $ do
        for_ (?params.enemies.hitboxes e) $ \hb -> do
          GPU.c2ShapeFilled ?screen (c2Move (v22c2v p) hb) GPU.yellow

          void $ FC.drawScale
            ?assets.fonts.debug.rawFont
            ?screen
            p.x p.y
            (FC.Scale 0.5 0.5)
            (show ety)

  , withCamera $ do
      cmapM_ $ \(Joke'Order n, Position p, mjf :: Maybe Joke'Flickering) -> flickerDraw mjf $ do
        let hb = c2Move (v22c2v p) ?params.jokes.jokeHitbox
        GPU.c2ShapeFilled ?screen hb GPU.blue
        void $ FC.drawScale
          ?assets.fonts.debug.rawFont
          ?screen
          p.x p.y
          (FC.Scale 0.5 0.5)
          (show n)
  , withCamera $ do
      cmapM_ $ \(Joke'Platform Surface{line = Seg (V2 x1 y1) (V2 x2 y2)}, mjf :: Maybe Joke'Flickering) -> flickerDraw mjf $ do
        GPU.withLineThickness 3.0 $ GPU.line ?screen x1 y1 x2 y2 GPU.violet
  , withoutCamera $ do
      Joke'Score{..} <- gget
      void $ FC.draw ?assets.fonts.debug.rawFont ?screen 400 20 (show score)

      cmapM_ $ \(Player, Velocity v) ->
        void $ FC.drawScale ?assets.fonts.debug.rawFont ?screen 200 20 (FC.Scale 0.5 0.5) (show v)
  ]

flickerDraw :: Maybe Joke'Flickering -> Eff (Framed LD53) () -> Eff (Framed LD53) ()
flickerDraw mjf k = cmapM_ $ autoEsac $ \case
  _ | Nothing <- mjf -> k
  Spotlight'Flicker isOn -> when isOn k
  Spotlight'Off{} -> pure ()
  Spotlight'On{} -> k
drawDiamond :: MonadIO m => Ptr GPU.Target -> GPU.Color -> Diamond -> m ()
drawDiamond screen color diamond =
  GPU.polygonFilled screen
  [ diamond.at BOTTOM
  , diamond.at LEFT
  , diamond.at TOP
  , diamond.at RIGHT
  ] color

playerDebugColor :: Player'Automaton -> GPU.Color
playerDebugColor = autoEsac $ \case
  Player'Idle -> GPU.green
  Player'Dash -> GPU.dark GPU.orange
  Player'Walk -> GPU.blue
  Player'Run -> GPU.red
  Player'CrouchSquat -> GPU.greyN 0.5
  Player'Crouch -> GPU.greyN 0.75
  Player'StandUp -> GPU.greyN 0.25
  Player'Skid -> GPU.rose
  Player'SlowTurn -> GPU.magenta
  Player'JumpSquat -> GPU.cyan
  Player'Jump -> GPU.aquamarine
  Player'Land -> GPU.violet
  Player'Hit{} -> GPU.dark GPU.red
  Player'StrongCharge -> GPU.chartreuse
  Player'StrongCharged -> GPU.light GPU.chartreuse
  Player'StrongAttack -> GPU.light $ GPU.light $ GPU.chartreuse
  Player'WeakWindup{} -> GPU.orange
  Player'WeakAttack{} -> GPU.light GPU.orange
  Player'WeakRecovery{} -> GPU.light $ GPU.light $ GPU.orange
  Player'WeakLand -> GPU.dark GPU.red

playerDebugCode :: Player'Automaton -> String
playerDebugCode = autoEsac $ \case
  Player'Idle -> "IDLE"
  Player'Dash -> "DASH"
  Player'Walk -> "WALK"
  Player'Run -> "RUN"
  Player'CrouchSquat -> "CSQUAT"
  Player'Crouch -> "CROUCH"
  Player'StandUp -> "STAND"
  Player'Skid -> "SKID"
  Player'SlowTurn -> "TURN"
  Player'JumpSquat -> "JSQUAT"
  Player'Jump -> "JUMP"
  Player'Land -> "LAND"
  Player'Hit{} -> "HIT"
  Player'StrongCharge -> "CHARGE1"
  Player'StrongCharged -> "CHARGE2"
  Player'StrongAttack -> "STRATK"
  Player'WeakWindup{} -> "PREWEAK"
  Player'WeakAttack{} -> "WEAK"
  Player'WeakRecovery{} -> "POSTWEAK"
  Player'WeakLand -> "WEAKLAND"

drawLevel :: MonadUnliftIO m => Ptr GPU.Target -> Level -> m ()
drawLevel screen Level{..} = for_ surfaces $ \Surface{..} -> do
  let Seg (V2 x1 y1) (V2 x2 y2) = line
  let color = case ty of
        Ground -> GPU.white
        Ceiling -> GPU.red
        RightWall -> GPU.green
        LeftWall -> GPU.blue
  GPU.withLineThickness 3.0 $ GPU.line screen x1 y1 x2 y2 color

surfaceDebugColor :: SurfaceType -> GPU.Color
surfaceDebugColor = \case
  Ground -> GPU.Color 255 255 255 255
  Ceiling -> GPU.Color 255 0 0 255
  RightWall -> GPU.Color 0 255 0 255
  LeftWall -> GPU.Color 0 0 255 255

---------------------------
withCamera :: Eff (Framed LD53) r -> Eff (Framed LD53) r
withCamera k = gpuly $ GPU.withFreshMatrix ?screen GPU.view $ do
  -- Translate origin first
  let halve = (// (2 :: Int))
  NativeResolution{..} <- askNativeRez
  GPU.translate (halve nativeWidth) (halve nativeHeight) 0

  Camera{..} <- gget

  -- Zoom about origin
  GPU.scale zoom zoom 1.0
  GPU.translate (negate $ pos.x + offset.x) (negate $ pos.y + offset.y) 0
  k

-- | I think this doesn't quite work right when fullscreened
withoutCamera :: Eff (Framed LD53) r -> Eff (Framed LD53) r
withoutCamera k = gpuly $ GPU.withFreshMatrix ?screen GPU.model k
