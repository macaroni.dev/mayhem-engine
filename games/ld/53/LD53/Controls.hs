module LD53.Controls where

import Data.Foldable
import Control.Monad.IO.Class
import Control.Monad
import GHC.Generics hiding (C)
import Data.Monoid.Generic

import SDL qualified
import SDL.Input.GameController qualified as SDL
import Linear

import Mayhem.Engine.Controller
import Mayhem.Engine.Effects
import Mayhem.Engine.Automaton
import Mayhem.Engine.LinearDotSyntax
import Mayhem.Engine.Loop
import Mayhem.Engine.Utils

import LD53.M
import LD53.Params qualified as P
import LD53.Params.Utils
import LD53.World
import LD53.Utils

data C = C
  { stick :: ArcadeStick
  , jumpButton :: Button
  , weakButton :: Button
  , strongButton :: Button
  , cameraButton :: Button
  } deriving stock (Show, Generic)
  deriving Semigroup via GenericSemigroup C
  deriving Monoid via GenericMonoid C

parseController :: [SDL.Event] -> Eff (Framed LD53) C
parseController es = foldA
  [ do
      stick <- dpadArcadeStick2 <$> wasd es
      jumpButton <- scancodeButton SDL.ScancodeJ es
      weakButton <- scancodeButton SDL.ScancodeK es
      strongButton <- scancodeButton SDL.ScancodeL es
      cameraButton <- scancodeButton SDL.ScancodeSpace es
      pure C {..}
  , do
      gcs <- listControllers
      stick <- dpadArcadeStick2 <$> unifiedGameControllerDPad gcs es

      let mkButton b = unifiedGameControllerButton gcs b es
      jumpButton <- mkButton SDL.ControllerButtonA
      weakButton <- mkButton SDL.ControllerButtonB
      strongButton <- mkButton SDL.ControllerButtonX
      cameraButton <- mkButton SDL.ControllerButtonY
      pure C{..}
  ]

handleSDI :: C -> Eff (Framed LD53) ()
handleSDI C{..} = paramly $ cmap $ \(Player{}, _ :: Hitlag'Automaton, Position p) ->
  let dir =
        if | stick.up.wasPressed -> V2 0 -1
           | stick.down.wasPressed -> V2 0 1
           | stick.right.wasPressed -> V2 1 0
           | stick.left.wasPressed -> V2 -1 0
           | otherwise -> V2 0 0
  in Position $ (dir ^* ?params.player.sdiDistance) + p

handleInput :: C -> Eff (Framed LD53) ()
handleInput c = handleSDI c >> handleInputMain c

handleInputMain :: C -> Eff (Framed LD53) ()
handleInputMain C{..} = paramly $ autoCmapM_ $ \(Player{}, _ :: Not Hitlag'Automaton, facing, dj :: Maybe Player'DJ, Velocity v, ety :: Entity) ->
  let jumpCheck =
          when jumpButton.wasPressed
        $ cset @_ @_ @(Framed LD53) ety
        $ mkPlayerAuto Player'JumpSquat
      djCheck =
        -- TODO: Use jumpMomentum here
        for_ dj $ \_ -> when jumpButton.wasPressed $
        cset @_ @_ @(Framed LD53) ety $
        let vx = if | stick.left.isHeld -> negate ?params.player.jumpSpeedX
                    | stick.right.isHeld -> ?params.player.jumpSpeedX
                    | otherwise -> 0
        in (tickForever Player'Jump, Velocity (V2 vx (negate ?params.player.djSpeed)), Nothing @Player'DJ, Nothing @Player'FastFall)
      isFacing = eqFacing facing
      fv2 = facingDir facing
      strongCheck =
          when strongButton.wasPressed $ do
            let strongFacing =
                  if | stick.left.isHeld -> Just Facing'Left
                     | stick.right.isHeld -> Just Facing'Right
                     | otherwise -> Just facing
            for_ strongFacing $ \sf ->
              cset @_ @_ @(Framed LD53) ety
              ( Velocity 0
              , sf
              , mkPlayerAuto Player'StrongCharge
              )
      weakCheck a =
        when (weakButton.wasPressed) $ do
          cset @_ @_ @(Framed LD53) ety
            ( mkPlayerAuto $ Player'WeakAttack a
            --, Velocity $ V2 (v.x / 2) 0
            )
      actionCheck = jumpCheck >> strongCheck
      fastFall = when (v.y >= 0 && stick.down.isHeld) $ cset @_ @_ @(Framed LD53) ety Player'FastFall
      drift =
        if | stick.left.isHeld -> cset @_ @_ @(Framed LD53) ety (Player'Drifting Facing'Left)
           | stick.right.isHeld -> cset ety (Player'Drifting Facing'Right)
           | otherwise -> cset ety (Nothing @Player'Drifting)
  in \case
    Player'Idle -> do
      if | stick.left.wasPressed -> cset ety
           ( Facing'Left
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Left
           )
         | stick.right.wasPressed -> cset ety
           ( Facing'Right
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Right
           )
         -- If hard left/right are held but weren't pressed, it means the player was holding them before
         -- they became idle. In which case, they want to buffer a walk.
         | stick.down'left.isHeld || stick.left.isHeld || stick.up'left.isHeld -> cset ety
           ( Facing'Left
           , tickForever Player'Walk
           , walkVel Facing'Left
           )
         | stick.down'right.isHeld || stick.right.isHeld || stick.up'right.isHeld -> cset ety
           ( Facing'Right
           , tickForever Player'Walk
           , walkVel Facing'Right
           )
         | stick.down.isHeld ->
           cset ety $ mkPlayerAuto Player'CrouchSquat
         | otherwise -> pure ()

      actionCheck
      weakCheck Attack'Grounded
    Player'CrouchSquat -> actionCheck >> weakCheck Attack'Grounded
    Player'Crouch -> do
      if | stick.left.wasPressed -> cset ety
           ( Facing'Left
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Left
           )
         | stick.right.wasPressed -> cset ety
           ( Facing'Right
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Right
           )
         | stick.down'left.isHeld || stick.left.isHeld -> cset ety
           ( Facing'Left
           , tickForever Player'Walk
           , walkVel Facing'Left
           )
         | stick.down'right.isHeld || stick.right.isHeld -> cset ety
           ( Facing'Right
           , tickForever Player'Walk
           , walkVel Facing'Right
           )
         | not stick.down.isHeld ->
           cset ety $ mkPlayerAuto Player'StandUp
         | otherwise -> pure ()
      actionCheck
      weakCheck Attack'Grounded
    Player'StandUp -> do
      if | stick.left.wasPressed -> cset ety
           ( Facing'Left
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Left
           )
         | stick.right.wasPressed -> cset ety
           ( Facing'Right
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Right
           )
         | stick.down'left.isHeld || stick.left.isHeld -> cset ety
           ( Facing'Left
           , tickForever Player'Walk
           , walkVel Facing'Left
           )
         | stick.down'right.isHeld || stick.right.isHeld -> cset ety
           ( Facing'Right
           , tickForever Player'Walk
           , walkVel Facing'Right
           )
         | otherwise -> pure ()
      actionCheck
      weakCheck Attack'Grounded
    ---
    Player'Dash -> do
      if | stick.left.wasPressed && isFacing Facing'Right -> cset ety
           ( Facing'Left
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Left
           )
         | stick.right.wasPressed && isFacing Facing'Left -> cset ety
           ( Facing'Right
           , mkPlayerAuto Player'Dash
           , dashVel Facing'Right
           )
         | otherwise -> pure ()

      actionCheck
      weakCheck Attack'Grounded
    ---
    Player'Walk -> do
      if | stick.leftish.isHeld -> cset ety
           ( Facing'Left
           , tickForever Player'Walk
           , walkVel Facing'Left
           )
         | stick.rightish.isHeld -> cset ety
           ( Facing'Right
           , tickForever Player'Walk
           , walkVel Facing'Right
           )
         | otherwise -> cset ety $ tickForever Player'Idle

      actionCheck
      weakCheck Attack'Grounded
    ---
    Player'Run -> do
      if | stick.left.isHeld || stick.down'left.isHeld -> cset ety $ (Facing'Left, if isFacing Facing'Left then tickForever Player'Run else mkPlayerAuto Player'SlowTurn)
         | stick.right.isHeld || stick.down'right.isHeld -> cset ety $ (Facing'Right, if isFacing Facing'Right then tickForever Player'Run else mkPlayerAuto Player'SlowTurn)
         | otherwise -> cset ety $ mkPlayerAuto Player'Skid

      jumpCheck
      weakCheck Attack'Grounded
    ---
    Player'Skid -> do
      if | stick.down.wasPressed ->
           cset ety $ mkPlayerAuto Player'CrouchSquat
         | stick.right.isHeld && isFacing Facing'Left ->
           cset ety $ (Facing'Right, mkPlayerAuto Player'SlowTurn)
         | stick.left.isHeld && isFacing Facing'Right ->
           cset ety $ (Facing'Left, mkPlayerAuto Player'SlowTurn)
         | otherwise -> pure ()
      actionCheck
      weakCheck Attack'Grounded
    ---
    Player'SlowTurn -> actionCheck >> weakCheck Attack'Grounded
    ---
    Player'JumpSquat -> pure ()
    ---
    Player'Jump -> do
      strongCheck
      weakCheck Attack'Airborne
      djCheck
      drift
      fastFall
    ---
    Player'Land -> pure ()
    ---
    Player'Hit{} -> pure ()
    ---
    Player'StrongCharge -> do
      if | stick.down.wasPressed ->
           cset ety $ tickForever Player'Idle
         | not strongButton.isHeld ->
           cset ety $ tickForever Player'Idle
         | otherwise -> pure ()
    ---
    Player'StrongCharged ->
      if | stick.down.wasPressed ->
           cset ety $ tickForever Player'Idle
         | not strongButton.isHeld ->
           cset ety $ mkPlayerAuto Player'StrongAttack
         | otherwise -> pure ()
    ---
    Player'StrongAttack -> pure ()
    --
    Player'WeakWindup a -> do
      when (stick.down.wasPressed) $ cset ety $ mkPlayerAuto $ Player'WeakRecovery a
      case a of
        Attack'Airborne -> do
          djCheck >> drift
        Attack'Grounded -> jumpCheck
    Player'WeakAttack a -> do
      when (stick.down.wasPressed) $ cset ety $ mkPlayerAuto $ Player'WeakRecovery a
      case a of
        Attack'Airborne -> fastFall >> djCheck >> drift
        Attack'Grounded -> jumpCheck
    Player'WeakRecovery a ->
      case a of
        Attack'Airborne -> drift >> fastFall >> djCheck
        Attack'Grounded -> jumpCheck
    Player'WeakLand -> pure ()

canAerialDrift :: forall t. Player'State t -> Bool
canAerialDrift = \case
  Player'Jump -> True
  Player'WeakWindup Attack'Airborne -> True
  Player'WeakAttack Attack'Airborne -> True
  Player'WeakRecovery Attack'Airborne -> True
  _ -> False
