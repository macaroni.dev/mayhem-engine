module LD53.Collisions where

import Control.Monad
import Data.Foldable

import CuteC2
import Linear

import Mayhem.Engine.Effects
import Mayhem.Engine.Automaton
import Mayhem.Engine.Loop
import Mayhem.Engine.Utils
import Mayhem.Engine.Frame

import LD53.M
import LD53.World
import LD53.Destroy
import LD53.Params.Utils
import LD53.Utils
import LD53.Params qualified as P

handleCollisions :: Eff (Framed LD53) ()
handleCollisions = do
  handlePlayerAttack
  handleEnemyAttack

-- TODO: Different hitlag per attack
-- TODO: Only set hitlag once
handlePlayerAttack :: Eff (Framed LD53) ()
handlePlayerAttack = paramly $
  cmapM_ $ \(ps :: Player'Automaton, Position pp, facing :: Facing, pEty) -> do
      let playerHBs = autoCase @[C2Shape] ps $ \case
            Player'StrongAttack -> c2Move (v22c2v pp) <$> ?params.player.strongHitboxes facing
            Player'WeakAttack{} -> c2Move (v22c2v pp) <$> ?params.player.weakHitboxes facing
            _ -> []
      let collidesWithPlayer hb = any (c2Collided hb) playerHBs
      cmapM_ $ \(e@Enemy{}, _ :: Not Joke'Flickering, Position ep, ety) -> do
        when (any collidesWithPlayer (c2Move (v22c2v ep) <$> ?params.enemies.hitboxes e)) $ do
          cset pEty (TickDown Hitlag ?params.player.attackHitlag)
          destroyEnemy ety

      minJoke <- cminBy (id @Joke'Order)
      cmapM_ $ \(jo@Joke'Order{}, Position jp, _ :: Not Joke'Flickering, ety :: Entity) -> do
        let hb = c2Move (v22c2v jp) ?params.jokes.jokeHitbox
        when (Just jo == minJoke && any (c2Collided hb) playerHBs) $ do
          cset pEty (TickDown Hitlag ?params.player.attackHitlag)
          destroyJoke ety

handleEnemyAttack :: Eff (Framed LD53) ()
handleEnemyAttack = paramly $
  cmapM $ \(ps :: Player'Automaton, NotHitlag, Position p, Velocity v, drift :: Maybe Player'Drifting) -> do
    enemies <- cList @(Enemy, Not Joke'Flickering, Position, Entity)
    let res = asum $ do
          (e, _, Position ep, e'ety) <- enemies
          ehb <- ?params.enemies.hitboxes e
          phb <- ?params.player.hurtboxes ps
          guard $ not $ wasHitBy e'ety ps
          pure $ fmap (e'ety,) $ c2Collide (c2Move (v22c2v p) phb) (c2Move (v22c2v ep) ehb)

    let knockback ety (c2v2v2->n) =
          let fixAng a =
                if (n `dot` a) > 0
                then a * V2 -1 1
                else a
          in
            (   Velocity
              $ (fixAng ?params.enemies.knockback'angle)
              ^* ?params.enemies.knockback'speed
            , Just $ TickDown Hitlag ?params.enemies.hitlag
            , hitPlayer ety ?params.enemies.knockback'dur ps
            , Nothing @Player'Drifting
            )

    pure $ case res of
      Nothing -> (Velocity v, Nothing, ps, drift)
      Just (e'ety, C2Manifold'One{n}) -> knockback e'ety n
      Just (e'ety, C2Manifold'Two{n}) -> knockback e'ety n

hitPlayer :: (?params :: P.Main) => Entity -> Frame -> Player'Automaton -> Player'Automaton
hitPlayer ety newDur = autoEsac $ \case
  Player'Hit{..} -> mkPlayerAuto $ Player'Hit{dur = newDur, hitBy = [ety]}
  _ -> mkPlayerAuto $ Player'Hit{dur = newDur, hitBy = [ety]}

wasHitBy :: Entity -> Player'Automaton -> Bool
wasHitBy ety = autoEsac $ \case
  Player'Hit{..} -> ety `elem` hitBy
  _ -> False
