#! /usr/bin/env bash

prefixes=$(fd .png --max-depth=1 --search-path "$BAUBLE1_ASSET_WORK_DIR"/animations --exec echo {/.} | sed 's/[0-9]//g' | sort | uniq)

echo -e "$prefixes" | xargs -rn 1 -I{} cabal run compile-animations -- --prefix {} --src "$BAUBLE1_ASSET_WORK_DIR"/animations --dst ./assets/animations

cp "$BAUBLE1_ASSET_WORK_DIR"/images ./assets/images
