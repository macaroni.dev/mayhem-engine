#! /usr/bin/env bash

set -ex

prefixes=$(fd .png --max-depth=1 --search-path "$1"/animations --exec echo {/.} | sed 's/[0-9]//g' | sort | uniq)

echo -e "$prefixes" | xargs -rn 1 -I{} cabal run compile-animations -- --prefix {} --src "$1"/animations --dst ./assets/animations
