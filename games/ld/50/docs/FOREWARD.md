# Foreward

> After cheating Death and conning Hades, Sisyphus had been condemned to eternally roll a `BOULDER` up a mountain in the underworld. At the peak, the `BOULDER` was cursed to roll back down to the bottom.
> 
> One day, Hades went to go check on Sisyphus, only to notice him [smiling](http://dbanach.com/sisyphus.htm) as he strolled down after his `BOULDER` to the bottom.
> 
> Clearly, Sisyphus had not been punished enough...yet.
