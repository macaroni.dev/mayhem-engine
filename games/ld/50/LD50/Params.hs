{-# LANGUAGE TemplateHaskell #-}

module LD50.Params where

import Data.Word
import Data.Int

import Linear.V2
import CuteC2

import Mayhem.Engine.TH
import Mayhem.Engine.Frame
import Mayhem.Engine.Utils

import LD50.World qualified as W

data Main = Main
  { sisyphus :: Sisyphus
  , mountainDegrees :: Float
  , hazardStartLoc :: W.Loc
  , hazards :: W.Hazard -> Hazard
  , hazardRNG :: W.Loc -> HazardRNG
  }

data Sisyphus = Sisyphus
  { dashStopTime :: Frame
  , dashStartTime :: Frame
  , dashDistance :: Word64
  , dashTime :: Frame
  , walkStartTime :: Frame
  , walkSpeed :: Word64
  , holdSpeed :: Word64 -- we slidin
  , locOffset :: W.Loc
  , depth :: W.Depth
  , walkSpriteOffset :: V2 Float
  , boulderSpriteOffset :: V2 Float
  , aabb :: C2AABB
  , deathSpeed :: Word64
  }

data Hazard = Hazard
  { startupTimeRange :: (Word16, Word16)
  , attackTimeRange :: (Word16, Word16)
  , aabb :: C2AABB
  , depth :: W.Depth
  } deriving (Eq, Show)

data HazardRNG = HazardRNG
  { oddsToSpawn :: Double
  , spawnRate :: Frame
  , targetAreas :: [(Int16, Int16)]
  , maxConcurrent :: Int
  , cooldown :: Word16
  , hazardSplit :: Double
  -- TODO: Cooldown along w/max concurrent?
  }

hardcoded :: Main
hardcoded = Main
  { sisyphus = Sisyphus
    { dashStopTime = 7
    , dashStartTime = 10
    , dashDistance = 100
    , dashTime = 15
    , walkStartTime = 10
    , walkSpeed = 2
    , holdSpeed = 1 -- If this is too fast, we gotta refactor to Loc ~ Double OR ADD LOGIC TO SLIDIN
    , locOffset = 150
    , depth = 5
    , boulderSpriteOffset = V2 65 -35
    , walkSpriteOffset = V2 -3 7
    , aabb = C2AABB (C2V 0 0) (C2V 25 50)
    , deathSpeed = 24
    }
  , mountainDegrees = 25
  , hazardStartLoc = 50
  , hazards = \case
      W.Lava -> Hazard
        { startupTimeRange = (45, 45)
        , attackTimeRange = (30, 30)
        , aabb = C2AABB (C2V 10 10) (C2V 30 500)
        , depth = 40
        }
      W.Lightning -> Hazard
        { startupTimeRange = (60, 60)
        , attackTimeRange = (20, 20)
        , aabb = C2AABB (C2V 5 10) (C2V 30 500)
        , depth = 40
        }
  , hazardRNG = \loc -> if -- TODO: Abstract so we can use a list (Len, Area)
      | loc < 300 -> tutorialLava
      | loc < 600 -> tutorialLightning
      | loc < 1200 -> tutorialBoth
      | loc < 5280 -> HazardRNG
        { oddsToSpawn = 0.75
        , targetAreas = [(-25, 120)]
        , spawnRate = 40
        , maxConcurrent = 3
        , cooldown = 0
        , hazardSplit = 0.6
        }
      | otherwise -> HazardRNG
        { oddsToSpawn = 0.8
        , targetAreas = [(-50, 150)]
        , spawnRate = 10
        , maxConcurrent = 5
        , cooldown = 30
        , hazardSplit = 0.4
        }
  }

tutorialBoth :: HazardRNG
tutorialBoth = HazardRNG
  { oddsToSpawn = 1.0
  , targetAreas = [(-25, -25), (75, 75)]
  , spawnRate = 25
  , maxConcurrent = 2
  , cooldown = 60
  , hazardSplit = 0.5
  }

tutorialLava :: HazardRNG
tutorialLava = HazardRNG
  { oddsToSpawn = 1.0
  , targetAreas = [(75, 75)]
  , spawnRate = 1
  , maxConcurrent = 1
  , cooldown = 180
  , hazardSplit = 0.0
  }

tutorialLightning :: HazardRNG
tutorialLightning = HazardRNG
  { oddsToSpawn = 1.0
  , targetAreas = [(75, 75)]
  , spawnRate = 1
  , maxConcurrent = 1
  , cooldown = 180
  , hazardSplit = 1.0
  }

reactions :: HazardRNG
reactions = HazardRNG
  { oddsToSpawn = 1.0
  , spawnRate = 1
  , targetAreas = [(0, 0)]
  , maxConcurrent = 1
  , cooldown = 0
  , hazardSplit = 0.5
  }

-- TH_CODE
mkOpticsLabels ''Main
mkOpticsLabels ''Sisyphus
mkOpticsLabels ''Hazard
mkOpticsLabels ''HazardRNG
