{-# LANGUAGE TemplateHaskell #-}

module LD50.Assets where

import GHC.Generics

import qualified SDL.GPU.Utils as GPU

import Mayhem.Engine.Assets
import qualified Mayhem.Engine.Animation as Animation
import Mayhem.Engine.TH
import Mayhem.Engine.Utils

import LD50.World

data Main = Main
  { animations :: Animations
  , fonts :: Fonts
  , images:: Images
  , sounds :: Sounds
  , music :: Music
  }
  deriving stock Generic
  deriving anyclass Asset

data Sounds = Sounds
  { attack :: Hazard -> CuteSoundN 10
  , start :: Hazard -> CuteSoundN 10
  }
  deriving stock Generic
  deriving anyclass Asset

data Music = Music
  { bgm :: CuteSound
  }
  deriving stock Generic
  deriving anyclass Asset

data Fonts = Fonts
  { _Romulus :: Font 16 ('Color 255 255 255 255)
  }
  deriving stock Generic
  deriving anyclass Asset

data Images = Images
  { testSprite :: Ptr GPU.Image
  , testBackground :: Ptr GPU.Image
  , testMountain1 :: Ptr GPU.Image
  , terrain :: Terrain -> Ptr GPU.Image
  , testTower :: Ptr GPU.Image
  , _Boulder :: Ptr GPU.Image
  , _Ground :: Ptr GPU.Image
  , _SisyphusDead :: Ptr GPU.Image
  , _Mountains :: Ptr GPU.Image
  , _Sky :: Ptr GPU.Image
  }
  deriving stock Generic
  deriving anyclass Asset

-- 26 Lightning
-- 10 Lava
data Animations = Animations
  { _SisyphusWalk :: Animation.Script
  , _SisyphusDash :: Animation.Script
  , _SisyphusSlide :: Animation.Script
  , _SisyphusSlap :: Animation.Script
  , _SisyphusCatch :: Animation.Script
  , _HazardStart :: Hazard -> Animation.Script
  , _HazardAttack :: Hazard -> Animation.Script
  }
  deriving stock Generic
  deriving anyclass Asset

-- TH_CODE
mkOpticsLabels ''Main
mkOpticsLabels ''Fonts
mkOpticsLabels ''Images
mkOpticsLabels ''Animations
mkOpticsLabels ''Sounds
mkOpticsLabels ''Music
