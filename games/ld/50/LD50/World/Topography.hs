module LD50.World.Topography where

import Data.Foldable
import Data.Map (Map)
import Data.Map.Strict qualified as Map
import Data.Monoid (Ap (..))

-- NOTE: roads and/or the Map itself should maybe be lazy for perf reasons
data Topography dir r a = Stop
  { stopLabel :: a
  , roads :: Map dir (Road dir r a)
  } deriving stock (Eq, Ord, Show, Functor, Foldable, Traversable)

data Road dir r a = Road
  { roadLabel :: r
  , to :: Topography dir r a
  } deriving stock (Eq, Ord, Show, Functor, Foldable, Traversable)

annofoldWalkStops
  :: Monoid b
  => (a -> dir -> r -> b)
  -> Topography dir r a
  -> Topography dir r (a, b)
annofoldWalkStops f = go mempty
  where
    go b Stop{..} = Stop
      { stopLabel = (stopLabel, b)
      , roads = flip Map.mapWithKey roads $ \dir Road{..} ->
          Road
          { to = go (f stopLabel dir roadLabel) to
          , ..
          }
      }

foldWalkStops
  :: Monoid b
  => (a -> dir -> r -> b)
  -> Topography dir r a
  -> b
foldWalkStops f = go mempty
  where
    go b Stop{..} =
      Map.foldrWithKey (\dir Road{..} bcc -> go (bcc <> f stopLabel dir roadLabel) to) b roads

foldWalkRoads
  :: Monoid b
  => (a -> a -> dir -> r -> b)
  -> Topography dir r a
  -> b
foldWalkRoads f = go mempty
  where
    go b here =
      Map.foldrWithKey (\dir Road{..} bcc -> go (bcc <> f (stopLabel here) (stopLabel to) dir roadLabel) to) b (roads here)

walkRoads_
  :: Applicative f
  => (a -> a -> dir -> r -> f ())
  -> Topography dir r a
  -> f ()
walkRoads_ f = getAp . foldWalkRoads (\a a' dir r -> Ap (f a a' dir r))

walkStops_
  :: Applicative f
  => (a -> dir -> r -> f ())
  -> Topography dir r a
  -> f ()
walkStops_ f = getAp . foldWalkStops (\a dir r -> Ap (f a dir r))
