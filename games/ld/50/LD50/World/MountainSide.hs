{-# LANGUAGE OverloadedStrings #-}
module LD50.World.MountainSide where

import Data.Foldable
import Data.String
import Data.Text (Text)
import Data.Text qualified as T
import Data.Map (Map)
import Data.Monoid (Sum (..))
import Data.Map.Strict qualified as Map
import GHC.Generics

import Apecs hiding (Map)
import Linear.V2
import Linear.Vector

import Mayhem.Engine.Geometry

import LD50.World.MountainLevel
import LD50.World.Topography

data MountainSide = MountainSide
  { waypoints :: Map WaypointId Waypoint
  }
  deriving stock (Eq, Ord, Show)

instance Semigroup MountainSide where
  x <> _ = x

instance Monoid MountainSide where
  mempty = MountainSide
    { waypoints = mempty
    }

instance Component MountainSide where type Storage MountainSide = Global MountainSide

data WaypointId = WaypointStart | WaypointId Text
  deriving stock (Eq, Ord, Show)

instance IsString WaypointId where
  fromString = WaypointId . T.pack

data Waypoint = Waypoint
  { loc :: V2 Float
  , trails :: [Trail]
  } deriving stock (Eq, Ord, Show)

data Trail = Trail
  { to :: WaypointId
  } deriving stock (Eq, Ord, Show)

type Angle = Float
data MtnSideDir = MtnSideLeft Angle | MtnSideRight Angle | MtnSideUp
  deriving stock (Eq, Ord, Show)

data LilSisyphus = LilSisyphus
  { loc :: WaypointId
  , lives :: Int
  -- TODO: Refine this data model
  -- I think I'm using apecs poorly with all these Globals
  , destination :: Maybe WaypointId
  , trailResult :: Maybe Bool
  }
  deriving stock (Eq, Ord, Show)

instance Semigroup LilSisyphus where
  x <> _ = x

instance Monoid LilSisyphus where
  mempty = LilSisyphus WaypointStart 5 Nothing Nothing

instance Component LilSisyphus where type Storage LilSisyphus = Global LilSisyphus

newtype TrailSelect = TrailSelect Int
  deriving stock (Eq, Show)
  deriving (Semigroup, Monoid) via Sum Int

instance Component TrailSelect where type Storage TrailSelect = Global TrailSelect

dirV2 :: MtnSideDir -> V2 Float
dirV2 = \case
  MtnSideUp -> V2 0 -1
  MtnSideLeft deg -> angle (degToRad deg) * V2 -1 -1
  MtnSideRight deg -> angle (degToRad deg) * V2 1 -1

testMtnSide :: MountainSide
testMtnSide = MountainSide
  { waypoints = Map.fromList
    [ ( WaypointStart
      , Waypoint
        { loc = V2 0 0
        , trails =
          [ Trail
            { to = "test1"
            }
          , Trail
            { to = "test2"
            }
          ]
        }
      )
    , ( "test1"
      , Waypoint
        { loc = V2 0 -10
        , trails =
          [ Trail
            { to = "test3"
            }
          ]
        }
      )
    , ( "test2"
      , Waypoint
        { loc = V2 10 -20
        , trails =
          [ Trail
            { to = "test3"
            }
          ]
        }
      )
    , ( "test3"
      , Waypoint
        { loc = V2 0 -50
        , trails = []
        }
      )
    ]
  }
