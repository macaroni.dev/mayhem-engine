{-# LANGUAGE OverloadedStrings #-}
module LD50.Game where

import Control.Monad
import Data.Maybe (isJust)
import Data.Foldable
import Data.Functor (void)
import Data.Word
import System.Random.Stateful
import Control.Monad.State.Strict qualified as StateT

import qualified Cute.Sound.C as CS
import CuteC2
import qualified SDL.GPU.Utils as GPU
import Optics hiding (set, element)
import qualified SDL
import qualified SDL.GPU.FC.Simple as FC
import qualified SDL.Utils as SDL
import Foreign.C.String
import Memorable
import Linear.V2 as L
import System.Random

import Mayhem.Engine.Loop
import Mayhem.Engine.Frame
import Mayhem.Engine.Resolution
import Mayhem.Engine.Frame
import Mayhem.Engine.Geometry
import Mayhem.Engine.Assets
import Mayhem.Engine.Input
import Mayhem.Engine.Utils
import Mayhem.Engine.Effects.Debug
import Mayhem.Engine.Effects
import Mayhem.Engine.Menu qualified as Menu
import qualified Mayhem.Engine.Animation as Animation

import qualified LD50.Params as P
import qualified LD50.Assets as A
import LD50.Game.Scene qualified as Scene
import LD50.Game.MountainSide qualified as MountainSide
import LD50.World
import LD50.M
import LD50.Repl qualified as Repl

gameRez :: NativeResolution
gameHeight :: Word16
gameWidth :: Word16
gameRez@NativeResolution{nativeWidth=gameWidth, nativeHeight=gameHeight} = genesis

main :: IO ()
main = runManaged $ do
  Run console <-
      runLiftIOE
    &   exp'SDL_GPU SDL_GPU'Config {enableVsync = False, nativeRez = gameRez}
    >>= exp'Cute_Sound
    >>= exp'Log
    >>= exp'Apecs initWorld
    >>= exp'Params (pure P.hardcoded)
    >>= exp'Assets @A.Main "assets"
    >>= exp'Clock
    >>= exp'Debug (theViewAssets $ #fonts % #_Romulus % #rawFont)

  console $ do
    initGame
    framed $ gameloop >>= \Loop{..} -> do
      askGPU >>= liftIO . GPU.clear
      f7ToggleDebug
      f11Fullscreen
      adaptToWindowResize
      backtickConsole

      -- HACK: This ensures the camera doesn't "leak" between frames
      -- Ideally, we do the camera ourself with the matrix stack for
      -- proper scoping. But due to the global variable nature of
      -- sdl-gpu's camera, this is what we do for now.
      askGPU >>= liftIO . GPU.resetCamera

      clocked loopTick
      loopDraw

      flush
      liftIO performGC
      askCuteSound >>= liftIO . CS.mix
      askGPU >>= liftIO . GPU.flip

backtickConsole :: CanGame es => Eff es ()
backtickConsole = do
  isConsole <- Scene.get >>= \case
    Menu'Console : _ -> pure True
    _ -> pure False
  unless isConsole $ do
    events <- input
    for_ (keypresses events) $ \case
      SDL.KeycodeBackquote -> do
        logString "Opening console.."
        Scene.push Menu'Console
      _ -> pure ()

gameloop
  :: CanGame es => Eff es (Game es)
gameloop = do
  g <- Scene.get >>= \case
    [] -> error "no scene"
    TitleScreen : _ -> do
      pure Loop
        { loopTick = do
            events <- inputs (fmap SDL.eventPayload)
            for_ @[] events $ \case
              KeypressReturn -> Scene.push Mountain'Base
              _ -> pure ()
        , loopDraw = do
            screen <- askGPU
            NativeResolution{..} <- askNativeRez
            Font font <- theViewAssets (#fonts % #_Romulus)
            let eff = FC.Effect
                  { alignment = FC.alignCenter
                  , scale = FC.Scale
                    { x = 4
                    , y = 4
                    }
                  , color = GPU.Color 255 255 255 255
                  }
            liftIO $ void $ FC.drawEffect font screen (fromIntegral nativeWidth / 2) (fromIntegral nativeHeight / 2) eff "BOULDER"
        }
    Mountain'Base : _ -> do
      pure Loop
        { loopTick = do
            events <- inputs (fmap SDL.eventPayload)
            for_ @[] events $ \case
              KeypressReturn -> Scene.push Mountain'Side
              KeypressBackspace -> Scene.pop
              _ -> pure ()
        , loopDraw = do
            screen <- askGPU
            NativeResolution{..} <- askNativeRez
            Font font <- theViewAssets (#fonts % #_Romulus)
            let eff = FC.Effect
                  { alignment = FC.alignCenter
                  , scale = FC.Scale
                    { x = 4
                    , y = 4
                    }
                  , color = GPU.Color 255 255 255 255
                  }
            liftIO $ void $ FC.drawEffect font screen (fromIntegral nativeWidth / 2) (fromIntegral nativeHeight / 2) eff "BASE OF MT"
        }
    Mountain'Side : _ -> pure MountainSide.loop
    Mountain'Path : _ -> pure gameplayLoop
    Menu'Console : _ -> pure Loop
      { loopTick = Menu.tickConsole Repl.run Scene.pop
      , loopDraw = do
          Font font <- theViewAssets (#fonts % #_Romulus)
          Menu.drawConsole Menu.Console'Config {..}
      }

  pure g

initGame
  :: The ApecsE es World
  => The Assets es A.Main
  => The Params es P.Main
  => [SDL_GPU, Cute_Sound, IOE] :>> es => Eff es ()
initGame = do
  Scene.push Mountain'Path
  cs_ctx <- askCuteSound
  assets <- theAssets
  liftIO $ do
    for_ [minBound..maxBound :: Hazard] $ \hz -> do
      for_
        [ (assets ^. #sounds % #attack) hz ^. #pool
        , (assets ^. #sounds % #start) hz ^. #pool
        ] $ traverse (\s -> CS.setVolume s 0.25 0.25)

    _ <- GPU.toggleFullscreen
    CS.loopSound (assets ^. #music % #bgm % #sound) 1
    _ <- CS.insertSound cs_ctx (assets ^. #music % #bgm % #sound)
    pure ()

gameplayLoop :: CanGame es => Game es
gameplayLoop = Loop
  { loopTick = do
      params <- theParams
      Sisyphus{..} <- gget
      if alive then tickSisyphus else tickDeath
      cmap Animation.step
      hazardTick
      handleCollisions
      hazardGen
      hazardGC
      gmodify (<> HighLoc loc)
      gmodify $ \(HazardCooldown mcd) ->
        let locCooldown :: FrameDown =
              fromIntegral $ (params ^. #hazardRNG) loc ^. #cooldown
        in HazardCooldown (fmap (min locCooldown) mcd >>= cntDown)
  , loopDraw = drawGameplay
  }

stepSisyphusAnimation
  :: CanGame es => Eff es ()
stepSisyphusAnimation = gmodify $ \(SisyphusAnimation a) -> SisyphusAnimation $ Animation.step a

initSisyphusAnimation
  :: CanGame es => Animation.Loop -> Animation.Script -> Eff es ()
initSisyphusAnimation loop script = gset $ SisyphusAnimation $ Animation.init loop script

tickDeath
  :: CanGame es => Eff es ()
tickDeath = do
  deathSpeed <- theViewParams (#sisyphus % #deathSpeed)
  gmodify $ \Sisyphus{..} -> Sisyphus{loc = loc -. Loc deathSpeed, ..}
  gmodify $ \Sisyphus{..} ->
    if loc == 0
    then Sisyphus{loc = 0, boulder = BoulderHold, alive = True}
    else Sisyphus{..}

tickSisyphus
  :: CanGame es => Eff es ()
tickSisyphus = do
  events <- input
  P.Sisyphus{..} <- theViewParams #sisyphus
  assets <- theAssets
  keyPressed <- SDL.getKeyboardState
  gmodifyM $ \ssp@Sisyphus{..} ->
    case boulder of
      BoulderHold ->
        if SDL.ScancodeSpace `elem` scanpresses events
        then do
          initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusWalk
          pure $ ssp & #boulder .~ BoulderWalkStart (FrameDown walkStartTime)
        else do
          if loc > 0 then stepSisyphusAnimation else initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusWalk
          pure $ ssp & #loc %~ (-. (Loc holdSpeed))
      BoulderWalkStart f ->
        if SDL.ScancodeSpace `elem` scanpresses events
        then do
          initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusSlap
          pure $ ssp & #boulder .~ BoulderDashStart (FrameDown dashStartTime)
        else do
          case cntDown f of
            Nothing -> do
              initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusWalk
              pure $ ssp & #boulder .~ BoulderWalk
            Just f' -> pure $ ssp & #boulder .~ BoulderWalkStart f'
      BoulderWalk ->
        if not (keyPressed SDL.ScancodeSpace)
        then do
          initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusSlide
          pure $ ssp & #boulder .~ BoulderHold
        else do
          stepSisyphusAnimation
          pure $ ssp & #loc %~ (+ Loc walkSpeed)
      BoulderDashStart f ->
        case cntDown f of
          Nothing -> do
            initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusDash
            pure $ ssp & #boulder .~ BoulderDash (FrameDown dashTime) (loc + Loc dashDistance)
          Just f' -> do
            stepSisyphusAnimation
            pure $ ssp & #boulder .~ BoulderDashStart f'
      BoulderDash f dest ->
        case cntDown f of
          Nothing -> do
            initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusCatch
            pure $ ssp & #loc .~ dest & #boulder .~ BoulderDashStop (FrameDown dashStopTime)
          Just f' -> do
            stepSisyphusAnimation
            pure $
              ssp & #loc %~ (+ (fromIntegral dashDistance `div` fromIntegral dashTime))
                  & #boulder .~ BoulderDash f' dest
      BoulderDashStop f ->
        case cntDown f of
          Nothing -> do
            initSisyphusAnimation Animation.Loop'Always $ assets ^. #animations % #_SisyphusSlide
            pure $ ssp & #boulder .~ BoulderHold
          Just f' -> do
            stepSisyphusAnimation
            pure $ ssp & #boulder .~ BoulderDashStop f'

handleCollisions
  :: CanGame es => Eff es ()
handleCollisions = do
  Sisyphus{..} <- gget
  params <- theParams
  SisyphusSpriteV2s{..} <- getSisyphusSpriteV2s
  let sisyphusAABB = aabbBottomLeft sisyphusPosV2 (params ^. #sisyphus % #aabb)
  debug_ $ debugAABB (GPU.Color 0 255 0 255) sisyphusAABB

  cmapM_ $ \(hz :: Hazard, hzs :: HazardState, pos :: Position) -> do
    case hzs of
      HazardStartup{} -> pure ()
      HazardAttack Nothing -> pure ()
      HazardAttack (Just _) -> do
        let P.Hazard{..} = (params ^. #hazards) hz
        let hzAABB = aabbBottomLeft (positionAtLoc (params ^. #mountainDegrees) loc pos) aabb
        debug_ $ debugAABBFilled (GPU.Color 255 0 0 128) hzAABB
        when (c2Collided (C2AABB' hzAABB) (C2AABB' sisyphusAABB)) $
          gmodify @Sisyphus (set' #alive False)

-- Dunno if this is right..top-left anchors make it awkward for sure..
-- Beware NaNs!
positionAtLoc :: Float -> Loc -> Position -> V2 Float
positionAtLoc mountainDegs (Loc refLoc) = \case
  OnMountain (Loc loc) (Depth dF) ->
    let locF' :: Float = fromIntegral (loc - refLoc)
        h = sqrt ((locF' * locF') + (dF * dF))
        thetaRad = atan (dF / locF')
        deltaRad = (degToRad mountainDegs) - thetaRad
        x = h * cos deltaRad
        y = h * sin deltaRad
     in V2 x (fromIntegral gameHeight - y)

drawGameplay
  :: CanGame es => Eff es ()
drawGameplay = do
  --testDraw

  screen <- askGPU
  images@A.Images{..} <- theViewAssets #images
  params@P.Main{mountainDegrees} <- theParams
  Sisyphus{..} <- gget
  liftIO $ GPU.runBlit screen $ GPU.Blit
    { image = images ^. #_Sky
    , src_rect = Nothing
    , x = 0
    , y = 0
    }
  liftIO $ GPU.withAnchor (images ^. #_Mountains) 0 1 $
    GPU.scrollHorizontal screen loc 0.1
    [ GPU.Blit
      { image = images ^. #_Mountains
      , src_rect = Nothing
      , x = 0
      , y = fromIntegral gameHeight
      }
    ]

  liftIO $ do
    GPU.pushMatrix
    GPU.matrixMode screen GPU.model
    GPU.loadIdentity
    GPU.translate 0 (fromIntegral gameHeight) 0

    -- NOTE: We have to flip mountainDegrees here to conform w/sdl-gpu
    GPU.rotate (-mountainDegrees) 0 0 1
    GPU.scrollHorizontal screen loc 1.0
      [ GPU.Blit
        { image = images ^. #_Ground
        , src_rect = Nothing
        , x = 0
        , y = 0
        }
      ]

    GPU.popMatrix

  let renderHazard pos anim = do
        let hzV2@(V2 hzX hzY) = positionAtLoc mountainDegrees loc pos
        debug_ $ ShapeEntry hzV2 (GPU.Color 255 0 255 180) (Circle 5)
        liftIO $ Animation.render anim $ \img rect (_ :: V2 Int) -> do
          GPU.withAnchor img 0 1 $
            GPU.blit img (Just rect) screen hzX hzY

  cmapM_ $ \(_ :: Hazard, hzs, pos :: Position, anim :: Animation.State) -> case hzs of
    HazardStartup{} -> renderHazard pos anim
    _ -> pure ()

  let boulderNumRot = fromIntegral loc / (60 * pi)
  let boulderRotDeg = getDecimal boulderNumRot * 360
  SisyphusSpriteV2s{boulderV2 = V2 bldX bldY, ..} <- getSisyphusSpriteV2s
  SisyphusAnimation sspAnim <- gget
  debug_ $ ShapeEntry sisyphusPosV2 (GPU.Color 255 0 0 180) (Circle 5)
  debug_ $ ShapeEntry sisyphusSpriteV2 (GPU.Color 0 255 0 180) (Circle 5)
  let drawBoulderSprite sprite = do
        GPU.withCenterAnchor sprite $
          GPU.blitRotate sprite Nothing screen bldX bldY boulderRotDeg
  liftIO $ if not alive then drawBoulderSprite (images ^. #_SisyphusDead) else do
    drawBoulderSprite (images ^. #_Boulder)
    Animation.render sspAnim $ \img rect offset -> do
      let V2 x y = sisyphusSpriteV2 + offset
      GPU.withAnchor img 0 1 $
        GPU.blit img (Just rect) screen x y

  cmapM_ $ \(_ :: Hazard, hzs, pos :: Position, anim :: Animation.State) -> case hzs of
    HazardAttack{} -> renderHazard pos anim
    _ -> pure ()


  cmapM_ $ \(t, pos) -> do
    let V2 rx ry = positionAtLoc mountainDegrees loc pos
    -- TODO: Check for NaN or out of bounds & GC
    liftIO $ GPU.blit (terrain t) Nothing screen rx ry

  Font font <- theViewAssets (#fonts % #_Romulus)
  let Loc currLoc = loc
  HighLoc (Loc highLoc) <- gget
  let scoreStr = unlines [show currLoc ++ " ft", "Best: " ++ show highLoc ++ " ft"]
  liftIO $ void $ FC.drawAlign font screen (fromIntegral gameWidth) 0 FC.alignRight scoreStr

  when (loc < params ^. #hazardStartLoc) $ do
    let helpStr = unlines ["Press SPACE to PUSH", "DOUBLE-TAP to DASH", "Beware HAZARDS"]
    liftIO $ void $ FC.drawAlign font screen 0 0 FC.alignLeft helpStr

data SisyphusSpriteV2s = SisyphusSpriteV2s
  { sisyphusSpriteV2 :: V2 Float
  , sisyphusPosV2 :: V2 Float
  , boulderV2 :: V2 Float
  } deriving (Eq, Show)

getSisyphusSpriteV2s :: CanGame es => Eff es (SisyphusSpriteV2s)
getSisyphusSpriteV2s = do
  Sisyphus{..} <- gget
  params <- theParams
  let sisyphusLoc = loc + params ^. #sisyphus % #locOffset
  let sisyphusDepth = params ^. #sisyphus % #depth
  let sisyphusPosV2 =
        positionAtLoc (params ^. #mountainDegrees) loc (OnMountain sisyphusLoc sisyphusDepth)
  -- TODO: Potentially vary this based on his state
  let sisyphusSpriteV2 = sisyphusPosV2 + params ^. #sisyphus % #walkSpriteOffset
  let boulderV2 = sisyphusPosV2 + params ^. #sisyphus % #boulderSpriteOffset
  pure SisyphusSpriteV2s{..}

aabbBottomLeft :: V2 Float -> C2AABB -> C2AABB
aabbBottomLeft (V2 ax ay) (C2AABB (C2V x1 y1) (C2V x2 y2)) =
  let h = y2 - y1
  in C2AABB (C2V (x1 + ax) (y1 + ay - h)) (C2V (x2 + ax) (y2 + ay - h))

hazardTick :: CanGame es => Eff es ()
hazardTick = do
  cmapM $ \(hz :: Hazard, hzs, ety :: Entity) -> case hzs of
    HazardStartup{..} ->
      case cntDown startupTime of
        Nothing -> do
          assets <- theAssets
          -- SET animation to attack for ety + hz
          cset ety $ Animation.init Animation.Loop'Always $ (assets ^. #animations % #_HazardAttack) hz
          cs_ctx <- askCuteSound
          liftIO $ void $ withPooledSound ((assets ^. #sounds % #attack) hz) $ CS.insertSound cs_ctx
          pure $ HazardAttack (Just attackTime)
        Just t' -> pure $ HazardStartup{startupTime = t', ..}
    HazardAttack mf ->
      -- Once it becomes Nothing, hazardGC will find it
      pure $ HazardAttack (mf >>= cntDown)

hazardGen :: CanGame es => Eff es ()
hazardGen = do
  assets <- theAssets
  params <- theParams
  clk <- getClock
  Sisyphus{..} <- gget
  RNG{..} <- gget
  rngParams <- theViewParams #hazardRNG
  hazardParams <- theViewParams #hazards
  let P.HazardRNG{..} = rngParams loc
  let timeToSpawn = clk `mod` spawnRate == 0
  numSpawned <- ccount @Hazard
  onCooldown <- gget <&> \(HazardCooldown mcd) -> isJust mcd
  unless (not timeToSpawn || onCooldown || numSpawned >= maxConcurrent || loc < params ^. #hazardStartLoc) $ do
    (_, g') <- runStateGenT hazardRNG $ \g -> do
      roll :: Double <- uniformRM (0.0, 1.0) g
      when (roll <= oddsToSpawn) $ do
        pickedArea <- uniformRM (0, length targetAreas - 1) g
        let targetArea = targetAreas !! pickedArea
        hazardLocOffset <- uniformRM targetArea g
        sisyphusLocOffset <- lift $ theViewParams (#sisyphus % #locOffset)
        let spawnLoc = loc + sisyphusLocOffset +-. hazardLocOffset
        hazardRoll :: Double <- uniformRM (0.0, 1.0) g
        let hazardType = if hazardRoll < hazardSplit then Lightning else Lava
        let P.Hazard{..} = hazardParams hazardType
        startupTime <- fromIntegral <$> uniformRM startupTimeRange g
        attackTime <- fromIntegral <$> uniformRM attackTimeRange g
        cs_ctx <- lift askCuteSound
        liftIO $ void $ withPooledSound ((assets ^. #sounds % #start) hazardType) $ CS.insertSound cs_ctx
        when (numSpawned + 1 >= maxConcurrent) $
          lift $ gset $ HazardCooldown $ Just $ fromIntegral cooldown
        void $ lift $ newEntity
          ( hazardType
          , OnMountain spawnLoc (hazardParams hazardType ^. #depth)
          , HazardStartup{..}
          , Animation.init Animation.Loop'Always $ (assets ^. #animations % #_HazardStart) hazardType
          )
    gset RNG{hazardRNG = g', ..}


hazardGC :: CanGame es => Eff es ()
hazardGC = cmapM_ $ \(hs :: HazardState, ety :: Entity) -> do
  let shouldGC = case hs of
        HazardAttack Nothing -> True
        _ -> False

  when shouldGC $ do
    traverse_ ($ ety)
      [ destroyC @Hazard
      , destroyC @Position
      , destroyC @HazardState
      , destroyC @Animation.State
      ]

todo :: a
todo = error "todo"
