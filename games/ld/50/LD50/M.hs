module LD50.M where

import Mayhem.Engine.Loop
import SDL qualified

import qualified LD50.Params as P
import qualified LD50.Assets as A
import LD50.World

type CanGame es =
  ([IOE, SDL_GPU, Cute_Sound, Input [SDL.Event], Clock, Log, Debug] :>> es
  , The ApecsE es World
  , The Params es P.Main
  , The Assets es A.Main
  )
