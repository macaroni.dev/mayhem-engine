module LD50.Game.Scene where

import Prelude hiding (init)

import Optics
import System.Random
import Linear.V2

import Mayhem.Engine.Loop
import Mayhem.Engine.Menu qualified as Menu
import Mayhem.Engine.Animation qualified as Animation

import LD50.World
import LD50.World.MountainSide (testMtnSide)
import qualified LD50.Params as P
import qualified LD50.Assets as A

type C es =
  ( The ApecsE es World
  , The Assets es A.Main
  , The Params es P.Main
  , '[IOE, SDL_GPU] :>> es
  )

get :: C es => Eff es [Scene]
get = unScreen <$> gget

push
  :: C es
  => Scene
  -> Eff es ()
push s = do
  gmodify (\(Screen ss) -> Screen $ s : ss)
  init s

pop
  :: C es
  => Eff es ()
pop = do
  gget >>= \case
    Screen [] -> pure ()
    Screen (s : rest) -> do
      cleanup s
      gset $ Screen rest

cleanup
  :: C es
  => Scene
  -> Eff es ()
cleanup = \case
  TitleScreen -> pure ()
  Mountain'Base -> pure ()
  Mountain'Side -> pure ()
  Mountain'Path -> pure ()
  Menu'Console -> pure ()

init
  :: C es
  => Scene
  -> Eff es ()
init = \case
  Menu'Console -> Menu.initConsole
  Mountain'Path -> do
    assets <- theAssets

    clearAllC @Terrain
    clearAllC @Position
    clearAllC @Animation.State

    _ <- newEntity $ SisyphusAnimation $ Animation.init Animation.Loop'Always $ assets ^. #animations % #_SisyphusWalk
    gset Sisyphus
      { loc = 0
      , boulder = BoulderHold
      , alive = True
      }
    gset Camera
      { offset = V2 0 0
      , angle = 0
      , zoom = V2 0 0
      }
    liftIO initStdGen >>= gset . uncurry RNG . split

  Mountain'Side -> gset testMtnSide
  _ -> pure ()
