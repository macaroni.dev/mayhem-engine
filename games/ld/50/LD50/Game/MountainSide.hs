module LD50.Game.MountainSide where

import Data.Function ((&))
import Data.Functor ((<&>))
import Data.Foldable
import Data.Map.Strict qualified as Map
import Safe

import SDL.GPU.Utils qualified as GPU
import Memorable
import SDL qualified
import Linear.V2 hiding (angle)

import Mayhem.Engine.Loop
import Mayhem.Engine.Input qualified as SDL
import Mayhem.Engine.Effects.Debug

import LD50.Params qualified as P
import LD50.Assets qualified as A
import LD50.World
import LD50.M
import LD50.World.Topography
import LD50.World.MountainSide
import LD50.World.MountainSide qualified as Waypoint (Waypoint (..))

loop :: CanGame es => Game es
loop = Loop
  { loopTick = do
      keypresses <- SDL.keypresses <$> input
      keypressed <- SDL.getKeyboardState
      -- TODO:
      -- - [ ] Arrow keys cycle left and right
      -- - [ ] Enter chooses level
      -- - [ ] Add some sounds
      -- - [ ] Handle destination/trailResult (if set)
      -- - [ ] Maybe try a more "modal" control?
      --        - One button to start level select,
      --          another for free camera controls?
      if | keypressed SDL.ScancodeI ->
           gmodify $ \Camera{..} -> Camera{offset = offset + V2 0 -1, ..}
         | keypressed SDL.ScancodeJ ->
           gmodify $ \Camera{..} -> Camera{offset = offset + V2 -1 0, ..}
         | keypressed SDL.ScancodeL ->
             gmodify $ \Camera{..} -> Camera{offset = offset + V2 1 0, ..}
         | keypressed SDL.ScancodeK ->
             gmodify $ \Camera{..} -> Camera{offset = offset + V2 0 1, ..}
         -- TODO: Ability to "reset" camera to lil sisyphus' loc
         | otherwise -> pure ()

      LilSisyphus{loc=currLoc} <- gget
      MountainSide{..} <- gget
      let currWaypoint = fromJustNote (show currLoc ++ " not found") $ Map.lookup currLoc waypoints
      let numTrails = length $ Waypoint.trails currWaypoint
      -- TODO: Handle when there are no trails (e.g. at the end)
      for_ keypresses $ \case
        SDL.KeycodeUp -> do
             ts@(TrailSelect n) <- gget @TrailSelect
             let trs = Waypoint.trails currWaypoint
             let (Trail next) = atNote ("tick: " ++ show ts ++ " invalid for " ++ show trs) trs n
             -- TODO: Transition to a level instead of immediate movement
             gmodify $ \LilSisyphus{..} -> LilSisyphus{loc = next, ..}
             gset $ TrailSelect 0 -- It'll crash if you don't!
        SDL.KeycodeRight ->
           gmodify $ \(TrailSelect n) ->
             TrailSelect $ if n == numTrails - 1 then 0 else n + 1
        SDL.KeycodeLeft -> gmodify $ \(TrailSelect n) ->
             TrailSelect $ if n == 0 then numTrails - 1 else n - 1
        _ -> pure ()

  , loopDraw = do
      -- TODO: Use lil sisyphus' location to highlight where he is
      -- TODO: Use trail select to highlight selection

      LilSisyphus{loc=currLoc} <- gget
      MountainSide{..} <- gget
      let currWaypoint = fromJustNote (show currLoc ++ " not found") $ Map.lookup currLoc waypoints
      ts@(TrailSelect n) <- gget @TrailSelect
      let trs = Waypoint.trails currWaypoint
      let (Trail selection) = atNote ("draw: " ++ show ts ++ " invalid for " ++ show trs) trs n

      for_ (Map.toAscList waypoints) $ \(currwpid, Waypoint{loc=hereLoc, ..}) -> do
        let wpColor = if currwpid == currLoc then GPU.white else GPU.red
        debug_ $ ShapeEntry hereLoc wpColor $ Circle 5
        let nextWaypoints = trails <&> \Trail{..} -> (to, fromJustNote (show to ++ " not found") $ Map.lookup to waypoints)
        for_ nextWaypoints $ \(wpid, Waypoint{loc=thereLoc}) -> do
          let color = if currwpid == currLoc && wpid == selection then GPU.white else GPU.green
          debug_ $ ShapeEntry 0 color $ Line hereLoc thereLoc

      -- TODO: Print lives
      screen <- askGPU
      -- Origin sanity circle
      debug_ $ ShapeEntry (V2 0 0) GPU.blue $ Circle 2
      Camera
        { offset = V2 camX camY
        , angle = camA
        , zoom = V2 camZoomX camZoomY
        } <- gget
      GPU.modifyCamera screen $ \GPU.Camera{..} -> GPU.Camera
        { x = camX
        , y = camY
        , angle = camA
        , zoom_x = camZoomX
        , zoom_y = camZoomY
        , use_centered_origin = GPU.true -- TODO: Should really be a Prelude bool
        , ..
        }
  }
