{-# LANGUAGE OverloadedStrings #-}
module LD50.Repl where

import Data.Text (Text)
import Data.Text qualified as T
import Data.Text.Lazy qualified as TL
import Text.Pretty.Simple (pShowNoColor)
import Mayhem.Engine.Effects
import Mayhem.Engine.Utils
import LD50.M
import LD50.World qualified as LD50
import LD50.World.MountainSide qualified as LD50

data Cmd =
  Gget GComponent
  deriving stock (Eq, Read, Show)

data GComponent =
    Camera
  | TrailSelect
  | LilSisyphus
  deriving stock (Eq, Read, Show)

ptshow :: Show a => a -> Text
ptshow = TL.toStrict . pShowNoColor

run :: CanGame es => Text -> Eff es Text
run raw = case treadEither raw of
  Left err -> pure $ "Error: " <> T.pack err
  Right cmd -> case cmd of
    Gget g -> case g of
      Camera -> ptshow <$> gget @LD50.Camera
      TrailSelect -> ptshow <$> gget @LD50.TrailSelect
      LilSisyphus -> ptshow <$> gget @LD50.LilSisyphus
