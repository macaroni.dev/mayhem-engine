{-# LANGUAGE TemplateHaskell #-}

module LD50.World where

import Data.Word
import Data.Semigroup

import System.Random
import Apecs
import Linear

import Mayhem.Engine.TH
import Mayhem.Engine.Utils
import Mayhem.Engine.Frame
import Mayhem.Engine.Menu qualified as Menu
import qualified Mayhem.Engine.Animation as Animation

import LD50.World.MountainSide qualified as MountainSide

newtype Screen = Screen { unScreen :: [Scene] }
  deriving stock (Eq, Show)
  deriving newtype (Semigroup, Monoid)

data Scene = Menu'Console | TitleScreen | Mountain'Base | Mountain'Side | Mountain'Path
  deriving stock (Eq, Show)

instance Component Screen where type Storage Screen = Global Screen

-- | Distance along the edge (hypotenuse) of the mountain
newtype Loc = Loc Word64
  deriving stock (Eq, Ord, Show)
  deriving newtype (Enum, Bounded, Num, Integral, Real)

newtype HighLoc = HighLoc Loc
  deriving stock (Eq, Ord, Show)
  deriving newtype (Enum, Bounded, Num, Integral, Real)
  deriving (Semigroup, Monoid) via (Max HighLoc)

instance Component HighLoc where type Storage HighLoc = Global HighLoc

data Sisyphus = Sisyphus
  { loc :: Loc
  , boulder :: Boulder
  , alive :: Bool
  }

instance Component Sisyphus where type Storage Sisyphus = Global Sisyphus

instance Semigroup Sisyphus where
  x <> _ = x

instance Monoid Sisyphus where
  mempty = Sisyphus 0 BoulderHold True


newtype SisyphusAnimation = SisyphusAnimation Animation.State
instance Component SisyphusAnimation where type Storage SisyphusAnimation = Global SisyphusAnimation

instance Semigroup SisyphusAnimation where
  x <> _ = x

instance Monoid SisyphusAnimation where
  mempty = SisyphusAnimation Animation.emptyState

data Boulder =
    BoulderHold
  | BoulderWalkStart FrameDown
  | BoulderWalk
  | BoulderDashStart FrameDown
  | BoulderDash FrameDown Loc
  | BoulderDashStop FrameDown
  deriving stock (Eq, Show)

data Camera = Camera
  { offset :: V2 Float
  , angle  :: Float
  , zoom   :: V2 Float
  } deriving stock (Eq, Show)

instance Component Camera where type Storage Camera = Global Camera

instance Semigroup Camera where
  x <> _ = x

instance Monoid Camera where
  mempty = Camera 0 0 1

-- | Absolute distance from the edge (downward, perpendicular)
newtype Depth = Depth Float
  deriving stock (Eq, Ord, Show)
  deriving newtype (Num, Real, Fractional)

-- | Absolute distance from the edge (upward, perpendicular)
newtype Height = Height Float
  deriving stock (Eq, Ord, Show)
  deriving newtype (Num, Real, Fractional)

data Position = OnMountain Loc Depth
  deriving (Eq, Show)

instance Component Position where type Storage Position = Map Position

data Terrain = Test1 | Test2 deriving (Eq, Ord, Show, Enum, Bounded)

instance Component Terrain where type Storage Terrain = Map Terrain

data Hazard = Lava | Lightning deriving (Eq, Ord, Show, Enum, Bounded)

instance Component Hazard where type Storage Hazard = Map Hazard

data HazardState =
    HazardStartup { startupTime :: FrameDown, attackTime :: FrameDown }
  | HazardAttack (Maybe FrameDown)
  deriving (Eq, Show)

instance Component HazardState where type Storage HazardState = Map HazardState

data RNG = RNG
  { terrainRNG :: StdGen -- Held constant & split in regions
  , hazardRNG :: StdGen -- Threaded serially & updated
  } deriving (Eq, Show)

instance Component RNG where type Storage RNG = Global RNG

instance Semigroup RNG where
  x <> _ = x

instance Monoid RNG where
  mempty = uncurry RNG $ split (mkStdGen 0)

newtype HazardCooldown = HazardCooldown (Maybe FrameDown)
  deriving stock (Eq, Ord, Show)

instance Semigroup HazardCooldown where
  x <> _ = x

instance Monoid HazardCooldown where
  mempty = HazardCooldown Nothing

instance Component HazardCooldown where type Storage HazardCooldown = Global HazardCooldown

-- TH_CODE
mkOpticsLabels ''Sisyphus
mkOpticsLabels ''Boulder
mkOpticsLabels ''Camera
mkOpticsLabels ''RNG

makeWorld "World"
  [ ''Sisyphus
  , ''Camera
  , ''Position
  , ''Terrain
  , ''Animation.State
  , ''SisyphusAnimation
  , ''Hazard
  , ''HazardState
  , ''HazardCooldown
  , ''RNG
  , ''HighLoc
  , ''Screen
  , ''MountainSide.MountainSide
  , ''MountainSide.LilSisyphus
  , ''MountainSide.TrailSelect
  , ''Menu.Console
  ]
