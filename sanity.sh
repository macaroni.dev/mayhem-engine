#! /usr/bin/env bash

set -ex

function build_ld() {
    pushd "games/ld/$1"
    ./release-windows.sh
    popd
}

if [[ $# -gt 0 ]]; then
    target="$1"
else
    echo "No target given - defaulting to all"
    target="all"
fi

case $target in
    ld47)
        build_ld 47
        ;;
    ld48)
        build_ld 48
        ;;
    ld49)
        build_ld 49
        ;;
    ld50)
        build_ld 50
        ;;
    ld51)
        build_ld 51
        ;;
    ld52)
        build_ld 52
        ;;
    ld53)
        build_ld 53
        ;;
    all)
        build_ld 47
        build_ld 48
        build_ld 49
        build_ld 50
        build_ld 51
        ;;
    *)
        echo "Unknown target: $target"
        exit 1
esac

echo "SANITY SUCCESS"
