# mayhem-engine

## Summary

`mayhem-engine` is the "engine" (if you could call it that) we (macaroni.dev) have been using to make games since 2020.

It isn't especially meant for public consumption. It won't be on Hackage anytime soon. But it's public and open-source
in case anyone wants to read it and take inspiration.

## Cool Stuff

- `cleff` extensible effects
- `Mayhem.Engine.Expansion` - Composable way of initializing effect interpreters
- `ghci` integration
  - Query/modify a running game's state
  - Hot reloading with `:hot`
  - `games/ld/53` has this set up. See [its README](/games/ld/53) for how to try it out.
- Assets - `Generic`-based asset management. Record fields reflect directory structure.
- Params - Hot-reloading game parameters. Nice for tweaking values.
- `compile-animations.hs` compiles image sequences to an animation type.
- `Mayhem.Engine.Loop` - 60fps game loop with hot reloading.
- Frame stepping
- `Mayhem.Engine.Effects.Apecs` - Embedding `apecs` in `cleff`.
  - `Mayhem.Engine.Effects` has wrappers around `cmap` and friends that integrate with `cleff`.
- `Mayhem.Engine.Geometry` - 2D geometry functions (originally cribbed from `gloss-geometry`)
- `Mayhem.Engine.Automaton` - Action state management.
  - `games/ld/53` has example usage.
- `Mayhem.Engine.Diamond` aka `diamond` - A library implementation of a line-segment-intersection-based 2D collision algorithm inspired by Super Smash Bros. Melee.
  - It is still very early-stage. Your character can slip through certain geometries.
  - `games/ld/53` uses this if you want to try it out.
  - This library will likely drive 2D platformers, Metroidvanias, and fighting games alike.
- `Mayhem.Engine.Effects.Debug` - "Graphical logging." Log shapes in your game logic instead of rendering code.
- Automatic letterboxing when window is resized
- `Mayhem.Engine.Controller` - Abstractions over controls using virtual "controllers" (e.g. buttons, dpads, arcade stick)
- `Mayhem.Console.Jam'22` - A skeumorphic fantasy "console" which is basically just a pre-rolled effects stack reflecting what we used in 2022 to do game jams (hence `Jam'22).

## Games

The `games/` directory has all the Ludum Dare games we have made. [Details can be found on our LD profile page.](https://ldjam.com/users/macaroni-dev/games) Each one has its own `Makefile` with some standard targets:

- `repl`
- `run`
- `debug` - Run in debug mode. If you get a black screen, try this command. The game may be incomplete and only have debug graphics.

In addition, the games (except `ld46` and `ld47`, which predate `mayhem-engine` and were made with `gloss`) all have some standard controls:

- F11 to toggle fullscreen
- In debug mode (`make debug`):
  - F7 to toggle debug mode when run with `make debug`
  - F2 to activate the frame stepper

## Platform support

All games cross-compile to Windows using `macaroni.nix`. `sanity.sh` runs the Windows build for all games. Windows is the only platform supported for distribution.

Linux is the only platform supported for development right now. The dev env has only ever been used on NixOS.
