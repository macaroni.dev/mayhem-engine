# 2023-04-28

- Left/Right Walls in the same place cause the player to get stuck when
  they collide with their Ground point.
- Short-hopping on steep slopes causes you to phase through.

# 2023-04-07

- Got it working with ground and ceiling with no help from the caller.
- Sloped ceilings work-ish
- Left and Right wall work (with some caller help)
- I was able to remove the rounding near zero of the collision param.

## Bugs:
- I'm not able to hop over super small walls.
  - **Fixed it with `pointOnSeg`**
- I now sometimes go through the little wall (after a SH?)
  - I think it's because the ground->ground diamond-cast is perpendicular and barely touching
    the wall segment, which doesn't trip the collision or the endpoint test.
  - Having the wall go even slightly below the ground (I did `0.0001` units) fixes it. But this is
    definitely annoying as a user.
  - Could I also do the reverse - test for the surface points on the diamond-cast?
    - **This fixed it.**

## Next up:
- Aerial drift would make it easier to put it to the test.
- Left/Right wall don't work for "jumping" without help - you get stuck due to 
  constantly having motion on the x axis. This feels like a caller issue though
- "Slide along the wall" function acts weird for diagonal walls.
  - Maybe proper Melee-style sliding would be more natural.
    - **I might have fixed this?** I zero out velocity x or y based on the surface.
- A concept of "grounded" would be nice. Not sure which is library vs caller:
  - Follow the ground (including slopes)
    - **Walking up slopes works**
      - The user must explicitly place the diamond on the slope frame-over-frame
        to work around FP error
    - **Walking down slopes works**
      - It's downward movement, which causes the diamond to intersect with the ground.
      - Need to take into account the angle of a surface when figuring out if it's relevant.
      - The solution: Check if the slope + diamond-cast are parallel by diffing their `abs` normals.
  - Airborne slope collisions
    - Horizontal movement hitting a sloped ground will phase through
  - Detect lack of ground and initiate fall
    - **I implemented this in LD52** ..without changing `diamond`.
    - **I took the simple logic and put it in `diamond` as a helper.**
