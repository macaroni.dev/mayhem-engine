## Must-Haves
- Cross-platform everything
  - Priority order: Windows, Linux, NixOS, MacOS, iOS, Android
  - e.g. saves & replays from Windows should work on Linux. Cross-play multiplayer.

## Allegorical Console
- Eff for in-game effects - the extra expressivity goes a long way
- Build on Eff with composable Games
- Effects can be composed via Expansions
  - Expansions can have dependencies on other Expansions already being loaded (via `:>`)
- They are composed
