tidy:
	fd -e hs --exec ./scripts/wstrip.sh
.PHONY: tidy

update-gamecontrollerdb:
	curl "https://github.com/gabomdq/SDL_GameControllerDB/blob/master/gamecontrollerdb.txt" > ./assets/gamecontrollerdb.txt
.PHONY: update-gamecontrollerdb

repl:
	cabal repl "lib:mayhem-engine" --repl-options='+RTS' --repl-options='-xn' --repl-options='-N' --repl-options='-RTS'
.PHONY: repl

test-repl:
	cabal repl spec --repl-options='+RTS' --repl-options='-xn' --repl-options='-N' --repl-options='-RTS'
.PHONY: test-repl
