let
  pkgs = import ./nix/pkgs.nix;
in pkgs.haskell-nix.project {
  # 'cleanGit' cleans a source directory based on the files known by git
  src = pkgs.haskell-nix.haskellLib.cleanGit {
    name = "mayhem-engine";
    src = ./.;
  };

  # We may need this someday for hint
  #modules = [{ reinstallableLibGhc = true; }];

  compiler-nix-name = "ghc928";
  # This is necessary for x-compilation to get a valid cabal plan
  # that leverages our pkgconfig mappings
  evalPackages = pkgs;
}
