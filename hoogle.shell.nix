(import ./default.nix).shellFor {
  packages = ps: [ps.mayhem-engine];
  withHoogle = true;
}
