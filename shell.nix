{ crossMingwW64 ? false }:
let pkgs = import ./nix/pkgs.nix;
in (import ./default.nix).shellFor {
  tools = {
    cabal = "latest";
    hsc2hs = "latest";
  };

  # See https://github.com/input-output-hk/haskell.nix/issues/1608 for why
  # we set exactDeps = false
  exactDeps = false;

  withHoogle = false;

  buildInputs = [ pkgs.imagemagick pkgs.remake ];

  crossPlatforms = ps: with ps; pkgs.lib.optionals crossMingwW64 [
    # This doesn't work currently:
    mingwW64 # Adds support for `x86_64-W64-mingw32-cabal build` in the shell
  ];
}
