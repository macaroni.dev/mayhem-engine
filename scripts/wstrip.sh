#! /usr/bin/env bash

# TODO: Also strip multiple newlines at the end of files

function wstrip () {
    if [ -z "$1" ]; then
	echo 'USAGE: wstrip $FILE'
	return 1
    else
        sed --in-place 's/[[:space:]]\+$//' "$1"
    fi
}

wstrip $1
